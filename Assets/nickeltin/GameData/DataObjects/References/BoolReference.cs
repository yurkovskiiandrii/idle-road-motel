﻿using System;

namespace nickeltin.GameData.DataObjects
{
    [Serializable]
    public class BoolReference : DataObjectReference<BoolObject, bool>
    {
        
    }
}