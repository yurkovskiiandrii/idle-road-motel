﻿using UnityEngine;

namespace nickeltin.GameData.DataObjects
{
    [CreateAssetMenu(menuName = "GameData/BoolObject")]
    public class BoolObject : DataObject<bool>
    {
        
    }
}