﻿using UnityEngine;

namespace nickeltin.GameData.DataObjects
{
    [CreateAssetMenu(menuName = "GameData/StringObject")]
    public class StringObject : DataObject<string>
    {
        
    }
}