using System;
using nickeltin.Tweening;
using UnityEngine;

namespace nickeltin.GameData.DataObjects
{
    [CreateAssetMenu(menuName = "GameData/Currency")]
    public class Currency : NumberObject
    {
        private float _lastInvokeUpdateTime;
        public string Id;
        public Sprite Icon;
        public event Action OnNotEnough;

        public event Action<int> OnValueDecreased = null;
        public event Action<int> OnValueIncreased = null;

        private void OnValidate()
        {
            //Debug.Log("validate");
            //InvokeUpdate();
        }

        public override float Value
        {
            get
            {
                if (m_type.Equals(NumberType.Int)) return Mathf.RoundToInt(m_value);
                return m_value;
            }
            set
            {
                float prev = m_value;
                m_value = Mathf.Clamp(value, m_minValue, m_maxValue);

                if (m_type.Equals(NumberType.Int)) m_value = Mathf.RoundToInt(m_value);
                InvokeUpdate();

                int difference = (int)(m_value - prev);
                if (difference > 0)
                {
                    OnValueIncreased?.Invoke(difference);
                }
            }
        }

        public bool EnoughtCurrency(float amount)
        {
            if (amount > m_value)
            {

                OnNotEnough?.Invoke();
                return false;
            }

            return true;
        }

        public bool TryToGet(float amount)
        {
            if (amount > m_value)
            {
                OnNotEnough?.Invoke();
                return false;
            }

            Value -= amount;

            OnValueDecreased?.Invoke((int)amount);

            return true;
        }

        public bool TryToGet(int amount)
        {
            if (amount > m_value)
            {
                OnNotEnough?.Invoke();
                return false;
            }

            Value -= amount;

            OnValueDecreased?.Invoke(amount);

            return true;
        }

        public void SubscribeOnValueChanged(Action<float> action) => onValueChanged += action;

        public void SubscribeOnNotEnough(Action action) => OnNotEnough += action;

        public void RemoveOnNotEnough(Action action) => OnNotEnough -= action;

        public void RemoveOnValueChanged(Action<float> action) => onValueChanged -= action;
    }
}