﻿using System;
using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using UnityEngine;
using Object = UnityEngine.Object;

namespace nickeltin.Singletons
{
    public class SOSInitializer : MonoSingleton<SOSInitializer>
    {
        [SerializeField, ReorderableList("SOSingleton")] private List<SOSBase> toInitialize;
        [SerializeField, ReorderableList("Reference")] private List<Object> editorReferences;

        public void AddItems(SOSBase[] targets)
        {
            if (toInitialize == null) toInitialize = new List<SOSBase>();
            toInitialize.AddRange(targets);
        }

        protected override void Awake()
        {
            base.Awake();

            foreach (var sos in toInitialize)
            {
                if (!sos.Initialize())
                {
                    Debug.Log($"Scriptable Object Singleton of type {this.GetType().Name} is already initialized");
                }
            }
        }
    }
}