﻿using UnityEngine;

namespace nickeltin.Localization
{
    [AddComponentMenu(ComponentMenuRoot + "Localized Texture")]
    public class LocalizedTextureBehaviour : LocalizedGenericAssetBehaviour<LocalizedTexture, Texture>
    {
    }
}
