﻿using UnityEngine;

namespace nickeltin.Localization
{
    [AddComponentMenu(ComponentMenuRoot + "Localized Font")]
    public class LocalizedFontBehaviour : LocalizedGenericAssetBehaviour<LocalizedFont, Font>
    {
    }
}
