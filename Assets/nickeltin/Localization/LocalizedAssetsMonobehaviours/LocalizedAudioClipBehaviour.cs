﻿using UnityEngine;

namespace nickeltin.Localization
{
    [AddComponentMenu(ComponentMenuRoot + "Localized Audio Clip")]
    public class LocalizedAudioClipBehaviour : LocalizedGenericAssetBehaviour<LocalizedAudioClip, AudioClip>
    {
    }
}
