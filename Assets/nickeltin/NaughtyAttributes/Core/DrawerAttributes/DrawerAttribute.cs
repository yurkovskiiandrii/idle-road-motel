﻿using UnityEngine;

namespace nickeltin.Editor.Attributes
{
	/// <summary>
	/// Base class for all drawer attributes
	/// </summary>
	public class DrawerAttribute : PropertyAttribute, INaughtyAttribute
	{
	}
}
