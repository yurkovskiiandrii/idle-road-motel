﻿using UnityEngine;

namespace nickeltin.Events
{
    [CreateAssetMenu(menuName = "Events/FloatEvent")]
    public sealed class FloatEvent : GenericEventObject<float> { }
}