﻿using UnityEngine;

namespace nickeltin.Events
{
    [AddComponentMenu("Events/Float")]
    public sealed class FloatEventListener : GenericEventListener<float> { }
}