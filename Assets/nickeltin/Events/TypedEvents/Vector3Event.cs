﻿using UnityEngine;

namespace nickeltin.Events
{
    [CreateAssetMenu(menuName = "Events/Vector3Event")]
    public sealed class Vector3Event : GenericEventObject<Vector3> { }
}