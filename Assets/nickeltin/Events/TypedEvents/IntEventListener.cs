﻿using UnityEngine;

namespace nickeltin.Events
{
    [AddComponentMenu("Events/Int")]
    public sealed class IntEventListener : GenericEventListener<int> { }
}