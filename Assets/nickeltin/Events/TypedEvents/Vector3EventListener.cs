﻿using UnityEngine;

namespace nickeltin.Events
{
    [AddComponentMenu("Events/Vector3")]
    public sealed class Vector3EventListener : GenericEventListener<Vector3> { }
}