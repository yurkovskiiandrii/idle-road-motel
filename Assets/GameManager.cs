using Destructables;
using DG.Tweening;
using nickeltin.Singletons;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager>
{
    [SerializeField] private GameProgressSave _progressSave;

    private void Start()
    {
        Application.targetFrameRate = 60;
        // ManualUpdateTween();
    }

    private void ManualUpdateTween()
    {
        DOTween.ManualUpdate(1, 1);
        DOVirtual.DelayedCall(1, ManualUpdateTween);
    }
}
