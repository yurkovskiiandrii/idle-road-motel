using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CameraSetupDebugMenu : MonoBehaviour
{
    [SerializeField] CameraMove cameraController;
    public TMP_InputField DefaultDampSmoothTimeIF;
    public TMP_InputField DampMaxSpeedIF;
    public TMP_InputField SuccessiveVelocityMultIF;
    public TMP_InputField SuccessiveVelocityThresholdIF;
    public TMP_InputField SmoothTimeVelocityDependenceModifIF;
    public TMP_InputField SmoothTimeVelocityDependenceClampIF;

    void Start()
    {
        DefaultDampSmoothTimeIF.text = cameraController.DefaultDampSmoothTime.ToString();
        DampMaxSpeedIF.text = cameraController.DampMaxSpeed.ToString();
        SuccessiveVelocityMultIF.text = cameraController.SuccessiveVelocityMult.ToString();
        SuccessiveVelocityThresholdIF.text = cameraController.SuccessiveVelocityThreshold.ToString();
        SmoothTimeVelocityDependenceModifIF.text = cameraController.SmoothTimeVelocityDependenceModif.ToString();
        SmoothTimeVelocityDependenceClampIF.text = cameraController.SmoothTimeVelocityDependenceClamp.ToString();
    }

    public void UpdateValues()
    {
        cameraController.DefaultDampSmoothTime = float.Parse(DefaultDampSmoothTimeIF.text);
        cameraController.DampMaxSpeed = float.Parse(DampMaxSpeedIF.text);
        cameraController.SuccessiveVelocityMult = float.Parse(SuccessiveVelocityMultIF.text);
        cameraController.SuccessiveVelocityThreshold = float.Parse(SuccessiveVelocityThresholdIF.text);
        cameraController.SmoothTimeVelocityDependenceModif = float.Parse(SmoothTimeVelocityDependenceModifIF.text);
        cameraController.SmoothTimeVelocityDependenceClamp = float.Parse(SmoothTimeVelocityDependenceClampIF.text);
    }
}
