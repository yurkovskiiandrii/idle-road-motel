ServiceID,Names,CustomDescription,LevelUpgrade,ResourcesCapacity,UpgradePrice,MaxPropsCount,MaxNPCCount
PlumbersService,Plumbers,"No motel can survive without a good handymen. Upgrade and hire more staff to keep
all things working smooth.",,,,,
,,,1,200,0,2,2
,,,2,600,5000,4,4
,,,3,1400,20000,6,6
CleanersService,Cleaners,Cleaning is crucial for managing a motel. Upgrade it and hire more staff to handle all dirtiness.,,,,,
,,,1,200,3000,2,3
,,,2,600,7500,3,4
,,,3,1400,20000,4,5
Room1,Apartment 201,Cosy motel room with a bathroom,,,,,
,,,1,200,2500,15,15
,,,2,600,5000,15,15
,,,3,1400,20000,15,15
,,,4,2200,35000,15,15
,,,5,3000,50000,15,15
Room2,Apartment 202,Cosy motel room with a bathroom,,,,,
,,,1,200,2500,2,1
,,,2,600,5000,4,2
,,,3,1400,20000,6,3
,,,4,2200,35000,8,4
,,,5,3000,50000,10,5
Room3,Apartment 203,Cosy motel room with a bathroom,,,,,
,,,1,200,2500,2,1
,,,2,600,5000,4,2
,,,3,1400,20000,6,3
,,,4,2200,35000,8,4
,,,5,3000,50000,10,5
Room4,Apartment 204,Cosy motel room with a bathroom,,,,,
,,,1,200,2500,2,1
,,,2,600,5000,4,2
,,,3,1400,20000,6,3
,,,4,2200,35000,8,4
,,,5,3000,50000,10,5
PoolService,Swimming pool,Pool description,,,,,
,,,1,200,2500,2,1
,,,2,600,5000,4,2
,,,3,1400,20000,6,3
,,,4,2200,35000,8,4
,,,5,3000,50000,10,5
BarService,Bar,Bar description,,,,,
,,,1,200,2500,2,1
,,,2,600,5000,4,1
,,,3,1400,20000,6,3
,,,4,2200,35000,8,4
,,,5,3000,50000,10,5
ParkingService,Parking,Parking description,,,,,
,,,1,200,2500,2,0
,,,2,600,5000,4,0
,,,3,1400,20000,6,0
,,,4,2200,35000,6,0
,,,5,3000,50000,6,0
ReceptionService,Reception,Reception description,,,,,
,,,1,200,2500,2,0
,,,2,600,5000,4,0
,,,3,1400,20000,6,0
,,,4,2200,35000,6,0
,,,5,3000,50000,6,0
HotdogService,Hotdog,Hotdog description,,,,,
,,,1,200,2500,2,0
,,,2,600,5000,4,0
,,,3,1400,20000,6,0
,,,4,2200,35000,6,0
,,,5,3000,50000,6,0
IcecreamService,Icecream,Icecream description,,,,,
,,,1,200,2500,2,0
,,,2,600,5000,4,0
,,,3,1400,20000,6,0
,,,4,2200,35000,6,0
,,,5,3000,50000,6,0