using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ActionButtonDataContainer : MonoBehaviour
{
    [SerializeField] string freePriceLabel = "Free";

    public TextMeshProUGUI actionLabel;
    public Image buttonImage;
    [SerializeField] TextMeshProUGUI coinsPrice;
    [SerializeField] TextMeshProUGUI redGemsPrice;
    [SerializeField] TextMeshProUGUI greenGemsPrice;
    [SerializeField] TextMeshProUGUI blueGemsPrice;

    public void SetItemPrice(int coins, int blueGems = 0, int redGems = 0, int greenGems = 0)
    {
        ShowPriceItem(coins, coinsPrice);
        ShowPriceItem(redGems, redGemsPrice);
        ShowPriceItem(greenGems, greenGemsPrice);
        ShowPriceItem(blueGems, blueGemsPrice);

        if(coins == 0 && redGems == 0 && redGems == 0 && greenGems == 0)
        {
            coinsPrice.transform.parent.gameObject.SetActive(true);
            coinsPrice.text = freePriceLabel;
        }
    }

    void ShowPriceItem(int value, TextMeshProUGUI textRef)
    {
        if (value <= 0) 
        {
            textRef.transform.parent.gameObject.SetActive(false);
            return;
        }

        textRef.text = CoreGame.UI.LargeNumber.FormatEveryThirdPower(value);
    }

    public void Show(bool isShow)
    {
        coinsPrice.transform.parent.gameObject.SetActive(isShow);
        redGemsPrice.transform.parent.gameObject.SetActive(isShow);
        greenGemsPrice.transform.parent.gameObject.SetActive(isShow);
        blueGemsPrice.transform.parent.gameObject.SetActive(isShow);
    }

    public void SetLabelText(string text) => actionLabel.text = text;
}
