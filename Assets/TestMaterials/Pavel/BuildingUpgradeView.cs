﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace Source
{
    public class BuildingUpgradeView : MonoBehaviour, IObjectID
    {
        [HideInInspector] public string Id;

        [FormerlySerializedAs("CustomCameraTarget")] [SerializeField]
        private Transform _customCameraTarget;

        [SerializeField] private List<Transform> _customUpgradeCameraTarget = new List<Transform>();

        [Tooltip("Non additive upgrade states.")]
        public List<GameObject> States = new List<GameObject>();

        [Tooltip("If selected non additive states become additive")]
        public bool StatesAdditive;

        [Tooltip("Use for combining these states to non additive states")]
        public List<GameObject> AdditionalStates = new List<GameObject>();

        public List<Transform> CustomUpgradeVfxPoints = new List<Transform>();

        [HideInInspector]
        public List<BuildingUpgradeViewCachedState> StatesCachedData = new List<BuildingUpgradeViewCachedState>();

        [HideInInspector] [SerializeField] private List<Renderer> _particleSystemRenderers;

        private int _currentStateIndex = -1;
        private bool _canPlayUpgradeFx;

        public int CurrentStateIndex => _currentStateIndex;

        public event Action<int> OnLevelChanged;

        private void Awake()
        {
            foreach (var additionalState in AdditionalStates)
            {
                if (additionalState != null)
                {
                    additionalState.SetActive(false);
                }
            }
        }

        void Start()
        {
            _canPlayUpgradeFx = true;
        }

        public void OnValidate()
        {
            /*if (_audioMonoSpawner == null)
            {
                _audioMonoSpawner = GetComponent<AudioMonoSpawner>();
            }*/

            if (gameObject.scene.name == null)
            {
                return;
            }

            name = string.IsNullOrEmpty(Id) ? "---" : Id;
        }

        public void SetLevel(int level)
        {
            var stateInd = level - 1;

            foreach (var state in States)
                state.SetActive(false);

            if (stateInd < 0)
            {
                _currentStateIndex = -1;
                return;
            }

            if (stateInd >= States.Count)
            {
                stateInd = States.Count - 1;
                Debug.LogError(
                    $"[View] Building ': upgrade '{Id}': State of upgrade level {level} was not found. The highest available state ({States.Count}) was used.");
            }

            if (stateInd > _currentStateIndex && _canPlayUpgradeFx)
            {
                PlayUpgradeVfx(stateInd);
                PlayUpgradeSfx();
            }

            _currentStateIndex = stateInd;

            if (StatesAdditive)
            {
                for (int i = 0; i < stateInd + 1; i++)
                    States[i].SetActive(true);
            }
            else
            {
                States[stateInd].SetActive(true);

                if (AdditionalStates.Count > 0)
                {
                    for (int i = 0; i < AdditionalStates.Count; i++)
                    {
                        AdditionalStates[i].SetActive(i <= stateInd);
                    }
                }
            }

            OnLevelChanged?.Invoke(level);
        }

        private void PlayUpgradeSfx()
        {
            //_audioMonoSpawner?.PlaySound();
        }

        private void PlayUpgradeVfx(int stateInd)
        {
            if (CustomUpgradeVfxPoints.Count > 0)
            {
                if (StatesAdditive)
                {
                    if (CustomUpgradeVfxPoints.Count < States.Count)
                        SpawnUpgradeVfx(States[stateInd].transform.position);
                    else
                    {
                        Debug.LogError(
                            $"[View] Building : upgrade '{Id}': custom upgrade vfx points must have array size of states");
                        return;
                    }
                }
                else
                {
                    foreach (var t in CustomUpgradeVfxPoints)
                        SpawnUpgradeVfx(t.position);
                }
            }
            else
            {
                SpawnUpgradeVfx(States[stateInd].transform.position);
            }
        }

        private void SpawnUpgradeVfx(Vector3 pos)
        {
            //Get.Settings.Prefabs.Visual.FX_BuildingUpgrade.Spawn(pos);
        }

        public void CacheFromEditor()
        {
            StatesCachedData.Clear();
            _particleSystemRenderers.Clear();
            for (int i = 0; i < States.Count; i++)
            {
                var stateCache = new BuildingUpgradeViewCachedState();
                var meshRenderers = States[i].GetComponentsInChildren<Renderer>(true).ToList();

                if (AdditionalStates.Count > 0)
                {
                    for (int j = 0; j <= i && j <AdditionalStates.Count; j++)
                    {
                        if (AdditionalStates[j] != null)
                            meshRenderers.AddRange(AdditionalStates[j].GetComponentsInChildren<Renderer>(true).ToList());
                    }
                }

                foreach (var mr in meshRenderers)
                {
                    var mrMats = new List<Material>();
                    mr.GetSharedMaterials(mrMats);
                    var mrCache = new MeshRendererCachedData
                    {
                        MeshRenderer = mr,
                        DefaultMaterials = mrMats.ToArray()
                    };

                    stateCache.MeshRenderersData.Add(mrCache);
                }

                StatesCachedData.Add(stateCache);
            }

            _particleSystemRenderers = GetComponentsInChildren<ParticleSystem>(true)
                .Select(e => e.GetComponent<Renderer>()).ToList();
        }

        public Transform GetCustomCameraTarget()
        {
            var customIndex = Mathf.Clamp(_currentStateIndex + 1, 0, _customUpgradeCameraTarget.Count - 1);
            if (_customUpgradeCameraTarget.Count > 0 && _customUpgradeCameraTarget.Count > customIndex)
            {
                return _customUpgradeCameraTarget[customIndex];
            }

            return _customCameraTarget;
        }

        public void SetActiveVFX(bool isActive)
        {
            foreach (var pSystem in _particleSystemRenderers)
            {
                pSystem.enabled = isActive;
            }
        }

        public string ID { get; }
    }

    [Serializable]
    public class BuildingUpgradeViewCachedState
    {
        public List<MeshRendererCachedData> MeshRenderersData = new List<MeshRendererCachedData>();
    }

    [Serializable]
    public class MeshRendererCachedData
    {
        public Renderer MeshRenderer;
        public Material[] DefaultMaterials;
    }
}