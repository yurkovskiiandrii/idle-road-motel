using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Source.Editor
{
    [CustomEditor(typeof(BuildingUpgradeView))]
    public class BuildingUpgradeViewEditor : UnityEditor.Editor
    {
        private int _upgradesCount;
        private int _currentUpgrade;
        
        private int _entranceIndex = 0;
        private List<string> _upgradeIds;
        private List<string> _combinedIndexes = new List<string>();

        private PurchaseSoundType _selectedPurchaseSound = PurchaseSoundType.Empty;
        
        private enum PurchaseSoundType
        {
            Empty = 0,
            Wood = 1,
            Metal = 2,
            Premium = 3
        }

        private void OnEnable()
        {
            var states = ((BuildingUpgradeView) target).States;
            _upgradesCount = states.Count;
            for (int i = 0; i < states.Count; i++)
            {
                if (states[i].gameObject.activeInHierarchy)
                    _currentUpgrade = i + 1;
            }
            
            //_selectedPurchaseSound = GetPurchaseSoundType();
        }

        public override void OnInspectorGUI()
        {
            BuildingUpgradeView view = (BuildingUpgradeView) target;

            DrawSetUpgrades(view);
            DrawButtons(view);
            DrawSoundsButtons(view);
            
            EditorGUILayout.BeginVertical("Box");
            //DrawIdHelpbox(view);
            DrawIdSelection(view);
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.Space(15);

            base.OnInspectorGUI();
        }
        
        private void DrawSetUpgrades(BuildingUpgradeView view)
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.LabelField("Helper method to create or remove upgrade holders");
            
            EditorGUILayout.BeginHorizontal();

            _upgradesCount = EditorGUILayout.IntField("Number of upgrades:", _upgradesCount);
            if (GUILayout.Button("Set"))
            {
                if (_upgradesCount < view.States.Count)
                {
                    for (int i = view.States.Count - 1; i >= _upgradesCount; i--)
                    {
                        DestroyImmediate(view.States[i].gameObject);
                        view.States.RemoveAt(i);
                    }
                }
                else
                {
                    for (int i = 0; i < _upgradesCount; i++)
                    {
                        var goName = $"Upgrade_{i + 1}";
                        if (view.States.FirstOrDefault(x => string.Equals(x.name, goName)) != null)
                            continue;

                        var go = new GameObject(goName);
                        go.transform.SetParent(view.transform);
                        go.transform.localPosition = Vector3.zero;
                        go.transform.localRotation = Quaternion.identity;
                        view.States.Add(go);
                    }
                }
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        private void DrawButtons(BuildingUpgradeView view)
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.LabelField($"Toggle upgrades levels. Selected: {_currentUpgrade}/{view.States.Count}");
            
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Min level"))
            {
                _currentUpgrade = 1;
                view.SetLevel(_currentUpgrade);
            }
            
            if (GUILayout.Button("Prev level"))
            {
                _currentUpgrade--;
                
                if (_currentUpgrade <= 0)
                    _currentUpgrade = view.States.Count;
                
                view.SetLevel(_currentUpgrade);
            }
            
            if (GUILayout.Button("Next level"))
            {
                _currentUpgrade++;
                
                if (_currentUpgrade > view.States.Count)
                    _currentUpgrade = 1;
                
                view.SetLevel(_currentUpgrade);
            }
            
            if (GUILayout.Button("Max level"))
            {
                _currentUpgrade = view.States.Count;
                view.SetLevel(_currentUpgrade);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        private void DrawSoundsButtons(BuildingUpgradeView view)
        {
            EditorGUILayout.BeginVertical("Box");
            
            if (_selectedPurchaseSound == PurchaseSoundType.Empty)
                EditorGUILayout.HelpBox("No purchase sound is selected. Please choose one", MessageType.Error);
            else 
                EditorGUILayout.HelpBox($"Purchase sound {_selectedPurchaseSound.ToString().ToUpper()} is selected.", MessageType.Info);
            
            EditorGUILayout.BeginHorizontal();

            foreach (PurchaseSoundType value in Enum.GetValues(typeof(PurchaseSoundType)))
            {
                if (value == PurchaseSoundType.Empty) continue;
                if (GUILayout.Button($"Set {value} sound"))
                {
                    _selectedPurchaseSound = value;
                    //AddPurchaseSound(view, value);
                    EditorUtility.SetDirty(view);
                }
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        /*private void DrawIdHelpbox(BuildingUpgradeView view)
        {
            if (_upgradeIds == null || _upgradeIds.Count == 0)
            {
                EditorGUILayout.HelpBox($"LocationIDsEconomy {view.Id} is not found! Check location index and economy file for location IDs", MessageType.Error);
            }
            else if (_upgradeIds.FirstOrDefault(x=> x == view.Id) == null)
            {
                EditorGUILayout.HelpBox($"Building Upgrade ID {view.Id} is not found! Please check economy file", MessageType.Warning);
            }
            else
            {
                EditorGUILayout.HelpBox("Choose Building Upgrade Id from drop down menu or type it in manually...", MessageType.Info);
            }
        }*/
        
        private void DrawIdSelection(BuildingUpgradeView view)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("ID:", GUILayout.Width(20));
        
            if (_upgradeIds == null || _upgradeIds.Count == 0)
            {
                
            }
            else
            {
                _combinedIndexes.Clear();
                foreach (var replace in _upgradeIds.Select(title => title.Replace('_', '/')))
                {
                    _combinedIndexes.Add(replace);
                }
            
                var currId = _upgradeIds.FirstOrDefault(x => x == view.Id);
                if (currId != null)
                {
                    _entranceIndex = _upgradeIds.IndexOf(currId);
                }
            
                var result = EditorGUILayout.Popup(_entranceIndex, _combinedIndexes.ToArray());
                if (result != _entranceIndex)
                {
                    _entranceIndex = result;
                    SetNewId(view, _upgradeIds[_entranceIndex]);
                }
            }
        
            var newId = EditorGUILayout.TextArea(view.Id);
            if (newId != view.Id)
            {
                SetNewId(view, newId);
            }

            EditorGUILayout.EndHorizontal();
        }
        
        private void SetNewId(BuildingUpgradeView view, string newId)
        {
            view.Id = newId;
            view.OnValidate();
            EditorUtility.SetDirty(this);
        }

        /*private void AddPurchaseSound(BuildingUpgradeView view, PurchaseSoundType value)
        {
            var audioMonoSpawner = view.PurchaseSoundSpawner;
            if (view.PurchaseSoundSpawner == null)
            {
                /*audioMonoSpawner = view.gameObject.AddComponent<AudioMonoSpawner>();
                audioMonoSpawner.PlayOnEnable = false;#1#
                view.PurchaseSoundSpawner = audioMonoSpawner;
            }
            
        }
        
        private PurchaseSoundType GetPurchaseSoundType()
        {
            var view = (BuildingUpgradeView) target;
            if (view.PurchaseSoundSpawner != null && view.PurchaseSoundSpawner.Shell != null)
            {
                var shellName = view.PurchaseSoundSpawner.Shell.name;
                if (shellName == "sfx_obj_woodbought")
                    return PurchaseSoundType.Wood;
                if (shellName == "sfx_obj_metalbought")
                    return PurchaseSoundType.Metal; 
                if (shellName == "sfx_obj_premiumbought")
                    return PurchaseSoundType.Premium;
            }
            return PurchaseSoundType.Empty;
        }*/
        
    }
}