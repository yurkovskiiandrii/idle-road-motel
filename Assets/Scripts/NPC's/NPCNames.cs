﻿using System.Collections.Generic;
using UnityEngine;

namespace NPCs
{
    [CreateAssetMenu(menuName = "Enviorment/NPCNameSet")]
    public class NPCNames : ScriptableObject
    {
        public List<string> Names;
    }
}