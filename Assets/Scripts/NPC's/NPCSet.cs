﻿using nickeltin.GameData.DataObjects;
using UnityEngine;

namespace NPCs
{
    [CreateAssetMenu(menuName = "Enviorment/NPCSet")]
    public class NPCSet : RuntimeCollection<NPC>
    {
        public bool FreeWorkerExist()
        {
            for (int i = m_collection.Count - 1; i >= 0; i--)
            {
                //if (m_collection[i].FreeAndReadyToWork) return true;
            }
            return false;
        }
    }
}