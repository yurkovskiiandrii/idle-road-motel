﻿using System;
using System.Linq;
using DG.Tweening;
using GameEnvironment;
using GameEnvironment.Village.Structures;
using GameData;
using nickeltin.Editor.Attributes;
using nickeltin.StateMachine;
using UI;
using UnityEngine;
using UnityEngine.Events;
using WaypointsSystem;
using static NPCs.ServiceManager.ServiceSave;
//using static UnityEditor.Experimental.GraphView.Port;
using Random = UnityEngine.Random;
using System.Collections.Generic;

namespace NPCs
{
    [SelectionBase, DisallowMultipleComponent, RequireComponent(typeof(Animator)), RequireComponent(typeof(CameraOrtoSize))]
    public class NPC : MonoBehaviour, IObjectID
    {
        [Serializable]
        private class NPCStateEvent: StateEvent
        {
            public NPCState State;
        }

        //private static readonly int WalkAnimator = Animator.StringToHash("Walk");
        //private static readonly int GetResourcesAnimator = Animator.StringToHash("GetResources");
        //private static readonly int IdleAnimator = Animator.StringToHash("Idle");
        private static readonly int SleepAnimator = Animator.StringToHash("Sleep");
        //private static readonly int CarryAnimator = Animator.StringToHash("Carry");
        //private static readonly int SpendResourcesAnimator = Animator.StringToHash("SpendResources");
        private static readonly int SpeedAnimator = Animator.StringToHash("Speed");
        private static readonly int StorageValueAnimator = Animator.StringToHash("StorageValue");
        

        [SerializeField] string lastAnimatorState = "";

        public enum NPCState
        {
            Start,
            Idle,
            Getting,
            Spending,
            Walk,
            Carry,
            Sleep,
            DoWork
        }

        [SerializeField] UnityEvent onUpgrade;

        [SerializeField] private string _id;
        private Animator _animator;
        [SerializeField] private RuntimeAnimatorController _animatorController;
        [SerializeField] private ServiceManager _serviceManager;

        [Header("Stats")] [SerializeField] private NpcStats _stats;

        public NpcStats Stats
        {
            get => _stats;
            set
            {
                _storage.Capacity = value.MaxCarriedResources;
                _stats = value;
            }
        }

        public ServiceManager ServiceManager
        {
            get => _serviceManager;
            set => _serviceManager = value;
        }

        [Space] [SerializeField] private float _delayBeforeWalk;
        private float _currentWalkDelay;
        [SerializeField] private float _spendingDelay = 1;
        [SerializeField] private float _rotationSpeed = 5;
        [SerializeField] private float _baseSpeed = 1;
        [SerializeField, ReadOnly] private ServiceZone _currentZone;
        [SerializeField, ReadOnly] private GettingZone[] _gettingZones;
        [SerializeField, ReadOnly] private SpendingZone[] _spendingZones;
        [SerializeField, ReadOnly] private WaitingZone[] _waitingZones;
        [SerializeField, ReadOnly] private SleepingZone[] _sleepingZones;

        [Header("Events"), Space, SerializeField]
        private NPCStateEvent[] _stateEvents;

        private NPCStateEvent[] _existsStateEvents;

        private Storage _storage;
        private Vector3 _prevPosition;
        private Quaternion _targetRotation;
        private WaypointMovement _movement;
        private StateMachine _states;
        private Vector3 _spawnPoint;

        int interactebleObjectIndex;

        public bool isSubscribed;

        public string ID
        {
            get
            {
                if (_id == "")
                    _id = gameObject.name;
                return _id;
            }
            set => _id = value;
        }

        public float CarriedResources => _storage.CurrentAmount;
        public float MaxCarriedResources => _storage.Capacity;

        [SerializeField] private string Name;

        private void Awake()
        {
            _storage = GetComponent<Storage>();
            _movement = new WaypointMovement(transform.position, _stats.MoveSpeed);
            _animator = GetComponent<Animator>();
            if (Bootstrap.IsLoaded == false)
            {
                _serviceManager.NpcToInitialize.Add(this);
            }
        }

        public void ActivateNPC(bool enable)
        {
            if (enable)
            {
                //TimeCycle.onTimeChanged += TimeCycleChange;
                _animator.runtimeAnimatorController = _animatorController;
                _movement.Teleport(transform.position);
                _states.enabled = true;
                TryGetNextZone();
                _states.SwitchState(NPCState.Walk);
            }
            else
            {
                TimeCycle.onTimeChanged -= TimeCycleChange;
                _states.ExitState();
                _states.enabled = false;
                _movement.Stop();
            }
        }

        public void Upgrade() => onUpgrade.Invoke();

        public void Initialize()
        {
            _states = new StateMachine(transform, true, false, StartState(), IdleState(),
                WalkState(), GettingState(), SleepState(), SpendingState());

            if (_stateEvents.Length > 0)
            {
                var values = Enum.GetValues(typeof(NPCState));
                _existsStateEvents = new NPCStateEvent[values.Length];

                for (int i = 0; i < values.Length; i++)
                    _existsStateEvents[i] = _stateEvents.FirstOrDefault(x => (int)x.State == i);
            }

            _gettingZones = _serviceManager.GetZones<GettingZone>(ID, transform);
            _spendingZones = _serviceManager.GetZones<SpendingZone>(ID, transform);
            //_spendingZones = _serviceManager.TryGetZone<SpendingZone>(ID, transform);
            _waitingZones = _serviceManager.TryGetZone<WaitingZone>(ID, transform);
            _sleepingZones = _serviceManager.TryGetZone<SleepingZone>(ID, transform);

            ActivateNPC(true);

            //GUIController.LogDebug(name + " SpeedMult: " + m_movementSpeedMultiplier[m_level] + ". Speedcurve: " + m_movementSpeedMultiplier[100]);
        }

        private State StartState()
        {
            return new State(NPCState.Start, () =>
            {
                _existsStateEvents?[(int)NPCState.Start]?.Invoke(true);
                return true;
            });
        }

        private void SubscribeOnWorkAvailable()
        {
            if (CarriedResources < MaxCarriedResources)
            {
                _serviceManager.OnGettingAvailable += NextZoneAvailable;
                _serviceManager.subscribed.Add(name);
            }

            if (CarriedResources > 0)
            {
                isSubscribed = true;
                _serviceManager.OnSpendingAvailable += NextZoneAvailable;
                _serviceManager.subscribed.Add(name);
            }
        }

        private void UnsubscribeOnWorkAvailable()
        {
            isSubscribed = false;

            _serviceManager.subscribed.Remove(name);

            _serviceManager.OnGettingAvailable -= NextZoneAvailable;
            _serviceManager.OnSpendingAvailable -= NextZoneAvailable;
        }

        private void NextZoneAvailable(ServiceZone zone)
        {
            if (!zone.Unavailable)
            {
                UnsubscribeOnWorkAvailable();

                _currentZone.EndInteraction(transform, interactebleObjectIndex);

                _currentZone = zone;

                if (_currentZone is SpendingZone resSpend)
                {
                    if (resSpend.InteractableObjects.Length == 1)
                        _currentZone.StartInteraction(transform, 0, false);
                }

                if (_currentZone is GettingZone res)
                {
                    interactebleObjectIndex = 0;
                    res.BookService(true);
                }



                _states.SwitchState(NPCState.Walk);
                return;
            }

            TryGetNextZone();

            //_currentZone = null;
        }

        private bool GoToNextZone<T>(T[] zones) where T : ServiceZone
        {
            float minDistance = float.MaxValue;

            foreach (var zone in zones)
            {
                float distance = Vector3.Distance(transform.position, zone.transform.position);

                if (!zone.Unavailable && distance < minDistance)
                {
                    minDistance = distance;
                    _currentZone = zone;
                }
            }

            return minDistance < float.MaxValue;
        }

        private bool TryGetNextZone()
        {
            if (TimeCycle.timeOfDay == TimeCycle.TimesOfDay.Night && GoToNextZone(_sleepingZones))
            {
            }
            else if (CarriedResources < MaxCarriedResources && GoToNextZone(_gettingZones))
            {
                (_currentZone as GettingZone).BookService(true);
            }
            else if (CarriedResources > 0 && GoToNextZone(_spendingZones))
            {
                if (_currentZone is SpendingZone resSpend)
                {
                    if (resSpend.InteractableObjects.Length == 1)
                        _currentZone.StartInteraction(transform, 0, false);

                }
            }
            else if (_states.CurrentState != IdleState() && GoToNextZone(_waitingZones))
            {
            }
            else if (_currentZone.GetType() == typeof(WaitingZone))
            {
                return false;
            }
            else
            {
                //Debug.LogError("Not enough waiting zone for worker " + _serviceManager.name);
                return false;
            }
            return true;
        }

        #region IdleState

        private State IdleState()
        {
            return new State(NPCState.Idle, onStateStart: () =>
            {
                lastAnimatorState = _currentZone.InteractableObjects[0].InteractionStartAnimationTrigger;
                _animator.SetBool(lastAnimatorState, true);

                SubscribeOnWorkAvailable();
                //_animator.SetBool(IdleAnimator, true);
                _existsStateEvents?[(int)NPCState.Idle]?.Invoke(true);
                return true;
            }, onStateEnd: () =>
            {
                UnsubscribeOnWorkAvailable();
                _animator.SetBool(lastAnimatorState, false);
                //_animator.SetBool(IdleAnimator, false);
                _existsStateEvents?[(int)NPCState.Idle]?.Invoke(false);
                return true;
            });
        }

        #endregion

        #region Walk

        private bool Move()
        {
            if (_currentWalkDelay < Time.time)
            {
                UpdatePosition(_movement.CurrentPosition);
                UpdateRotation();
                _movement.Update(Time.deltaTime);
            }

            return _movement.IsMoving;
        }

        private void UpdateRotation()
        {
            transform.rotation =
                Quaternion.Lerp(
                    transform.rotation,
                    _targetRotation,
                    Time.deltaTime * _rotationSpeed);
        }

        private void UpdatePosition(Vector3 originPos)
        {
            transform.position = originPos;
            var delta = transform.position - _prevPosition;
            delta.y = 0;
            if (delta != Vector3.zero)
            {
                _targetRotation = Quaternion.LookRotation(delta);
            }

            _prevPosition = originPos;
        }

        private State WalkState()
        {
            return new State(NPCState.Walk, onStateStart: () =>
                {
                    _currentWalkDelay = Time.time + _delayBeforeWalk;

                    _currentZone?.BookService(false);

                    _movement.GoTo(_currentZone.GetInteractable(out interactebleObjectIndex, true, false).position, b =>
                    {

                        if (!b) return;

                        var destination = _currentZone;

                        if (_currentZone.GetType() == typeof(WaitingZone))
                        {
                            _currentZone.EndInteraction(transform, interactebleObjectIndex);
                            TryGetNextZone();
                        }

                        if (_currentZone != destination)
                        {
                            //_states.BreakState();
                            _states.SwitchState(NPCState.Idle);
                            _states.SwitchState(NPCState.Walk);
                            return;
                        }

                        _currentZone.StartInteraction(transform, interactebleObjectIndex);

                        lastAnimatorState = _currentZone.InteractableObjects[0].InteractionMovingAnimationTrigger;
                        _animator.SetBool(lastAnimatorState, true);

                        if (_currentZone.GetType() == typeof(GettingZone))
                            _states.SwitchState(NPCState.Getting);
                        else if (_currentZone.GetType() == typeof(SpendingZone))
                            _states.SwitchState(NPCState.Spending);
                        else if (_currentZone.GetType() == typeof(WaitingZone))
                            _states.SwitchState(NPCState.Idle);
                        else if (_currentZone.GetType() == typeof(SleepingZone))
                            _states.SwitchState(NPCState.Sleep);
                    });

                    lastAnimatorState = _currentZone.InteractableObjects[0].InteractionMovingAnimationTrigger;
                    _animator.SetBool(lastAnimatorState, true);

                    if (CarriedResources == 0)
                    {
                        _existsStateEvents?[(int)NPCState.Walk]?.Invoke(true);
                        //_animator.SetBool(WalkAnimator, true);

                        _animator.SetFloat(SpeedAnimator, _stats.MoveSpeed);
                        _movement.SetSpeedCustom(_baseSpeed * _stats.MoveSpeed);
                    }
                    else
                    {
                        _animator.SetFloat(SpeedAnimator, _stats.CarriedMoveSpeed);
                        _movement.SetSpeedCustom(_baseSpeed * _stats.CarriedMoveSpeed);

                        //_animator.SetBool(IdleAnimator, false);

                        if (_currentZone is WaitingZone)
                        {
                            //_animator.SetBool(WalkAnimator, true);
                            //_animator.SetBool(CarryAnimator, false);
                        }
                        else
                        {
                            //_animator.SetBool(WalkAnimator, false);
                            //_animator.SetBool(CarryAnimator, true);
                            _existsStateEvents?[(int)NPCState.Carry]?.Invoke(true);
                        }
                    }

                    return true;
                }, Move,
                onStateEnd: () =>
                {
                    if (CarriedResources == 0)
                    {
                        //_animator.SetBool(WalkAnimator, false);
                        _existsStateEvents?[(int)NPCState.Walk]?.Invoke(false);
                    }
                    else
                    {
                        //_animator.SetBool(WalkAnimator, false);
                        //_animator.SetBool(CarryAnimator, false);
                        _existsStateEvents?[(int)NPCState.Carry]?.Invoke(false);
                    }

                    _animator.SetBool(lastAnimatorState, false);

                    _movement.SetSpeedCustom(_baseSpeed);
                    _animator.SetFloat(SpeedAnimator, 1);

                    return true;
                });
        }

        #endregion

        #region Work

        public void DoWork()
        {
            (_currentZone as GettingZone)?.DoWork();

            //if (!_currentZone.Unavailable)
            //{
            _existsStateEvents?[(int)NPCState.DoWork]?.Invoke(true);
            //    if (_states.CurrentState == GettingState())
            //        return;
            //}
        }

        private State GettingState()
        {
            return new State(NPCState.Getting, onStateStart: () =>
            {
                //if (_currentZone.Unavailable)
                //{
                //    _states.SwitchState(NPCState.Walk);
                //    return false;
                //}

                _animator.SetBool(lastAnimatorState, false);
                lastAnimatorState = _currentZone.InteractableObjects[0].InteractionStartAnimationTrigger;
                _animator.SetBool(lastAnimatorState, true);

                _existsStateEvents?[(int)NPCState.Getting]?.Invoke(true);
                _animator.SetFloat(SpeedAnimator, _stats.WorkSpeed);
                //_animator.SetBool(GetResourcesAnimator, true);

                ShowProgressBar(Stats.TimeCycle);

                DOVirtual.DelayedCall(2/*Stats.TimeCycle*/, () => _states.SwitchState(NPCState.Walk));

                return true;
            }, onStateEnd: () =>
            {
                _animator.SetBool(lastAnimatorState, false);
                _animator.SetFloat(SpeedAnimator, 1);
                //_animator.SetBool(GetResourcesAnimator, false);
                _existsStateEvents?[(int)NPCState.Getting]?.Invoke(false);

                _currentZone.EndInteraction(transform, interactebleObjectIndex);

                if (_currentZone is GettingZone res)
                {
                    var countToAdd = res.IsEndless() ? 1 : res.GetMaxCount();

                    _storage.Set(countToAdd, true, false);
                    PopupText.PopupWorld(res, transform.position, countToAdd);

                    res.BookService(false);
                }

                TryGetNextZone();

                return true;
            });
        }

        #endregion

        private void ShowProgressBar(float time)
        {
            _storage.TryToShowProgressBar(time / Stats.TimeCycle, time);
            if (time < 0)
                return;

            DOVirtual.DelayedCall(1, () => ShowProgressBar(time - 1));
        }

        #region Spending

        private void Spending(SpendingZone spendingZone)
        {
            PopupText.PopupWorld(spendingZone, transform.position, CarriedResources);
            _storage.Get(spendingZone.Add(_storage.CurrentAmount), false);
            _states.SwitchState(NPCState.Walk);
        }

        private State SpendingState()
        {
            return new State(NPCState.Spending, () =>
            {
                _animator.SetBool(lastAnimatorState, false);
                lastAnimatorState = _currentZone.InteractableObjects[0].InteractionStartAnimationTrigger;
                _animator.SetBool(lastAnimatorState, true);

                //_animator.SetBool(SpendResourcesAnimator, true);
                _existsStateEvents?[(int)NPCState.Spending]?.Invoke(true);
                if (_currentZone is SpendingZone z)
                {
                    DOVirtual.DelayedCall(1, () => Spending(z));
                }

                return true;
            }, onStateEnd: () =>
            {
                _animator.SetBool(lastAnimatorState, false);
                //_animator.SetBool(SpendResourcesAnimator, false);

                _currentZone.EndInteraction(transform, interactebleObjectIndex);
                TryGetNextZone();
                _existsStateEvents?[(int)NPCState.Spending]?.Invoke(false);
                _storage.Set(0, updateProgressBar: false);
                return true;
            });
        }

        #endregion

        #region SleepState

        private void TimeCycleChange(TimeCycle.TimesOfDay timesOfDay)
        {
            TryGetNextZone();
            _states.ExitState();
            _states.SwitchState(NPCState.Walk);
        }

        private State SleepState()
        {
            var sleepToWalk = new Transition(StateMachineBase.UpdateType.Update, NPCState.Walk,
                () => TimeCycle.timeOfDay == TimeCycle.TimesOfDay.Day);
            return new State(NPCState.Sleep, onStateStart: () =>
            {
                _existsStateEvents?[(int)NPCState.Sleep]?.Invoke(true);
                _animator.SetBool(SleepAnimator, true);
                return true;
            }, onStateEnd: () =>
            {
                _animator.SetBool(SleepAnimator, false);

                _currentZone.EndInteraction(transform, interactebleObjectIndex);
                TryGetNextZone();
                _existsStateEvents?[(int)NPCState.Sleep]?.Invoke(false);
                return true;
            }, transitions: sleepToWalk);
        }

        #endregion
    }
}
