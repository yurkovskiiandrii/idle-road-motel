﻿using System;
using System.Collections;
using System.Collections.Generic;
using Destructables;
using UnityEngine;
using FXs;
using GameData;
using nickeltin.Extensions;
using NPCs;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class MinerWorld : MonoBehaviour
{
    private const float LOOP_HITS_DETECTION_TOLERANCE = 0.1f;
    private const float POSITION_DETECTION_TOLERANCE = 0.01f;

    private Animator _animator;
    [SerializeField] private RuntimeAnimatorController _animatorController;
    [SerializeField] private float loopHitOffset = 0.1f;
    [SerializeField, Range(0,1)] private float directionInterpolationSpeed;
    [SerializeField] private UnityEvent m_onDamageDealt;
    public ParticleSystem vfx;
    private readonly List<Vector3> lastThreeHits = new List<Vector3>();

    private NpcStats _stats;
    private Rigidbody m_rigidbody;
    private Collider m_collider;
    private Vector3 m_velocityBeforeStop = Vector3.zero;
    private Vector3 m_previousPosition;
    private static readonly int Hit = Animator.StringToHash("hit");


    private static event Action<Vector3> onDestiantionSet;

    public void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_collider = GetComponent<Collider>();
        _animator = GetComponent<Animator>();
        m_velocityBeforeStop = GetRandomDirection();
    }

    public void OnEnable()
    {
        _stats = GetComponent<NPC>().Stats;
        _animator.runtimeAnimatorController = _animatorController;
        onDestiantionSet += SetDestination_Internal;
        StartMovement(5);
    }

    private void OnDisable()
    {
        onDestiantionSet -= SetDestination_Internal;
        m_rigidbody.velocity = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if (Math.Abs(m_previousPosition.x - transform.position.x) < POSITION_DETECTION_TOLERANCE
            && Math.Abs(m_previousPosition.y - transform.position.y) < POSITION_DETECTION_TOLERANCE
            && Math.Abs(m_previousPosition.z - transform.position.z) < POSITION_DETECTION_TOLERANCE)
        {
            m_rigidbody.velocity = GetRandomDirection();
        }
        
        m_previousPosition = transform.position;
        
        var expected = Quaternion.LookRotation(transform.position.normalized + m_rigidbody.velocity, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, expected, directionInterpolationSpeed);
    }

    public void PlayVFX()
    {
        vfx.Play();
    }
    private void StartMovement(float delay)
    {
        IEnumerator DelayedStart()
        {
            m_rigidbody.velocity = Vector3.zero;
            yield return new WaitForSeconds(delay);
            m_rigidbody.velocity = m_velocityBeforeStop;
            ApplySpeedMultiplier(_stats.WorkSpeed);
        }

        StartCoroutine(DelayedStart());
    }

    private void OnCollisionEnter(Collision other)
    {
        if (TryToDealDamage(other))
        {
            //TODO: Vibration
            Vibration.Vibrate(Vibration.VibrationTarget.BallCollision);
        }
        else OnWallHit();

        ApplySpeedMultiplier(_stats.WorkSpeed);
    }

    private Vector3 GetRandomDirection() => Random.insideUnitSphere.normalized * _stats.WorkSpeed;

    private void ApplySpeedMultiplier(float multiplier)
    {
        m_rigidbody.velocity = m_rigidbody.velocity.normalized * _stats.WorkSpeed;
    }

    private void OnWallHit()
    {
        if (lastThreeHits.Count == 3) lastThreeHits.RemoveAt(0);
        lastThreeHits.Add(transform.position);
        if (lastThreeHits.Count == 3)
        {
            float tolerance = LOOP_HITS_DETECTION_TOLERANCE;
            //is position of the hit is approximately the same
            if (Math.Abs(lastThreeHits[0].x - lastThreeHits[2].x) < tolerance
                || Math.Abs(lastThreeHits[0].y - lastThreeHits[2].y) < tolerance
                || Math.Abs(lastThreeHits[0].z - lastThreeHits[2].z) < tolerance)
            {
                //Debug.Log("Loop hit");
                // var velocity = m_rigidbody.velocity;
                // float randomOffset = Random.Range(-loopHitOffset/2, loopHitOffset/2);
                // velocity = new Vector3(velocity.normalized.x - randomOffset/4, velocity.y - randomOffset/4,
                //                velocity.normalized.z + randomOffset/2) * m_source.Speed;
                // m_rigidbody.velocity = velocity;
                //ApplySpeedMultiplier(m_source.Speed);
                m_rigidbody.velocity = GetRandomDirection();
            }
        }
    }

    private bool TryToDealDamage(Collision other)
    {
        if (other.collider.TryGetComponent<IDamageable>(out var damageable))
        {
            damageable.TakeDamage(new Damage(_stats.WorkEfficiency, 
                transform.position, 
                m_rigidbody.velocity, _stats.CritChance, _stats.CritMultiplier));
            m_onDamageDealt?.Invoke();
            return true;
        }

        return false;
    }

    private void SetDestination_Internal(Vector3 destiantion)
    {
        _animator.SetTrigger(Hit);
        m_rigidbody.velocity = transform.position.DirectionTo(destiantion);
        ApplySpeedMultiplier(_stats.MoveSpeed);

    }
    
    public static void SetDestination(Vector3 worldDest) => onDestiantionSet?.Invoke(worldDest);
}
