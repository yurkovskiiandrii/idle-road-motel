using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Customers.CustomerAI;
using UnityEngine.Events;

[Serializable]
public class StateEvent
{
    public UnityEvent OnStart;
    public UnityEvent OnEnd;

    public void Invoke(bool onStart)
    {
        if (onStart) OnStart.Invoke();
        else OnEnd.Invoke();
    }
}