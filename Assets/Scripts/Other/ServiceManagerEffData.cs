﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEnvironment.Village.Structures;
using GameData;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using Other;
using UI;
using UnityEngine;
using nickeltin.Editor.Attributes;
using DG.Tweening;
using UnityEngine.Events;

namespace NPCs
{
    [CreateAssetMenu(menuName = "Environment/ServiceManagerEffData")]
    [Serializable]
    public class ServiceManagerEffData : ScriptableObject
    {
        public bool IsUpdatedData
        {
            get; set;
        }

        public ServiceManager regainService;
        public ServiceManager gettingService;
        public ServiceManager spendingService;
    }
}