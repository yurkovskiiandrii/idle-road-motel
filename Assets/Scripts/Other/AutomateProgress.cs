using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CoreGame.UI;
using Destructables;
using DG.Tweening;
using GameData;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using nickeltin.Singletons;
using NPCs;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu(menuName = "Managers/Automate Progress", fileName = "AutomateProgress")]
public class AutomateProgress : SOSingleton<AutomateProgress>
{
    [System.Serializable]
    public struct CalculationResult
    {
        public float TimeWasLeft;
        public float CurrencyAcquired;
    }

    [System.Serializable]
    public struct MiningValue
    {
        public int Gold;
        
        public int RedGem;
        
        public int GreenGem;
        
        public int BlueGem;
        
    }
    [SerializeField] private GameProgressSave _save;

    [Header("��������� ��� ������� ���������")]
    public Double RGrowth;
    public float Base;
    public float Mult;
    [Header("������ N ������ �������� ������")]
    [HideInInspector]

    [SerializeField, Range(1, 10)] private float _maxDelay;

    private float _delay = 1;
    [Space(20)]
    [Header("��������� ����������(������� �� 0, ���� ��������)")]
    [SerializeField]
    public int MaxIter ;
    [SerializeField]
    public int nowInter;
    [SerializeField]
    public float DelayProf; 
    // [Header("������ � ������")]
    public float ProfMin;
    [Header("����� ������� � ��������")]
    [SerializeField]
    public float TimeMinning;
    [SerializeField]
    public float _CurrentMining;
    [SerializeField]
    public bool Mining;

    [SerializeField]
    public MiningValue mines;
   
    [Space(10)]
    public UnitsData unitdata;
    [Space(10)]
    [SerializeField] private ServiceManager _miningService;
    

    [SerializeField] private CalculationResult _calculationResult;
    
    

    [HideInInspector] public ServiceManager[] ServiceManagers;
    
    [SerializeField]
    public List<RequiredGem> GemList = new List<RequiredGem>();


    public void CalculateOfflineProgress(int i)
    {
        Debug.LogError("CalculateOfflineProgress");
        ProgressUI.GetButton = Mining ? 1 : 0;

        if (Mining)
        {
            CalculateMiningEfficiency();
            MaxIter = (int)TimeMinning / 10;

            var difference = DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime);
            Debug.Log(difference.TotalSeconds.ToString());
            //
            if (difference.TotalSeconds < _CurrentMining && i == 0 )
            {
                _calculationResult = CalculateProgress( (float)difference.TotalSeconds);
            }
            else if(difference.TotalSeconds >=_CurrentMining && i == 0)
            {
                _calculationResult = CalculateProgress((float)difference.TotalSeconds);
            }
            else if( difference.TotalSeconds <= _CurrentMining && i == 1 )
            {
                _calculationResult = CalculateProgress(0);

            }
           // ProgressUI.opnButton.SetActive(true);

        }
        else
        {
            ProgressUI.takeVFX.SetActive(false);
           // ProgressUI.opnButton.SetActive(false);
            GemList.Clear();
        }

        DOVirtual.DelayedCall(_delay, CycleIncome);        
    }

   public void GetAutomateVFX()
    {
        Debug.LogError("GetAutomateVFX");
        ProgressUI.GetButton = Mining ? 1 : 0;

        ProgressUI.takeVFX.SetActive(mines.Gold > 0 || mines.BlueGem > 0 || mines.RedGem > 0 || mines.GreenGem > 0);
    }
    public void SetMin(float y)
    {
        _CurrentMining = y;
    }

    private CalculationResult CalculateProgress( float time)
    {
        if (time >= _CurrentMining)
        {
            float x = (float)Math.Round(time) / 10;
            Debug.Log(x.ToString());
            float y = (float)Math.Round(time) - ((int)(x) * 10);
            Debug.Log(y.ToString());

            DelayProf += y;
            int e = nowInter;
            nowInter += (int)x;
            if (nowInter >= MaxIter)
            {
                int r = MaxIter - e;
                CalculateEfficiency(r);
                Debug.Log("����� ������ ��� ������������");

                _calculationResult.TimeWasLeft = 0;
                _CurrentMining = 0;
                Mining = false;
                ProfMin = 0;
                DelayProf = 0;
                MaxIter = 0;
                nowInter = 0;
                ProgressUI.takeVFX.SetActive(true);
                GetAutomateVFX();

            }
            //  int x = (int)time / 10;
            //  CalculateEfficiency(x);
            //  _calculationResult.TimeWasLeft = 0;
            //  _CurrentMining = 0;
            //  Mining = false;
            //  ProfMin = 0;
            //  DelayProf = 0;
            //  MaxIter = 0;
            //  nowInter = 0;
            return new CalculationResult
            {
                TimeWasLeft = 0,
                

            };
        }
        else
        {
            float x = (float)Math.Round(time) / 10;
            Debug.Log(x.ToString());
            float y = (float)Math.Round(time) - ((int)(x) * 10);
            Debug.Log(y.ToString());

            DelayProf += y;
            int e = nowInter;
            nowInter += (int)x;
            if (nowInter >= MaxIter)
            {
                int r = MaxIter - e;
                CalculateEfficiency(r);
                Debug.Log("�������� ������ ��� ������������");

                _calculationResult.TimeWasLeft = 0;
                _CurrentMining = 0;
                Mining = false;
                ProfMin = 0;
                DelayProf = 0;
                MaxIter = 0;
                nowInter = 0;
                ProgressUI.takeVFX.SetActive(true);
                GetAutomateVFX();

            }
            else
            {
               
                Debug.Log("�������� ������ ��� ������������");
                SetMin(_CurrentMining - (float)Math.Round(time));
                int r = nowInter - e;
                Debug.Log(r.ToString());
                CalculateEfficiency((int)r);
                ProgressUI.takeVFX.SetActive(true);


            }
            //if ((nowInter += (int)Math.Round(x)) < MaxIter) {
            //    nowInter += (int)Math.Round(x);
            //}

            return new CalculationResult
            {
                TimeWasLeft = time,
                

            };
        }

    }
    public void CalculateEfficiency(int x)
    {
        
            foreach (RequiredGem a in GemList)
            {

                if (a.currencyType == CurrencyType.GreenGem && a.GetReward)
                {
                    mines.GreenGem += (int)Math.Round((ProfMin / 6) * x);
                }
                if (a.currencyType == CurrencyType.BlueGem && a.GetReward)
                {
                    mines.BlueGem += (int)Math.Round((ProfMin / 6) * x);
                }
                if (a.currencyType == CurrencyType.RedGem && a.GetReward)
                {
                    mines.RedGem += (int)Math.Round((ProfMin / 6) * x); ;

                }
                if (a.currencyType == CurrencyType.GoldGem && a.GetReward)
                {
                    mines.Gold += (int)Math.Round((ProfMin / 6) * x);
                }
            

            // Debug.Log((Math.Round((ProfMin / 6)) * x.ToString());
            }

        

        if(SceneLoader.currentScene == "Regular_Mine")
        {
            GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");
            GetGemFrom getGem = mine[0].gameObject.GetComponent<GetGemFrom>();
            Mineshaft mineScript = mine[0].gameObject.GetComponent<Mineshaft>();

            GemList = mineScript.requiredGem();

            foreach(RequiredGem a in GemList)
            {
                if(a.currencyType == CurrencyType.GreenGem &&a.GetReward)
                {
                    mines.GreenGem += (int)Math.Round((ProfMin / 6) * x);
                    getGem.AddGemFromAutomate(mines.GreenGem, CurrencyType.GreenGem);
                    mines.GreenGem = 0;
                }
                if (a.currencyType == CurrencyType.BlueGem && a.GetReward)
                {
                    mines.BlueGem += (int)Math.Round((ProfMin / 6) * x);
                    getGem.AddGemFromAutomate(mines.BlueGem, CurrencyType.BlueGem);
                    mines.BlueGem = 0;
                }
                if (a.currencyType == CurrencyType.RedGem && a.GetReward)
                {
                    mines.RedGem += (int)Math.Round((ProfMin / 6) * x); ;
                    getGem.AddGemFromAutomate(mines.RedGem, CurrencyType.RedGem);
                    mines.RedGem = 0;
                }
                if (a.currencyType == CurrencyType.GoldGem && a.GetReward)
                {
                    mines.Gold += (int)Math.Round((ProfMin / 6) * x);
                    getGem.AddGemFromAutomate(mines.Gold, CurrencyType.GoldGem);
                    mines.Gold = 0;
                }
              
            }


           GemList.Clear();
            if (x != 0)
            {
                foreach (var miner in _miningService.ActiveNpc.Values)
                {

                    miner.gameObject.GetComponent<MinerWorld>().PlayVFX();
                }

            }




                Debug.Log("we on scene mine");

        }

    }
    public void goToVillage()
    {
        GemList.Clear();
        GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");

        Mineshaft mineScript = mine[0].gameObject.GetComponent<Mineshaft>();

        GemList = mineScript.requiredGem();

    }

    public void CalculateMiningEfficiency()
    {
       // Debug.Log(_miningService.Units.Count.ToString());
      ////////////////foreach(var unit in _miningService.File.Units)
      ////////////////{
      ////////////////    foreach(var GetId in unitdata.MiningServiceUnits)
      ////////////////      {
      ////////////////          if(unit.ID == GetId.ID)
      ////////////////          {
      ////////////////              if(unit.Level>=2)
      ////////////////              {
      ////////////////                  Double y = (GetId.Stats[0].WorkEfficiency * (Math.Pow(RGrowth, 2)) + Base) * Mult;
      ////////////////                  //Double ps = ((y * Math.Pow(RGrowth, s) + Base) * Mult);
      ////////////////                  for (int a = 1; a < unit.Level-1; a++)
      ////////////////                {
                          
      ////////////////                    y = ((y * Math.Pow(RGrowth, a+2) + Base) * Mult);
                     
      ////////////////                }
      ////////////////                  // Double ps = ((y * Math.Pow(RGrowth, s) + Base) * Mult);
      ////////////////                 // var i = unit.Level - 1;
                       
      ////////////////                      // Double y = (GetId.Stats[i-1].WorkEfficiency * (Math.Pow(RGrowth, s)) + Base) * Mult;
      ////////////////                 // Debug.Log(y.ToString());



      ////////////////                 // Debug.Log("Lvl : " + s + "   Work : " + GetId.Stats[i - 1].WorkEfficiency + " Lvl-1 : " + (unit.Level - 1).ToString() + " " + Math.Pow(RGrowth, s).ToString());
                            
      ////////////////                    //int x =  (int)Math.Round(y);
      ////////////////                  ProfMin = (int)Math.Round(y);
      ////////////////                  Debug.Log((int)Math.Round(ProfMin / 6));
      ////////////////                  ProfMin = (int)Math.Round(ProfMin / 6) * 6;

      ////////////////                  // Debug.Log(Math.Round(x).ToString());

      ////////////////              }
      ////////////////              else
      ////////////////              {
                       
      ////////////////                  Double y = (GetId.Stats[0].WorkEfficiency * Math.Pow(RGrowth, 2) + Base) * Mult;
      ////////////////                 // var s  = (float)Math.Round(y);
      ////////////////                  ProfMin = (int)Math.Round(y);
      ////////////////                  ProfMin = (int)Math.Round(ProfMin / 6)*6;

      ////////////////                  //Debug.Log(Math.Round(x).ToString());
      ////////////////              }
      ////////////////          }
      ////////////////      }
      ////////////////}
       // for(int i = 0; )
        // _miningService.ActiveNpc.Values.Sum(npc => npc.Stats.WorkEfficiency);
      //_debug = 0;
      //var x = units.Level - 1;
      //Double s = units.Level;
      //Double y = (ins.Stats[x].WorkEfficiency * Math.Pow(RGrowth, s) + Base) * Mult;
      //_debug = (float)y;
      //debug += _debug;
      //Debug.Log(units.Level.ToString() + " /// " + s.ToString() + " /// " + debug.ToString());
    }

    private void CycleIncome()
    {
        Debug.LogError("CycleIncome");
        ProgressUI.GetButton = Mining ? 1 : 0;

        if (Mining)
        {
            Debug.Log("1111111111111111111");
            _delay = Random.Range(_maxDelay, _maxDelay);
            _CurrentMining -= 1f;
            DelayProf += 1;
            if(DelayProf >= 10)
            {
                float r = DelayProf - 10;
                DelayProf = r;
                CalculateEfficiency(1);
                nowInter++;
                ProgressUI.takeVFX.SetActive(true);
                // ProgressUI.opnButton.SetActive(true);
                GetAutomateVFX();

            }
            //_calculationResult = CalculateProgress( _delay);

            if (_CurrentMining <= 0)
            {
                if (nowInter != MaxIter)
                {
                    CalculateEfficiency(1);
                }
                _calculationResult.TimeWasLeft = 0;
                _CurrentMining = 0;
                Mining = false;
                ProfMin = 0;
                DelayProf = 0;
                MaxIter = 0;
                nowInter = 0;
                Debug.LogError("CycleIncome");
                GetAutomateVFX();

            }
            DOVirtual.DelayedCall(_delay, CycleIncome);
        }
        else
        {
            ProgressUI.takeVFX.SetActive(false);
            //ProgressUI.opnButton.SetActive(false);
            GetAutomateVFX();



        }
        // if (Dome.IsOnScene)
        //     return;
        //
        // _calculationResult = CalculateProgress(_minersEfficiency, _delay);
        // if (_calculationResult.CurrencyAcquired + _calculationResultBuffer < 1)
        // {
        //     _calculationResultBuffer += _calculationResult.CurrencyAcquired;
        //     return;
        // }
        // _currency.Value += _calculationResult.CurrencyAcquired + _calculationResultBuffer;
        //_calculationResultBuffer = 0;
    }
}