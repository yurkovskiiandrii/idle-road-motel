using UnityEngine;
using UnityEngine.Events;

public class VisibleEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent OnRenderingStart;
    [SerializeField] private UnityEvent OnRenderingEnd;
    private bool _isRendering;
 

    private void OnBecameVisible()
    {
        OnRenderingStart?.Invoke();
    }

    private void OnBecameInvisible()
    {
        OnRenderingEnd?.Invoke();
    }
}
