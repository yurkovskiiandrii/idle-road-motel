﻿using System;
using nickeltin.Tweening;
using TMPro;
using UnityEngine;

namespace UtilsLocal
{
    public static class Extentions
    {
        public static void ScaleInOut(this Transform transform, Vector3 scaleIn, Vector3 scaleOut, float t, Action onComplete = null)
        {
            LeanTween.scale(transform.gameObject, scaleIn, t / 2).setOnComplete(() =>
            {
                LeanTween.scale(transform.gameObject, scaleOut, t / 2).setOnComplete(onComplete);
            });
        }

        public static LTDescr LeanAlphaText(this TMP_Text textMesh, float to, float time) {
            var _color = textMesh.color;
            var _tween = LeanTween
                .value (textMesh.gameObject, _color.a, to, time)
                .setOnUpdate ((float _value) => {
                    _color.a = _value;
                    textMesh.color = _color;
                });
            return _tween;
        }
        

        /// <param name="x">root to be extracted form</param>
        /// <param name="n">root power, clamped to minmum of 1</param>
        /// <returns></returns>
        public static float Root(this float x, int n)
        {
            n = Mathf.Clamp(n, 1, int.MaxValue);
            return Mathf.Pow(x, 1.0f / n);
        }
        

        public static void AllChildrensSetActive(this Transform transform, bool isActive)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
               transform.GetChild(i).gameObject.SetActive(isActive); 
            }
        }
    }
}