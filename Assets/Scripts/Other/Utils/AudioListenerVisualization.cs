using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenerVisualization : MonoBehaviour
{
    [SerializeField] private float _radius;
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0,0,1,0.3f);
        Gizmos.DrawSphere(transform.position, _radius);
    }
}
