using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class ResourcesLoader
    {
        public static Sprite LoadSprite(string name) => Resources.Load<Sprite>(name);
    }
}