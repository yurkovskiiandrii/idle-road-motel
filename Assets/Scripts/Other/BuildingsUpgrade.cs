using nickeltin.Editor.Attributes;
using NPCs;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System.Collections.Generic;

public class BuildingsUpgrade : MonoBehaviour
{
    [SerializeField] private ServiceManager _service;
    [SerializeField] private UnityEvent[] OnBuildingsLeveling;

    private void Start()
    {
        _service.OnBuildingsLevelChange += InvokeEventOnLeveling;
        InvokeEventOnLeveling(_service.File.BuildingsLevel);
        _service.OnBuildingsSelected += (Material material) => ChangeMaterial(material, true);
        _service.OnBuildingsDeselected += (Material material) => ChangeMaterial(material, false);
    }

    private void InvokeEventOnLeveling(int level)
    {
        OnBuildingsLeveling[level]?.Invoke();
    }

    private void ChangeMaterial(Material material, bool isSelected)
    {
        //if (_service.File.BuildingsLevel > 0)
        //    return;

        GetComponent<MaterialChanger>()?.ChangeMaterial(material, isSelected);
    }
}