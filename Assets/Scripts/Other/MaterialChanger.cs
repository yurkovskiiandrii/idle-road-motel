using System;
using System.Collections;
using System.Collections.Generic;
using GameEnvironment.Village.Structures;
using UnityEngine;
using UnityEngine.Events;

public class MaterialChanger : MonoBehaviour
{
    [SerializeField] bool needToHideObjects = false;

    [SerializeField] List<GameObject> graphicsContainer;

    [Header("Object components ")] [SerializeField]
    UnityEvent OnBuildingSelect;

    [SerializeField] UnityEvent OnBuildingDeselect;

    [SerializeField] private GameObject currentLevelProp;

    [SerializeField] private PropsBehaviour _propsBehaviour;

    Dictionary<MeshRenderer, Material> normalMaterialsDictionary;

    Dictionary<GameObject, bool> interactableObjects;

    public void HideObject(GameObject go)
    {
        if (interactableObjects.ContainsKey(go) && interactableObjects[go])
            return;

        var particles = go.GetComponent<ParticleSystem>();

        if (particles)
        {
            if (!interactableObjects.ContainsKey(go))
                interactableObjects.Add(go, particles.isPlaying);
            particles.Stop();
            return;
        }

        if (!interactableObjects.ContainsKey(go))
            interactableObjects.Add(go, go.activeInHierarchy);
        go.SetActive(false);
    }

    public void ShowObject(GameObject go)
    {
        if (interactableObjects.ContainsKey(go) && !interactableObjects[go])
            return;

        var particles = go.GetComponent<ParticleSystem>();

        if (particles)
        {
            if (!interactableObjects.ContainsKey(go))
                interactableObjects.Add(go, particles.isPlaying);
            particles.Play();
            return;
        }

        if (!interactableObjects.ContainsKey(go))
            interactableObjects.Add(go, go.activeInHierarchy);
        go.SetActive(true);
    }

    public void ChangeMaterial(Material material, bool isSelected)
    {
        //if (_service.File.BuildingsLevel > 0)
        //    return;
        if (needToHideObjects)
            graphicsContainer.ForEach(x =>
            {
                if (isSelected) ShowObject(x);
                else HideObject(x);
            });

        if (isSelected)
        {
            interactableObjects = new Dictionary<GameObject, bool>();
            OnBuildingSelect.Invoke();
        }

        if (!isSelected)
        {
            OnBuildingDeselect.Invoke();

            interactableObjects.Clear();
            interactableObjects = null;

            foreach (var pair in normalMaterialsDictionary)
                pair.Key.material = pair.Value;

            normalMaterialsDictionary.Clear();
            normalMaterialsDictionary = null;
            return;
        }

        normalMaterialsDictionary = new Dictionary<MeshRenderer, Material>();

        graphicsContainer.ForEach(x => ChangeMaterial(x.transform, material));
    }

    void ChangeMaterial(Transform obj, Material material, bool changedMaterial = false)
    {
        if (obj.childCount == 0 && !changedMaterial)
        {
            var meshRenderer = obj.gameObject.GetComponent<MeshRenderer>();

            if (meshRenderer)
            {
                normalMaterialsDictionary[meshRenderer] = meshRenderer.material;
                meshRenderer.material = material;
            }

            return;
        }

        for (int i = 0; i < obj.childCount; ++i)
        {
            var currentNode = obj.GetChild(i);
            var meshRenderer = currentNode.gameObject.GetComponent<MeshRenderer>();

            if (meshRenderer)
            {
                normalMaterialsDictionary[meshRenderer] = meshRenderer.material;
                meshRenderer.material = material;
            }

            ChangeMaterial(currentNode, material, true);
        }
    }

    private void ChangeObject(int level)
    {
        if (level == 0 || graphicsContainer.Count == 0)
            return;

        GameObject newProp = graphicsContainer[level - 1];
        if (currentLevelProp != null)
            currentLevelProp.SetActive(false);
        currentLevelProp = newProp;
        currentLevelProp.SetActive(true);
    }

    private void OnUpgrade()
    {
        ChangeObject(_propsBehaviour.Stats.Level);
    }

    private void Start()
    {
        try
        {
            _propsBehaviour = this.GetComponent<PropsBehaviour>();
            _propsBehaviour?.onUpgraded.AddListener(OnUpgrade);
            ChangeObject(_propsBehaviour.Stats.Level);
        }
        catch
        {
        }
    }
}