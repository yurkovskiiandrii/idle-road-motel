using FXs;
using nickeltin.ObjectPooling;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioSourceController : PoolObject<AudioSourceController>
{
    [SerializeField] private AudioClip[] _audioClips;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = Audio.GetInstance().SoundsVolume;
    }

    private void OnEnable()
    {
        Audio.GetInstance().SoundsVolume.onValueChanged += SetVolume;
        SceneLoader.onSceneLoad += SceneChange;
    }

    private void OnDisable()
    {
        Audio.GetInstance().SoundsVolume.onValueChanged -= SetVolume;
    }

    private void SetVolume(float volume)
    {
        _audioSource.volume = volume;
    }

    private void SceneChange(string scene)
    {
        if (gameObject.scene.name == scene)
            _audioSource.enabled = true;
        else
        {
            _audioSource.enabled = true;
        }
    }

    public void PlayRandom()
    {
        _audioSource.clip = _audioClips[Random.Range(0, _audioClips.Length)];
        _audioSource.Play();
    }
}
