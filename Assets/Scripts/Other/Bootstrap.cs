using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.Saving;
using nickeltin.Singletons;
using NPCs;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class Bootstrap : MonoSingleton<Bootstrap>
{
   public bool DoBootstrap;
   [SerializeField] private bool doNewGame;
   public static bool DoNewGame => instance.doNewGame;
   private bool _isLoaded;
   public static bool IsLoaded => instance._isLoaded;
   [Scene] public string[] ScenesToLoad;
   [Scene] public string ActiveScene;
   private int sceneIndex;
   private InputSettings _inputSettings;
   public AutomateProgress auto;


    public static event Action OnAllScenesLoaded;
   
   public UnityEvent AllScenesLoaded;
   private void Start()
   {
      if (DoBootstrap)
      {
         LoadAllScenes();
      }

      OnAllScenesLoaded += Initialization;
      CheckAllSceneLoaded();
   }
   private void LoadAllScenes()
   {
      if (sceneIndex < ScenesToLoad.Length)
      {
         SceneLoader.LoadScene(ScenesToLoad[sceneIndex], LoadSceneMode.Additive, LoadAllScenes);
         sceneIndex++;
      }
       

    }
   private void SetActiveScene()
   {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(ActiveScene));

        try
        {
            GameObject.FindObjectOfType<Game.Tutorial.TutorialController>().StartTutorial();
        }
        catch
        {

        }
   }

   private void Initialization()
   {
      SetActiveScene();
      SaveSystem.GetSavedItem("service_package", out SavePackage services);
      var progressCalculator = ProgressCalculator.GetInstance();
      //progressCalculator.ServiceManagers = services.Saves.Cast<ServiceManager>().ToArray();
      foreach (var service in services.Saves)
      {
         var s = service as ServiceManager;
         s.Initialization();
      }

        if (Game.Tutorial.TutorialController.isStartedTutorial)
            return;

        DG.Tweening.DOVirtual.DelayedCall(0.2f, progressCalculator.CalculateOfflineProgress);
        DG.Tweening.DOVirtual.DelayedCall(0.2f, progressCalculator.StartCanvaDisable);

        //progressCalculator.CalculateOfflineProgress();
    }

    IEnumerator SceneCheck()
   {
      yield return new WaitForSeconds(1);
      CheckAllSceneLoaded();
   }
   private void CheckAllSceneLoaded()
   {
      foreach (var sceneName in ScenesToLoad)
      {
         if (!SceneManager.GetSceneByName(sceneName).isLoaded)
         {
            StartCoroutine(SceneCheck());
            return;
         }
      }

      _isLoaded = true;
      OnAllScenesLoaded?.Invoke();
      AllScenesLoaded?.Invoke();
        if (auto.Mining)
        {
            auto.CalculateOfflineProgress(0);
            
        }
        
    }
}
