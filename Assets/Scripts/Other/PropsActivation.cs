using System;
using System.Linq;
using GameData;
using nickeltin.Singletons;
using NPCs;
using UnityEngine;
using GameEnvironment.Village.Structures;

namespace Other
{
    public class PropsActivation : MonoSingleton<PropsActivation>
    {
        [SerializeField] private PropsBehaviour[] PlumbersServiceProps;
        [SerializeField] private PropsBehaviour[] CleanersServiceProps;
        [SerializeField] private PropsBehaviour[] Room1Props;
        [SerializeField] private PropsBehaviour[] Room2Props;
        [SerializeField] private PropsBehaviour[] Room3Props;
        [SerializeField] private PropsBehaviour[] Room4Props;
        [SerializeField] private PropsBehaviour[] PoolServiceProps;
        [SerializeField] private PropsBehaviour[] BarServiceProps;
        [SerializeField] private PropsBehaviour[] ParkingServiceProps;
        [SerializeField] private PropsBehaviour[] ReceptionServiceProps;

        public static PropsBehaviour ActivateProps(ServiceManager service, Props props)
        {
            PropsBehaviour result = GetProps(service, props);

            result.Activate(service ?? result.GetService());

            return result;
        }

        public static PropsBehaviour[] GetProps(ServiceManager service) =>
            service.Service switch
            {
                ServiceType.PlumbersService => instance.PlumbersServiceProps,
                ServiceType.CleanersService => instance.CleanersServiceProps,
                ServiceType.Room1 => instance.Room1Props,
                ServiceType.Room2 => instance.Room2Props,
                ServiceType.Room3 => instance.Room3Props,
                ServiceType.Room4 => instance.Room4Props,
                ServiceType.PoolService => instance.PoolServiceProps,
                ServiceType.BarService => instance.BarServiceProps,
                ServiceType.ParkingService => instance.ParkingServiceProps,
                ServiceType.ReceptionService => instance.ReceptionServiceProps,
                _ => null
            };

        public static PropsBehaviour GetProps(ServiceManager service, Props props)
        {
            service = service ?? instance.FindService(props);
            try
            {
                return GetProps(service).FirstOrDefault(x => x.ID == props.ID);
            }
            catch
            {
                Debug.LogError(string.Format("Props with ID {0} didn`t found", props.ID));
                return null;
            }
        }

        public static int GetPropsCount(ServiceManager service)
        {
            return GetProps(service).Count(x => x.showAtServiceMenu);
        }

        public static bool NeedToShowPropsAtMenu(ServiceManager service, Props props)
        {
            try
            {
                return GetProps(service, props).showAtServiceMenu;
            }
            catch
            {
                Debug.LogError("Some exception. Check props at service");
                return false;
            }
        }

        ServiceManager FindService(Props props)
        {
            //if (instance.LumberjacksProps.Any(x => x.ID == props.ID))
            //    return instance.LumberjacksProps.FirstOrDefault(x => x.ID == props.ID).GetService();

            return null;
        }

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.green;
        //    Gizmos.DrawSphere(transform.position, 2);
        //}
    }
}