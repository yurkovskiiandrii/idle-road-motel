﻿using System;
using UI;
using UnityEngine;
using FXs;
using Random = UnityEngine.Random;

public readonly struct Damage
{
    public const float CRITICAL_MULTIPLIER = 2;
    public const float CRITICAL_CHANCE = 30;

    public readonly bool IsCritical;
    public readonly float RawValue;
    public readonly float CriticalMultiplier;
    public float Value { get => IsCritical ? RawValue * CriticalMultiplier : RawValue; }
    public readonly Vector3 Position;
    public readonly Vector3 Velocity;
    public readonly float ProfitMultiplier;

    public static Action<float> onDamage;

    public Damage(float value, Vector3 position, Vector3 velocity, float critChance = CRITICAL_CHANCE , float critMultiplier = CRITICAL_MULTIPLIER , float profitMultiplier = 1)
    {
        IsCritical = Random.Range(0, 100) < critChance;
        CriticalMultiplier = critMultiplier;
        
        RawValue = value;
        
        Position = position;
        Velocity = velocity;

        ProfitMultiplier = profitMultiplier;
        
        onDamage?.Invoke(Value);
        
        //TODO: Vibration
        /*if (IsCritical)
        {
            PopupText.PopupForSure(this);
            Vibration.Vibrate(Vibration.VibrationTarget.CriticalDamage);
        }
        else PopupText.Popup(this);*/
    }

    public Damage(Stats stats, Vector3 position, Vector3 velocity, float profitMultiplier = 1) : this(stats.value, 
        position, velocity, stats.critChance, stats.critMultiplier, profitMultiplier) { }
    
    [Serializable]
    public class Stats
    {
        public float value = 1;
        public float critChance = Damage.CRITICAL_CHANCE;
        public float critMultiplier = Damage.CRITICAL_MULTIPLIER;
    }
}
