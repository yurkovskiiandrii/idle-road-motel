﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEnvironment.Village.Structures;
using GameData;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using Other;
using UI;
using UnityEngine;
using nickeltin.Editor.Attributes;
using DG.Tweening;
using UnityEngine.Events;
using Customers;
using System.Drawing;
using static System.Collections.Specialized.BitVector32;

namespace NPCs
{
    [CreateAssetMenu(menuName = "Environment/ServiceManager")]
    [Serializable]
    public class ServiceManager : Saveable<ServiceManager.ServiceSave>
    {
        [Serializable]
        public class ServiceSave
        {
           [Serializable]
           public class Save
           {
               public string ID;
               public int Level;
               public int CurrentAmount;

               public Save(string id, int level, int currentAmount = 0)
               {
                   ID = id;
                   Level = level;
                   CurrentAmount = currentAmount;
               }
           }
           public int BuildingsLevel;
           public List<Save> Resource = new List<Save>();
           public List<Save> Units= new List<Save>();
           public List<Save> Props = new List<Save>();
        }

        public bool needCameraTarget = true;

        public ServiceSpendingType serviceType;

        public bool IsStorageActivated
        {
            get;
            set;
        }

        public Buildings Buildings { get; private set; }
        public List<Props> Props { get; private set; }
        public List<PropsBehaviour> PropsBehaviourContainer { get; private set; }
        public List<Unit> Units{ get; private set; }

        public ServiceType Service {get; private set; }

        public ServiceManagerEffData services;
        public ServiceWaitTimers timers;

        readonly Dictionary<string, ServiceZone> ServiceZones = new Dictionary<string, ServiceZone>();
        public readonly Dictionary<string, NPC> ActiveNpc = new Dictionary<string, NPC>();
        public readonly Dictionary<string, PropsBehaviour> ActiveProps = new Dictionary<string, PropsBehaviour>();
        [NonSerialized] public List<NPC> NpcToInitialize = new List<NPC>();
        public event Action<GettingZone> OnGettingAvailable;

        List<CustomerPleasureZone> pleasuresZonesCashed;

        [SerializeField] string sleepZoneID = "Sleep";

        public void InvokeGettingAvailable(GettingZone zone)
        {
            OnGettingAvailable?.Invoke(zone);
        }

        public event Action<SpendingZone> OnSpendingAvailable;
        public void InvokeSpendingAvailable(SpendingZone zone)
        {
            //foreach (var go in subscribed)
            //    Debug.Log(go.name);

            OnSpendingAvailable?.Invoke(zone);
        }
        public event Action<int> OnBuildingsLevelChange;
        public event Action OnUnitLevelChange;
        public event Action OnPropsLevelChange;

        public Action<Material> OnBuildingsSelected;
        public Action<Material> OnBuildingsDeselected;

        Storage serviceStorage;

        [HideInInspector]
        public float SpendServiceMoney;
        [HideInInspector]
        public float OfflineProggressSpendServ;
        float lastStorageSize = 0;

        public List<string> subscribed = new List<string>();

        [SerializeField] bool isEmptyStorageOnStart = false;

        public List<string> customerPleasureTypes;

        public List<string> GetCustomerPleasureTypes()
        {
            if (customerPleasureTypes == null || customerPleasureTypes.Count == 0)
            {
                customerPleasureTypes = new List<string>();

                var pleasureZones = GetZones<CustomerPleasureZone>();
               
                foreach(var zone in pleasureZones)
                    if(!customerPleasureTypes.Contains(zone.requirementType))
                        customerPleasureTypes.Add(zone.requirementType);
            }

            return customerPleasureTypes;
        }

        public void Initialization()
        {
            pleasuresZonesCashed = new List<CustomerPleasureZone>();

            ActiveNpc.Clear();
            ActiveProps.Clear();
            if (File.Units == null)
            {
                File = new ServiceSave();
            }
            Service = (ServiceType)Enum.Parse(typeof(ServiceType), m_saveId);

            SaveSystem.GetSavedItem(out UnitsData unitsData);
            Units = unitsData.GetServiceUnits(Service);
            SaveSystem.GetSavedItem(out BuildingsData buildingsData);
            Buildings = buildingsData.GetBuildingsData(Service);

            SaveSystem.GetSavedItem(out PropsData propsData);
            Props = propsData.GetServiceProps(Service);

            if (Bootstrap.DoNewGame)
                File = new ServiceSave();

            //Debug.Log("Initialiazation " + name + "   " + Props.Count);

            IsStorageActivated = false;

            foreach (var props in File.Props)
            {
                //if (ActiveProps.ContainsKey(props.ID))
                //    continue;

                var propsDat = Props.FirstOrDefault(x => x.ID == props.ID);

                if (propsDat == null)
                    continue;

                var propsBeh = PropsActivation.ActivateProps(this, propsDat);

                if(serviceStorage == null)
                    serviceStorage = propsBeh.GetStorage();

                propsBeh.Stats = propsDat.Stats[props.Level - 1];

                if (serviceStorage != null)
                {
                    serviceStorage.GetComponent<SpendingZone>()?.Activate();

                    var propsID = props.ID;

                    if(!IsStorageActivated)
                    {
                        IsStorageActivated = true;

                        if (serviceType == ServiceSpendingType.Spending)
                            DOVirtual.DelayedCall(0.1f, delegate { CalculateSpendingStorageSize(propsID); });
                        else
                            DOVirtual.DelayedCall(0.1f, delegate { CalculateGettingStorageSize(propsID); });
                    }
                        
                    serviceStorage.SubscribeOnValueChanged(SaveStorageAmount);
                }

                ActiveProps.Add(props.ID, propsBeh);

                //DOVirtual.DelayedCall(0.2f, UpdateStorageAmount);
            }

            foreach (var serviceZone in ServiceZones)
                serviceZone.Value.Initialization();

            if (services && !services.IsUpdatedData && GetMaxWorkingZonesCount() > 0
                && (serviceType == ServiceSpendingType.Getting
                || serviceType == ServiceSpendingType.Regain))
            {
                DOVirtual.DelayedCall(0.1f, ActivateWorkingZones);
                DOVirtual.DelayedCall(1f, ResetDataUpdate);
            }

            DOVirtual.DelayedCall(0.15f, UnitsSpawn);
            //ActivateWorkingZones();
        }

        void ResetDataUpdate()
        {
            IsStorageActivated = false;
            services.IsUpdatedData = false;
        }

        void ActivateWorkingZones()
        {
            if (services.IsUpdatedData)
                return;

            services.IsUpdatedData = true;

            var AbTime = ProgressCalculator.GetInstance().PlayerAbsenceSeconds;
            //var AbTime = 20;

            int treesLast = serviceType == ServiceSpendingType.Getting ? AmountAvailableWorkZones() : AmountAvailableSpendingZones();

            var AvCycle1 = services.regainService.GetAvCycleTime();
            var AvCycle2 = services.gettingService.GetAvCycleTime();
            var Units1Amount = services.regainService.GetNPCCount();
            var Units2Amount = services.gettingService.GetNPCCount();

            //ОкруглВверх(TreesLast + ((AbTime / AvCycle1) * Units1Amount - (AbTime / AvCycle2) * Units2Amount)) )

            var regainCount = AvCycle1 != 0 ? (AbTime / AvCycle1) * Units1Amount : 0;
            var gettingCount = AvCycle2 != 0 ? (AbTime / AvCycle2) * Units2Amount : 0;

            var maxTreesCount = GetMaxWorkingZonesCount();

            int treesResult = (int)Mathf.Clamp(treesLast + Mathf.RoundToInt(regainCount) - gettingCount, 0, maxTreesCount);

            if (serviceType == ServiceSpendingType.Regain)
            {
                var spendingZones = ServiceZones.Values.Where(x => x is SpendingZone && x.IsActivated()).ToArray();

                foreach (var zone in spendingZones)
                {
                    var _zone = zone.gameObject.GetComponent<GettingZone>();

                    _zone.Set(treesResult > 0 ? _zone.Capacity : 0);

                    --treesResult;
                }

                return;
            }

            var gettingZones = ServiceZones.Values.Where(x => x is GettingZone && x.IsActivated()).ToArray();

            foreach (var zone in gettingZones.Reverse())
            {
                var _zone = zone as GettingZone;

                _zone.Set(treesResult > 0 ? _zone.Capacity : 0);

                --treesResult;
            }
        }

        void UnitsSpawn()
        {
            foreach (var npc in NpcToInitialize)
                npc.Initialize();

            NpcToInitialize = null;
            foreach (var unit in File.Units)
            {
                if (ActiveNpc.ContainsKey(unit.ID))
                    continue;
                UnitSpawner.SpawnUnit(this,
                   Units.FirstOrDefault(x => x.ID == unit.ID));
            }
        }

        public void SubscribeOnPropsAvailable(UnityAction action) => PropsActivation.GetProps(this).ToList().ForEach(x => x.SubscribeOnAvailable(action));

        public void UnSubscribeOnPropsAvailable(UnityAction action) => PropsActivation.GetProps(this).ToList().ForEach(x => x.UnSubscribeOnAvailable(action));

        public void PropsBecameAvailable() => PropsActivation.GetProps(this).ToList().ForEach(x => x.OnAvailable());

        public BuildingsStats GetCurrentBuildingStats() => Buildings.Stats[Mathf.Max(0, File.BuildingsLevel - 1)];

        public void BuildingsLevelUp()
        {
            File.BuildingsLevel++;
            OnBuildingsLevelChange?.Invoke(File.BuildingsLevel);
        }

        public void AddZoneToService(string id, ServiceZone service)
        {
            if (!ServiceZones.ContainsKey(id))
                ServiceZones.Add(id, service);
            //else
            //    Debug.LogError("Object with same id :" + id);
        }

        public void AddValueToServiceZone(string id, int count)
        {
            try
            {
                ServiceZones[id].GetComponent<GettingZone>().Add(count);
            }
            catch
            {
                ServiceZones[id].GetComponent<SpendingZone>().Add(count);
            }
        }

        public void AddNPCToService(string id, NPC npc)
        {
            if (ActiveNpc.ContainsKey(id))
            {
                ActiveNpc.Remove(id);
                Destroy(npc.gameObject);
            }
            
            ActiveNpc.Add(id, npc);
            if (File.Units.All(x => x.ID != id))
            {
                File.Units.Add(new ServiceSave.Save(id, npc.Stats.Level));
            }
            OnUnitLevelChange?.Invoke();
           // progress.UpdatePerMin();
        }

        public void AddPropsToContainer(PropsBehaviour propsBehaviour)
        {
            if (PropsBehaviourContainer == null)
                PropsBehaviourContainer = new List<PropsBehaviour>();

            if (!PropsBehaviourContainer.Contains(propsBehaviour))
                PropsBehaviourContainer.Add(propsBehaviour);
        }

        public void AddPropsToService(PropsBehaviour propsBehaviour/*int index*/, Storage storage = null)
        {
            int index = -1;
            
            for(int i = 0; i < Props.Count; i++)
            {
                if (Props[i].ID == propsBehaviour.ID)
                {
                    index = i;
                    break;
                }
            }

            if(index == -1)
            {
                Debug.LogError(string.Format("Props with ID {0} didn`t found", propsBehaviour.ID));
                return;
            }
            
            if (index == 0 && storage != null)
                IsStorageActivated = true;

            var props = Props[index];

            /*var propsBehaviour = */PropsActivation.ActivateProps(this,
                Props.FirstOrDefault(x => x.ID == props.ID));

            propsBehaviour.Stats = props.Stats[0];

            if (storage == null)
                storage = propsBehaviour.GetStorage();

            if (ActiveProps.ContainsKey(props.ID))
                ActiveProps.Remove(props.ID);

            ActiveProps.Add(props.ID, propsBehaviour);

            if (File.Props.All(x => x.ID != props.ID))
                File.Props.Add(new ServiceSave.Save(props.ID, propsBehaviour.Stats.Level));

            if(storage != null)
            {
                serviceStorage = storage;

                storage.Set(File.Props.FirstOrDefault(x => x.ID == props.ID).CurrentAmount);
                storage.SubscribeOnValueChanged(SaveStorageAmount);

                try
                {
                    var spendingZone = storage.GetComponent<SpendingZone>();
                    spendingZone.Activate();
                    InvokeSpendingAvailable(spendingZone);
                }
                catch
                {

                }
            }

            propsBehaviour.ReceiveResources(this);

            OnPropsLevelChange?.Invoke();
        }

        public void AddPropsToService(int index, Storage storage = null)
        {
            if (index == 0 && storage != null)
                IsStorageActivated = true;

            var props = Props[index];

            var propsBehaviour = PropsActivation.ActivateProps(this,
                Props.FirstOrDefault(x => x.ID == props.ID));

            propsBehaviour.Stats = props.Stats[0];

            if (storage == null)
                storage = propsBehaviour.GetStorage();

            if (ActiveProps.ContainsKey(props.ID))
                ActiveProps.Remove(props.ID);

            ActiveProps.Add(props.ID, propsBehaviour);

            if (File.Props.All(x => x.ID != props.ID))
                File.Props.Add(new ServiceSave.Save(props.ID, propsBehaviour.Stats.Level));

            if (storage != null)
            {
                serviceStorage = storage;

                storage.Set(File.Props.FirstOrDefault(x => x.ID == props.ID).CurrentAmount);
                storage.SubscribeOnValueChanged(SaveStorageAmount);

                try
                {
                    var spendingZone = storage.GetComponent<SpendingZone>();
                    spendingZone.Activate();
                    InvokeSpendingAvailable(spendingZone);
                }
                catch
                {

                }
            }

            propsBehaviour.ReceiveResources(this);

            OnPropsLevelChange?.Invoke();
        }

        void SaveStorageAmount(int count)
        {
            var props = Props[0];
            File.Props.FirstOrDefault(x => x.ID == props.ID).CurrentAmount = count;
        }

        public int GetUnitLevel(int unitIndex)
        {
            if (ActiveNpc.TryGetValue(Units[unitIndex].ID, out var npc))
            {
                return npc.Stats.Level;
            }

            return -1;
        }

        public int GetPropsLevel(int propsIndex)
        {
            if (ActiveProps.TryGetValue(Props[propsIndex].ID, out var props))
            {
                return props.Stats.Level;
            }
            return -1;
        }

        public int GetUnitLevel(string unitID)
        {
            if (ActiveNpc.TryGetValue(unitID, out var npc))
            {
                return npc.Stats.Level;
            }

            return -1;
        }

        public int GetPropsLevel(string propsID)
        {
            if (ActiveProps.TryGetValue(propsID, out var props))
            {
                return props.Stats.Level;
            }
            return -1;
        }

        public void IncreaseUnitLevel(int unitIndex, Currency currency)
        {
            var currentUnit = Units[unitIndex];
            if(ActiveNpc.TryGetValue(currentUnit.ID, out var npc))
            {
                if(!currency.TryToGet(currentUnit.Stats[npc.Stats.Level-1].UpgradePrice))
                    return;
                var nextLevel = npc.Stats.Level + 1;
                File.Units.First(x=>x.ID == currentUnit.ID).Level = nextLevel;
                npc.Stats = currentUnit.Stats[nextLevel - 1];
                npc.gameObject.GetComponent<MinerWorld>()?.OnEnable();
                OnUnitLevelChange?.Invoke();
                npc.Upgrade();
            }
            else
            {
                Debug.LogError("You try increase level of unexisted unit");
            }
        }

        public void IncreasePropsLevel(int propsIndex, Currency currency, Currency red, Currency blue, Currency green)
        {
            var currentProps = Props[propsIndex];
            if (ActiveProps.TryGetValue(currentProps.ID, out var props))
            {
                if (!currency.TryToGet(currentProps.Stats[props.Stats.Level - 1].UpgradePriceCoins) && !red.TryToGet(currentProps.Stats[props.Stats.Level - 1].UpgradePriceRedGems) && !blue.TryToGet(currentProps.Stats[props.Stats.Level - 1].UpgradePriceBlueGems ) && !green.TryToGet(currentProps.Stats[props.Stats.Level - 1].UpgradePriceGreenGems))
                {
                    return;
                }

                currency.Value -= currentProps.Stats[props.Stats.Level - 1].UpgradePriceCoins;
                red.Value -= currentProps.Stats[props.Stats.Level - 1].UpgradePriceRedGems;
                blue.Value -= currentProps.Stats[props.Stats.Level - 1].UpgradePriceBlueGems;
                green.Value -= currentProps.Stats[props.Stats.Level - 1].UpgradePriceGreenGems;

                var nextLevel = props.Stats.Level + 1;
                File.Props.First(x => x.ID == currentProps.ID).Level = nextLevel;
                props.Stats = currentProps.Stats[nextLevel - 1];

                var storage = props.GetStorage();

                if (storage != null)
                    InvokeSpendingAvailable(storage.gameObject.GetComponent<SpendingZone>());

                //props.gameObject.GetComponent<MinerWorld>()?.OnEnable();
                OnPropsLevelChange?.Invoke();
            }
            else
            {
                Debug.LogError("You try increase level of unexisted unit");
            }
        }

        public bool IsUnitIdBelongsToService(string unitId) =>
            Units?.Count > 0
            && Units[0].ID.Contains(unitId);

        public NPC GetHighestLvlNpc(string unitId)
        {
            if (ActiveNpc.Count == 0)
            {
                return null;
            }
            
            NPC highestLvlNpc = ActiveNpc.Values.OrderByDescending(npc => npc.Stats.Level)
                .FirstOrDefault(s => s.ID.Contains(unitId));
           
            return highestLvlNpc;
        }

        public int GetActivePropsCountForMenu()
        {
            return ActiveProps.Where(x => x.Value.showAtServiceMenu).Count();
        }

        List<CustomerPleasureZone> GetPleasureZonesByType(string requirementType)
        {
            var pleasureZones = new List<CustomerPleasureZone>();

            if (pleasuresZonesCashed.Count == 0)
            {
                pleasureZones = GetZones<CustomerPleasureZone>().ToList();
                pleasuresZonesCashed.AddRange(pleasureZones);
            }
            else
                pleasureZones.AddRange(pleasuresZonesCashed);

            pleasureZones.RemoveAll(x => !x.IsActivated() || x.requirementType != requirementType);

            return pleasureZones;
        }

        public int GetActivePleasureZonesCount(string requirementType)
        {
            return GetPleasureZonesByType(requirementType).Count();
        }

        public int GetMaxPleasureZonesLevel(string requirementType)
        {
            int maxLevel = 0;

            var pleasureZones = GetPleasureZonesByType(requirementType);

            foreach(var zone in pleasureZones)
            {
                int zoneUpgradeLevel = zone.GetComponentInParent<PropsBehaviour>().Stats.Level;

                if(maxLevel < zoneUpgradeLevel)
                    maxLevel = zoneUpgradeLevel;
            }

            return maxLevel;
        }

        public PropsBehaviour GetHighestLvlProps(string propsId)
        {
            if (ActiveProps.Count == 0)
            {
                return null;
            }

            PropsBehaviour highestLvlProps = ActiveProps.Values.OrderByDescending(props => props.Stats.Level)
                .FirstOrDefault(s => s.ID.Contains(propsId));

            return highestLvlProps;
        }
        /*public bool TryGetZone<T>(out ServiceZone zone) where T : ServiceZone
        {
             zone = _serviceZones.Values.FirstOrDefault(x =>
                x.GetType() == typeof(T) &&
                !x.Unavailable &&
                x.GetFirstInteractableObject() != null);
            if (zone != null)
            {
                return true;
            }
            return false;
        }*/
        public T[] TryGetZone<T>(string id, Transform npc) where T : ServiceZone
        {
            try
            {
                var zones = ServiceZones.Values.Where(x =>
                    x.GetType() == typeof(T) && x.InteractableObjects.Any(i => i.NPCId == id));
                if (zones.Any())
                {
                    foreach (var zone in zones)
                    {
                        zone.RegisterToZone(npc, id);
                    }
                }
                else
                {
                    Debug.LogError($"Can't get interactable object in {typeof(T)} for id: {id}");
                }
                
                return zones.Cast<T>().ToArray();
            }
            catch (Exception)
            {
                Debug.LogError("Can't get interactable object for id: " +id);
            }
            return null;
        }

        public T[] GetZones<T>(string id, Transform npc) where T : ServiceZone
        {
            try
            {
                var zones = ServiceZones.Values.Where(x => x.GetType() == typeof(T));

                if (zones.Any())
                {
                    foreach (var zone in zones)
                    {
                        zone.RegisterToZone(npc);
                    }
                }
                else
                {
                    Debug.LogError($"Can't get interactable object in {typeof(T)} for id: {id}");
                }

                return zones.Cast<T>().ToArray();
            }
            catch (Exception)
            {
                //Debug.LogError("Can't get interactable object for id: " + id);
            }
            return null;
        }

        public T[] GetZones<T>() where T : ServiceZone
        {
            try
            {
                var zones = new List<T>();

                foreach (var zone in ServiceZones.Values)
                    if (zone.GetType() == typeof(T))
                        zones.Add((T)zone);

                //ServiceZones.Values.Where(x => x.GetType() == typeof(T));

                return zones.ToArray();
            }
            catch (Exception)
            {
                //Debug.LogError("Can't get interactable object for id: " + id);
            }
            return null;
        }

        protected override void Deserialize(ServiceSave obj)
        {
            File = obj;
        }

        protected override ServiceSave Serialize()
        {
            return File;
        }

        protected override void LoadDefault()
        {
            File = new ServiceSave();
        }

        public float GetAvCycleTime()
        {
            if (File.Units.Count == 0)
                return 0;

            float sum = 0;

            foreach (var unit in File.Units)
            {
                var stats = Units.FirstOrDefault(x => x.ID == unit.ID).Stats;
                sum += stats[unit.Level - 1].TimeCycle;
            }

            return sum / File.Units.Count;
            //return ActiveNpc.Sum(x => x.Value.Stats.TimeCycle) / ActiveNpc.Count();
        }

        public int AmountAvailableWorkZones()
        {
            foreach (var serviceZone in ServiceZones)
                serviceZone.Value.Initialization();

            var gettingZones = ServiceZones.Values.Where(x => x is GettingZone);

            return gettingZones.Count(x => (x as GettingZone).CurrentAmount > 0 && x.IsActivated());
        }

        int AmountAvailableSpendingZones()
        {
            foreach (var serviceZone in ServiceZones)
                serviceZone.Value.Initialization();

            var spendingZones = ServiceZones.Values.Where(x => x is SpendingZone);

            return spendingZones.Count(x => x.gameObject.GetComponent<GettingZone>().CurrentAmount > 0 && x.IsActivated());
        }

        void UpdateStorageAmount()
        {
            foreach (var props in ActiveProps)
                props.Value.ReceiveResources(this);

            //Debug.LogError(ProgressCalculator.PlayerAbsenceSeconds);

            //serviceStorage.Set(GetServiceEff());
            //serviceStorage.SubscribeOnValueChanged(SaveStorageAmount);
        }

        float GetServiceEff() => GetUnitsEff() * ActiveNpc.Count;

        float GetUnitsEff()
        {
            var avgTime = GetAvCycleTime();

            if (avgTime == 0)
                return 0;

            return GetAvgResourceCapacity() / avgTime;
        }

        public int GetNPCCount() => File.Units.Count;

        public int GetMaxWorkingZonesCount()
        {
            return ActiveProps.Sum(x => x.Value.GetWorkingZonesCount());
        }

        void CalculateGettingStorageSize(string propsID)
        {
            if (isEmptyStorageOnStart)
            {
                serviceStorage.Set(0);
                return;
            }

            IsStorageActivated = false;
            var AbTime = ProgressCalculator.GetInstance().PlayerAbsenceSeconds;
            lastStorageSize = File.Props.FirstOrDefault(x => x.ID == propsID).CurrentAmount;

            var Ef1 = services.regainService.GetServiceEff();
            var Ef2 = services.gettingService.GetServiceEff();
            var Ef3 = services.spendingService.GetServiceEff();

            var S1 = (Ef1 - Ef2) >= 0 ? Ef2 : Ef1;

            ////S1 = Ef2 якщо(Ef1 - Ef2) >= 0, якщо ж(Ef-Ef2) < 0 , то S1 = Ef1

            var PtsSt = (S1 - Ef3) * AbTime + lastStorageSize;

            var newSize = Mathf.Clamp(PtsSt, 0, serviceStorage.Capacity);

            serviceStorage.Set(newSize);

            UpdateStorageAmount();
        }

        void CalculateSpendingStorageSize(string propsID)
        {
            if (isEmptyStorageOnStart)
            {
                serviceStorage.Set(0);
                return;
            }

            IsStorageActivated = false;
            var AbTime = ProgressCalculator.GetInstance().PlayerAbsenceSeconds;

            var Ef1 = services.regainService.GetServiceEff();
            var Ef2 = services.gettingService.GetServiceEff();
            var Ef3 = services.spendingService.GetServiceEff();

            var S1 = (Ef1 - Ef2) >= 0 ? Ef2 : Ef1;

            var StoreLastSawmill = File.Props.FirstOrDefault(x => x.ID == propsID).CurrentAmount;
            var PtsSt = 0f;

            var StoreLastLumber = services.gettingService.GetLastStorageSize();

            var TimeEst = Ef3 == 0 ? 0 : (StoreLastLumber / Ef3);

            var TimeDif = AbTime - TimeEst;

            if (S1 >= Ef3)
                PtsSt = (Ef3 * AbTime) + StoreLastSawmill;
            else
            {
                if (S1 != 0)
                {
                    if (AbTime <= TimeEst)
                        PtsSt = (Ef3 * AbTime) + StoreLastSawmill;
                    else
                        PtsSt = (TimeDif * S1) + StoreLastSawmill;
                }
                else
                    PtsSt = StoreLastSawmill;
            }

            OfflineProggressSpendServ = PtsSt;
            var spendingZones = ServiceZones.Values.Where(x => x is SpendingZone);
            (spendingZones.First() as SpendingZone).Add(PtsSt);
        }

        public void GetSpendEff()
        {
            
            //var AbTime = 60f;

            //var Ef1 = services.regainService.GetServiceEff();
            //var Ef2 = services.gettingService.GetServiceEff();
            //var Ef3 = services.spendingService.GetServiceEff();

            //var S1 = (Ef1 - Ef2) >= 0 ? Ef2 : Ef1;

            //var StoreLastSawmill = 0f;
            //var PtsSt = 0f;

            //var StoreLastLumber = services.gettingService.GetLastStorageSize();

            //var TimeEst = Ef3 == 0 ? 0 : (StoreLastLumber / Ef3);

            //var TimeDif = AbTime - TimeEst;

            //if (S1 >= Ef3)
            //    PtsSt = (Ef3 * AbTime) + StoreLastSawmill;
            //else
            //{
            //    if (S1 != 0)
            //    {
            //        if (AbTime <= TimeEst)
            //            PtsSt = (Ef3 * AbTime) + StoreLastSawmill;
            //        else
            //            PtsSt = (TimeDif * S1) + StoreLastSawmill;
            //    }
            //    else
            //        PtsSt = 0;
            //}
            //SpendServiceMoney = PtsSt;
            //Debug.Log("Eff " + PtsSt + name);
        }

        float GetLastStorageSize() => lastStorageSize;

        public float GetAvgResourceCapacity()
        {
            if (serviceType == ServiceSpendingType.Regain)
                return services.gettingService.GetAvgResourceCapacity();

            float sum = 0;
            int count = 0;

            foreach (var props in ActiveProps)
                if (props.Value.GetWorkingZonesCount() != 0)
                {
                    ++count;
                    sum += props.Value.Stats.ResourcesCapacity;
                }

            if (count == 0)
                return 0;

            return sum / count;
        }

        public IObjectID GetCheapestObject(string checkCurrency, out float price, out int index, UI.Village.ServiceMenu serviceMenu)
        {
            IObjectID cheapestObject = null;

            price = float.MaxValue;
            index = 0;

            if (checkCurrency == MoneyType.Gold)
            {
                //buildings
                if(!serviceMenu || serviceMenu.IsAvailableObject(Buildings, this))
                {
                    var buildingsLevel = File.BuildingsLevel;
                    var buildingsUpgradePrice = Buildings.Stats.Count != buildingsLevel
                                        ? Buildings.Stats[buildingsLevel].UpgradePrice : -1;

                    if (buildingsUpgradePrice > 0 && buildingsUpgradePrice < price)
                    {
                        price = buildingsUpgradePrice;
                        index = 0;
                        cheapestObject = Buildings;
                    }
                }

                //units
                for (int i = 0; i < Units.Count; ++i)
                {
                    var npc = Units[i];

                    if (serviceMenu && !serviceMenu.IsAvailableObject(npc, this))
                        continue;
                    
                    float goldPrice = 0;

                    if (ActiveNpc.TryGetValue(npc.ID, out var _npc))
                        goldPrice = Units[i].Stats[_npc.Stats.Level - 1].UpgradePrice;
                    else
                        goldPrice = npc.Stats[0].UpgradePrice;

                    if (goldPrice > 0 && goldPrice < price)
                    {
                        price = goldPrice;
                        index = i + 1;
                        cheapestObject = Units[i];
                    }
                }
            }

            //props
            for (int i = 0; i < Props.Count; ++i)
            {
                var props = Props[i];

                if (serviceMenu && !serviceMenu.IsAvailableObject(props, this))
                    continue;
                //Debug.Log(props.ID);
                float goldPrice = 0;
                float blueGemsPrice = 0;
                float redGemsPrice = 0;
                float greenGemsPrice = 0;

                if (ActiveProps.TryGetValue(props.ID, out var _props))
                {
                    goldPrice = props.Stats[_props.Stats.Level - 1].UpgradePriceCoins;
                    blueGemsPrice = props.Stats[_props.Stats.Level - 1].UpgradePriceBlueGems;
                    redGemsPrice = props.Stats[_props.Stats.Level - 1].UpgradePriceRedGems;
                    greenGemsPrice = props.Stats[_props.Stats.Level - 1].UpgradePriceGreenGems;
                    Debug.Log(goldPrice);
                }
                else
                {
                    goldPrice = props.Stats[0].UpgradePriceCoins;
                    blueGemsPrice = props.Stats[0].UpgradePriceBlueGems;
                    redGemsPrice = props.Stats[0].UpgradePriceRedGems;
                    greenGemsPrice = props.Stats[0].UpgradePriceGreenGems;
                }

                float tempPrice = goldPrice;
                switch (checkCurrency)
                {
                    case MoneyType.BlueGems:
                        tempPrice = blueGemsPrice;
                        break;
                    case MoneyType.RedGems:
                        tempPrice = redGemsPrice;
                        break;
                    case MoneyType.GreenGems:
                        tempPrice = greenGemsPrice;
                        break;
                }

                if (tempPrice > 0 && tempPrice < price)
                {
                    price = tempPrice;
                    index = i + 1;
                    cheapestObject = Props[i];
                }
            }

            return cheapestObject;
        }
    
        //public bool IsFreeSpaceInStorage()
        //{
        //    var storage = ActiveProps[Props[0].ID].GetStorage();//.FirstOrDefault(x => x.ID == Props[0].ID);
        //    return storage.CurrentAmount >= 0 && storage.CurrentAmount < storage.Capacity;
        //}

        public Storage GetStorage() => serviceStorage;

        public bool CanCheckinFamily(CustomersPseudofamily family)
        {
            return GetSleepPlaceCount() - serviceStorage.CurrentAmount >= family.FamilySize;
        }

        public int GetSleepPlaceCount()
        {
            var sleepZones = GetZones<CustomerPleasureZone>().Where(x => x.requirementType == sleepZoneID);

            int sleepPlaceCount = 0;

            foreach (var item in sleepZones)
                sleepPlaceCount += (int)item.GetComponentInParent<PropsBehaviour>().Stats.ResourcesCapacity;

            return sleepPlaceCount;
        }

        public int GetStorageSize() => (int)serviceStorage.CurrentAmount;

        public int GetSleepZonesCount() => GetZones<CustomerPleasureZone>().Where(x => x.requirementType == sleepZoneID).Count();

        [ContextMenu("LogZones")]
        public void LogZones()
        {
            Debug.LogError(string.Join(' ', ServiceZones.Values.Where(x => x is CustomerPleasureZone).ToList()));
        }

        public void LogZones(int i)
        {
            Debug.LogError(string.Join(' ', ServiceZones.Values.Where(x => x is GettingZone).ToList()));
        }

        public int GetZoneIndex(ServiceZone zone)
        {
            int i = 0;

            foreach (var serviceZone in ServiceZones.Values)
            {
                if (serviceZone == zone)
                    return i;

                ++i;
            }

            return -1;
        }

        public bool IsLastZone(int index) => ServiceZones.Count - 1 == index;

        public int GetFreeSpendingZonesCount(SpendingZone[] zones, string queueTag)
        {
            int count = zones.Count(x => !x.Unavailable);
            return count;
        }

        public int GetMaxPropsLevel()
        {
            int maxLevel = 0;

            foreach(var props in ActiveProps)
                if(props.Value.showAtServiceMenu && props.Value.Stats.Level > maxLevel)
                    maxLevel = props.Value.Stats.Level;

            return maxLevel;
        }
    }
}