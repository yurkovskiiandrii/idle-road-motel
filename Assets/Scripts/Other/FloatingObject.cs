﻿using nickeltin.Extensions;
using nickeltin.Tweening;
using UnityEngine;

namespace Other
{
    public class FloatingObject : MonoBehaviour
    {
        [Header("Top")]
        [SerializeField] private Transform m_topPoint;
        [SerializeField] private float m_upMoveTime;
        [SerializeField] private float m_topDelay;
        [SerializeField] private AnimationCurve m_upMove;
        
        [Header("Bottom")]
        [SerializeField] private Transform m_bottomPoint;
        [SerializeField] private float m_downMoveTime;
        [SerializeField] private float m_bottomDelay;
        [SerializeField] private AnimationCurve m_downMove;
        
        private void Awake()
        {
            void UpMove()
            {
                LeanTween.moveY(gameObject, m_topPoint.position.y, m_upMoveTime).setEase(m_upMove).setDelay(m_topDelay)
                    .setOnComplete(DownMove);
            }

            void DownMove()
            {
                LeanTween.moveY(gameObject, m_bottomPoint.position.y, m_downMoveTime).setEase(m_downMove)
                    .setDelay(m_bottomDelay).setOnComplete(UpMove);
            }

            transform.position = transform.position.With(y: m_bottomPoint.position.y);
            UpMove();
        }
    }
}