﻿public interface IObjectID
{
    public abstract string ID { get;}
}