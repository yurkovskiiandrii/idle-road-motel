using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnvironment.Village.Structures;

namespace NPCs
{
    public class ZonesInit : MonoBehaviour
    {
        [SerializeField] bool isReverse;
        [SerializeField] ServiceManager service;

        void Awake()
        {
            int childCount = transform.childCount;
            int startCount = isReverse ? (childCount - 1)  : 0;

            while(true)
            {
                var zone = transform.GetChild(startCount).gameObject;
                var gettingZone = zone.GetComponentInChildren<GettingZone>();
                var spendingZone = zone.GetComponentInChildren<SpendingZone>();

                service.AddZoneToService(gettingZone.ID, gettingZone);
                //service.AddZoneToService(spendingZone.ID, spendingZone);

                startCount += isReverse ? -1 : 1;

                if (isReverse && startCount < 0 || !isReverse && startCount == childCount)
                    break;
            }
        }
    }
}