using System;
using System.Collections.Generic;
using UnityEngine;

namespace Other
{
    [CreateAssetMenu(menuName = "Create IconsScheme/QuestIcons", fileName = "QuestIconsIconsScheme")]
    public class QuestIcons : ScriptableObject
    {
        public List<QuestSprites> RewardSprites;
        public List<QuestSprites> ConditionSprites;

        [Serializable]
        public struct QuestSprites
        {
            public string ID;
            public Sprite Sprite;
            public Sprite frameRewardIcon;
        }
    }
}
