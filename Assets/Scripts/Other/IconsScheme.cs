using System;
using System.Collections.Generic;
using NPCs;
using UnityEngine;
using UnityEngine.Serialization;


[CreateAssetMenu(menuName = "Create IconsScheme/ServiceMenu", fileName = "ServiceMenuIconsScheme")]
public class IconsScheme : ScriptableObject
{
    public ServiceManager Service;
    public Sprite BuildingsIcon;
    public Sprite BuildingsBigIcon;
    public Sprite BuildingsGreyIcon;
    public Sprite BuildingsNotBuyedIcon;
    public List<Sprite> UnitsIcons;
    public List<Sprite> UnitsBigIcons;
    public List<Sprite> UnitsGreyIcons;
    public List<Sprite> PropsIcons;
    public List<Sprite> PropsBigIcons;
    public List<Sprite> PropsGreyIcons;
    public List<Sprite> BuildingsStatsIcons;
    public List<Sprite> UnitsStatsIcons;
    public List<Sprite> PropsStatsIcons;
}