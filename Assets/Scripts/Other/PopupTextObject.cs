﻿using System;
using DG.Tweening;
using nickeltin.Extensions;
using nickeltin.ObjectPooling;
using Source;
using TMPro;
using UI;
using UnityEngine;

namespace Other
{
    public class PopupTextObject : PoolObject<PopupTextObject>
    {
        public TMP_Text Text;
        [SerializeField] private Color _reguralColor;
        [SerializeField] private Color _criticalColor;
        [SerializeField] private int _digitsAfterPointInNumbers = 1;
        [SerializeField] private Vector2 _appearanceOffset;
        [SerializeField] private Vector2 _disappearanceOffset;
        [SerializeField] private float _appeareTime;
        [SerializeField] private float _stayTime;
        [SerializeField] private float _disappeareTime;
        

        private float m_defaultTextSize;

        private void Start()
        {
            m_defaultTextSize = Text.fontSize;
        }

        public void Appear(Vector2 screenPoint, float text, Action callback, bool isCritical)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(PopUpCanvas.Canvas.GetComponent<RectTransform>(),
                screenPoint, GameCamera.MainCamera, out var localPoint);

            Text.rectTransform.localScale = Vector3.one;
            Text.text = text.ToString("F" + _digitsAfterPointInNumbers);
            Text.color =  isCritical ? _criticalColor : _reguralColor;
            Text.rectTransform.localPosition = localPoint;
            
            var lifetime = _appeareTime + _stayTime + _disappeareTime;
            var rect = Text.rectTransform;

            DOVirtual.DelayedCall(lifetime, callback.Invoke);
            rect.DOLocalMove(  rect.localPosition + _appearanceOffset.ToVector3(0), _appeareTime).onComplete += () =>
            {
                rect.DOScale(Vector3.zero, _disappeareTime).SetDelay(_stayTime);
            };
        }
        
    }
}