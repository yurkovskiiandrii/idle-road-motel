using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Create IconsScheme/CustomerRequirements")]
public class CustomerRequirementsUIScheme : ScriptableObject
{
    public List<CustomerRequirementsIcon> requirementsIcons;

    public Sprite GetSprite(string pleasureName) => requirementsIcons.Find(x => x.requirementType == pleasureName).Icon;
}