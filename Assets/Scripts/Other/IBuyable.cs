﻿namespace Other
{
    public interface IBuyable
    {
        float Price { get; }
    }
}