
using UnityEngine;

[CreateAssetMenu(menuName = "Create IconsScheme/PopUpWorldRequirement")]
public class CustomerRequirementsIcon : PopUpWorldIconsScheme
{
    public string requirementType;
}