using UnityEngine;

[CreateAssetMenu(menuName = "Create IconsScheme/PopUpWorld", fileName = "PopUpWorldIconsScheme")]
public class PopUpWorldIconsScheme : ScriptableObject
{
    public Sprite Icon;
    public string Prefix;
}