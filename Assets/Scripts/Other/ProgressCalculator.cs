﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CoreGame.UI;
using Destructables;
using DG.Tweening;
using GameData;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using nickeltin.Singletons;
using NPCs;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu(menuName = "Managers/ProgressCalculator", fileName = "ProgressCalculator")]
public class ProgressCalculator : SOSingleton<ProgressCalculator>
{
    [System.Serializable]
    public struct CalculationResult
    {
        public float TimeWasLeft;
        public float GeneralEfficiency;
        public float CurrencyAcquired;
    }
    [SerializeField] private GameProgressSave _save;
    [SerializeField] private float _progressModificator =1;
    [SerializeField] private float _incomeModificator =1;
    [SerializeField] private Currency _currency;
    [SerializeField] private ServiceManager _miningService;
    [SerializeField] private int _digitsAfterPointInNumbers = 1;
    [SerializeField] private float mult = 0.05f;
    [SerializeField] private NumberObject _goldPerMin;

    [SerializeField] private CalculationResult _calculationResult;
    [SerializeField, Range(1, 10)] private float _maxDelay;

    private float _delay = 10;
    private float _calculationResultBuffer;
    private float _minersEfficiency;
    private float _unitsEfficiency;
    [HideInInspector] public ServiceManager[] ServiceManagers;

    float playerAbsenceSeconds = 0;
    public bool SetMiningValue = true;

    public float PlayerAbsenceSeconds
    {
        get
        {
           // playerAbsenceSeconds = (float)(DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime)).TotalSeconds;
            return playerAbsenceSeconds;
        }
        set
        {
            playerAbsenceSeconds = value;
        }
    }
    public void Awake()
    {
       // CalculateOfflineProgress();

    }
    public void StartCanvaDisable()
    {
        ProgressUI.CanvasObject.SetActive(true);
        CalculateEfficiency();
        CalculateMiningEfficiency();
        if (_unitsEfficiency <= 0 && _minersEfficiency <= 0)
        {
             ProgressUI.CanvasObject.SetActive(false);

        }
    }
    public void CalculateOfflineProgress()
    {
        playerAbsenceSeconds = (float)(DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime)).TotalSeconds;
        //Debug.Log("CLSOE NOTIF");
       // ProgressUI.CanvasObject.SetActive(false);

        CalculateMiningEfficiency();
       // DOVirtual.DelayedCall(_delay, CycleIncome);
        //CalculateMiningEfficiency();
        // _calculationResult = CalculateProgress(_minersEfficiency, PlayerAbsenceSeconds);
        //
        //_currency.Value += _minersEfficiency;
        //Debug.Log(_minersEfficiency + "////////" + PlayerAbsenceSeconds);
       // ProgressUI.notificationMore.SetActive(false);
        CalculateEfficiency();
        //CalculateMiningEfficiency();
        
        if (_save.SuccessfulyLoaded && _unitsEfficiency > 0 || _minersEfficiency > 0 )
        {
                        //Debug.Log("CLSOE NOTIF");

            ProgressUI.CanvasObject.SetActive(true);

            PlayerAbsenceSeconds = (int)(DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime)).TotalSeconds;
            ProgressUI.TimeDev = (string)(DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime)).TotalSeconds.ToString();
            ProgressUI.CurrencyDev = (_minersEfficiency + "////////" + PlayerAbsenceSeconds).ToString();
            float m = (float)Math.Round((PlayerAbsenceSeconds / 60));
            if (m > 120)
            {
                PlayerAbsenceSeconds = 120 * 60;
                ProgressUI.notificationMore.SetActive(true);

            }
            _calculationResult = CalculateProgress(_unitsEfficiency, PlayerAbsenceSeconds);
            //PlayerAbsenceSeconds);
           
            
           //if (s > 30)
           //{
           //    m++;
           //}
            _currency.Value += _calculationResult.CurrencyAcquired;
            //Debug.Log(_minersEfficiency + "////////" + playerAbsenceSeconds);
            if (m != 0 && m <= 120)
            {
                ProgressUI.TimeWasLeftText = $"{m}" + " min.";
                ProgressUI._under2Hours.Invoke();

            }
            else if (m != 0 && m > 120)
            {
                ProgressUI.TimeWasLeftText = $"{120}" + " min.";
                ProgressUI._more2Hours.Invoke();
               // ProgressUI._under2Hours.Invoke();
            }
            else
            {
                ProgressUI.TimeWasLeftText = "1" + " min.";
                ProgressUI._under2Hours.Invoke();

            }

            ProgressUI.GeneralEfficiencyText = _calculationResult.GeneralEfficiency.ToString();
            ProgressUI.CurrencyAcquiredText = LargeNumber.FormatEveryThirdPower((float)Math.Round(_calculationResult.CurrencyAcquired));
            //Debug.Log(ProgressUI.CanvasObject.name);
            _calculationResultBuffer = 0;
           // DOVirtual.DelayedCall(_delay, CycleIncome);
        }
        else if(_minersEfficiency > 0)
        {
            //Debug.Log("CLSOE NOTIF");

            ProgressUI.CanvasObject.SetActive(true);

            PlayerAbsenceSeconds = (int)(DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime)).TotalSeconds;
            ProgressUI.TimeDev = (string)(DateTime.UtcNow - DateTime.Parse(_save.File.LastGameDateTime)).TotalSeconds.ToString();
            ProgressUI.CurrencyDev = (_minersEfficiency + "////////" + PlayerAbsenceSeconds).ToString();
            _calculationResult = CalculateProgress(_unitsEfficiency, playerAbsenceSeconds);
            float m = (float)Math.Round((PlayerAbsenceSeconds / 60));
            if (m > 120)
            {
                PlayerAbsenceSeconds = 120 * 60;
                ProgressUI.notificationMore.SetActive(true);

            }
            //if (s > 30)
            //{
            //    m++;
            //}
            _currency.Value += _calculationResult.CurrencyAcquired;
            //Debug.Log(_minersEfficiency + "////////" + PlayerAbsenceSeconds);
            if (m != 0 && m <= 120)
            {
                ProgressUI.TimeWasLeftText = $"{m}" + " min.";
                ProgressUI._under2Hours.Invoke();

            }
            else if (m != 0 && m > 120)
            {
                ProgressUI.TimeWasLeftText = $"{120}" + " min.";
                ProgressUI._more2Hours.Invoke();
               // ProgressUI._under2Hours.Invoke();
            }
            else
            {
                ProgressUI.TimeWasLeftText = "1" + " min.";
                ProgressUI._under2Hours.Invoke();

            }

            ProgressUI.GeneralEfficiencyText = _calculationResult.GeneralEfficiency.ToString();
            ProgressUI.CurrencyAcquiredText = LargeNumber.FormatEveryThirdPower((float)Math.Round(_calculationResult.CurrencyAcquired));
           
            //Debug.Log(ProgressUI.CanvasObject.name);
            _calculationResultBuffer = 0;
           // DOVirtual.DelayedCall(_delay, CycleIncome);
            // _calculationResult = CalculateProgress(_minersEfficiency, PlayerAbsenceSeconds);
            //
            _currency.Value += _minersEfficiency;
            //Debug.Log(_minersEfficiency + "////////" + PlayerAbsenceSeconds);
        }
         if(_unitsEfficiency == 0  && _minersEfficiency == 0)
        {
            //Debug.Log("CLSOE NOTIF");
          //  ProgressUI.CanvasObject.SetActive(false);

            CalculateMiningEfficiency();
          //  DOVirtual.DelayedCall(_delay, CycleIncome);
            //CalculateMiningEfficiency();
           // _calculationResult = CalculateProgress(_minersEfficiency, PlayerAbsenceSeconds);
           //
            _currency.Value += _minersEfficiency;
           // Debug.Log(_minersEfficiency + "////////" + PlayerAbsenceSeconds);
        }
        _miningService.OnUnitLevelChange += CalculateMiningEfficiency;


        _unitsEfficiency = 0;

        foreach (var service in ServiceManagers)
        {
            if (service.serviceType == ServiceSpendingType.Spending)
            {
                service.GetSpendEff();
                _unitsEfficiency += service.SpendServiceMoney;
                //_unitsEfficiency += service.ActiveNpc.Values.Sum(npc => npc.Stats.WorkEfficiency);
                // service.services.spendingService.Get
            }
        }

        _goldPerMin.Value = _unitsEfficiency + CalculateMiningPerSecond(60f);

        _goldPerMin.onValueChanged += (float x) => Debug.Log("new value = " + x);
    }
    private CalculationResult CalculateProgress(float efficiency, float time)
    {
        _unitsEfficiency = 0;

        CalculateMiningEfficiency();

        // Debug.Log((().ToString() + "////////////////////////");
        foreach (var service in ServiceManagers)
        {
            if (service.serviceType == ServiceSpendingType.Spending)
            {
                service.GetSpendEff();
                _unitsEfficiency += service.OfflineProggressSpendServ;
                //_unitsEfficiency += service.ActiveNpc.Values.Sum(npc => npc.Stats.WorkEfficiency);
                // service.services.spendingService.Get
            }
        }
        // _unitsEfficiency *= t;
        return new CalculationResult
        {
            TimeWasLeft = time,
            GeneralEfficiency = (efficiency / 60f * time),
            CurrencyAcquired = ((float)(Math.Round(_minersEfficiency) + (efficiency / 60f * time)))//_unitsEfficiency +
        };
      
    }

    public void SetMiningBool(bool mine)
    {
        SetMiningValue = mine;
    }

    private void CalculateEfficiency()
    {
        _unitsEfficiency = 0;
        foreach (var service in ServiceManagers)
        {
            if (service.serviceType == ServiceSpendingType.Spending)
            {
                service.GetSpendEff();
                _unitsEfficiency += service.SpendServiceMoney;
                //_unitsEfficiency += service.ActiveNpc.Values.Sum(npc => npc.Stats.WorkEfficiency);
               // service.services.spendingService.Get
            }
        }

        _goldPerMin.Value = _unitsEfficiency + CalculateMiningPerSecond(_delay);
    }

    private void CalculateMiningEfficiency()
    {
        _minersEfficiency = (_miningService.ActiveNpc.Values.Sum(npc => npc.Stats.WorkEfficiency) * PlayerAbsenceSeconds) * mult;
        //Debug.Log(_minersEfficiency);
    }

    private float CalculateMiningPerSecond(float del)
    {
        return 0 /*((_miningService.ActiveNpc.Values.Sum(npc => npc.Stats.WorkEfficiency) * del) * mult)*/;
    }


    public void CycleIncome()
    {

        CalculateEfficiency();
        CalculateMiningEfficiency();
        if ( _unitsEfficiency <= 0 && _minersEfficiency <= 0 )
        {
          //  ProgressUI.CanvasObject.SetActive(false);

        }
        _delay = 2f;
        _goldPerMin.Value = _unitsEfficiency + CalculateMiningPerSecond(60f);

        Debug.Log("units: " + _unitsEfficiency);
        Debug.Log("mining: " + CalculateMiningPerSecond(60f));

        Debug.Break();

        DOVirtual.DelayedCall(_delay, CycleIncome);
      // if (SetMiningValue)
      // {
      //
      //
      //     _currency.Value += CalculateMiningPerSecond(_delay);
      //     Debug.Log(CalculateMiningPerSecond(_delay));
      // }

        if (Dome.IsOnScene)
            return;


        if (SetMiningValue)
        {

            // _currency.Value += CalculateMiningPerSecond(_delay);
            float ss = (_goldPerMin.Value / 60) * _delay;
            if (ss > 0 && ss > 0.5f)
            {
                _currency.Value += (_goldPerMin.Value / 60) * _delay;
                //Debug.Log((_goldPerMin.Value / 60) * _delay);
            }
            else if (ss > 0 && ss < 0.5f)
            {
                // Debug.Log(ss.ToString());
                _currency.Value += (float)Math.Round(ss * 2);
                //Debug.Log(Math.Round(ss * 2).ToString());
            }
        }
        // _calculationResult = CalculateProgress(_minersEfficiency, _delay);
        // if (_calculationResult.CurrencyAcquired + _calculationResultBuffer < 1)
        //  {
        //    _calculationResultBuffer += _calculationResult.CurrencyAcquired;
        //    return;
        // }











        //  _currency.Value += _calculationResult.CurrencyAcquired + _calculationResultBuffer;

        _calculationResultBuffer = 0;
    }
  //  [System.Serializable]
    public void UpdatePerMin()
    {
        CalculateEfficiency();
        //Debug.LogError(_unitsEfficiency);

        var miningValue = CalculateMiningPerSecond(_delay);

        //Debug.Log("units: " + _unitsEfficiency);
        //Debug.Log("mining: " + miningValue);

        _goldPerMin.Value = _unitsEfficiency + miningValue;

        //Debug.Break();
    }
}
