﻿using System;
using UnityEngine;

namespace Other
{
    [RequireComponent(typeof(Collider))]
    public class Interactable : MonoBehaviour
    {
        protected Collider m_collider;
        public event Action<Vector3> onInteraction;
        
        protected virtual void Awake()
        {
            m_collider = GetComponent<Collider>();
            m_collider.isTrigger = true;
        }
        
        public virtual void Interact(Vector3 interactionPosition) => onInteraction?.Invoke(interactionPosition);
    }
}