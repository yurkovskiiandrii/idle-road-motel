using System;
using System.Linq;
using GameData;
using nickeltin.Singletons;
using NPCs;
using UnityEngine;

namespace Other
{
    public class UnitSpawner : MonoSingleton<UnitSpawner>
    {
        [SerializeField] private NPC[] PlumbersServiceUnits;
        [SerializeField] private NPC[] CleanersServiceUnits;
        [SerializeField] private NPC[] Room1Units;
        [SerializeField] private NPC[] Room2Units;
        [SerializeField] private NPC[] Room3Units;
        [SerializeField] private NPC[] Room4Units;
        [SerializeField] private NPC[] PoolServiceUnits;
        [SerializeField] private NPC[] BarServiceUnits;
        
        public static NPC SpawnUnit(ServiceManager service, Unit unit)
        {
            return service.Service switch
            {
                ServiceType.PlumbersService => instance.Spawn(instance.PlumbersServiceUnits, unit, service),
                ServiceType.CleanersService => Dome.SpawnMiner(instance.Spawn(instance.CleanersServiceUnits, unit, service)),
                ServiceType.Room1 => instance.Spawn(instance.Room1Units, unit, service),
                ServiceType.Room2 => instance.Spawn(instance.Room2Units, unit, service),
                ServiceType.Room3 => instance.Spawn(instance.Room3Units, unit, service),
                ServiceType.Room4 => instance.Spawn(instance.Room4Units, unit, service),
                ServiceType.PoolService => instance.Spawn(instance.PoolServiceUnits, unit, service),
                ServiceType.BarService => instance.Spawn(instance.BarServiceUnits, unit, service),
                _ => null
            };
        }
        private NPC Spawn(NPC[] prefabs, Unit unit, ServiceManager service)
        {
            var objToInstantiate = prefabs.FirstOrDefault(x => x.ID == unit.ID);
            if (objToInstantiate == null)
            {
                Debug.LogError($"Can't find prefab with id {unit.ID} to instantiate.");
                return null;
            }
            if (service.ActiveNpc.ContainsKey(objToInstantiate.ID))
            {
                Debug.LogError($" {objToInstantiate.ID} is already instantiated.");
                return null;
            }
            var obj = Instantiate(objToInstantiate, transform.position, Quaternion.identity);
            var npc = obj.GetComponent<NPC>();
            npc.ServiceManager = service; 
            npc.ID = unit.ID;
            var save = service.File.Units.FirstOrDefault(x => x.ID == npc.ID);
            npc.Stats = unit.Stats[save?.Level-1 ?? 0  ];
            service.AddNPCToService(unit.ID,npc);
            npc.Initialize();
            return npc;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 2);
        }
    }
}