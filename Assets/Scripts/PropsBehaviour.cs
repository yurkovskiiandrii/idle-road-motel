using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using UnityEngine.Events;
using nickeltin.GameData.Saving;
using NPCs;
using System.Linq;
using DG.Tweening;
using System;

namespace GameEnvironment.Village.Structures
{
    [RequireComponent(typeof(CameraOrtoSize))]
    public class PropsBehaviour : ServiceZone
    {
        [Header("Stats")] [SerializeField] private PropsStats _stats;

        [SerializeField] CameraOrtoSize _cameraTarget;

        [SerializeField] Storage _storage;
        [SerializeField] ResourceHolder[] workingZones;

        [SerializeField] UnityEvent onActivated;
        [SerializeField] public UnityEvent onUpgraded;

        [SerializeField] private MaterialChanger _materialChanger;
        
        [SerializeField] private int _resourceLevel;

        private GameData.Resource _resource;
        private ServiceManager.ServiceSave.Save _save;

        public bool showAtServiceMenu = true;

        UnityEvent<int> onPropsLevelChanged = new UnityEvent<int>();

        UnityEvent onAvailable = new UnityEvent();

        public PropsStats Stats
        {
            get => _stats;
            set
            {
                _stats = value;

                onPropsLevelChanged.Invoke(_stats.Level);
                onUpgraded.Invoke();

                foreach (var workZone in workingZones)
                    workZone.SetNewStats(value.ResourcesCapacity);

                if(_storage)
                    _storage.Capacity = _stats.ResourcesCapacity;
            }
        }

        public override bool Unavailable => false;

        private void Start()
        {
            //Activate();
            _materialChanger = this.GetComponent<MaterialChanger>();
            //Invoke(nameof(Initialization), 0.1f);
        }

        public void Activate(PropsStats stats)
        {
            onActivated.Invoke();

            isActivated = true;
            Upgrade(stats, false);

            foreach (var workZone in workingZones)
                workZone.Activate();
        }

        public void Activate(ServiceManager service)
        {
            onActivated.Invoke();

            isActivated = true;

            foreach (var workZone in workingZones)
            {
                DOVirtual.DelayedCall(Time.deltaTime, () =>
                {
                    workZone.Activate(true);

                    if(workZone.GetComponent<GettingZone>())
                        service.InvokeGettingAvailable(workZone.GetComponent<GettingZone>());

                    onAvailable.Invoke();
                });

            }
        }

        public void SubscribeOnAvailable(UnityAction action) => onAvailable.AddListener(action);

        public void UnSubscribeOnAvailable(UnityAction action) => onAvailable.RemoveListener(action);

        public void OnAvailable() => onAvailable.Invoke();

        public void ReceiveResources(ServiceManager service)
        {
            foreach (var workZone in workingZones)
            {
                workZone.DestroyEventInvoke();
                //service.InvokeGettingAvailable(workZone);
            }
        }

        public void Upgrade(PropsStats stats, bool needUpgradeEvent = true)
        {
            if(needUpgradeEvent)
                onUpgraded.Invoke();

            foreach (var workZone in workingZones)
                workZone.SetNewStats(stats.ResourcesCapacity);
        }

        public Storage GetStorage() => _storage;

        public int GetWorkingZonesCount() => workingZones.Length;

        public override void Initialization()
        {
            //Debug.Log("Init " + ID);

            if (_service && isActivated && !_service.IsStorageActivated)
            {
                _service.AddPropsToService(this, _storage);
            }

            _service.AddPropsToContainer(this);

            SaveSystem.GetSavedItem(out ResourcesData resourcesData);

            if (!resourcesData.GetResourceData(_service.Service, out var resource))
                return;

            _resource = new GameData.Resource();
            _resource = resource;

            if (_service)
            {
                _save = _service.File.Resource.FirstOrDefault(x => x.ID == _id);
                if (_save == null)
                {
                    _service.File.Resource.Add(new ServiceManager.ServiceSave.Save(_id, 1));
                    _resourceLevel = 1;
                }
                else
                {
                    _resourceLevel = _save.Level;
                }
            }
        }

        public int GetLevel() => _stats.Level;

        public CameraOrtoSize GetCameraTarget() => _cameraTarget ? _cameraTarget : GetComponent<CameraOrtoSize>();

        public void SubscribeOnPropsLeveling(UnityAction<int> action) => onPropsLevelChanged.AddListener(action);
    }
}
