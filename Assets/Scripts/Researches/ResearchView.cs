using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Researches
{
    public class ResearchView : MonoBehaviour
    {
        [SerializeField] private Image researchIcon;
        [SerializeField] private Button researchButton;
        //[SerializeField] private Button activeButton;
        //[SerializeField] private GameObject inactiveButton;
        [SerializeField] private GameObject border;

        [Header("Backgrounds"), Space(5)]
        [SerializeField] IconUIContainer available;
        [SerializeField] IconUIContainer blocked;
        [SerializeField] IconUIContainer inProgress;
        [SerializeField] IconUIContainer maxLevel;

        private IResearchService _researchService;
        private ResearchCenter.ResearchTree _researchTree;
        private ResearchCenter.ResearchData _researchData;
        private bool _isInitialized;
        
        private Button buttonClicks;

        bool isMaxLevel = false;
        public int researchIndex;

        public void Init(IResearchService researchService, ResearchCenter.ResearchTree researchTree , Button buttonClick, int researchIndex)
        {
            DisposeSubscriptions();
            _researchTree = researchTree;
            _researchService = researchService;
            _researchData = researchTree.ResearchDatas.FirstOrDefault(researchData => !researchData.IsUnlocked);
            buttonClicks = buttonClick;

            isMaxLevel = _researchData == null;
            this.researchIndex = researchIndex;
            //if (_researchData == null)
            //    return;

            researchIcon.sprite = isMaxLevel ? researchService.GetResearchData(researchIndex).Icon : _researchData.Icon;
            // activeButton.onClick.AddListener(Research);

            available.GetIconButton().onClick.AddListener(Select);
            blocked.GetIconButton().onClick.AddListener(Select);
            inProgress.GetIconButton().onClick.AddListener(Select);
            maxLevel.GetIconButton().onClick.AddListener(Select);

            _isInitialized = true;
            SetAvailableState();
            _researchService.OnResearchCompleted += CheckCompletion;
            _researchService.backgroundsUpdate += UpdateBackground;
            _researchService.OnTimeUpdated += UpdateBackground;

            UpdateBackground();
        }

        public void SetBorder(bool state)
        {
            border.SetActive(state);
        }

        private void CheckCompletion(int index)
        {
            if (isMaxLevel || index != _researchData.Id)
                return;
            
            DisposeSubscriptions();
            _researchData = _researchTree.ResearchDatas.FirstOrDefault(data => !data.IsUnlocked);
            if (_researchData != null)
            {
                researchIcon.sprite = _researchData.Icon;
                foreach (var price in _researchData.Prices)
                {
                    price.Currency.onValueChanged += SetAvailableState;
                }
                SetAvailableState();
                
                return;
            }

            isMaxLevel = true;

            //ShowMaxLevelBackground();

            //Destroy(gameObject);
        }


        private void DisposeSubscriptions()
        {
            if (_researchTree == null)
            {
                return;
            }
            foreach (var price in _researchData.Prices)
            {
                price.Currency.onValueChanged -= SetAvailableState;
            }
        }

        private void SetAvailableState(float amount = 0)
        {
            //if (!gameObject.activeInHierarchy
            //    || !_isInitialized)
            //{
            //    return;
            //}
            
            //var isAvailable = _researchService.IsResearchAvailable(_researchData.Id);
            //activeButton.gameObject.SetActive(isAvailable);
            //inactiveButton.SetActive(!isAvailable);
        }

        public void Select()
        {
            // buttonClick.onClick.AddListener(Research);
            buttonClicks.onClick.RemoveAllListeners();
            buttonClicks.onClick.AddListener(isMaxLevel ? () => { } : Research);
            _researchService.SelectResearch(isMaxLevel ? researchIndex : _researchData.Id, isMaxLevel);

            SetBorder(true);
        }

        public void UpdateBackground()
        {
            if(isMaxLevel)
            {
                ShowMaxLevelBackground();
                return;
            }

            if(_researchService.IsStartedResearch())
            {
                if(_researchService.GetCurrentResearchID() == _researchData.Id)
                {
                    ShowInProgressBackground();
                    return;
                }

                ShowBlockedBackground();
                return;
            }

            if (_researchService.IsEnoughtMoneyToResearch(_researchData.Id))
            {
                ShowAvailableBackground();
                return;
            }

            ShowBlockedBackground();
        }

        private void Research()
        {
            //foreach (var price in _researchesMap[_researchData.Id].Prices)
            //{
            //    if (price.Currency.Value < price.Amount)
            //        return;
            //}

            _researchService.StartResearch(_researchData);
        }

        private void OnEnable()
        {
            UpdateBackground();
            SetAvailableState();
        }

        void ShowAvailableBackground() => UpdateBackground(available);

        void ShowBlockedBackground() => UpdateBackground(blocked);

        void ShowInProgressBackground() => UpdateBackground(inProgress);

        void ShowMaxLevelBackground() => UpdateBackground(maxLevel);

        void UpdateBackground(IconUIContainer targetIcon)
        {
            available.SetActive(targetIcon == available);
            blocked.SetActive(targetIcon == blocked);
            inProgress.SetActive(targetIcon == inProgress);
            maxLevel.SetActive(targetIcon == maxLevel);

            researchButton = targetIcon.GetIconButton();
        }
    }
}
