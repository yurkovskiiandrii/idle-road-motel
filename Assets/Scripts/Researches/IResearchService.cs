using System;
using Researches;

public interface IResearchService
{
    void SelectResearch(int id, bool isMaxLevel);
    void StartResearch(ResearchCenter.ResearchData researchData);
    bool IsResearchAvailable(int id);

    event Action<int> OnResearchCompleted;
    event Action backgroundsUpdate;
    event Action OnTimeUpdated;
    int GetTimePassed(int index);

    bool IsStartedResearch();
    bool IsEnoughtMoneyToResearch(int id);
    int GetCurrentResearchID();
    ResearchCenter.ResearchData GetResearchData(int index);
}
