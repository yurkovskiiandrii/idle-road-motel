using Cinemachine;
using Researches;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class ResearchWorldVisual : MonoBehaviour
{
    [SerializeField] private ResearchController _researchController;

    [SerializeField] private Transform _content;
    [SerializeField] private ProgressBar _progressBar;
    [SerializeField] private ParticleSystem _particle;
    [SerializeField] private Image _icon;

    [SerializeField] private CinemachineVirtualCamera _virtualCamera;

    private ResearchCenter.ResearchData _data;
    private void Awake()
    {
        _researchController.OnResearchStarted += OnResearchStarted;
        _researchController.OnResearchCompleted += OnResearchCompleted;
    }

    private void OnResearchCompleted(int obj)
    {
        _particle.Stop();
        _content.gameObject.SetActive(false);
        _data = null;

    }

    private void LateUpdate()
    {
        if (_data == null)
            return;

        var timePassed = _researchController.GetTimePassed(_data.Id);
        _progressBar.SetValue((float)timePassed, _data.Time);

        transform.forward = _virtualCamera.transform.forward;
    }
    private void OnDestroy()
    {
        _researchController.OnResearchStarted -= OnResearchStarted;
    }
    private void OnResearchStarted(ResearchCenter.ResearchData data)
    {
        if (_content.gameObject.activeSelf)
            return;

        _data = data;
        _icon.sprite = data.Icon;
        _content.gameObject.SetActive(true);
        _particle.Play();
    }
}
