using System;
using System.Linq;
using Researches;
using UnityEngine;
using UnityEngine.Events;

namespace Researches
{
    public class Research : MonoBehaviour
    {
        [SerializeField] private ResearchCenter researchCenter;
        [SerializeField] private string id;
        [SerializeField] private UnityEvent onResearchComplete;

        private bool _isRegistered;
        private void OnEnable()
        {
            var isComplete = researchCenter.IsResearchCompleted(id);

            if (isComplete)
            {
                onResearchComplete?.Invoke();
                return;
            }
            
            researchCenter.OnResearchComplete += OnComplete;
            _isRegistered = true;
        }

        private void OnDisable()
        {
            if (_isRegistered)
            {
                researchCenter.OnResearchComplete -= OnComplete;
            }
        }

        private void OnComplete(ResearchCenter.ResearchData data)
        {
            if (!data.Unlocks.Any(unlock => unlock.Id.Equals(id)))
            {
                return;
            }
            
            onResearchComplete?.Invoke();

            researchCenter.OnResearchComplete -= OnComplete;
            _isRegistered = false;
        }
    }

}