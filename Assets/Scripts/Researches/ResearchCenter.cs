using System;
using System.Collections.Generic;
using System.Linq;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using UnityEngine;

namespace Researches
{
    [CreateAssetMenu(menuName = "Environment/ResearchCenter")]
    public class ResearchCenter : Saveable<ResearchCenter.SaveData>
    {
        [Serializable]
        public class SaveData
        {
            public List<ResearchDataNew> StartedResearches;
        }

        [Serializable]
        public class ResearchDataNew
        {
            public int Id { get; set; }
            public int Level;
            public int Time;
            public bool IsUnlocked;
            public long StartTime;

            public UnlockNew[] Unlocks;
        }

        [Serializable]
        public class UnlockNew
        {
            public int Level;
            public string Id;
            public string Label;
        }

        [Serializable]
        public class Unlock
        {
            public Sprite Icon;
            public int Level;
            public string Id;
            public string Label;
        }
        [Serializable]
        public class ResearchData
        {
            public int Id { get; set; }
            public Price[] Prices;
            public int Level;
            public int Time;
            public string Name;
            public string Description;
            public Sprite Icon;
            public Unlock[] Unlocks;
            public bool IsUnlocked;
            public long StartTime;
        }
        
        [Serializable]
        public class Price
        {
            public float Amount;
            public Currency Currency;
        }

        [Serializable]
        public class ResearchTree
        {
            public ResearchData[] ResearchDatas;
        }
        
        public ResearchTree[] ResearchTrees;
        
        public event Action<ResearchData> OnResearchComplete;

        public void ResearchComplete(ResearchData researchData)
        {
            var research = File.StartedResearches.FirstOrDefault(data => data.Id == researchData.Id);
            if (research != null)
            {
                research.IsUnlocked = true;               
                researchData.IsUnlocked = true;
            }

            OnResearchComplete?.Invoke(researchData);
        }
        
        public bool IsResearchCompleted(string idUnlock, int lvlUnlock = 0) =>
            File.StartedResearches.Any(data => 
                data.Unlocks.Any(unlock => unlock.Id.Equals(idUnlock) 
                                           && (lvlUnlock == 0 || unlock.Level == lvlUnlock)) && data.IsUnlocked);

        protected override void LoadDefault()
        {
            //Debug.Log("Research: Loading default");
            File = new SaveData();
            File.StartedResearches = new List<ResearchDataNew>();
            InitResearchTreeIDs();
        }

        protected override SaveData Serialize()
        {
            return File;
        }

        private void InitResearchTreeIDs()
        {
            var currentIndex = 0;
            for (int i = 0; i < ResearchTrees.Length; i++)
            {
                var tree = ResearchTrees[i];
                var researches = tree.ResearchDatas.ToList();
                for (int j = 0; j < researches.Count; j++)
                {
                    researches[j].Id = currentIndex++;
                }
            }
        }

        protected override void Deserialize(SaveData obj)
        {
            File = obj;

            InitResearchTreeIDs();

            //Debug.Log("Started researches " + File.StartedResearches.Count);
            for (int i = 0; i < File.StartedResearches.Count; i++)
            {
                //Debug.Log("Unlocked: " + File.StartedResearches[i].IsUnlocked);
                for (int j = 0; j < ResearchTrees.Length; j++)
                {
                    var research = ResearchTrees[j].ResearchDatas.FirstOrDefault(res => res.Id == File.StartedResearches[i].Id);
                    if (research == null) continue;

                    research.StartTime = File.StartedResearches[i].StartTime;
                    research.IsUnlocked = File.StartedResearches[i].IsUnlocked;
                    //Debug.Log("Research - " + research.Id + " unlocked: " + research.IsUnlocked);
                }
            }
        }

    }
}
