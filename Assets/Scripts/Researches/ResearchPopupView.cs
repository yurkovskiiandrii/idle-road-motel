using System;
using UnityEngine;
using UnityEngine.UI;

namespace Researches
{
    public class ResearchPopupView : MonoBehaviour
    {
        [SerializeField] private Image _mainIcon;
        [SerializeField] private Image[] _icons;

        private void Close()
        {
            gameObject.SetActive(false);
        }

        public void Open(ResearchCenter.ResearchData researchData)
        {
            _mainIcon.sprite = researchData.Icon;
            for (var index = 0; index < researchData.Unlocks.Length; index++)
            {
                if (index > 2)
                {
                    Debug.LogError($"{researchData.Id} contains >3 unlocked items, this is not supported");
                    break;
                }
                
                var unlock = researchData.Unlocks[index];
                _icons[index].sprite = unlock.Icon;
                _icons[index].gameObject.SetActive(true);
            }

            for (int i = researchData.Unlocks.Length; i < _icons.Length; i++)
            {
                _icons[i].gameObject.SetActive(false);
            }
            gameObject.SetActive(true);
        }
    }
}
