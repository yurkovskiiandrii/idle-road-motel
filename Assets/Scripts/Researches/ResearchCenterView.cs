using CoreGame.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Researches
{
    public class ResearchCenterView : MonoBehaviour
    {
        [SerializeField] string maxLevelTextValue = "Maxlevel";
        [SerializeField] string defaultLabelRext = "Research";
        [SerializeField] private Image icon;
        [SerializeField] private TMP_Text[] unlockLabels;
        [SerializeField] private TMP_Text[] unlockNamesLabels;
        [SerializeField] private Image[] unlockIcons;
        [SerializeField] private TMP_Text time;
        [SerializeField] private Image progressBar;
        [SerializeField] private Image[] costIcons;
        [SerializeField] private GameObject[] objIcon;
        [SerializeField] private TMP_Text[] costAmount;
        [SerializeField] private TMP_Text level;
        [SerializeField] private Transform content;
        [SerializeField] private GameObject researchFrame;
        [SerializeField] private ResearchView researchView;
        [SerializeField] private GameObject timerGO;

        [SerializeField] private Button ActionButton;
        [SerializeField] private ResearchMenuRedirect researchMenuRedirect;

        [SerializeField] private List<Animator> redirectAnimators;

        private IResearchService _researchService;
        private ResearchCenter.ResearchData _data;

        ActionButtonDataContainer actionButtonDataContainer;

        [SerializeField]
        private List<ResearchView> item;
        public void Init(IResearchService researchService)
        {
            _researchService = researchService;
            actionButtonDataContainer = ActionButton.GetComponent<ActionButtonDataContainer>();
        }

        private void OnEnable()
        {
            _researchService.OnTimeUpdated += UpdateTime;
            ClickOnResearch(0);
            //content.GetChild(0).GetChild(0).GetComponent<Button>().onClick.Invoke();
        }

        private void OnDisable()
        {
            _researchService.OnTimeUpdated -= UpdateTime;
        }

        public void AddResearch(ResearchCenter.ResearchTree tree, int researchIndex)
        {
            var view = Instantiate(researchView, content);
            view.Init(_researchService, tree, ActionButton, researchIndex);
            item.Add(view);
            //item[0].SetBorder(true);
        }

        public void ClickOnResearch(int index) => item[index].Select();

        public void SelectResearch((ResearchCenter.ResearchData, bool) data)
        {
            for(int s = 0; s < item.Count; s++)
                item[s].SetBorder(false);

            _data = data.Item1;

            bool isMaxLevelResearch = data.Item2;

            researchFrame.SetActive(true);
            timerGO.SetActive(true);
            actionButtonDataContainer.SetLabelText(defaultLabelRext);

            researchMenuRedirect.SetCurrentResearch(_data);

            icon.sprite = _data.Icon;
            level.text = "Level " + _data.Level;

            for (int j = 0; j < 3; j++)
            {
                if (j >= _data.Unlocks.Length)
                {
                    unlockLabels[j].text = string.Empty;
                    unlockIcons[j].sprite = null;
                    unlockIcons[j].gameObject.SetActive(false);

                    continue;
                }

                unlockLabels[j].text = _data.Description;
                unlockNamesLabels[j].text = _data.Name;
                unlockIcons[j].gameObject.SetActive(true);
                unlockIcons[j].sprite = _data.Unlocks[j].Icon;
            }

            for (int i = 0; i < 3; i++)
            {
                if (i >= _data.Prices.Length)
                {
                    costIcons[i].sprite = null;
                    costIcons[i].gameObject.SetActive(false);
                    costAmount[i].text = string.Empty;
                    objIcon[i].gameObject.SetActive(false);

                    continue;
                }
                
                var price = _data.Prices[i];
                costIcons[i].sprite = price.Currency.Icon;
                costIcons[i].gameObject.SetActive(true);
                objIcon[i].gameObject.SetActive(true);
                costAmount[i].text = $"{price.Amount}";

                if(isMaxLevelResearch)
                {
                    costIcons[i].gameObject.SetActive(false);
                    objIcon[i].gameObject.SetActive(false);
                }
            }

            if(isMaxLevelResearch)
            {
                timerGO.SetActive(false);
                actionButtonDataContainer.SetLabelText(maxLevelTextValue);
            }

            UpdateTime();
        }

        private void UpdateTime()
        {
            var timePassed = _researchService.GetTimePassed(_data.Id);
            SetProgress(_data.Time, timePassed);
        }

        private void SetProgress(int secondsTotal, int timePassed)
        { 
            var timeLeft = secondsTotal - timePassed;
            progressBar.fillAmount = (float) timePassed / secondsTotal;
            time.text = $"{timeLeft / 60}m:{timeLeft % 60}s";
        }

        public void ShowScaleTip() => redirectAnimators.ForEach(x => x.SetTrigger("Scale"));
    }
}
