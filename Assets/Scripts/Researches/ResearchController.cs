using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Facebook.Unity;
using UnityEngine;
using static Researches.ResearchCenter;

namespace Researches
{
    public class ResearchController : MonoBehaviour, IResearchService
    {
        [SerializeField] private ResearchCenterView researchCenterView;
        [SerializeField] private ResearchPopupView _researchPopupView;
        [SerializeField] private ResearchCenter researchCenter;

        private Dictionary<int, ResearchCenter.ResearchData> _researchesMap = new Dictionary<int, ResearchCenter.ResearchData>();
        public readonly Dictionary<string, bool> Unlocks = new Dictionary<string, bool>();


        private int _currentResearchIndex = -1;

        private DateTime _researchStartTime;
        private Sequence _sequence;
        public event Action OnTimeUpdated;
        public event Action backgroundsUpdate;
        public event Action<int> OnResearchCompleted;
        public event Action<ResearchData> OnResearchStarted;
        private void Awake()
        {
            researchCenterView.Init(this);

            for (var j = 0; j < researchCenter.ResearchTrees.Length; j++)
            {
                var tree = researchCenter.ResearchTrees[j];
                //var researches = tree.ResearchDatas.Where(data => !data.IsUnlocked).ToList();
                //if (researches.Count == 0)
                //{
                //    continue;
                //}

                var researches = tree.ResearchDatas.ToList();

                for (var i = 0; i < researches.Count; i++)
                {
                    var research = researches[i];

                    //research.Id = _currentResearchIndex++;
                    _researchesMap.Add(research.Id, research);
                    //if (!research.IsUnlocked
                    //    && research.StartTime > 0
                    //    && DateTime.UtcNow.TotalSeconds() - research.StartTime > research.Time)
                    //{
                    //    print("Research completed " + (DateTime.Now.TotalSeconds() - research.StartTime).ToString());
                    //    researchCenter.ResearchComplete(research);
                    //    _researchPopupView.Open(research);
                    //}

                    foreach (var unlock in research.Unlocks)
                    {
                        Unlocks.Add(unlock.Id + unlock.Level, research.IsUnlocked);
                    }
                }

                //if(tree.ResearchDatas.FirstOrDefault(res => !res.IsUnlocked) != null)
                researchCenterView.AddResearch(tree, j);
            }

            _currentResearchIndex = -1;
            SelectFirstResearch();
            _sequence = DOTween.Sequence()
                .SetLoops(-1)
                .PrependInterval(1)
                .AppendCallback(UpdateTime);
        }

        private void OnDestroy()
        {
            _sequence?.Kill();
        }

        private void UpdateTime()
        {
            if (_currentResearchIndex < 0)
            {
                return;
            }

            if (!_researchesMap.TryGetValue(_currentResearchIndex, out var data))
            {
                return;
            }
            if (DateTime.UtcNow.TotalSeconds() - data.StartTime >= data.Time)
            {
                foreach (var unlock in _researchesMap[_currentResearchIndex].Unlocks)
                {
                    Unlocks[unlock.Id + unlock.Level] = true;
                }

                //Debug.Log("Researches: research completed");

                researchCenter.ResearchComplete(_researchesMap[_currentResearchIndex]);
                OnResearchCompleted?.Invoke(_currentResearchIndex);
                _researchPopupView.Open(_researchesMap[_currentResearchIndex]);
                _currentResearchIndex = -1;
                backgroundsUpdate?.Invoke();
                SelectFirstResearch();
                return;
            }
            OnTimeUpdated?.Invoke();
            OnResearchStarted?.Invoke(_researchesMap[_currentResearchIndex]);
        }

        public int GetTimePassed(int index)
        {
            if (index != _currentResearchIndex)
                return 0;

            var data = _researchesMap[index];
            var timePassed = DateTime.UtcNow.TotalSeconds() - data.StartTime;
            return (int)timePassed;
        }

        public void SelectResearch(int id, bool isMaxLevel)
        {
            researchCenterView.SelectResearch(!isMaxLevel ? (_researchesMap[id], false) : (GetResearchData(id), true));
        }

        public int GetData(string id)
        {
            for (int i = 0; i < researchCenter.ResearchTrees.Length; i++)
            {
                for (int s = 0; s < researchCenter.ResearchTrees[i].ResearchDatas.Length; s++)
                {

                    if (researchCenter.ResearchTrees[i].ResearchDatas[s].Unlocks[0].Id == id && researchCenter.ResearchTrees[i].ResearchDatas[s].IsUnlocked)
                    {
                        return researchCenter.ResearchTrees[i].ResearchDatas[s].Level + 1;
                    }
                }
            }
            return 1;
        }
        public string GetLabel(string id)
        {
            for (int i = 0; i < researchCenter.ResearchTrees.Length; i++)
            {
                for (int s = 0; s < researchCenter.ResearchTrees[i].ResearchDatas.Length; s++)
                {

                    if (researchCenter.ResearchTrees[i].ResearchDatas[s].Unlocks[0].Id == id && researchCenter.ResearchTrees[i].ResearchDatas[s].IsUnlocked)
                    {
                        return researchCenter.ResearchTrees[i].ResearchDatas[s].Unlocks[0].Label;
                    }
                    else if (researchCenter.ResearchTrees[i].ResearchDatas[s].Unlocks[0].Id == id)
                    {
                        return researchCenter.ResearchTrees[i].ResearchDatas[s].Unlocks[0].Label;

                    }
                }
            }
            return "1";
        }

        public bool IsStartedResearch() => _currentResearchIndex >= 0;

        public int GetCurrentResearchID() => _currentResearchIndex;

        public bool IsEnoughtMoneyToResearch(int id)
        {
            foreach (var price in _researchesMap[id].Prices)
            {
                if (price.Currency.Value < price.Amount)
                    return false;
            }

            return true;
        }

        public void StartResearch(ResearchCenter.ResearchData researchData)
        {
            bool enoughtMoney = true;
            foreach (var price in _researchesMap[researchData.Id].Prices)
            {
                if (!price.Currency.EnoughtCurrency(price.Amount))
                    enoughtMoney = false;
            }

            if (!enoughtMoney || _currentResearchIndex >= 0)
                return;

            _currentResearchIndex = researchData.Id;

            foreach (var price in _researchesMap[researchData.Id].Prices)
            {
                price.Currency.TryToGet(price.Amount);
            }

            OnTimeUpdated.Invoke();

            _researchesMap[researchData.Id].StartTime = DateTime.UtcNow.TotalSeconds();

            if (researchCenter.File.StartedResearches.All(data => data.Id != _researchesMap[researchData.Id].Id))
            {
                var researchNew = new ResearchCenter.ResearchDataNew()
                {
                    Id = _researchesMap[researchData.Id].Id,
                    Level = _researchesMap[researchData.Id].Level,
                    Time = _researchesMap[researchData.Id].Time,
                    IsUnlocked = _researchesMap[researchData.Id].IsUnlocked,
                    StartTime = _researchesMap[researchData.Id].StartTime
                };

                researchNew.Unlocks = new ResearchCenter.UnlockNew[_researchesMap[researchData.Id].Unlocks.Length];
                for (int i = 0; i < researchNew.Unlocks.Length; i++)
                {
                    researchNew.Unlocks[i] = new ResearchCenter.UnlockNew()
                    {
                        Level = _researchesMap[researchData.Id].Unlocks[i].Level,
                        Id = _researchesMap[researchData.Id].Unlocks[i].Id,
                        Label = _researchesMap[researchData.Id].Unlocks[i].Label
                    };
                }

                researchCenter.File.StartedResearches.Add(researchNew);
            }

            OnResearchStarted?.Invoke(researchData);
            //Debug.Log($"Researches: start research current time = {_researchesMap[id].StartTime},  research time = {_researchesMap[id].Time}");
        }

        public bool IsResearchAvailable(int id) =>
            _researchesMap.TryGetValue(id, out var data)
            && !data.Prices.Any(price => price.Amount > price.Currency.Value);

        public bool IsUnlocked(string id, string lvl)
        {
            var key = id + lvl;
            if (!Unlocks.TryGetValue(key, out var isUnlocked))
            {
                //Debug.Log("Researches: no research found with key=" + key);
                return true;
            }

            //Debug.Log($"Researches: research with key {key} is completed - {isUnlocked}");
            return isUnlocked;
        }

        private void SelectFirstResearch()
        {
            //var research = _researchesMap.FirstOrDefault(pair => pair.Value.IsUnlocked == false);
            var startedResearch = researchCenter.File.StartedResearches.FirstOrDefault(data => !data.IsUnlocked);
            if (startedResearch != null)
            {
                var research = _researchesMap.FirstOrDefault(pair => startedResearch.Id == pair.Key);
                _currentResearchIndex = startedResearch.Id;
            }

            SelectResearch(startedResearch == null ? 0 : _currentResearchIndex, startedResearch == null);
        }

        public ResearchData GetResearchData(int index) => researchCenter.ResearchTrees[index].ResearchDatas.Last();
    }
}
