using System;
using DG.Tweening;
using GameData;
using UnityEngine;
using UnityEngine.UI;

namespace Quests
{
    public class QuestRewardView : MonoBehaviour
    {
        [SerializeField] private GameObject chestBig;
        [SerializeField] private GameObject chestSmall;
        [SerializeField] private RewardSlotView questRewardBig;
        [SerializeField] private RewardSlotView[] rewardSlots;
        [SerializeField] private Button closeButton;
        [SerializeField] private AnimationSettings animationSettings;
        
        private Quest _quest;
        private Sequence _animation;

        private bool _isRewardShown = true;
        private void Start()
        {
            closeButton.onClick.AddListener(ShowRewards);
        }

        private void ShowRewards()
        {
            _animation.Complete();
            if (_isRewardShown)
            {
                gameObject.SetActive(false);
                
                return;
            }
            chestBig.SetActive(false);
            var sequence = DOTween.Sequence();
            for (int i = 0; i < _quest.Rewards.Count; i++)
            {
                var index = i;
                sequence.AppendCallback(() =>
                {
                    chestSmall.SetActive(true);
                    questRewardBig.SetPayout(_quest.Rewards[index]);
                    questRewardBig.gameObject.SetActive(true);
                    questRewardBig.Animate(true, animationSettings.fadeInDuration);
                    questRewardBig.transform.position = animationSettings.bigRewardAppearPlace.position;
                });
                sequence.AppendInterval(animationSettings.fadeInDuration);
                sequence.Append(questRewardBig.transform.DOMove(animationSettings.bigRewardTargetPlace.position, animationSettings.flyDuration));
                sequence.AppendInterval(animationSettings.delayBetweenRewards);
                sequence.AppendCallback(() => questRewardBig.Animate(false, animationSettings.fadeOutDuration));
                sequence.AppendInterval(animationSettings.fadeOutDuration);
                sequence.AppendCallback(() => questRewardBig.gameObject.SetActive(false));
            }

            sequence.AppendCallback(() =>
            {
                for (int i = 0; i < rewardSlots.Length; i++)
                {
                    if (i < _quest.Rewards.Count)
                    {
                        rewardSlots[i].SetPayout(_quest.Rewards[i]);
                        rewardSlots[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        rewardSlots[i].gameObject.SetActive(false);
                    }
                }

                _isRewardShown = true;
                chestSmall.SetActive(false);
            });
        }

        public void SetPayout(Quest quest)
        {
            _quest = quest;
            _isRewardShown = false;
            chestBig.SetActive(true);
            gameObject.SetActive(true);
            chestSmall.SetActive(false);
            foreach (var reward in rewardSlots)
            {
                reward.gameObject.SetActive(false);
            }
            questRewardBig.gameObject.SetActive(false);
        }

        [Serializable]
        private struct AnimationSettings
        {
            public Transform bigRewardAppearPlace;
            public Transform bigRewardTargetPlace;
            public float delayBetweenRewards;
            public float flyDuration;
            public float fadeInDuration;
            public float fadeOutDuration;
        }
    }
}
