using System;
using System.Collections.Generic;
using System.Linq;
using Destructables;
using GameData;
using nickeltin.GameData.Saving;
using NPCs;
using UnityEngine;

namespace Quests
{
    public class QuestController : MonoBehaviour
    {
        [SerializeField] private QuestsPanelView questsPanelView;
        [SerializeField] private GameProgressSave gameProgressSave;
        [SerializeField] private ServiceManager[] serviceManagers;
        [SerializeField] private nickeltin.GameData.DataObjects.Currency[] currencies;
        [Space(20)] 
        [SerializeField] private QuestRewardView questRewardView;

        [SerializeField] public GameObject VFXGetReward;
        private QuestData _questData;
        private List<Quest> _currentQuests = new List<Quest>();
        

        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            SaveSystem.GetSavedItem(out _questData);
            for (int i = 0; i < 5; i++)
            {
                SetNewQuest(out var quest);
                if (quest != null)
                {
                    quest.SetProgress(gameProgressSave.GetQuestProgress(quest.Number));
                }
            }
            ActiveVfxReward();
        }
        public void ActiveVfxReward()
        {
            if (questsPanelView.ActiveClaims() == true)
            {
                VFXGetReward.SetActive(true);
            }
            else
            {
                VFXGetReward.SetActive(false);
            }
        }
        private void SetNewQuest(out Quest quest)
        {
            quest = _questData.Quests.FirstOrDefault(quest =>
                !gameProgressSave.CompletedQuests.Contains(quest.Number) 
                && !_currentQuests.Contains(quest));
            if (quest == null)
                return; 
                
            quest.Init(gameProgressSave);
            _currentQuests.Add(quest);
            questsPanelView.SetQuest(quest);
            SetPayout(quest);
            RegisterQuestProgress(quest);
            ActiveVfxReward();

        }

        private void RegisterQuestProgress(Quest currentQuest)
        {
            if(Enum.TryParse<QuestsTypes>(currentQuest.Condition.Type, out var questsTypes))
            {
                switch (questsTypes)
                {
                    case QuestsTypes.UpgradeToLevel:
                        if (currentQuest.Condition.Target.Contains("Service"))
                        {
                            var service = serviceManagers.FirstOrDefault(manager =>
                                manager.SaveID.Equals(currentQuest.Condition.Target));
                            if (service)
                            {
                                var targetLvl = service.File.BuildingsLevel;
                                currentQuest.UpdateProgress(targetLvl);
                                gameProgressSave.UpdateQuestProgress(currentQuest.Number,currentQuest.GetProgress());
                                service.OnBuildingsLevelChange += UpdateCurrentQuests;
                                currentQuest.OnQuestComplete +=
                                    () => service.OnBuildingsLevelChange -= UpdateCurrentQuests;
                            }
                        }
                        else
                        {
                            foreach (var service in serviceManagers)
                            {
                                if (!service.IsUnitIdBelongsToService(currentQuest.Condition.Target))
                                {
                                    continue;
                                }
                                
                                var targetLvl = service.GetHighestLvlNpc(currentQuest.Condition.Target)?.Stats.Level;
                                if (targetLvl != null)
                                {
                                    currentQuest.UpdateProgress(targetLvl.Value);
                                    gameProgressSave.UpdateQuestProgress(currentQuest.Number,currentQuest.GetProgress());
                                    if (targetLvl >= currentQuest.Condition.Amount)
                                    {
                                        return;
                                    }
                                }
                            }
                            
                            foreach (var service in serviceManagers)
                            {
                                service.OnUnitLevelChange += UpdateCurrentQuests;
                                currentQuest.OnQuestComplete += () => service.OnUnitLevelChange -= UpdateCurrentQuests;
                            }
                        }
                        break;
                    case QuestsTypes.CollectAmount:
                    {
                        var target = currencies.FirstOrDefault(currency =>
                            currency.Id.Equals(currentQuest.Condition.Target));
                        if (target != null)
                        {
                            target.OnValueIncreased += currentQuest.UpdateProgress;
                            currentQuest.OnQuestComplete +=
                                () => target.OnValueIncreased -= currentQuest.UpdateProgress;
                            currentQuest.UpdateProgress((int)target.Value);
                            
                        }
                        else
                        {
                            Debug.LogError($"Currency {currentQuest.Condition.Target} not found");
                        }
                    }

                        break;
                    case QuestsTypes.SpendAmount:
                    {
                        var target = currencies.FirstOrDefault(currency =>
                            currency.Id.Equals(currentQuest.Condition.Target));
                        if (target != null)
                        {
                            target.OnValueDecreased += currentQuest.UpdateProgress;
                            currentQuest.OnQuestComplete +=
                                () => target.OnValueDecreased -= currentQuest.UpdateProgress;
                        }
                        else
                        {
                            Debug.LogError($"Currency {currentQuest.Condition.Target} not found");
                        }
                    }
                        break;
                    case QuestsTypes.HireAmount:
                        var targetService = serviceManagers.FirstOrDefault(manager =>
                            manager.IsUnitIdBelongsToService(currentQuest.Condition.Target));
                        if (targetService)
                        {
                            currentQuest.SetProgress(targetService.File.Units.Count);
                            if (targetService.File.Units.Count >= currentQuest.Condition.Amount)
                            {
                                return;
                            }

                            targetService.OnUnitLevelChange += UpdateCurrentQuests;
                            currentQuest.OnQuestComplete +=
                                () => targetService.OnUnitLevelChange -= UpdateCurrentQuests;
                        }
                        break;
                    default:
                        Debug.LogError("Unknown questType");
                        break;
                }
            }
        }

        private void UpdateCurrentQuests(int obj)
        {
            UpdateCurrentQuests();
        }

        private void SetPayout(Quest quest)
        {
            quest.OnQuestComplete += () =>
            {
                questsPanelView.QuestComplete(quest, () =>
                {
                    foreach (var reward in quest.Rewards)
                    {
                        var target = currencies.FirstOrDefault(currency => currency.Id == reward.Type);
                        if (target != null)
                        {
                            target.Value += reward.Amount;
                        }
                        else
                        {
                            Debug.LogError($"Couldn't find currency {reward.Type}");
                        }
                    }
                    
                    _currentQuests.Remove(quest);
                    gameProgressSave.CompletedQuests.Add(quest.Number);
                    SetNewQuest(out _);
                    questRewardView.SetPayout(quest);
                });
            };
        }
        
        private void UpdateCurrentQuests()
        {
            foreach (var quest in _currentQuests)
            {
                if (!Enum.TryParse<QuestsTypes>(quest.Condition.Type, out var questsTypes))
                {
                    continue;
                }

                if (questsTypes == QuestsTypes.UpgradeToLevel)
                {
                    if (quest.Condition.Target.Contains("Service"))
                    {
                        var service = serviceManagers.FirstOrDefault(manager =>
                            manager.SaveID.Equals(quest.Condition.Target));
                        if(service)
                        {
                            var targetLvl = service.File.BuildingsLevel;
                            quest.UpdateProgress(targetLvl);
                        }
                    }
                    else
                    {
                        foreach (var service in serviceManagers)
                        {
                            if (!service.IsUnitIdBelongsToService(quest.Condition.Target))
                            {
                                continue;
                            }

                            var targetLvl = service.GetHighestLvlNpc(quest.Condition.Target)?.Stats.Level;
                            if(targetLvl != null)
                            {
                                quest.SetProgress(targetLvl.Value);
                                gameProgressSave.UpdateQuestProgress(quest.Number,quest.GetProgress());
                            }
                            break;
                        }
                    }
                } else if (questsTypes == QuestsTypes.HireAmount)
                {
                    var service = serviceManagers.FirstOrDefault(manager =>
                        manager.IsUnitIdBelongsToService(quest.Condition.Target));
                    if(service)
                    {
                        var workersAmount = service.File.Units.Count;
                        
                        quest.SetProgress(workersAmount);
                        gameProgressSave.UpdateQuestProgress(quest.Number,quest.GetProgress());
                    }
                }
            }
            ActiveVfxReward();
        }
        

        private enum QuestsTypes
        {
            UpgradeToLevel,
            CollectAmount,
            HireAmount,
            SpendAmount,
        }
        public void OpenCloseQuestPanel(GameObject panel)
        {
             if(panel.active == false)
            {
                panel.SetActive(true);
            }
             else
            {
                panel.SetActive(false);
            }
        }
    }
   
}
