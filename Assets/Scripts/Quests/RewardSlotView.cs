using System.Linq;
using DG.Tweening;
using GameData;
using Other;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Quests
{
    public class RewardSlotView : MonoBehaviour
    {
        [SerializeField] private TMP_Text amountText;
        [SerializeField] private Image image;
        [SerializeField] private QuestIcons questIcons;

        public void SetPayout(Reward reward)
        {
            if(amountText)
            {
                amountText.text = reward.Amount.ToString("0");
            }
            var sprite = GetRewardTexture(reward);
            image.sprite = sprite;
            image.gameObject.SetActive(sprite != null);
        }

        public void Animate(bool isAppearing, float time)
        {
            if (isAppearing)
            {
                image.color = new Color(1, 1, 1, 0);
                image.DOFade(1, time);
            } else
            {
                image.color = new Color(1, 1, 1, 1);
                image.DOFade(0, time);
            }
        }
        private Sprite GetRewardTexture(Reward reward) => 
            questIcons.RewardSprites.FirstOrDefault(sprites => sprites.ID.Equals(reward.Type)).Sprite;
    }
}
