using System;
using System.Linq;
using GameData;
using UnityEngine;

namespace Quests
{
    public class QuestsPanelView : MonoBehaviour
    {
        [SerializeField] private QuestView[] questViews;

        public void SetQuest(Quest currentQuest)
        {
            var questView = questViews.First(view => view.CurrentQuest == null);
            if (questView)
            {
                questView.Populate(currentQuest);
            } else
            {
                Debug.LogError("Trying to set new quest, but no vacant quest frame found");
            }
        }

        public void QuestComplete(Quest quest, Action doOnConfirm)
        {
            var completedQuest = questViews.FirstOrDefault(view => view.CurrentQuest == quest);
            if (completedQuest)
            {
                completedQuest.SetCompletedState(doOnConfirm);
            }
            else
            {
                Debug.LogError("Completed quest cannot be found on UI");
            }
        }

        public bool ActiveClaims()
        {
            for(int i = 0; i < questViews.Length; i++)
            {
                if (questViews[i].frameComplete.activeSelf)
                {
                  //  Debug.Log("ACTIVE VFX");
                    return true;
                }
            }
            return false;

        }
    }
}
