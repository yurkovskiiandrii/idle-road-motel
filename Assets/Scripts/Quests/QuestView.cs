using System;
using System.Linq;
using GameData;
using Other;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Quests
{
    public class QuestView : MonoBehaviour
    {
        [SerializeField] private TMP_Text text;
        [SerializeField] private TMP_Text progress;
        [SerializeField] private Image progressBar;
        [SerializeField] private TMP_Text textComplete;
        [SerializeField] private TMP_Text progressComplete;
        [SerializeField] private Image progressBarComplete;
        [SerializeField] public GameObject frameComplete;
        [SerializeField] private GameObject frameInProgress;
        [SerializeField] private Button button;
        [SerializeField] private Button redirectButton;
        [SerializeField] private Image rewardIcon;
        [SerializeField] private Transform rewardsContainer;
        [SerializeField] private QuestIcons questIcons;

        public Quest CurrentQuest { get; private set; }

        public void Populate(Quest currentQuest)
        {
            gameObject.SetActive(true);
            CurrentQuest = currentQuest;
            frameComplete.SetActive(false);
           // ActiveVfxReward(
            frameInProgress.SetActive(true);
            text.text = CurrentQuest.Name;
            UpdateProgress(0);
            CurrentQuest.OnProgressUpdated += UpdateProgress;
            button.onClick.RemoveAllListeners();
            var sprite = GetConditionTexture(currentQuest.Condition);
            rewardIcon.sprite = sprite;
            rewardIcon.gameObject.SetActive(true);
            ShowReward();

            QuestMenuRedirect redirection = transform.parent.gameObject.GetComponent<QuestMenuRedirect>();
            redirectButton.onClick.RemoveAllListeners();
            redirectButton.onClick.AddListener(() => redirection.SetQuest(CurrentQuest));
            redirectButton.onClick.AddListener(() => redirection.Redirect());
        }

        void ShowReward()
        {
            int rewardCount = CurrentQuest.Rewards.Count;

            foreach (Transform child in rewardsContainer)
                child.gameObject.SetActive(false);

            for (int i = 0; i < rewardCount; ++i) 
            {
                var reward = rewardsContainer.GetChild(i);
                reward.gameObject.SetActive(true);
                reward.GetComponent<Image>().sprite = questIcons.RewardSprites.First(x => x.ID == CurrentQuest.Rewards[i].Type).frameRewardIcon;
                reward.GetChild(0).GetComponent<TMP_Text>().text = CurrentQuest.Rewards[i].Amount.ToString();
            }
        }

        private void UpdateProgress(int amount)
        {
            progress.text = amount + "/" + CurrentQuest.Condition.Amount;
            progressBar.fillAmount = amount / CurrentQuest.Condition.Amount;
        }

        public void SetCompletedState(Action doOnConfirm)
        {
            textComplete.text = CurrentQuest.Name;
            progressComplete.text = CurrentQuest.Condition.Amount + "/" + CurrentQuest.Condition.Amount;
            progressBarComplete.fillAmount = 1;

            frameComplete.SetActive(true);
            frameInProgress.SetActive(false);
            button.onClick.AddListener(() =>
            {
                CurrentQuest = null;
                doOnConfirm?.Invoke(); 
            });
        }

        private Sprite GetConditionTexture(Condition condition) => 
            questIcons.ConditionSprites.FirstOrDefault(sprites => sprites.ID.Equals(condition.Target)).Sprite;
    }
}
