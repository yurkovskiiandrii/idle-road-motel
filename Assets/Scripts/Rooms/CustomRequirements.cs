using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RequirementsList/CustomRequirements")]
public class CustomRequirements : ScriptableObject
{
    public List<string> requirementsNames;
}
