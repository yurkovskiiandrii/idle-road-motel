using nickeltin.GameData.Saving;
using nickeltin.Singletons;
using NPCs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using GameEnvironment.Village.Structures;

[CreateAssetMenu(menuName = "GameData/RoomsContainer")]
public class RoomsContainer : ScriptableObject
{
    [SerializeField] List<ServiceManager> rooms;

    List<CustomerPleasureZone> pleasuresZonesCashed;

    public int GetRoomsCount() => rooms.Count;

    //public List<ServiceManager> GetFreeRooms() => rooms.Where(x => x.IsFreeSpaceInStorage() && x.AmountAvailableWorkZones() > 0).ToList();

    //public int GetFreeRoomsCount() => GetFreeRooms().Count;

    public ServiceManager GetRoom(int index) => rooms[index];

    public List<CustomerPleasureZone> GetPleasureZones()
    {
        //if (!(pleasuresZonesCashed == null || pleasuresZonesCashed.Count == 0))
        //    return pleasuresZonesCashed;
        var res = new List<CustomerPleasureZone>();

        rooms.ForEach(x => res.AddRange(x.GetZones<CustomerPleasureZone>()));

        return res;
    }
}
