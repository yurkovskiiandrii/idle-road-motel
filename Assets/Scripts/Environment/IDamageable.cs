﻿namespace Destructables
{
    public interface IDamageable
    {
        bool TakeDamage(Damage damage);
    }
}