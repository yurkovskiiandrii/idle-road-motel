﻿using System.Collections.Generic;
using CameraControlling;
using nickeltin.Editor.Attributes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Audio;

namespace GameEnvironment
{
    public class SceneContent : MonoBehaviour
    {
        [SerializeField] [Scene] private string scene;
        [Space] 
        [SerializeField] private List<LayerMask> m_renderingLayers;
        [Space]
        [SerializeField] private UnityEvent m_onLoad;
        [SerializeField] private UnityEvent m_onUnload;
        [SerializeField] private AudioMixer mix;
        private void Start() => OnLoad(gameObject.scene.name);

        private void OnLoad(string sceneName)
        {
            if (sceneName.Equals(scene)) m_onLoad.Invoke();
        }

        private void OnUnload(string sceneName)
        {
            if (sceneName.Equals(scene)) m_onUnload.Invoke();
            //mix.

        }
        public void EnableSoundVillage()
        {
            mix.SetFloat("Volume", 10f);

        }
        public void DisableSoundVillage()
        {
            mix.SetFloat("Volume", -80f);

        }

        public void SetCameraLayerMask(int i) => CameraController.camera.cullingMask = m_renderingLayers[i];

        private void OnEnable()
        {
            SceneLoader.onSceneLoad += OnLoad;
            SceneLoader.beforeSceneUnload += OnUnload;
           

        }

        private void OnDisable()
        {
            SceneLoader.onSceneLoad -= OnLoad;
            SceneLoader.beforeSceneUnload -= OnUnload;
        }
    }
}