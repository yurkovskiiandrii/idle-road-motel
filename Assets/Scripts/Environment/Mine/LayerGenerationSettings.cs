﻿using System;
using System.Collections.Generic;
using nickeltin.Extensions;
using UnityEngine;

namespace GameEnvironment.Shafts
{
    [Serializable]
    public struct LayerGenerationSettings
    {
        public Material material;
        public Vector2Int atlasSizeInTextures;
        public Vector3 size;
        public int chunkSize;
        public Vector3Int sizeInChunks;
        public AnimationCurve health;
        public AnimationCurve points;
        public List<LayerStruct> colors;


        public int VoxelsCount => (chunkSize * chunkSize * chunkSize) * (sizeInChunks.x * sizeInChunks.y * sizeInChunks.z); 
        
        public Color EvaluateColor(int layerIndex)
        {
            if(colors.Count <= 0) return Color.magenta;
            int i = (layerIndex % colors.Count).ClampAsIndex(colors);
            return colors[i].colors;
        }

        public Vector2Int EvaluateTextureCords(int layerIndex)
        {
            int atlasLength = atlasSizeInTextures.x * atlasSizeInTextures.y;
            int clampedIndex = layerIndex % atlasLength;
            int x = clampedIndex % atlasSizeInTextures.x;
            
            int y = Mathf.FloorToInt((float)clampedIndex / atlasSizeInTextures.x);
            return new Vector2Int(x, ((atlasSizeInTextures.y - 1) - y).Clamp0());
        }
    }
}