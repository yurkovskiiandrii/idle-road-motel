﻿namespace Voxelization
{
    public class Voxel
    {
        public int type;
        public Chunk chunk;
        
        public Voxel(VoxelType state)
        {
            this.type = (int)state;
            chunk = null;
        }
    }

    public enum VoxelType
    {
        Air = 0,
        Full = 1,
    }
}