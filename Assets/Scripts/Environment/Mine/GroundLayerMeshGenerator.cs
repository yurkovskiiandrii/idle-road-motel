﻿using System;
using UnityEngine;
using System.Collections.Generic;
using nickeltin.Extensions;
using Voxelization;
using Object = UnityEngine.Object;

namespace GameEnvironment
{
    public class GroundLayerGenerator
    {
        private readonly VoxelType m_voxelType;
        private readonly int m_chunkSize;
        private readonly Vector3 m_min;
        private readonly Vector3 m_scale;
        private readonly Vector3 m_meshSize;
        private readonly Material m_material;
        private readonly Transform m_transform;
        private readonly Func<Damage, bool> m_onDamage;
        private Vector3Int m_sizeInChunks;
        private Chunk[,,] m_chunks;
        
        private readonly int m_voxelPixelSize;
        private readonly Vector2Int m_voxelTexturePosition;
        private readonly Vector2Int m_atlasSize;

        private Vector3Int m_size => m_sizeInChunks * m_chunkSize;
        public Voxel[,,] Voxels { get; private set; }
        public int VoxelsCount { get; }
        public Vector3 MeshSize => m_meshSize.Mult(m_transform.localScale);
        public Vector3 VoxelSize => MeshSize.Div(m_size);

        public GroundLayerGenerator(VoxelType voxelType, Vector2Int atlasSize, Vector2Int texturePosition, Transform parent, Vector3 size, int chunkSize, Vector3Int sizeInChunks, 
            Material material, Func<Damage, bool> onDamageAction)
        {
            m_atlasSize = atlasSize;
            m_voxelTexturePosition = texturePosition;
            m_voxelType = voxelType;
            m_material = material;
            m_transform = parent;
            m_chunkSize = chunkSize;
            m_sizeInChunks = sizeInChunks;
            m_onDamage = onDamageAction;
            VoxelsCount = m_size.x * m_size.y * m_size.z;
            Bounds bounds = new Bounds(Vector3.zero, size); 
            m_meshSize = bounds.size;
            m_scale = new Vector3(
                bounds.size.x / m_size.x, 
                bounds.size.y / m_size.y, 
                bounds.size.z / m_size.z);
            
            m_min = bounds.min;
        }

        public void GenerateMesh()
        {
            CalculateVoxels();
            GenerateChunks();
        }

        public void UpdateChunks()
        {
            for (int z = 0; z < m_sizeInChunks.z; z++)
            {
                for (int y = 0; y < m_sizeInChunks.y; y++)
                {
                    for (int x = 0; x < m_sizeInChunks.x; x++)
                    {
                        m_chunks[x, y, z].UpdateMesh(Voxels);
                    }
                }
            }
        }
        
        public Vector3 MeshPositionToWorldPosition(Vector3Int meshPosition)
        {
            Vector3 meshCenter = m_size / 2;
            float xOffset = -(meshCenter.x - meshPosition.x);
            float yOffset = -(meshCenter.y - meshPosition.y);
            float zOffset = -(meshCenter.z - meshPosition.z);
            
            xOffset = xOffset * m_scale.x * m_transform.localScale.x;
            yOffset = yOffset * m_scale.y * m_transform.localScale.y;
            zOffset = zOffset * m_scale.z * m_transform.localScale.z;

            return m_transform.position + new Vector3(xOffset, yOffset, zOffset);
        }
        
        public bool WorldPositionToMeshPosition(Vector3 position, out Vector3Int meshPos)
        {
            Vector3 localPosUnscaled = position - m_transform.position;
            Vector3 localScale = m_transform.localScale;
            Vector3 meshCenter = m_size / 2;
            
            float localX = ((localPosUnscaled.x / m_scale.x) / localScale.x);
            float localY = ((localPosUnscaled.y / m_scale.y) / localScale.y);
            float localZ = ((localPosUnscaled.z / m_scale.z) / localScale.z);
            
            //Debug.Log((meshCenter.x + localX, meshCenter.y + localY, meshCenter.z + localZ));
            
            int meshX = Mathf.FloorToInt(meshCenter.x + localX);
            int meshY = Mathf.FloorToInt(meshCenter.y + localY);
            int meshZ = Mathf.FloorToInt(meshCenter.z + localZ);

            meshPos = Clamp(meshX, meshY, meshZ);
            
            Vector3Int Clamp(int x, int y, int z)
            {
                return new Vector3Int(Mathf.Clamp(x,0, m_size.x-1),
                    Mathf.Clamp(y,0,m_size.y-1),
                    Mathf.Clamp(z, 0, m_size.z-1));
            }
            
            return Voxels[meshPos.x, meshPos.y, meshPos.z].type >= 1;
        }
        
        ///<returns> how much actual voxels is removed </returns>
        public int RemoveVoxels(List<Vector3Int> positionsInMeshes)
        {
            if (positionsInMeshes.Count == 0) return 0;
            int removedVoxels = 0;

            HashSet<Chunk> modifiedChunks = new HashSet<Chunk>();
            foreach (var pos in positionsInMeshes)
            {
                if (Voxels[pos.x, pos.y, pos.z].type >= 1) removedVoxels++;
                else continue;

                Voxels[pos.x, pos.y, pos.z].type = 0;
                var chunk = Voxels[pos.x, pos.y, pos.z].chunk;
                modifiedChunks.Add(chunk);

                Vector3Int chunkPos = chunk.PositionInMesh;

                //RIGHT CHUNK
                if (pos.x == chunk.Max.x && TryGetNeighbourChunk(chunkPos, 
                    Vector3Int.right, out var rightNeighbour)) modifiedChunks.Add(rightNeighbour);

                //LEFT CHUNK
                if (pos.x == chunk.Min.x && TryGetNeighbourChunk(chunkPos, 
                    Vector3Int.left, out var leftNeighbour)) modifiedChunks.Add(leftNeighbour);
                
                //UP CHUNK
                if (pos.y == chunk.Max.y && TryGetNeighbourChunk(chunkPos, 
                    Vector3Int.up, out var upNeighbour)) modifiedChunks.Add(upNeighbour);

                //DOWN CHUNK
                if (pos.y == chunk.Min.y && TryGetNeighbourChunk(chunkPos, 
                    Vector3Int.down, out var downNeighbour)) modifiedChunks.Add(downNeighbour);

                //FRONT CHUNK
                if (pos.z == chunk.Max.z && TryGetNeighbourChunk(chunkPos, 
                    new Vector3Int(0,0,1), out var frontNeighbour)) modifiedChunks.Add(frontNeighbour);

                //BACK CHUNK
                if (pos.z == chunk.Min.z && TryGetNeighbourChunk(chunkPos, 
                    new Vector3Int(0,0,-1), out var backNeighbour)) modifiedChunks.Add(backNeighbour);
            }

            foreach (var chunk in modifiedChunks)
            {
                chunk.UpdateMesh(Voxels);
                if (chunk.VoxelsCount <= 0) chunk.gameObject.SetActive(false);
            }

            return removedVoxels;
        }

        private bool TryGetNeighbourChunk(Vector3Int pos, Vector3Int offset, out Chunk neighbour)
        {
            Vector3Int dir = new Vector3Int(Mathf.Clamp(offset.x, -1, 1),
                Mathf.Clamp(offset.y,-1, 1),
                Mathf.Clamp(offset.z,-1, 1)) + pos;

            if (m_chunks.InRange(dir.x, dir.y, dir.z))
            {
                neighbour = m_chunks[dir.x, dir.y, dir.z];
                return neighbour != null;
            }

            neighbour = default;
            return false;
        }

        public bool TryGetNeighbourVoxel(Vector3Int pos, Vector3Int offset, out Voxel neighbour)
        {
            Vector3Int dir = new Vector3Int(Mathf.Clamp(offset.x, -1, 1),
                Mathf.Clamp(offset.y,-1, 1),
                Mathf.Clamp(offset.z,-1, 1)) + pos;

            if (Voxels.InRange(dir.x, dir.y, dir.z))
            {
                neighbour = Voxels[dir.x, dir.y, dir.z];
                return neighbour != null;
            }

            neighbour = default;
            return false;
        }
        
        private void CacheVoxelsFromChunk(Chunk chunk)
        {
            for (int localZ = 0; localZ < m_chunkSize; localZ++)
            {
                for (int localY = 0; localY < m_chunkSize; localY++)
                {
                    for (int localX = 0; localX < m_chunkSize; localX++)
                    {
                        Vector3Int global = chunk.ToGlobalPosition(localX, localY, localZ);
                        if(Voxels[global.x,global.y,global.z].type == 0) continue;
                        Voxels[global.x, global.y, global.z].chunk = chunk;
                    }
                }
            }
        }
        
        private void CalculateVoxels()
        {
            Voxels = new Voxel[m_size.x, m_size.y, m_size.z];
            
            for (int z = 0; z < m_size.z; z++)
            {
                for (int y = 0; y < m_size.y; y++)
                {
                    for (int x = 0; x < m_size.x; x++)
                    {
                        Voxels[x, y, z] = new Voxel(m_voxelType);
                    }
                }
            }
        }
        
        private void GenerateChunks()
        {
            m_chunks = new Chunk[m_sizeInChunks.x, m_sizeInChunks.y, m_sizeInChunks.z];
            
            for (int z = 0; z < m_sizeInChunks.z; z++)
            {
                for (int y = 0; y < m_sizeInChunks.y; y++)
                {
                    for (int x = 0; x < m_sizeInChunks.x; x++)
                    {
                        Vector3Int chunkPosInMesh = new Vector3Int(x,y,z);

                        var chunk = Chunk.Create(chunkPosInMesh, m_chunkSize, m_atlasSize, 
                            m_voxelTexturePosition, m_min, m_scale, m_transform);
                        
                        CacheVoxelsFromChunk(chunk);
                        
                        if (!chunk.UpdateMesh(Voxels))
                        {
                            Object.Destroy(chunk.gameObject);
                            continue;
                        }

                        //TODO:
                        chunk.gameObject.layer = LayerMask.NameToLayer("Destructable");
                        chunk.onDamage = m_onDamage;
                        chunk.Renderer.material = m_material;
                        m_chunks[x, y, z] = chunk;
                    }
                }
            }
        }
    }
}