using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FXs;
using UnityEngine.Events;

[System.Serializable]
public struct GemBox
{
    public int GemCount;
    public int GemCountMax,GemCountMin;
    public Image iconGem;
    public GameObject GemObject;
    public Text GemText;

}
public class GetGemFrom : MonoBehaviour
{
    public GemBox Green, Red, Blue, Gold;
    [Space(50)]
    public GameObject ClaimButton;
    public VFX vfx;
    public Mineshaft mineScript;
    public static GameObject AutomatePanel;
    public List<RequiredGem> GemList = new List<RequiredGem>();
    int i;

    static UnityEvent onGemCollectedEvent = new UnityEvent();

    public static void SuscribeOnGemCollected(UnityAction action) => onGemCollectedEvent.AddListener(action);

    public static void RemoveOnGemCollected(UnityAction action) => onGemCollectedEvent.RemoveListener(action);

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");
        GetGemFrom getGem = mine[0].gameObject.GetComponent<GetGemFrom>();
        //Mineshaft mineScript = mine[0].gameObject.GetComponent<Mineshaft>();

        GemList = mineScript.requiredGem();
        foreach (RequiredGem a in GemList)
        {
            if (a.currencyType == CurrencyType.GreenGem)
            {
                getGem.AddGemFromAutomate(mineScript.auto.mines.GreenGem, CurrencyType.GreenGem);
                mineScript.auto.mines.GreenGem = 0;
            }
            if (a.currencyType == CurrencyType.BlueGem)
            {
                getGem.AddGemFromAutomate(mineScript.auto.mines.BlueGem, CurrencyType.BlueGem);
                mineScript.auto.mines.BlueGem = 0;
            }
            if (a.currencyType == CurrencyType.RedGem)
            {
                getGem.AddGemFromAutomate(mineScript.auto.mines.RedGem, CurrencyType.RedGem);
                mineScript.auto.mines.RedGem = 0;
            }
            if (a.currencyType == CurrencyType.GoldGem)
            {
                getGem.AddGemFromAutomate(mineScript.auto.mines.Gold, CurrencyType.GoldGem);
                mineScript.auto.mines.Gold = 0;
            }
        }
        GemList.Clear();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            //AddGem(CurrencyType.Red, 45,);
        }
    }
    
    public void OutScene()
    {
        mineScript.auto.mines.Gold += Gold.GemCount;
        Gold.GemCount = 0;

        mineScript.auto.mines.RedGem += Red.GemCount;
        Red.GemCount = 0;

        mineScript.auto.mines.GreenGem += Green.GemCount;
        Green.GemCount = 0;

        mineScript.auto.mines.BlueGem += Blue.GemCount;
        Blue.GemCount = 0;

        

    }

    private void OnApplicationQuit()
    {
        OutScene();
    }

    public void GetGems()
    {
        if (Green.GemCount >= Green.GemCountMin)
        {
            mineScript.currency[0].Value += Green.GemCount;
            Green.GemCount = 0;
            Green.GemObject.SetActive(false);
        }

        if (Blue.GemCount >= Blue.GemCountMin)
        {
            mineScript.currency[1].Value += Blue.GemCount;
            Blue.GemCount = 0;
            Blue.GemObject.SetActive(false);
        }

        if (Red.GemCount >= Red.GemCountMin)
        {
            mineScript.currency[2].Value += Red.GemCount;
            Red.GemCount = 0;
            Red.GemObject.SetActive(false);
        }

        if(Gold.GemCount >= Gold.GemCountMin)
        {
            mineScript.currency[3].Value += Gold.GemCount;
            Gold.GemCount = 0;
            Gold.GemObject.SetActive(false);
        }

        ClaimButton.SetActive(false);
        ProgressUI.takeVFX.SetActive(false);

        onGemCollectedEvent.Invoke();
    }

    [ContextMenu("GetGems")]
    void Collect()
    {
        onGemCollectedEvent.Invoke();
    }

    public void AddGem(CurrencyType typess, int count, Vector3 vector3)
    {
        i++;
        Debug.Log(vector3.ToString());
        if ( typess == CurrencyType.BlueGem)
        {
            
                Blue.GemCount += count;
           // VFX.Play(VFX.BlueGem, vector3, Color.blue);
          GameObject n = (GameObject)Instantiate(VFX.BlueGem, vector3, transform.rotation);


            n.transform.parent = ProgressUI.worldCan.transform;
            Vector3 pos = Camera.main.WorldToScreenPoint(vector3);
            // n.transform.position = pos;
            n.transform.GetChild(0).position = new Vector3(pos.x, pos.y, 0);
            n.GetComponent<Canvas>().sortingOrder = i;

            // Instantiate(VFX.BlueGem, vector3, transform.rotation);
            if (Blue.GemCount > Blue.GemCountMax)
            {
                int x = Blue.GemCount - Blue.GemCountMax;
                Blue.GemCount -= x;

            }
            if (Blue.GemCount >= Blue.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Blue.GemText.text = Blue.GemCount.ToString() + "/" + Blue.GemCountMax;
                Blue.iconGem.sprite = mineScript.currency[1].Icon;
                Blue.GemObject.SetActive(true);
            }
            else 
            {
               // ClaimButton.SetActive(false);
                Blue.GemObject.SetActive(false);

            }
            // int x = Blue.GemCountMax - Blue.GemCount;
            // if (x < 0)
            // {
            //     Blue.GemCount -= x;
            // }

        }
        if( typess == CurrencyType.GreenGem)
        {
            Green.GemCount += count;
            // VFX.Play(VFX.GreenGem, vector3, Color.green);
            GameObject n = (GameObject)Instantiate(VFX.GreenGem, vector3, transform.rotation);
            n.transform.parent = ProgressUI.worldCan.transform;
            Vector3 pos = Camera.main.WorldToScreenPoint(vector3);
            // n.transform.position = pos;
            n.transform.GetChild(0).position = new Vector3(pos.x, pos.y, 0);
            n.GetComponent<Canvas>().sortingOrder = i;

            if (Green.GemCount > Green.GemCountMax)
            {
                int x = Green.GemCount - Green.GemCountMax;
               Green.GemCount -= x;
            }
            if (Green.GemCount >= Green.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Green.GemText.text = Green.GemCount.ToString() + "/" + Green.GemCountMax;
                Green.iconGem.sprite = mineScript.currency[0].Icon;
                Green.GemObject.SetActive(true);
            }
            else
            {
               // ClaimButton.SetActive(false);
                Green.GemObject.SetActive(false);
            }

            //   Green.GemCount += count;
            //
            //   int x = Green.GemCountMax - Green.GemCount;
            //   if (x < 0)
            //   {
            //       Green.GemCount -= x;
            //   }

        }
        if( typess == CurrencyType.RedGem)
        {
            Red.GemCount += count;
            //VFX.Play(VFX.RedGem, vector3, Color.red);
            GameObject n = (GameObject)Instantiate(VFX.RedGem, vector3, transform.rotation);
            n.transform.parent = ProgressUI.worldCan.transform;
            Vector3 pos = Camera.main.WorldToScreenPoint(vector3);
            //  n.transform.position = pos;
            n.transform.GetChild(0).position = new Vector3(pos.x, pos.y, 0) ;
            // g.GetComponent<RectTransform>().position = pos;
            n.GetComponent<Canvas>().sortingOrder = i;

            if (Red.GemCount > Red.GemCountMax)
            {
                int x = Red.GemCount - Red.GemCountMax;
                Red.GemCount -= x;
            }
            if (Red.GemCount >= Red.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Red.GemText.text = Red.GemCount.ToString() + "/" + Red.GemCountMax;
                Red.iconGem.sprite = mineScript.currency[2].Icon;
                Red.GemObject.SetActive(true);
            }
            else
            {
              //  ClaimButton.SetActive(false);
                Red.GemObject.SetActive(false);
            }
            //  int x = Red.GemCountMax - Red.GemCount;
            //  if (x < 0)
            //  {
            //      Red.GemCount -= x;
            //  }
        }


        Debug.Log("Type : " + typess.ToString() + " Reward : " + count.ToString());
    }

    public void AddGemFromAutomate(int i , CurrencyType type)
    {
        if (type == CurrencyType.BlueGem)
        {
            Blue.GemCount += i;
            // VFX.Play(VFX.BlueGem, vector3, Color.blue);
          //  Instantiate(VFX.BlueGem, vector3, transform.rotation);
            // Instantiate(VFX.BlueGem, vector3, transform.rotation);
            if (Blue.GemCount > Blue.GemCountMax)
            {
                int x = Blue.GemCount - Blue.GemCountMax;
                Blue.GemCount -= x;

            }
            if (Blue.GemCount >= Blue.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Blue.GemText.text = Blue.GemCount.ToString() + "/" + Blue.GemCountMax;
                Blue.iconGem.sprite = mineScript.currency[1].Icon;
                Blue.GemObject.SetActive(true);
            }
            else
            {
                // ClaimButton.SetActive(false);
                Blue.GemObject.SetActive(false);

            }
        }
        if (type == CurrencyType.RedGem)
        {
            Red.GemCount += i;
            //VFX.Play(VFX.RedGem, vector3, Color.red);
           // Instantiate(VFX.RedGem, vector3, transform.rotation);

            if (Red.GemCount > Red.GemCountMax)
            {
                int x = Red.GemCount - Red.GemCountMax;
                Red.GemCount -= x;
            }
            if (Red.GemCount >= Red.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Red.GemText.text = Red.GemCount.ToString() + "/" + Red.GemCountMax;
                Red.iconGem.sprite = mineScript.currency[2].Icon;
                Red.GemObject.SetActive(true);
            }
            else
            {
                //  ClaimButton.SetActive(false);
                Red.GemObject.SetActive(false);
            }
        }
        if (type == CurrencyType.GreenGem)
        {
            Green.GemCount += i;
            // VFX.Play(VFX.GreenGem, vector3, Color.green);
          //  Instantiate(VFX.GreenGem, vector3, transform.rotation);

            if (Green.GemCount > Green.GemCountMax)
            {
                int x = Green.GemCount - Green.GemCountMax;
                Green.GemCount -= x;
            }
            if (Green.GemCount >= Green.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Green.GemText.text = Green.GemCount.ToString() + "/" + Green.GemCountMax;
                Green.iconGem.sprite = mineScript.currency[0].Icon;
                Green.GemObject.SetActive(true);
            }
            else
            {
                // ClaimButton.SetActive(false);
                Green.GemObject.SetActive(false);
            }
        }
        if (type == CurrencyType.GoldGem)
        {
            Gold.GemCount += i;
            // VFX.Play(VFX.BlueGem, vector3, Color.blue);
           // Instantiate(VFX.BlueGem, vector3, transform.rotation);
            // Instantiate(VFX.BlueGem, vector3, transform.rotation);
            if (Gold.GemCount > Gold.GemCountMax)
            {
                int x = Gold.GemCount - Gold.GemCountMax;
                Gold.GemCount -= x;

            }
            if (Gold.GemCount >= Gold.GemCountMin)
            {
                ClaimButton.SetActive(true);
                Gold.GemText.text = Gold.GemCount.ToString() + "/" + Gold.GemCountMax;
                Gold.iconGem.sprite = mineScript.currency[3].Icon;
                Gold.GemObject.SetActive(true);
            }
            else
            {
                // ClaimButton.SetActive(false);
                Gold.GemObject.SetActive(false);

            }
        }


    }


   


}
