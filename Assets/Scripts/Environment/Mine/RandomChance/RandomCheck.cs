using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCheck 
{
    // Start is called before the first frame update
    public int[] chances;//������ ��������

    public RandomCheck(int[] c) { chances = c; }

    public int Get()
    {
        int all = 0;//����� ����� ������
        foreach (int c in chances) all += c;
        int lr = 0;//������ ������� �� ����������� �����
        int r = Random.Range(0, all);//������� ��������� ����� �� ������ ���������� ������
        for (int i = 0; i < chances.Length; i++)
        {
            if (chances[i] > 0)//���� ������� ���� �� ��������� �������, ���� ������
            {
                if (r >= lr && r < lr + chances[i])//���� ��������� ����� ��������� � �������� �������...
                {
                    chances[i]--;//������� ���� ���� � �������
                    return i;//���������� ������
                }
                else lr += chances[i];//��� ���������� ����� �� ������� � ���������� ������
            }
        }
        return -1;//���� ������ �������� ���� ��� ��������� ��� �����, ���������� -1
    }

    public static void Shuffle<T>(T[] arr) // ���������� ������
    {

        for (int i = arr.Length - 1; i >= 1; i--)
        {
            int j = Random.Range(0, i + 1);

            T tmp = arr[j];
            arr[j] = arr[i];
            arr[i] = tmp;
        }
    }

    public static int GetRandomNumber<T>(T[] arr) // ��������� ������ �� ������� 
    {
        return Random.Range(0, arr.Length);
    }
}
