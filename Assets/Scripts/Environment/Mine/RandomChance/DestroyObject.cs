using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public float Timer = 2f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, Timer);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
