
using nickeltin.Singletons;
using NPCs;
using UnityEngine;


namespace GameEnvironment.Mine
{
    public class MinerTransfer : MonoSingleton<MinerTransfer>
    {
        [SerializeField] private ServiceManager _miningService;
        public static Transform Transform => instance.transform;
        private bool isMineLoaded;
        private void Start()
        {
            TimeCycle.onTimeChanged += DayTimeChanged;
        }

        private void DayTimeChanged(TimeCycle.TimesOfDay timesOfDay)
        {
            if (timesOfDay == TimeCycle.TimesOfDay.Night)
            {
                Activate();
            }
        }
        public void Activate()
        {
            foreach (var miner in _miningService.ActiveNpc.Values)
            {
                if(miner.gameObject.activeSelf) return;
                miner.gameObject.SetActive(true);
                miner.ActivateNPC(true);
            }
        }
    }
}