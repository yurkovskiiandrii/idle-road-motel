﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Destructables;
using GameEnvironment.Shafts;
using nickeltin.Extensions;
using nickeltin.GameData.DataObjects;
using UnityEngine;
using nickeltin.GameData.Saving;


[System.Serializable]
public enum CurrencyType
{
    GreenGem,RedGem,BlueGem,GoldGem
}

[System.Serializable]

public class CurrentChance
{
    [Header("Настройки выпадения гема из слоя")]
    [Tooltip("Тип гема")]
    public CurrencyType IndxCurrency = CurrencyType.BlueGem;
    [Tooltip("Шанс выпадения в %")]
    public int Chance;
    [Tooltip("Максимальная сумма и минимальная")]
    public int RewardMin, RewardMax;
    [Header("Автоматическое начисление гема")]
    [Space(10)]
    [Tooltip("Гем используется в слои при автоматизации майнинга?")]

    public bool GemAutomated;
    [Tooltip("получаем вознаграждение?")]

    public bool GetGemReward;
    [Tooltip("Сумма траты за слой")]
    public int GemRequired;
    public int Reward
    {
        get
        {
            return UnityEngine.Random.Range(RewardMin,RewardMax);
        }
    }
}
[System.Serializable]
public class RequiredGem
{
    public CurrencyType currencyType;
    public int GemRequired;
    public bool GetReward;
}
[System.Serializable]
public class LayerStruct
{
    [Tooltip("Цвет земли")]
    [ColorUsage(true)] public Color colors;
  
    public CurrentChance[] currencies = new CurrentChance[3];
}

public class Mineshaft : MonoBehaviour
{
    public event Action<GroundLayer> OnLayerSpawned = null;

    [SerializeField] private NumberObject m_layerDestructionProgress;
    [SerializeField] private NumberObject m_currentLayerNumber;
    [SerializeField] private Currency _currency;
    [SerializeField] public Currency[] currency;
    public AutomateProgress auto;

    [Space] [Header("Other")] [SerializeField]
    private GameProgressSave m_gameProgressSave;

    [SerializeField] private Rigidbody m_layersParent;
    [SerializeField] private float m_layersAscendTime;
    [SerializeField] private Dome m_dome;
    [SerializeField] private GetGemFrom gemsScript;
    [SerializeField] [Range(2, 16)] private int m_prespawnedLayerCount = 2;
    [Space] [SerializeField] private LayerGenerationSettings m_layerSettings;



    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(m_layersParent.transform.position.Sub(y: m_layerSettings.size.y / 2), m_layerSettings.size);
    }

    private readonly List<GroundLayer> m_spawnedLayers = new List<GroundLayer>();
    public int m_currentLayerIndex = 0;
    private int m_totalLayerCount = 0;
    private int m_totalLayersDestoryed = 0;
    public int m_startingIndex = 0;
    private Coroutine m_yTween;
    private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

    public Currency Currency => _currency;
    //[HideInInspector]
    public int[] mat = new int[100];
    RandomCheck rc;
    public Vector3 PlusPosForTakeGem;
    public static Vector3 posDamage;

    private List<RequiredGem> gems = new List<RequiredGem>() ;


    void GetRandom(int c)
    {
        for (int i = 0; i < mat.Length; i++)
        {
            mat[i] = 0;
        }
            if (m_layerSettings.colors[c].currencies.Length == 1)
        {
            rc = new RandomCheck(new int[] { m_layerSettings.colors[c].currencies[0].Chance });//задаем массив с шансами
        }
        else if (m_layerSettings.colors[c].currencies.Length == 2)
        {
            rc = new RandomCheck(new int[] { m_layerSettings.colors[c].currencies[0].Chance, m_layerSettings.colors[c].currencies[1].Chance });//задаем массив с шансами
        }
        else if (m_layerSettings.colors[c].currencies.Length == 3)
        {
            rc = new RandomCheck(new int[] { m_layerSettings.colors[c].currencies[0].Chance, m_layerSettings.colors[c].currencies[1].Chance, m_layerSettings.colors[c].currencies[2].Chance });//задаем массив с шансами
        }

        for (int i = 0; i < mat.Length; i++)
        {
            int ind = rc.Get();//берем случайный индекс
            if (ind > -1)//если индекс равен -1, значит закончились шансы
            {
                switch (ind)//заполняем элемент массива цветом
                {
                    case 0:
                        mat[i] = 1;
                        break;
                    case 1:
                        mat[i] = 2;
                        break;
                    case 2:
                        mat[i] = 3;
                        break;
                }
            }
        }
        RandomCheck.Shuffle(mat);
        //Debug.Log(c.ToString());
        //Debug.Log(m_layerSettings.colors[m_currentLayerIndex].currencies[mat[RandomCheck.GetRandomNumber(mat)]].IndxCurrency.ToString());
    }
    public List<RequiredGem> requiredGem()
    {
        //gems = null;
        
        bool found = false;
            foreach (CurrentChance current in m_layerSettings.colors[m_startingIndex].currencies)
            {
                if (current.GemAutomated )
                {
                  gems.Add(new RequiredGem { currencyType = current.IndxCurrency, GemRequired = current.GemRequired, GetReward = current.GetGemReward });
                    //return new RequiredGem { currencyType = current.IndxCurrency, GemRequired = current.GemRequired };
               }else if( current.GetGemReward == true)
            {
                gems.Add(new RequiredGem { currencyType = current.IndxCurrency, GemRequired = 0, GetReward = true });
            }
            }
            
        return gems;
            if(found == true)
        {
            return null;
        }
            else
        {
            return null;
        }
        
        

       
            
        }
    // return new RequiredGem { currencyType = m_layerSettings.colors[m_currentLayerIndex].currencies[i].IndxCurrency, GemRequired = m_layerSettings.colors[m_currentLayerIndex].currencies[i].GemRequired };

 // public RequiredGem requiredGold(CurrencyType type)
 // {
 //     bool found = false;
 //     foreach (CurrentChance current in m_layerSettings.colors[m_currentLayerIndex])
 //     {
 //         if (current.IndxCurrency == type && current.GemAutomated)
 //         {
 //
 //             return new RequiredGem { currencyType = current.IndxCurrency, GemRequired = current.GemRequired };
 //         }
 //
 //     }
 //     if (found == true)
 //     {
 //         return null;
 //     }
 //     else
 //     {
 //         return null;
 //     }
 //
 //
 //
 //
 //
 // }


    public void GetGem()
    {
      int x = RandomCheck.GetRandomNumber(mat);

      // if (x > 1)
      //  {
      //    x--;
      //   CurrencyType TypeCurrent = m_layerSettings.colors[m_currentLayerIndex].currencies[mat[x]].IndxCurrency;
      //    int count = m_layerSettings.colors[m_currentLayerIndex].currencies[mat[x]].Reward;
      //     gemsScript.AddGem(TypeCurrent, count);
      // }
      if (mat[x] != 0)
      {
            CurrencyType TypeCurrent = m_layerSettings.colors[m_currentLayerIndex].currencies[mat[x]-1].IndxCurrency;
            int count = m_layerSettings.colors[m_currentLayerIndex].currencies[mat[x]-1].Reward;
            Vector3 vec = posDamage + PlusPosForTakeGem;

            gemsScript.AddGem(TypeCurrent, count,vec);
            
           // Debug.Log(m_currentLayerIndex.ToString());
      }

    }

    public static void SetPos(Vector3 pos)
    {
        posDamage = pos;

       
    }
    private void Start()
    {
        m_layerSettings.material.EnableKeyword("_EMISSION");

        SaveSystem.onBeforeSave += SaveState;

        if (m_gameProgressSave.File.LayerHP > 0 
            && m_gameProgressSave.File.LayerIndex >= 0 
            && !Bootstrap.DoNewGame)
        {
            m_currentLayerIndex = m_gameProgressSave.File.LayerIndex;
            m_startingIndex = m_gameProgressSave.File.LayerIndex;
            m_totalLayersDestoryed = m_gameProgressSave.File.LayersDestoryed;
        }

        m_currentLayerNumber.Value = m_currentLayerIndex + 1;
        
        for (int i = m_startingIndex; i < m_startingIndex + m_prespawnedLayerCount; i++)
        {
            var newLayer = SpawnNextLayer();

            if (i == m_startingIndex)
            {
                AssignLayerEvents(newLayer);
                m_layerDestructionProgress.SetMinMax(0, newLayer.Health);

                if (m_gameProgressSave.File.LayerHP > 0 &&
                    !Bootstrap.DoNewGame) ApplyState(newLayer);

                newLayer.Vulnerable = true;
            }

            newLayer.onDestruction += OnLayerDestruction;
        }

        MoveLayers();
        GetRandom(m_startingIndex);

        SetEmmision(m_startingIndex);
    }

    private void SetEmmision(int layerIndex)
    {
        Color layerColor = m_layerSettings.EvaluateColor(layerIndex);
        if (layerColor.a > 0)
        {
            m_layerSettings.material.EnableKeyword("_EMISSION");
            m_layerSettings.material.SetColor(EmissionColor, layerColor.Mult(layerColor.a * 2));
        }
        else
        {
            m_layerSettings.material.SetColor(EmissionColor, Color.white.With(a: 0));
            m_layerSettings.material.DisableKeyword("_EMISSION");
        }
    }

    private void MoveLayers()
    {
        float highestLayersY = m_spawnedLayers.First().GetHighestSublayerYPosition();
        float y = (transform.position.y - highestLayersY) + m_layersParent.transform.position.y;

        IEnumerator Tween()
        {
            float fixedFramesCount = m_layersAscendTime / Time.fixedDeltaTime;
            Vector3 position = m_layersParent.transform.position;
            Vector3 direction = position.DirectionTo(position.With(y: y));
            Vector3 positionChange = direction / fixedFramesCount;

            for (int i = 1; i <= fixedFramesCount; i++)
            {
                m_layersParent.MovePosition(position + (positionChange * i));
                yield return new WaitForFixedUpdate();
            }

            m_yTween = null;
        }

        if (m_yTween != null) StopCoroutine(m_yTween);
        m_yTween = StartCoroutine(Tween());
    }

    private void ApplyState(GroundLayer layer)
    {
        layer.Health = m_gameProgressSave.File.LayerHP;
        layer.VoxelCount = m_gameProgressSave.File.VoxelsInLayerLeft;
        m_gameProgressSave.GetLayerState(layer.Voxels);
        layer.UpdateChunks();
    }

    private void OnDisable()
    {
        SaveState();
    }

    private void SaveState()
    {
        m_gameProgressSave.File.LastGameDateTime = DateTime.UtcNow.ToString();
        m_gameProgressSave.File.LayerIndex = m_currentLayerIndex - m_prespawnedLayerCount;

        GroundLayer firstLayer = m_spawnedLayers.FirstOrDefault();
        if (firstLayer == null)
            return;
        
        m_gameProgressSave.File.LayerHP = firstLayer.Health;
        m_gameProgressSave.File.VoxelsInLayerLeft = firstLayer.VoxelCount;
        m_gameProgressSave.SetLayerState(firstLayer.Voxels);
    }

    private void OnLayerDestruction(GroundLayer groundLayer)
    {
        m_spawnedLayers.Remove(groundLayer);
        var nextLayer = m_spawnedLayers.First();
        m_currentLayerNumber.Value = Mathf.Clamp(m_currentLayerIndex - (m_prespawnedLayerCount - 2), 1, int.MaxValue);
        AssignLayerEvents(nextLayer);

        MoveLayers();

        m_layerDestructionProgress.SetMinMax(0, nextLayer.InitialHealth);
        nextLayer.Vulnerable = true;

        m_totalLayersDestoryed++;
        m_gameProgressSave.layersDestroyed.Value = m_totalLayersDestoryed;

        var layer = SpawnNextLayer();
        layer.onDestruction += OnLayerDestruction;
        SetEmmision(m_currentLayerIndex - m_totalLayerCount);
        m_startingIndex++;
        GetRandom(m_startingIndex);
        //Debug.Log("Destroy Layer");

    }

    private void AssignLayerEvents(GroundLayer layer)
    {
        layer.onDamage += damage => m_layerDestructionProgress.Value = layer.Health ;
        
        layer.onVoxelRemove += (count, multiplier) =>
        {
            float value = (count * layer.OneVoxelPoints * multiplier);
            _currency.Value += value;
            m_gameProgressSave.superTap.Value += value;
            GetGem();
            
           
        };

        layer.onSublayerRemove += i => MoveLayers();

    }

    private GroundLayer SpawnNextLayer()
    {
        //FIRST LAYER CASE
        float m_currentLayerY = 0;
        if (m_spawnedLayers.Count == 0)
        {
            m_currentLayerY = 0 - m_layerSettings.size.y / 2;
        }
        else
        {
            var lastLayer = m_spawnedLayers.Last();
            m_currentLayerY = lastLayer.transform.localPosition.y - lastLayer.Size.y / 2;
            m_currentLayerY -= m_layerSettings.size.y / 2;
        }

        var newLayer = GroundLayer.Create(m_layersParent.transform, m_layerSettings,
            m_startingIndex + m_totalLayerCount);

        m_currentLayerIndex++;
        newLayer.gameObject.name = "Layer_" + ++m_totalLayerCount;
        newLayer.gameObject.SetActive(true);
        newLayer.transform.localPosition = new Vector3(0, m_currentLayerY, 0);

        m_spawnedLayers.Add(newLayer);

        OnLayerSpawned?.Invoke(newLayer);
        //Debug.Log("SpawnLayer");
        return newLayer;
        
    }
}