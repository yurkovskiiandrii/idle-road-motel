﻿using System;
using System.Collections.Generic;
using Destructables;
using UnityEngine;

namespace Voxelization
{
    [RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider))]
    public class Chunk : MonoBehaviour, IDamageable
    {
        [SerializeField] private MeshRenderer m_renderer;
        [SerializeField] private MeshFilter m_filter;
        [SerializeField] private MeshCollider m_collider;
        [SerializeField] private Vector3 m_scale;
        [SerializeField] private Vector3 m_min;
        [SerializeField] private Vector3Int m_posInMesh;
        [SerializeField] private int m_size;

        private readonly List<Vector3> m_vertices = new List<Vector3>();
        private readonly List<int> m_triangles = new List<int>();
        private readonly List<Vector2> m_uvs = new List<Vector2>();
        private float m_uMin;
        private float m_uMax;
        private float m_vMin;
        private float m_vMax;
        private int m_voxelsCount;

        public Func<Damage, bool> onDamage;
        
        public MeshRenderer Renderer => m_renderer;
        public Vector3Int PositionInMesh => m_posInMesh;
        public Vector3Int Max => ToGlobalPosition(Vector3Int.one * (m_size - 1));
        public Vector3Int Min => ToGlobalPosition(Vector3Int.zero);
        public int VoxelsCount => m_voxelsCount;
        
        public bool TakeDamage(Damage damage)
        {
            bool? value = onDamage?.Invoke(damage);
            if (value.HasValue) return (bool) value;
            return false;
        }

        public static Chunk Create(Vector3Int posInMesh, int size, Vector2Int atlasSize, Vector2Int uvPos, Vector3 min, Vector3 scale, Transform parent)
        {
            Chunk chunk = new GameObject($"{posInMesh.x}_{posInMesh.y}_{posInMesh.z}_chunk").AddComponent<Chunk>();

            chunk.m_filter = chunk.GetComponent<MeshFilter>();
            chunk.m_filter.mesh = new Mesh();
            chunk.m_collider = chunk.GetComponent<MeshCollider>();
            chunk.m_renderer = chunk.GetComponent<MeshRenderer>();
            chunk.m_collider.sharedMesh = chunk.m_filter.mesh;

            chunk.m_scale = scale;
            chunk.m_size = size;
            chunk.m_posInMesh = posInMesh;
            
            chunk.m_min = min + Vector3.Scale((posInMesh * size), scale);
            chunk.transform.SetParent(parent);
            chunk.transform.localScale = Vector3.one;
            chunk.transform.localPosition = Vector3.zero;
            Vector2 tilePercent = new Vector2(1 / (float)atlasSize.x, 1 / (float)atlasSize.y);

            chunk.m_uMin = tilePercent.x * uvPos.x;
            chunk.m_uMax = tilePercent.x * (uvPos.x + 1);
            chunk.m_vMin = tilePercent.y * uvPos.y;
            chunk.m_vMax = tilePercent.y * (uvPos.y + 1);

            return chunk;
        }

        public Vector3Int ToGlobalPosition(int localX, int localY, int localZ) =>
            ToGlobalPosition(new Vector3Int(localX, localY, localZ));
        public Vector3Int ToGlobalPosition(Vector3Int localPosition) => m_posInMesh * m_size + localPosition;

        public Mesh UpdateMesh(Voxel[,,] voxels)
        {
            m_voxelsCount = 0;
            m_vertices.Clear();
            m_triangles.Clear();
            m_uvs.Clear();

            int xSize = voxels.GetLength(0);
            int ySize = voxels.GetLength(1);
            int zSize = voxels.GetLength(2);
            
            for (int localZ = 0; localZ < m_size; localZ++)
            {
                for (int localY = 0; localY < m_size; localY++)
                {
                    for (int localX = 0; localX < m_size; localX++)
                    {
                        Vector3Int global = ToGlobalPosition(localX, localY, localZ);
                        
                        if (voxels[global.x, global.y, global.z].type == 0) continue;

                        m_voxelsCount++;
                        
                        Vector3 pos = m_min + new Vector3(localX * m_scale.x, localY * m_scale.y, localZ * m_scale.z);

                        if (global.y == ySize - 1 || voxels[global.x, global.y + 1, global.z].type == 0)
                            AddTopFace(pos);
                        
                        if (global.z == 0 || voxels[global.x, global.y, global.z - 1].type == 0)
                            AddBackFace(pos);

                        if (global.x == xSize - 1 || voxels[global.x + 1, global.y, global.z].type == 0)
                            AddRightFace(pos);
                        
                        if (global.z == zSize - 1 || voxels[global.x, global.y, global.z + 1].type == 0)
                            AddFrontFace(pos);

                        if (global.x == 0 || voxels[global.x - 1, global.y, global.z].type == 0)
                            AddLeftFace(pos);
                        
                        if (global.y == 0 || voxels[global.x, global.y - 1, global.z].type == 0)
                            AddBottomFace(pos);
                    }
                }
            }
            
            if(m_vertices.Count > 65000)
            {
                Debug.Log("Mesh has too many verts");
                return null;
            }
            
            Mesh mesh = m_filter.mesh;
            mesh.Clear();
            
            mesh.SetVertices(m_vertices);
            mesh.SetTriangles(m_triangles, 0);
            mesh.SetUVs(0, m_uvs);
            mesh.RecalculateNormals();

            m_collider.sharedMesh = mesh;

            return mesh;
        }
        
        private void AddTopFace(Vector3 pos)
        {
            AddTriangles();
            
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 1 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 1 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 1 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 1 * m_scale.y, 0 * m_scale.z));

            AddUVs();
        }
        
        //south
        private void AddBackFace(Vector3 pos)
        {
            AddTriangles();
            
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 0 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 1 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 1 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 0 * m_scale.y, 0 * m_scale.z));
            
            AddUVs();
        }
        
        private void AddRightFace(Vector3 pos)
        {
            AddTriangles();
            
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 0 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 1 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 1 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 0 * m_scale.y, 1 * m_scale.z));
            
            AddUVs();
        }
        
        //north
        private void AddFrontFace(Vector3 pos)
        {
            AddTriangles();
            
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 0 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 0 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 1 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 1 * m_scale.y, 1 * m_scale.z));
            
            AddUVs();
        }

        private void AddLeftFace(Vector3 pos)
        {
            AddTriangles();
            
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 0 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 1 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 1 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 0 * m_scale.y, 0 * m_scale.z));
            
            AddUVs();
        }
        
        private void AddBottomFace(Vector3 pos)
        {
            AddTriangles();
            
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 0 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 0 * m_scale.y, 0 * m_scale.z));
            m_vertices.Add(pos + new Vector3(1 * m_scale.x, 0 * m_scale.y, 1 * m_scale.z));
            m_vertices.Add(pos + new Vector3(0 * m_scale.x, 0 * m_scale.y, 1 * m_scale.z));
            
            AddUVs();
        }

        private void AddTriangles()
        {
            int count = m_vertices.Count;
            m_triangles.Add(count); 
            m_triangles.Add(count + 1); 
            m_triangles.Add(count + 2);
            m_triangles.Add(count + 2); 
            m_triangles.Add(count + 3); 
            m_triangles.Add(count);
        }

        private void AddUVs()
        {
            m_uvs.Add(new Vector2(m_uMin, m_vMin));
            m_uvs.Add(new Vector2(m_uMin, m_vMax));
            m_uvs.Add(new Vector2(m_uMax, m_vMax));
            m_uvs.Add(new Vector2(m_uMax, m_vMin));
        }
    }
}