﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameEnvironment;
using GameEnvironment.Shafts;
using Voxelization;
using UnityEngine;
using FXs;
using nickeltin.Extensions;
using UI;


namespace Destructables
{
    public class GroundLayer : MonoBehaviour, IDamageable
    {
        [SerializeField] private float m_totalPoints;
        [SerializeField] private Color m_color;
        
        private readonly Queue<Vector3Int> m_toExplore = new Queue<Vector3Int>();
        private readonly List<Vector3Int> m_explored = new List<Vector3Int>();
        private readonly List<Vector3Int> m_toRemove = new List<Vector3Int>();
        private readonly List<Vector3Int> m_removed = new List<Vector3Int>();
        private readonly HashSet<int> m_heights = new HashSet<int>();
        private float m_accumulatedDamage;
        private bool[] m_sublayersStates;
        private ParticleSystem m_collision;
        private ParticleSystem m_criticalCollision;
        private float m_lastCollisionTime;
        private Coroutine m_voxelDestroyer;
        private GroundLayerGenerator m_generator;
        private Transform m_chunkParent;

        public event Action<GroundLayer> onDestruction;
        public event Action<Damage> onDamage;
        public delegate void VoxelRemove(int voxelCount, float profitMultiplier);
        public event VoxelRemove onVoxelRemove;
        public event Action<int> onSublayerRemove;

        public Color Color => m_color;
        public Voxel[,,] Voxels => m_generator.Voxels;
        public float InitialHealth { get; private set; }
        public Vector3 Size => m_generator.MeshSize.Mult(transform.localScale);
        public float OneVoxelPoints { get; private set; }
        private float OneVoxelHealth => Health / VoxelCount;

        public bool Vulnerable = false;
        public float Health;
        public int VoxelCount;


        public static GroundLayer Create(Transform parent, LayerGenerationSettings settings, int layerIndex)
        {
            return Create(parent, settings.points.Evaluate(layerIndex), settings.health.Evaluate(layerIndex),
                settings.EvaluateColor(layerIndex).With(a: 1), settings.size, settings.chunkSize, settings.sizeInChunks,
                settings.material, settings.atlasSizeInTextures, settings.EvaluateTextureCords(layerIndex));
        }
        
        public static GroundLayer Create(Transform parent, float points, float health, Color color, Vector3 worldSpaceSize, int chunkSize, Vector3Int sizeInChunks, 
            Material material, Vector2Int atlasSize, Vector2Int texturePosition)
        {
            GroundLayer layer = new GameObject("layer").AddComponent<GroundLayer>();
            layer.transform.SetParent(parent);
            layer.Health = health;
            layer.m_totalPoints = points;
            layer.m_color = color;
            Transform t = new GameObject("chunks").transform;
            t.SetParent(layer.transform);
            layer.m_chunkParent = t;
            
            layer.m_generator = new GroundLayerGenerator(VoxelType.Full, atlasSize, texturePosition, t, worldSpaceSize, chunkSize, 
                sizeInChunks, material, layer.TakeDamage);
            
            layer.m_generator.GenerateMesh();
            
            layer.m_sublayersStates = new bool[layer.m_generator.Voxels.GetLength(1)];
            for (int i = 0; i < layer.m_sublayersStates.Length; i++) layer.m_sublayersStates[i] = true;

            layer.CalculateInitialData();
            
            layer.m_collision = Instantiate(VFX.LayerCollision, layer.transform);
            layer.m_criticalCollision = Instantiate(VFX.CriticalLayerCollision, layer.transform);

            return layer;
        }

        public void CalculateInitialData()
        {
            InitialHealth = Health;
            VoxelCount = m_generator.VoxelsCount;
            OneVoxelPoints = m_totalPoints / VoxelCount;
            m_accumulatedDamage = 0;
        }

        public void UpdateChunks()
        {
            m_generator.UpdateChunks();
            List<int> ys = new List<int>();
            for (int y = 0; y < m_generator.Voxels.GetLength(1); y++) ys.Add(y);
            CheckIsSublayersDestroyed(ys);
        }

        public int GetVoxelsCountFromDamage(float damage)
        {
            float totalDamage = damage + m_accumulatedDamage;
            int voxelsToRemoveCount = Mathf.FloorToInt(totalDamage / OneVoxelHealth);
            if (voxelsToRemoveCount > 0) m_accumulatedDamage = totalDamage % OneVoxelHealth;
            else m_accumulatedDamage += damage;
            return voxelsToRemoveCount;
        }

        public bool TakeDamage(Damage damage)
        {
            if (!Vulnerable || m_voxelDestroyer != null || Health <= 0
               || Time.time - m_lastCollisionTime < MineManager.LayerMinIntervalBetweenCollisions)
                return false;

            m_lastCollisionTime = Time.time;
            
            onDamage?.Invoke(damage);
            Health -= damage.Value;
            Mineshaft.SetPos(damage.Position);
            if (damage.IsCritical) VFX.Play(m_criticalCollision, damage.Position, m_color);
            else VFX.Play(m_collision, damage.Position, m_color);
            
            PopupText.Popup(damage);

            //CLEARING OLD DATA
            m_toExplore.Clear(); m_explored.Clear(); m_toRemove.Clear(); m_removed.Clear(); m_heights.Clear();

            int voxelsToRemoveCount = GetVoxelsCountFromDamage(damage.Value);
            int removedVoxelsCount = 0;

            Vector3 transformedPosition = transform.position + transform.InverseTransformPoint(damage.Position);
            if (m_generator.WorldPositionToMeshPosition(transformedPosition, out Vector3Int mp)) m_toRemove.Add(mp);
            m_toExplore.Enqueue(mp);
            
            bool InspectNeighbour(Vector3Int neighbourOffset, Vector3Int target)
            {
                if(m_removed.Count + m_toRemove.Count >= voxelsToRemoveCount) return true;
                if (m_generator.TryGetNeighbourVoxel(target, neighbourOffset, out Voxel neighbour))
                {
                    if (neighbour.type >= 1)
                    {
                        Vector3Int neighbourPos = target + neighbourOffset;
                        if (!m_explored.Contains(neighbourPos)) m_toExplore.Enqueue(neighbourPos);
                        if (!m_removed.Contains(neighbourPos) && !m_toRemove.Contains(neighbourPos))
                        {
                            m_toRemove.Add(neighbourPos);
                        }
                    }
                }
                return false;
            }
            
            IEnumerator FloodFill()
            {
                int iterations = 0;
                while (m_toExplore.Count > 0 
                       && m_removed.Count < voxelsToRemoveCount 
                       && iterations < MineManager.LayerDestructionMaxIterations)
                {
                    var beingExplored = m_toExplore.Dequeue();
                    m_explored.Add(beingExplored);
                    
                    for (int z = -1; z <= 1; z++) for (int y = 1; y >= -1; y--) for (int x = -1; x <= 1; x++)
                    {
                        iterations++;
                        
                        if(iterations % MineManager.LayerDestructionAsyncRate == 0) yield return null;
                        
                        if (iterations >= MineManager.LayerDestructionMaxIterations - 1 ||
                            InspectNeighbour(new Vector3Int(x, y, z), beingExplored)) goto END;
                    }

                    END:
                
                    int removedVoxels = m_generator.RemoveVoxels(m_toRemove);
                    VoxelCount -= removedVoxels;
                    removedVoxelsCount += removedVoxels;
                    m_removed.AddRange(m_toRemove);
                    m_toRemove.Clear();
                }

                if (voxelsToRemoveCount > 0)
                {
                    foreach (var position in m_removed) m_heights.Add(position.y);
                    CheckIsSublayersDestroyed(m_heights);
                    onVoxelRemove?.Invoke(removedVoxelsCount, damage.ProfitMultiplier);
                }
                
                if (VoxelCount == 0 || Health <= 0) Destroy(damage);
                m_voxelDestroyer = null;
            }

            m_voxelDestroyer = StartCoroutine(FloodFill());
            return true;
        }
        private void CheckIsSublayersDestroyed(IEnumerable<int> destroyedHeights)
        {
            foreach (var y in destroyedHeights)
            {
                for (int x = 0; x < m_generator.Voxels.GetLength(0); x++)
                    for (int z = 0; z < m_generator.Voxels.GetLength(2); z++)
                        if (m_generator.Voxels[x, y, z].type >= 1)
                            goto NEXT_HEIGHT;

                m_sublayersStates[y] = false;
                onSublayerRemove?.Invoke(y);
                
                NEXT_HEIGHT:;
            }
        }
        private void Destroy(Damage damage)
        {
            onDestruction?.Invoke(this);
            m_chunkParent.gameObject.SetActive(false);
            VFX.Play(m_collision, m_chunkParent.position, m_color);
            Destroy(gameObject,2);
        }

        public float GetHighestSublayerYPosition()
        {
            int highest = m_sublayersStates.Length - 1;

            for (int y = m_sublayersStates.Length - 1; y >= 0; y--)
            {
                if (m_sublayersStates[y])
                {
                    highest = y;
                    break;
                }
            }

            Vector3 top = m_generator.MeshPositionToWorldPosition(new Vector3Int(0, highest, 0));
            float result = transform.TransformPoint(transform.position.DirectionTo(top) + m_generator.VoxelSize).y;
            return result;
        }
    }
}
