﻿using System;
using System.Collections;
using System.Collections.Generic;
using CameraControlling;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.Saving;
using nickeltin.Events;
using nickeltin.StateMachine;
using UnityEngine;
using UI;
using Managers;

namespace GameEnvironment
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class TimeCycle : MonoSaveable
    {
        [Serializable]
        public enum TimesOfDay
        {
            Day,
            Night
        }

        [Serializable]
        private class SaveFile
        {
            public float time = 0;
            public TimesOfDay timeOfDay = TimesOfDay.Day;
        }

        [Header("Time info")]
        [SerializeField, Tooltip("Day/Night light transition length")]
        public float transitionDuration = 5f;

        [SerializeField, ReadOnly]
        private TimesOfDay m_timeOfDay;
        [SerializeField, ReadOnly]
        private float m_time;

        [SerializeField, ReadOnly, Tooltip("Current time Hour|Min|AM/PM format")]
        private string formattedTime;

        [Header("sunset/sunrise time")]
        [SerializeField, Tooltip("In seconds")]
        private float timeChangePeriod = 9f;

        [Header("Time settings")]
        [SerializeField, Tooltip("In seconds")]
        private float m_dayLength = 240;

        [SerializeField, Tooltip("In seconds")]
        private float m_nightLength = 240;

        [Header("Sun settings")]
        [SerializeField]
        public float SunMaxShiness;

        [SerializeField]
        public float SunMinShiness;
        [SerializeField, ReadOnly]
        public float SunCurrentShiness;
        [SerializeField]
        private Color SunDayColor;
        [SerializeField]
        private Color SunNightColor;

        [Header("Components")]
        [SerializeField, Tooltip("Scene light")]
        private Light Sun;

        [SerializeField]
        private Material m_nightMaterial;
        [SerializeField]
        private Material m_dayMaterial;

        [SerializeField]
        private List<GameObject> m_enableOnDay;
        [SerializeField]
        private List<GameObject> m_enableOnNight;

        private Coroutine m_cycle;
        private StateMachine m_dayState;
        private float m_maxTime;
        private TimesOfDay m_nextTimeOfDay;
        private Clock clockUI;
        private GameController gameController;
        public static TimesOfDay timeOfDay { get; private set; }
        public static event Action<TimesOfDay> onTimeChanged;

        public override object Save()
        {
            return new SaveFile
            {
                time = m_time,
                timeOfDay = m_timeOfDay
            };
        }

        public override void Load(object obj)
        {
            var save = obj != null ? (SaveFile)obj : new SaveFile();
            m_time = save.time;
            m_timeOfDay = save.timeOfDay;
        }

        private void Start()
        {
            Sun.intensity = SunMinShiness;

            gameController = GameController.Instance.GetComponent<GameController>();
            clockUI = gameController.clockUI;
            m_enableOnDay.ForEach(obj => obj.SetActive(false));
            m_enableOnNight.ForEach(obj => obj.SetActive(false));
            SunDayColor = Sun.color;
            m_dayState = new StateMachine(transform, false, false,
                new State(TimesOfDay.Day, onStateStart: () =>
                {
                    m_enableOnDay.ForEach(obj => obj.SetActive(true));
                    m_maxTime = m_dayLength;
                    m_timeOfDay = timeOfDay = TimesOfDay.Day;
                    m_nextTimeOfDay = TimesOfDay.Night;
                    gameController.m_DayChangesToNightEvent.Invoke(false);
                    return true;
                }, onStateEnd: () =>
                {
                    m_enableOnDay.ForEach(obj => obj.SetActive(false));
                    return true;
                }),
                new State(TimesOfDay.Night, onStateStart: () =>
                {
                    m_enableOnNight.ForEach(obj => obj.SetActive(true));
                    m_maxTime = m_nightLength;
                    m_timeOfDay = timeOfDay = TimesOfDay.Night;
                    m_nextTimeOfDay = TimesOfDay.Day;
                    gameController.m_DayChangesToNightEvent.Invoke(true);
                    return true;
                }, onStateEnd: () =>
                {
                    m_enableOnNight.ForEach(obj => obj.SetActive(false));
                    return true;
                }));
            StartCycle(m_time, m_timeOfDay);
        }

        private Coroutine StartCycle(float currentTime, TimesOfDay type)
        {
            IEnumerator Cycle()
            {
                m_dayState.SwitchState(type);
                onTimeChanged?.Invoke(type);
                for (float t = currentTime; t < m_maxTime; t += Time.deltaTime)
                {
                    m_time = t;
                    UpdateTimeDisplay(m_time);
                    if ((int)t == (int)((timeChangePeriod * m_maxTime / 24f) * 9f) && type == TimesOfDay.Day)
                        StartCoroutine(SmoothDayNightTransition(TimesOfDay.Night));
                    else if ((int)t == (int)((timeChangePeriod * m_maxTime / 24f) * 9f) && type == TimesOfDay.Night)
                        StartCoroutine(SmoothDayNightTransition(TimesOfDay.Day));
                    yield return null;
                }

                StartCycle(0, m_nextTimeOfDay);
            }

            return StartCoroutine(Cycle());
        }


        IEnumerator SmoothDayNightTransition(TimesOfDay type)
        {
            float elapsedTime = 0.0f;
            float startValue = 1f;
            float endValue = 0.0f;
            if (type == TimesOfDay.Day)
            {
                startValue = SunMinShiness;
                endValue = SunMaxShiness;
                Sun.color = SunDayColor;
            }
            else if (type == TimesOfDay.Night)
            {
                startValue = SunMaxShiness;
                endValue = SunMinShiness;
                Sun.color = SunNightColor;
            }
            else
            {
                Debug.LogError("Day cycle error");
            }

            while (elapsedTime < transitionDuration)
            {
                float t = elapsedTime / transitionDuration;
                SunCurrentShiness = Mathf.Lerp(startValue, endValue, t);
                Sun.intensity = SunCurrentShiness;

                elapsedTime += Time.deltaTime;
                yield return null;
            }

            SunCurrentShiness = Sun.intensity;
            Sun.intensity = endValue;
        }

        private void UpdateTimeDisplay(float time)
        {
            string amPm = "AM";
            if (m_timeOfDay == TimesOfDay.Night)
            {
                amPm = "PM";
            }

            int hours = 9 + Mathf.FloorToInt((time / (m_maxTime / 12))) % 12;
            if (hours >= 12)
            {
                hours -= 12;
                amPm = (amPm == "AM") ? "PM" : "AM";
            }
            if (hours == 0) hours = 12;

            int minutes = Mathf.FloorToInt((time / (m_maxTime / 720))) % 60;
            formattedTime = string.Format("{0:D2}:{1:D2} {2}", hours, minutes, amPm);
            ClockController(formattedTime, m_timeOfDay == TimesOfDay.Day);
        }

        public void ClockController(string time, bool check)
        {
            clockUI.m_TimeUpdate.Invoke(time, check);
        }
    }

}