﻿using System;
using nickeltin.StateMachine;
using UnityEngine;
using UnityEngine.Events;

namespace GameEnvironment
{
    [Serializable]
    public class Weather : State<Weather.WeatherType>
    {
        [Serializable]
        public enum WeatherType
        {
            Clear, Rain
        }

        [SerializeField, Tooltip("In seconds")] private float m_duration;
        [SerializeField] private UnityEvent m_onEnable;
        [SerializeField] private UnityEvent m_onDisable;

        public float Duration => m_duration;

        public override bool OnStateStart()
        {
            base.OnStateStart();
            m_onEnable.Invoke();
            return true;
        }

        public override bool OnStateEnd()
        {
            base.OnStateEnd();
            m_onDisable.Invoke();
            return true;
        }
    }
}