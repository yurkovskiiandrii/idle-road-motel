﻿using UnityEngine;
using UnityEngine.Events;

namespace GameEnvironment
{
    public class OnTimeOfDayChange : MonoBehaviour
    {
        [SerializeField] private UnityEvent onDay;
        [SerializeField] private UnityEvent onNight;
        
        private void OnTimeChanged(TimeCycle.TimesOfDay time)
        {
            if (time == TimeCycle.TimesOfDay.Day) onDay?.Invoke();
            else if (time == TimeCycle.TimesOfDay.Night) onNight?.Invoke();
        }
        
        private void OnEnable() => TimeCycle.onTimeChanged += OnTimeChanged;

        private void OnDisable() => TimeCycle.onTimeChanged -= OnTimeChanged;
    }
}