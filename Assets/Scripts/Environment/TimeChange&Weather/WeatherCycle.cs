﻿using System;
using System.Collections;
using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.Saving;
using nickeltin.StateMachine;
using UnityEngine;

namespace GameEnvironment
{
    public class WeatherCycle : MonoSaveable
    {
        [Serializable]
        private class SaveFile
        {
            public float time = 0;
            public Weather.WeatherType weatherType = Weather.WeatherType.Clear;
        }
        
        [SerializeField, ReadOnly] private Weather.WeatherType m_currentWeather;
        [SerializeField, ReadOnly] private float m_time;
        
        [SerializeField] private Weather m_clearWeather;
        [SerializeField] private Weather m_rainWeather;

        private StateMachine<Weather> m_weatherStates;

        public StateMachineBase StateMachine => m_weatherStates;
        
        public override object Save()
        {
            return new SaveFile
            {
                time = m_time,
                weatherType = m_currentWeather
            };
        }

        public override void Load(object obj)
        {
            var save = obj != null ? (SaveFile) obj : new SaveFile();
            m_time = save.time;
            m_currentWeather = save.weatherType;
        }
        
        private void Start()
        {
            m_weatherStates = new StateMachine<Weather>(transform, false, false, 
                m_clearWeather, m_rainWeather);
            
            m_clearWeather.Override(onStateStart: () =>
            {
                m_currentWeather = m_clearWeather.explicitType;
                return true;
            });
            
            m_rainWeather.Override(onStateStart: () =>
            {
                m_currentWeather = m_rainWeather.explicitType;
                return true;
            });
            
            StartCycle(m_time, m_currentWeather);
        }
        
        private Coroutine StartCycle(float currentTime, Weather.WeatherType type)
        {
            IEnumerator Cycle()
            {
                m_weatherStates.SwitchState(type);
                float maxTime = m_weatherStates.CurrentState.Duration;
                for (float t = currentTime; t < maxTime; t += Time.deltaTime)
                {
                    m_time = t;
                    yield return null;
                }
                
                StartCycle(0, m_weatherStates.GetNextState().explicitType);
            }

            return StartCoroutine(Cycle());
        }
    }
}