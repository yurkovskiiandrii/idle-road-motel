using UnityEngine;
using Managers;
using System.Collections.Generic;

public class MaterialChangeController : MonoBehaviour
{
    [SerializeField] private Material m_dayMaterial; //Day material that would be put on day
    [SerializeField] private string nightMaterialName = "NightMaterial"; //night material name
    private Dictionary<Renderer, Material[]> originalMaterialsDict = new Dictionary<Renderer, Material[]>();

    private void Awake()
    {
        Renderer[] childRenderers = GetComponentsInChildren<Renderer>();

        foreach (Renderer renderer in childRenderers)
        {
            Material[] materials = renderer.materials;
            originalMaterialsDict.Add(renderer, materials);
        }

        GameController.Instance.m_DayChangesToNightEvent.AddListener(ChangeMaterialsOnTime);
    }

    public void ChangeMaterialsOnTime(bool DayTime)
    {
        foreach (var entry in originalMaterialsDict)
        {
            Renderer renderer = entry.Key;
            Material[] originalMaterials = entry.Value;

            if (DayTime)
            {
                renderer.materials = originalMaterials;
            }
            else
            {
                for (int i = 0; i < originalMaterials.Length; i++)
                {
                    if (originalMaterials[i].name.Contains(nightMaterialName))
                    {
                        Material[] newMaterials = originalMaterials.Clone() as Material[];
                        newMaterials[i] = m_dayMaterial;
                        renderer.materials = newMaterials;
                        //Debug.Log("CHANGED to DAY");
                    }
                }
            }
        }
    }
}