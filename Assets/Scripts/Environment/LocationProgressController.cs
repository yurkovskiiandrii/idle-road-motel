using nickeltin.Singletons;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Managers/LocationProgressController", fileName = "LocationProgressController")]
public class LocationProgressController : SOSingleton<LocationProgressController>
{
    [SerializeField] float currentProgress = 0.1f;
    [SerializeField] LocationProgressContainer progressContainer;
    [SerializeField] RequirementsList customersRequirements;

    [SerializeField] LocationStats currentLocationStats;

    Action<LocationStats> onProgressChanged;

    public override bool Initialize()
    {
        if (base.Initialize())
        {
            onProgressChanged = null;
            currentLocationStats = progressContainer.progressions.Last(x => x.locationProgress <= currentProgress).locationStats;
            return true;
        }

         return false;
    }

    public void SubscribeOnProgressChanged(Action<LocationStats> action)
    {
        if(onProgressChanged == null)
        {
            onProgressChanged = action;
            return;
        }

        onProgressChanged += action;
    }

    public LocationStats GetLocationStats() => currentLocationStats;

    public CustomerLiveData GetUserRequirements(bool isChild = true)
    {
        var customerLiveData = new CustomerLiveData();

        var requirements = new List<CustomerInGameRequirement>();

        var availableReq = customersRequirements.GetRequirements().Where(x => x.Tier <= currentLocationStats.maxStatTier).ToList();

        if (isChild)
            availableReq.RemoveAll(x => x.ForKids != 1);

        int maxStats = currentLocationStats.maxStats;
        var reqValue = UnityEngine.Random.Range(currentLocationStats.clientStatsValueMin, currentLocationStats.clientStatsValueMax);

        while(reqValue > 0 && maxStats > 0)
        {
            for (int i = 0; i < availableReq.Count(); ++i)
            {
                if (availableReq[i].Value > reqValue)
                {
                    availableReq.RemoveAt(i);
                    --i;
                }
            }

            if (availableReq.Count() == 0)
                break;

            var requirementIndex = UnityEngine.Random.Range(0, availableReq.Count());

            if(reqValue >= availableReq[requirementIndex].Value)
            {
                reqValue -= (int)availableReq[requirementIndex].Value;
                requirements.Add(new CustomerInGameRequirement(availableReq[requirementIndex]));
                maxStats--;
            }

            availableReq.RemoveAt(requirementIndex);
        }

        var statBarMultiplier = UnityEngine.Random.Range(currentLocationStats.statBarMultiplierMin, currentLocationStats.statBarMultiplierMax);

        foreach (var requirement in requirements) 
        {
            requirement.requirementPropsLevel = UnityEngine.Random.Range(currentLocationStats.propLevelMin, currentLocationStats.propLevelMax + 1);
            requirement.isObligatory = UnityEngine.Random.Range(0, 1f) < requirement.Obligatory;
            requirement.StatBarCapacity *= (int)statBarMultiplier;
        }

        customerLiveData.requirements = requirements;
        customerLiveData.minPayment = currentLocationStats.clientBasePaymentMin;
        customerLiveData.maxPayment = currentLocationStats.clientBasePaymentMax;
        customerLiveData.leaveRankChance = currentLocationStats.leaveRankChance;
        customerLiveData.leaveRankBorder = currentLocationStats.leaveRankBorder;

        return customerLiveData;
    }

    public float GetBasePayment() => UnityEngine.Random.Range(currentLocationStats.clientBasePaymentMin, currentLocationStats.clientBasePaymentMax);
}
