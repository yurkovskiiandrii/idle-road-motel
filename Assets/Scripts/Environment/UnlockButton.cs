﻿using System;
using CameraControlling;
using GameData.DataObjects;
using nickeltin.GameData.DataObjects;
using nickeltin.Tweening;
using Other;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameEnvironment
{
    [RequireComponent(typeof(Canvas))]
    public class UnlockButton : MonoBehaviour
    {
        public enum InteractionVisual
        {
            SpriteSwap, ColorChange
        }
        
        [SerializeField] private Button m_btn;
        [SerializeField] private TMP_Text m_price;
        [SerializeField] private string m_postfix = "cPts.";
        [SerializeField] private NumberObject m_currency;
        [Space] 
        [SerializeField] private InteractionVisual m_interactionVisuals = InteractionVisual.ColorChange;
        [Space]
        [Header("Color change")]
        [SerializeField] private Color m_interactableColor;
        [SerializeField] private Color m_uninteractableColor;
        [Header("Sprite swap")]
        [SerializeField] private Sprite m_interactableSprite;
        [SerializeField] private Sprite m_uninteractableSprite;
        [Space] 
        [SerializeField] private UnityEvent m_onUnlock;
        

        private IBuyable m_source;
        private Canvas m_canvas;
        private Vector3 m_defaultScale;
        private RectTransform m_rect;
        private bool m_disabled;

        public event Action onUnlock;

        public float Price
        {
            get
            {
                if (m_source == null) return 0;
                return m_source.Price;
            }
        }

        public void Disable(string message)
        {
            m_disabled = true;
            GetComponents();
            OnDisable();
            onUnlock = null;
            m_currency = null;
            m_price.text = message;
            
            
            if (m_interactionVisuals.Equals(InteractionVisual.ColorChange))
            {
                m_btn.targetGraphic.color = m_uninteractableColor;
            }
            else if (m_interactionVisuals.Equals(InteractionVisual.SpriteSwap))
            {
                if (m_btn.targetGraphic is Image img) img.sprite = m_uninteractableSprite;
            }
        }

        public void Initialize(IBuyable s)
        {
            GetComponents();
            m_btn.onClick.RemoveAllListeners();
            m_btn.onClick.AddListener(() =>
            {
                if (!m_disabled && m_source.Price <= m_currency.Value)
                {
                    m_onUnlock?.Invoke();
                    onUnlock?.Invoke();
                }
            });
            m_defaultScale = transform.localScale;
            m_source = s;
            m_price.text = s.Price.ToString("F0") + m_postfix;
            m_canvas.worldCamera = CameraController.uiCamera;
            
            OnCPointsChanged(m_currency.Value);
        }

        private void GetComponents()
        {
            if (m_canvas == null)
            {
                m_canvas = GetComponent<Canvas>();
                m_rect = m_canvas.GetComponent<RectTransform>();
            }
        }
        
        private void OnCPointsChanged(float cp)
        {
            if(m_disabled) return;

            switch (m_interactionVisuals)
            {
                case InteractionVisual.SpriteSwap:
                    if (m_btn.targetGraphic is Image img)
                    {
                        if (Price <= m_currency.Value) img.sprite = m_interactableSprite;
                        else img.sprite = m_uninteractableSprite;
                    }
                    break;
                case InteractionVisual.ColorChange:
                    if (Price <= m_currency.Value) m_btn.targetGraphic.color = m_interactableColor;
                    else m_btn.targetGraphic.color = m_uninteractableColor;
                    break;
            }
        }

        public void FadeInOut(bool fadeIn, float t)
        {
            LeanTween.cancel(m_rect);

            if (fadeIn) LeanTween.scale(m_rect, m_defaultScale, t);
            else LeanTween.scale(m_rect, Vector3.zero, t);
        }

        private void OnEnable()
        {
            if (m_currency != null) m_currency.onValueChanged += OnCPointsChanged;
        }

        private void OnDisable()
        {
            if (m_currency != null) m_currency.onValueChanged -= OnCPointsChanged;
        }
    }
}