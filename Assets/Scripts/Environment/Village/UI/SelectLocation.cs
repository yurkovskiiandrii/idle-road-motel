using nickeltin.Editor.Attributes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Cinemachine;

public class SelectLocation : SelectableArea
{
    [SerializeField] CameraOrtoSize serviceCameraSize;
    [SerializeField] bool UseCamera;
    [SerializeField] private CinemachineVirtualCamera _serviceCamera;
    public UnityEvent OnLoad;

    protected override void Show()
    {
        if (_serviceCamera != null)
        {
            _serviceCamera.gameObject.SetActive(true);
            _serviceCamera.Follow = serviceCameraSize.transform;
            _serviceCamera.m_Lens.OrthographicSize = serviceCameraSize.cameraOrtoSize;
        }
      OnLoad.Invoke();
    }

    public void ShowPos()
    {
        if (_serviceCamera != null && UseCamera)
        {
            _serviceCamera.gameObject.SetActive(true);
            if (serviceCameraSize != null)
            {
                _serviceCamera.Follow = serviceCameraSize.transform;

                _serviceCamera.m_Lens.OrthographicSize = serviceCameraSize.cameraOrtoSize;
            }
        }
        OnLoad.Invoke();
    }
}