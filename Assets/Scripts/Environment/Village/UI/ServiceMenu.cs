﻿using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.DataObjects;
using NPCs;
using Other;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using System;
using CoreGame.UI;
using Researches;
using UnityEngine.Events;

namespace UI.Village
{
    public class ServiceMenu : MonoBehaviour
    {
        public event Action OnButtonClickedSuccessfully = null;

        [SerializeField] float fingerLifeTime = 2;

        [SerializeField, ReadOnly] private ServiceManager _currentService;
        [SerializeField] GameObject NPCButton;
        [SerializeField] GameObject roomPopulationLabel;
        [SerializeField] private Currency _currency;
        [SerializeField] private Currency _blueGems;
        [SerializeField] private Currency _greenGems;
        [SerializeField] private Currency _redGems;
        [SerializeField] private GameObject _questPanel;
        [SerializeField] private Image _infoBigIcon;
        [SerializeField] private CompareTextUI _infoCompareText1;
        [SerializeField] private CompareTextUI _infoCompareText2;
        [SerializeField] private CompareTextUI _infoCompareText3;
        [SerializeField] private TMP_Text _serviceInfoText;
        [SerializeField] private TMP_Text _levelText;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private Transform _unitsArea;
        [SerializeField] private CinemachineVirtualCamera _serviceCamera;
        [SerializeField] private Sprite _buttonImage;
        [SerializeField] private Sprite _buttonGreyImage;
        [SerializeField] private Sprite _buttonMaxImage;
        [SerializeField] private IconsScheme[] _iconsSchemes;
        [SerializeField] private ResearchController _researchController;
        [SerializeField] private Animator _bigIconAnimator;
        [SerializeField] private ServiceMenuRedirect serviceMenuRedirect;

        [SerializeField] private Material selectedObjectMaterial;
        [SerializeField] private Material upgradedObjectMaterial;

        [SerializeField] private UnityEvent exitEvent;
        [SerializeField] private UnityEvent showPropsPanel;
        [SerializeField] private UnityEvent showNPCPanel;

        public ProgressCalculator progress;

        private IconsScheme _currentIconScheme;

        [SerializeField] private UIEvents ActionButton;

        [SerializeField] private GameObject PanelNotif;
        [SerializeField] private TMP_Text notifTest;

        [SerializeField] private Color defaultIconColor = Color.white;
        [SerializeField] private Color notPurchasedIconColor = Color.blue;
        [SerializeField] private Color blockedIconColor = Color.gray;

        [Multiline(), SerializeField]
        private string BuildBlocker;
        [Space(10)]
        [Multiline(), SerializeField]
        private string ResearchBlocker;

        [SerializeField]
        private GameObject pointer;

        private List<IconUI> _icons = new List<IconUI>();
        private int _currentIconIndex;
        private GameObject _parentCanvas;

        List<GameData.Unit> unitsData;
        List<GameData.Props> propsData;

        GameObject currentBorder = null;

        float currentMoney;
        float currentGreenGems;
        float currentRedGems;
        float currentBlueGems;

        ActionButtonDataContainer actionButtonDataContainer;

        bool isPropsOpened;
        Transform lookPosition;
        CameraOrtoSize serviceCameraSize;

        IObjectID selectedObject = null;

        protected void Awake()
        {
            _parentCanvas = transform.parent.gameObject;
            var icons = _unitsArea.GetComponentsInChildren<IconUI>();

            actionButtonDataContainer = ActionButton.GetComponent<ActionButtonDataContainer>();

            for (int i = 0; i < icons.Length; i++)
            {
                icons[i].index = i;
                icons[i].OnSelected += (int i) => Deselect();
                icons[i].OnSelected += ShowInfo;
                _icons.Add(icons[i]);
            }
        }

        //public void SetLookPosition(Transform lookPos)
        //{
        //    this.lookPosition = lookPos;
        //    serviceCameraSize = lookPosition.gameObject.GetComponent<CameraOrtoSize>();
        //}

        public void ShowMenu(ServiceManager serviceManager, Transform lookPosition)
        {
            _currency.Value = 100000;
            _blueGems.Value = 5000;
            _redGems.Value = 5000;
            _greenGems.Value = 5000;

            _currentService = serviceManager;
            this.lookPosition = lookPosition;
            serviceMenuRedirect.SetCurrentService(serviceManager);

            serviceCameraSize = lookPosition.gameObject.GetComponent<CameraOrtoSize>();

            _currentIconScheme = _iconsSchemes.FirstOrDefault(x => x.Service == _currentService);
            if (_currentIconScheme == null)
            {
                Debug.LogError($"Can't find IconsScheme for {_currentService.name}");
            }

            unitsData = _currentService.Units;
            propsData = _currentService.Props;

            currentMoney = _currency.Value;
            currentGreenGems = _greenGems.Value;
            currentRedGems = _redGems.Value;
            currentBlueGems = _blueGems.Value;

            _serviceCamera.Follow = lookPosition;
            _serviceCamera.m_Lens.OrthographicSize = serviceCameraSize.cameraOrtoSize;
            _serviceCamera.gameObject.SetActive(_currentService.needCameraTarget);

            _questPanel.SetActive(false);
            ShowPropsPanel();

            _parentCanvas.SetActive(true);
            //_currency.onValueChanged += ShowIcons;

            NPCButton.SetActive(true);
            roomPopulationLabel.SetActive(false);

            if (unitsData.Count == 0)
                ShowRoomPopulationInfo();
        }

        void ShowRoomPopulationInfo()
        {
            if (_currentService.GetSleepZonesCount() == 0)
                return;

            NPCButton.SetActive(false);
            roomPopulationLabel.SetActive(true);

            roomPopulationLabel.GetComponentInChildren<TextMeshProUGUI>().text = _currentService.GetStorageSize().ToString() + "/" + _currentService.GetSleepPlaceCount().ToString();
        }

        public void SetService(ServiceManager serviceManager)
        {
            _currentService = serviceManager;
            _currentIconScheme = _iconsSchemes.FirstOrDefault(x => x.Service == _currentService);
            unitsData = _currentService.Units;
            propsData = _currentService.Props;
            serviceMenuRedirect.SetCurrentService(serviceManager);
        } 

        public void ShowMenu(ServiceManager serviceManager)
        {
            _currentService = serviceManager;
            _currentIconScheme = _iconsSchemes.FirstOrDefault(x => x.Service == _currentService);

            serviceMenuRedirect.SetCurrentService(serviceManager);

            if (_currentIconScheme == null)
            {
                Debug.LogError($"Can't find IconsScheme for {_currentService.name}");
            }

            unitsData = _currentService.Units;
            propsData = _currentService.Props;

            currentMoney = _currency.Value;
            currentGreenGems = _greenGems.Value;
            currentRedGems = _redGems.Value;
            currentBlueGems = _blueGems.Value;

            _serviceCamera.gameObject.SetActive(_currentService.needCameraTarget);

            _questPanel.SetActive(false);
            ShowPropsPanel();

            _parentCanvas.SetActive(true);
        }

        void CheckCurrency(int coins = -1, int redGems = -1, int greedGems = -1, int blueGems = -1)
        {
            if(PanelNotif.activeInHierarchy)
            {
                if (pointer.activeInHierarchy)
                    return;

                pointer.SetActive(true);

                DG.Tweening.DOVirtual.DelayedCall(fingerLifeTime, delegate { pointer.gameObject.SetActive(false); });
                return;
            }

            void check(int amount, Currency curr)
            {
                if(amount > 0)
                    curr.EnoughtCurrency(amount);
            }

            check(coins, _currency);
            check(redGems, _redGems);
            check(greedGems, _greenGems);
            check(blueGems, _blueGems);
        }

        public void Deselect()
        {
            if (selectedObject == null || selectedObject is GameData.Unit)
            {
                selectedObject = null;
                return;
            }

            if (selectedObject is GameData.Buildings)
            {
                _currentService.OnBuildingsDeselected.Invoke(null);
                return;
            }

            if (selectedObject is GameData.Props)
            {
                var props = PropsActivation.GetProps(_currentService, (GameData.Props)selectedObject);
                
                props.GetComponent<MaterialChanger>()?.ChangeMaterial(null, false);
                return;
            }

            selectedObject = null;
        }

        public void ShowInfo(int index)
        {
            PanelNotif.SetActive(false);
            _currentIconIndex = index;

            currentBorder?.SetActive(false);

            currentBorder = _icons[index].border;
            currentBorder.SetActive(true);

            var buildingsLevel = _currentService.File.BuildingsLevel;

            actionButtonDataContainer.Show(true);

            if (index == 0)
            {
                selectedObject = _currentService.Buildings;

                var stats = _currentService.GetCurrentBuildingStats();

                if (_currentService.needCameraTarget)
                {
                    _serviceCamera.Follow = lookPosition;
                    _serviceCamera.m_Lens.OrthographicSize = serviceCameraSize.cameraOrtoSize;
                }


                _icons[0].ActionButton = ActionButton;

                if (_currentIconScheme == null)
                    Debug.LogError("Cannot to find icon scheme for this service!");

                _infoBigIcon.sprite = _currentIconScheme.BuildingsBigIcon;
                _levelText.text = $"Level: {buildingsLevel}";
                _nameText.text = _currentService.Buildings.Name;

                _infoCompareText1.gameObject.SetActive(false);
                _infoCompareText2.gameObject.SetActive(true);
                _infoCompareText3.gameObject.SetActive(true);
                //_serviceInfoText.gameObject.SetActive(false);
                _serviceInfoText.text = _currentService.Buildings.Description;

                var buildingsUpgradePrice = _currentService.Buildings.Stats.Count != buildingsLevel
                 ? _currentService.Buildings.Stats[buildingsLevel].UpgradePrice
                 : -1;

                if (buildingsLevel == 0)
                {
                    _currentService.OnBuildingsSelected.Invoke(selectedObjectMaterial);

                    actionButtonDataContainer.SetItemPrice((int)buildingsUpgradePrice);

                    _infoCompareText2.ShowCompare("0", stats.MaxNPCCount.ToString(), _currentIconScheme.BuildingsStatsIcons[0]);
                    _infoCompareText3.ShowCompare("0", stats.MaxPropsCount.ToString(), _currentIconScheme.BuildingsStatsIcons[1]);

                    if (_currentService.Buildings.Stats[0].UpgradePrice <= currentMoney)
                    {
                        if (!_researchController.IsUnlocked(_currentService.Service.ToString(),
                                _currentService.File.BuildingsLevel.ToString()))
                            actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                        else
                            actionButtonDataContainer.buttonImage.sprite = _buttonImage;

                        PanelNotif.SetActive(false);
                        _icons[0].ChangeButton(ActionButtonType.Build, (int index) => {
                                                //Deselect();
                                                CheckCurrency(coins: (int)_currentService.Buildings.Stats[0].UpgradePrice);
                                                IncreaseBuildingsLevel(index); });
                        CheckBuild(0);
                    }
                    else
                    {
                        actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                        CheckBuild(0);

                        //Debug.Log("не хватает бабок у тебя");
                    }
                }
                else
                {
                    _currentService.OnBuildingsSelected.Invoke(upgradedObjectMaterial);
                    _icons[0].IconImage.sprite = _currentIconScheme.BuildingsIcon;

                    actionButtonDataContainer.SetItemPrice((int)buildingsUpgradePrice);

                    var buildingStats = _currentService.Buildings.Stats;

                    var showRight = buildingsLevel != buildingStats.Count;

                    var nextStats = showRight ? buildingStats[buildingsLevel] : default;

                    _infoCompareText2.ShowCompare(stats.MaxPropsCount.ToString(),
                        showRight
                            ? nextStats.MaxPropsCount.ToString()
                            : "Max", _currentIconScheme.BuildingsStatsIcons[0]);
                    _infoCompareText3.ShowCompare(stats.MaxNPCCount.ToString(),
                            showRight
                                ? nextStats.MaxNPCCount.ToString()
                                : "Max", _currentIconScheme.BuildingsStatsIcons[1]);

                    if (buildingsUpgradePrice < 0)
                    {
                        actionButtonDataContainer.Show(false);

                        actionButtonDataContainer.buttonImage.sprite = _buttonMaxImage;
                        PanelNotif.SetActive(false);

                        _icons[0].ChangeButton(ActionButtonType.MaxLevel, (int i) => { } /*Deselect()*/);
                    }
                    else if (buildingsUpgradePrice <= currentMoney)
                    {

                        if (!_researchController.IsUnlocked(_currentService.Service.ToString(),
        _currentService.File.BuildingsLevel.ToString()))
                            actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                        else
                            actionButtonDataContainer.buttonImage.sprite = _buttonImage;

                        PanelNotif.SetActive(false);

                        _icons[0].ChangeButton(ActionButtonType.UpgradeBuildings, (int index)=>
                                                        {
                                                            //Deselect();
                                                            CheckCurrency(coins: (int)buildingsUpgradePrice);
                                                            IncreaseBuildingsLevel(index);
                                                        });
                        CheckBuild(0);
                    }
                    else
                    {
                        actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                        _icons[0].ChangeButton(ActionButtonType.UpgradeBuildings, delegate { /*Deselect();*/ CheckCurrency(coins: (int)buildingsUpgradePrice); });
                        //Debug.Log("Не хватает бабок у тебя");
                        CheckBuild(0);
                    }
                }
            }
            else
            {
                if (isPropsOpened)
                {
                    ShowPropsInfo(index, buildingsLevel);
                    return;
                }

                ShowNPCInfo(index, buildingsLevel);
            }
            //Debug.Log("инфу о ИНФЕ показываем");

        }

        public void ShowPropsInfo(int index) => ShowInfo(index);

        public void ShowNPCInfo(int index)
        {
            ShowNPCPanel();
            ShowInfo(index);
        }

        void ShowPropsInfo(int index, int buildingsLevel)
        {
            _infoCompareText3.gameObject.SetActive(true);
            _infoCompareText2.gameObject.SetActive(false);
            //_serviceInfoText.gameObject.SetActive(false);


            var currentProps = _currentService.Props[index - 1];
            _infoBigIcon.sprite = _currentIconScheme.PropsBigIcons[index - 1];
            _nameText.text = currentProps.Name;
            _serviceInfoText.text = currentProps.Description;

            selectedObject = currentProps;

            if (_currentService.ActiveProps.TryGetValue(currentProps.ID, out var _props))
            {
                _props.GetComponent<MaterialChanger>()?.ChangeMaterial(upgradedObjectMaterial, true);

                var currentLevel = _props.Stats.Level;

                _levelText.text = $"Level: {currentLevel}";
                var showRight = currentLevel != currentProps.Stats.Count;

                var currentStats = currentProps.Stats[currentLevel - 1];
                var nextStats = showRight ? currentProps.Stats[currentLevel] : default;

                _infoCompareText3.ShowCompare(currentStats.ResourcesCapacity.ToString(),
                    showRight
                        ? nextStats.ResourcesCapacity.ToString()
                        : "Max", _currentIconScheme.PropsStatsIcons[0]);

                if (_currentService.needCameraTarget)
                {
                    var cameraTarget = _props.GetCameraTarget();

                    _serviceCamera.Follow = cameraTarget.transform;
                    _serviceCamera.m_Lens.OrthographicSize = cameraTarget.cameraOrtoSize;
                }
            }
            else
            {
                var stats = currentProps.Stats[0];

                _levelText.text = $"Level: {0}";
                _infoCompareText3.ShowCompare("0", stats.ResourcesCapacity.ToString(), _currentIconScheme.PropsStatsIcons[0]);

                var propsDat = _currentService.Props.FirstOrDefault(x => x.ID == currentProps.ID);

                if (_currentService.needCameraTarget)
                {
                    var pr = PropsActivation.GetProps(_currentService, propsDat);
                    var cameraTarget = pr.GetCameraTarget();

                    _serviceCamera.Follow = cameraTarget.transform;
                    _serviceCamera.m_Lens.OrthographicSize = cameraTarget.cameraOrtoSize;
                    
                    pr.GetComponent<MaterialChanger>()?.ChangeMaterial(selectedObjectMaterial, true);
                }
            }

            int i = index - 1;
            var n = index;
            _icons[n].ActionButton = ActionButton;

            if (_currentService.ActiveProps.TryGetValue(propsData[i].ID, out var props))
            {
                if (_currentService.needCameraTarget)
                {
                    var cameraTarget = props.GetCameraTarget();

                    _serviceCamera.Follow = cameraTarget.transform;
                    _serviceCamera.m_Lens.OrthographicSize = cameraTarget.cameraOrtoSize;
                }

                var updatePrice = propsData[i].Stats[props.Stats.Level - 1].UpgradePriceCoins;
                var updateGreenGemsPrice = propsData[i].Stats[props.Stats.Level - 1].UpgradePriceGreenGems;
                var updateRedGemsPrice = propsData[i].Stats[props.Stats.Level - 1].UpgradePriceRedGems;
                var updateBlueGemsPrice = propsData[i].Stats[props.Stats.Level - 1].UpgradePriceBlueGems;

                actionButtonDataContainer.SetItemPrice((int)updatePrice, (int)updateBlueGemsPrice, (int)updateRedGemsPrice, (int)updateGreenGemsPrice);

                _icons[n].PriceText.text = LargeNumber.FormatEveryThirdPower(updatePrice);
                _icons[n].IconImage.sprite = _currentIconScheme.PropsIcons[i];

                if (propsData[i].Stats.Count == props.Stats.Level)
                {
                    actionButtonDataContainer.Show(false);

                    actionButtonDataContainer.buttonImage.sprite = _buttonMaxImage;
                    _icons[n].ChangeButton(ActionButtonType.MaxLevel, null);
                    PanelNotif.SetActive(false);

                }
                else if (updatePrice <= currentMoney && updateGreenGemsPrice <= currentGreenGems
                    && updateRedGemsPrice <= currentRedGems && updateBlueGemsPrice <= currentBlueGems
                    && buildingsLevel > 0)
                {
                    actionButtonDataContainer.buttonImage.sprite = _buttonImage;
                    PanelNotif.SetActive(false);

                    _icons[n].ChangeButton(ActionButtonType.UpgradeProps, IncreasePropsLevel);
                }
                else
                {
                    actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                    _icons[n].ChangeButton(ActionButtonType.UpgradeProps, delegate { CheckCurrency((int)updatePrice, (int)updateRedGemsPrice, (int)updateGreenGemsPrice, (int)updateBlueGemsPrice); });
                    PanelNotif.SetActive(false);

                }
            }
            else
            {
                var updatePrice = propsData[i].Stats[0].UpgradePriceCoins;
                var updateGreenGemsPrice = propsData[i].Stats[0].UpgradePriceGreenGems;
                var updateRedGemsPrice = propsData[i].Stats[0].UpgradePriceRedGems;
                var updateBlueGemsPrice = propsData[i].Stats[0].UpgradePriceBlueGems;

                actionButtonDataContainer.SetItemPrice((int)updatePrice, (int)updateBlueGemsPrice, (int)updateRedGemsPrice, (int)updateGreenGemsPrice);

                _icons[n].IconImage.sprite = _currentIconScheme.PropsGreyIcons[i];

                var currentBuildingStats = _currentService.GetCurrentBuildingStats();

                if (updatePrice <= currentMoney && updateGreenGemsPrice <= currentGreenGems
                    && updateRedGemsPrice <= currentRedGems && updateBlueGemsPrice <= currentBlueGems
                    && buildingsLevel > 0 && currentBuildingStats.MaxPropsCount > _currentService.GetActivePropsCountForMenu())    
                {
                    actionButtonDataContainer.buttonImage.sprite = _buttonImage;
                    PanelNotif.SetActive(false);

                    _icons[n].ChangeButton(ActionButtonType.ActivateProps, ActivateProps);
                }
                else
                {
                  
                    actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                    _icons[n].ChangeButton(ActionButtonType.ActivateProps, delegate { CheckCurrency((int)updatePrice, (int)updateRedGemsPrice, (int)updateGreenGemsPrice, (int)updateBlueGemsPrice); });
                    // = "Upgrade " + propsData[i].Name + " to " + propsData[i].Stats[0].Level;//propsData[i].Name;

                    if (updatePrice >= currentMoney && currentBuildingStats.MaxPropsCount > _currentService.GetActivePropsCountForMenu() && buildingsLevel == 0)
                    {
                        ShowNotif();
                        //Debug.Log("ВКЛЮЧАЕМ 1");


                    }               
                                 
                     else if (updatePrice >= currentMoney && currentBuildingStats.MaxPropsCount == _currentService.GetActivePropsCountForMenu())
                    {
                        ShowNotif();
                        //Debug.Log("ВКЛЮЧАЕМ 2");

                    }
                    else if (updatePrice <= currentMoney && currentBuildingStats.MaxPropsCount == _currentService.GetActivePropsCountForMenu() &&  buildingsLevel > 0)
                    {
                        ShowNotif();
                        //Debug.Log("ВЫЛЮЧАЕМ 4");

                    }
                else if (updatePrice <= currentMoney && currentBuildingStats.MaxPropsCount >= _currentService.GetActivePropsCountForMenu() && buildingsLevel>0)
                 {
               

                     PanelNotif.SetActive(false);
               
                  // string s = BuildBlocker.Replace("{0}", _currentService.Buildings.Name);
                  // string r = s.Replace("{1}", (_currentService.File.BuildingsLevel + 1).ToString());
                  // notifTest.text = r;
                  // Debug.Log("ВЫЛЮЧАЕМ 3");
                 }
                    else if (updatePrice <= currentMoney && currentBuildingStats.MaxPropsCount == _currentService.GetActivePropsCountForMenu() && buildingsLevel >0)
                    {
                        ShowNotif();
                        //Debug.Log("ВЫЛЮЧАЕМ 4");

                    }
                   else  if (updatePrice <= currentMoney  && buildingsLevel == 0 &&currentBuildingStats.UpgradePrice == 0 )
                    {
                        ShowNotif();
                        //Debug.Log("ВЫЛЮЧАЕМ 5");

                    }
                    else if (updatePrice <= currentMoney && buildingsLevel == 0 && currentBuildingStats.UpgradePrice > 0)
                    {
                        ShowNotif();
                        //Debug.Log("ВЫЛЮЧАЕМ 5");

                    }
                    else
                    {
                        PanelNotif.SetActive(false);
                        //Debug.Log("ВЫКЛЮЧАЕМ");
                    }

                  //  CheckProp(n);
                }
            }
            //Debug.Log("инфу о пропсе показываем");
            
        }

        void ShowNPCInfo(int index, int buildingsLevel)
        {
            _infoCompareText3.gameObject.SetActive(true);
            _infoCompareText2.gameObject.SetActive(false);
            //_serviceInfoText.gameObject.SetActive(false);

            if(_currentService.Units.Count == 0)
            {
                Debug.LogError("Units count is 0!!!");
                return;
            }

            var currentUnit = _currentService.Units[index - 1];
            _infoBigIcon.sprite = _currentIconScheme.UnitsBigIcons[index - 1];
            _nameText.text = currentUnit.Name;
            _serviceInfoText.text = currentUnit.Description;

            selectedObject = currentUnit;

            if (_currentService.ActiveNpc.TryGetValue(currentUnit.ID, out var _npc))
            {
                var currentLevel = _npc.Stats.Level;
                _levelText.text = $"Level: {currentLevel}";
                var showRight = currentLevel != currentUnit.Stats.Count;

                var currentStats = currentUnit.Stats[currentLevel - 1];
                var nextStats = showRight ? currentUnit.Stats[currentLevel] : default;

                if (_currentService.SaveID == "MiningService")
                    _infoCompareText3.ShowCompare(currentStats.WorkEfficiency.ToString(), showRight
                        ? nextStats.TimeCycle.ToString() : "Max", _currentIconScheme.UnitsStatsIcons[0]);
                else
                    _infoCompareText3.ShowCompare(currentStats.TimeCycle.ToString(), showRight
                        ? nextStats.TimeCycle.ToString() : "Max", _currentIconScheme.UnitsStatsIcons[2]);

                if (_currentService.needCameraTarget)
                {
                    _serviceCamera.Follow = _npc.transform;
                    _serviceCamera.m_Lens.OrthographicSize = _npc.GetComponent<CameraOrtoSize>().cameraOrtoSize;
                }
            }
            else
            {
                var stats = currentUnit.Stats[0];

                _levelText.text = $"Level: {0}";

                if (_currentService.SaveID == "MiningService")
                    _infoCompareText3.ShowCompare("0", stats.WorkEfficiency.ToString(), _currentIconScheme.UnitsStatsIcons[0]);
                else
                    _infoCompareText3.ShowCompare("0", stats.TimeCycle.ToString(), _currentIconScheme.UnitsStatsIcons[2]);
            }

            int i = index - 1;
            var n = index;
            _icons[n].ActionButton = ActionButton;

            if (_currentService.ActiveNpc.TryGetValue(unitsData[i].ID, out var npc))
            {
                var updatePrice = unitsData[i].Stats[npc.Stats.Level - 1].UpgradePrice;

                actionButtonDataContainer.SetItemPrice((int)updatePrice);

                _icons[n].PriceText.text = LargeNumber.FormatEveryThirdPower(updatePrice);
                _icons[n].IconImage.sprite = _currentIconScheme.UnitsIcons[i];

                if (unitsData[i].Stats.Count == npc.Stats.Level)
                {
                    actionButtonDataContainer.Show(false);

                    actionButtonDataContainer.buttonImage.sprite = _buttonMaxImage;
                    _icons[n].ChangeButton(ActionButtonType.MaxLevel, null);
                    PanelNotif.SetActive(false);

                }
                else if (updatePrice <= currentMoney)
                {
                    actionButtonDataContainer.buttonImage.sprite = _buttonImage;
                    _icons[n].ChangeButton(ActionButtonType.UpgradeUnit, IncreaseUnitLevel);
                    
                    PanelNotif.SetActive(false);

                }
                else
                {
                    actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                    _icons[n].ChangeButton(ActionButtonType.UpgradeUnit, delegate { CheckCurrency((int)updatePrice); });
                    //Debug.Log("неактивна показываем");

                }
            }
            else
            {
                var updatePrice = unitsData[i].Stats[0].UpgradePrice;

                actionButtonDataContainer.SetItemPrice((int)updatePrice);
                _icons[n].IconImage.sprite = _currentIconScheme.UnitsGreyIcons[i];

                var currentBuildingStats = _currentService.GetCurrentBuildingStats();

                if (updatePrice <= currentMoney && buildingsLevel > 0 && currentBuildingStats.MaxNPCCount > _currentService.ActiveNpc.Count)
                {
                    actionButtonDataContainer.buttonImage.sprite = _buttonImage;
                    _icons[n].ChangeButton(ActionButtonType.Hire, SpawnUnit);
                    PanelNotif.SetActive(false);
                }
                else
                {
                    if (updatePrice >= currentMoney && currentBuildingStats.MaxNPCCount > _currentService.ActiveNpc.Count && buildingsLevel == 0 )
                    {
                        ShowNotif();
                        //Debug.Log("1");

                    }
                  else  if(updatePrice >= 0 && currentBuildingStats.MaxNPCCount == _currentService.ActiveNpc.Count && buildingsLevel == 0)
                    {
                        ShowNotif();
                        //Debug.Log("2");

                    }
                    else  if (updatePrice >= currentMoney && currentBuildingStats.MaxNPCCount == _currentService.ActiveNpc.Count)
                    {
                        ShowNotif();
                        //Debug.Log("3");

                    }
                    else if (updatePrice <= currentMoney && buildingsLevel == 0 && currentBuildingStats.UpgradePrice > 0)
                    {
                        PanelNotif.SetActive(true);

                        ShowNotif();
                        //Debug.Log("ВЫЛЮЧАЕМ 5");

                    }
                    else  if(updatePrice <= currentMoney && currentBuildingStats.MaxNPCCount == _currentService.ActiveNpc.Count)
                    {
                        ShowNotif();
                        //Debug.Log("4");

                    }
                    else if (updatePrice < currentMoney && currentBuildingStats.MaxNPCCount > _currentService.ActiveNpc.Count && buildingsLevel == 0)
                    {
                        ShowNotif();
                        //Debug.Log("1");

                    }
                    else
                    {
                        PanelNotif.SetActive(false);

                    }
                    //  CheckUnitLvl(n);
                    //  if (updatePrice <= currentMoney)
                    //  {


                    //progress.UpdatePerMin();

                    // else
                    // {
                    //  PanelNotif.SetActive(false);
                    // }
                    actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;

                    _icons[n].ChangeButton(ActionButtonType.Hire, delegate { CheckCurrency((int)updatePrice); });
                    
               //  }
                // else
                // /{
                   //  PanelNotif.SetActive(false);
                 //}
                }
            }
            //Debug.Log("инфу о НПС показываем");
        }

        void ShowNotif()
        {
            PanelNotif.SetActive(true);
            serviceMenuRedirect.SetBlockerType(BlockerType.BuildBlocker);

            string s = BuildBlocker.Replace("{0}", _currentService.Buildings.Name);
            string r = s.Replace("{1}", (_currentService.File.BuildingsLevel + 1).ToString());
            notifTest.text = r;
        }

        public void ShowNPCPanel()
        {
            Deselect();
            showNPCPanel.Invoke();
            isPropsOpened = false;

            for (int i = 0; i < _icons.Count; i++)
                _icons[i].gameObject.SetActive(false);

            _icons[1].border.SetActive(true);

            var currentBuildingStats = _currentService.GetCurrentBuildingStats();

            for (int i = 0; i < unitsData.Count; i++)
            {
                var n = i + 1;

                UpdateNPCIcon(n, currentBuildingStats);

                //_icons[n].gameObject.SetActive(true);

                //if (_currentService.ActiveNpc.TryGetValue(unitsData[i].ID, out var npc))
                //    _icons[n].IconImage.sprite = _currentIconScheme.UnitsIcons[i];
                //else
                //    _icons[n].IconImage.sprite = _currentIconScheme.UnitsGreyIcons[i];
            }

            ShowInfo(1);
        }

        public void ShowPropsPanel()
        {
            showPropsPanel.Invoke();
            isPropsOpened = true;
            ShowInfo(0);

            _icons[0].gameObject.SetActive(true);
            _icons[0].Price.SetActive(true);

            _icons[0].border.SetActive(true);

            UpdateBuildingIcon();

            for (int i = 1; i < _icons.Count; i++)
                _icons[i].gameObject.SetActive(false);

            var currentBuildingStats = _currentService.GetCurrentBuildingStats();

            int propsCount = PropsActivation.GetPropsCount(_currentService);

            for (int i = 0; i < propsData.Count; i++)
            {
                if (!PropsActivation.NeedToShowPropsAtMenu(_currentService, propsData[i]))
                    continue;

                var n = i + 1;
                UpdatePropsIcon(n, currentBuildingStats);
            }
        }

        void UpdateBuildingIcon()
        {
            var buildingsLevel = _currentService.File.BuildingsLevel;

            _icons[0].IconImage.sprite = _currentIconScheme.BuildingsIcon;

            if (buildingsLevel == 0)
            {
                _icons[0].ShowUnpurchasedBackground();
                _icons[0].IconImage.color = notPurchasedIconColor;
            }
            else
            {
                _icons[0].IconImage.sprite = _currentIconScheme.BuildingsIcon;

                if (_currentService.Buildings.Stats[0].UpgradePrice > currentMoney
                    || !_researchController.IsUnlocked(_currentService.Service.ToString(),
                        _currentService.File.BuildingsLevel.ToString()))
                {
                    _icons[0].ShowBlockedBackground();
                    _icons[0].IconImage.color = blockedIconColor;
                }
                else
                {

                    _icons[0].ShowReadyToBuyBackground();
                    _icons[0].IconImage.color = defaultIconColor;
                }
            }
        }

        void UpdatePropsIcon(int iconIndex, GameData.BuildingsStats currentBuildingStats)
        {
            _icons[iconIndex].gameObject.SetActive(true);

            _icons[iconIndex].IconImage.sprite = _currentIconScheme.PropsIcons[iconIndex - 1];

            if (_currentService.ActiveProps.TryGetValue(propsData[iconIndex - 1].ID, out var props))
            {
                var updatePrice = propsData[iconIndex - 1].Stats[props.Stats.Level - 1].UpgradePriceCoins;
                var updateGreenGemsPrice = propsData[iconIndex - 1].Stats[props.Stats.Level - 1].UpgradePriceGreenGems;
                var updateRedGemsPrice = propsData[iconIndex - 1].Stats[props.Stats.Level - 1].UpgradePriceRedGems;
                var updateBlueGemsPrice = propsData[iconIndex - 1].Stats[props.Stats.Level - 1].UpgradePriceBlueGems;

                if (!(updatePrice <= currentMoney && updateGreenGemsPrice <= currentGreenGems
                    && updateRedGemsPrice <= currentRedGems && updateBlueGemsPrice <= currentBlueGems
                    && currentBuildingStats.LevelUpgrade > 0))
                {
                    _icons[iconIndex].ShowBlockedBackground();
                    _icons[iconIndex].IconImage.color = blockedIconColor;
                    return;
                }

                _icons[iconIndex].ShowReadyToBuyBackground();
                _icons[iconIndex].IconImage.color = defaultIconColor;
            }
            else
            {
                _icons[iconIndex].ShowUnpurchasedBackground();
                _icons[iconIndex].IconImage.color = notPurchasedIconColor;
            }
        }

        void UpdateNPCIcon(int iconIndex, GameData.BuildingsStats currentBuildingStats)
        {
            _icons[iconIndex].gameObject.SetActive(true);

            _icons[iconIndex].IconImage.sprite = _currentIconScheme.UnitsIcons[iconIndex - 1];

            if (_currentService.ActiveNpc.TryGetValue(unitsData[iconIndex - 1].ID, out var npc))
            {
                var updatePrice = unitsData[iconIndex - 1].Stats[npc.Stats.Level - 1].UpgradePrice;

                currentMoney = _currency.Value;

                if (updatePrice > currentMoney)
                {
                    _icons[iconIndex].ShowBlockedBackground();
                    _icons[iconIndex].IconImage.color = blockedIconColor;
                    return;
                }

                _icons[iconIndex].ShowReadyToBuyBackground();
                _icons[iconIndex].IconImage.color = defaultIconColor;
            }
            else
            {
                _icons[iconIndex].ShowUnpurchasedBackground();
                _icons[iconIndex].IconImage.color = notPurchasedIconColor;
            }
        }

        private void SpawnUnit(int iconIndex)
        {
            var currentUnit = _currentService.Units[iconIndex - 1];

            if (!_currency.TryToGet(currentUnit.Stats[0].UpgradePrice))
                return;

            var currentLvl = _currentService.GetUnitLevel(iconIndex - 1);
            if (!_researchController.IsUnlocked(_currentService.Service + "Unit",
                    (currentLvl + 1).ToString()))
                return;

            var npc = UnitSpawner.SpawnUnit(_currentService, currentUnit);

            progress.UpdatePerMin();

            if (_currentService.needCameraTarget)
            {
                _serviceCamera.Follow = npc.transform;
                _serviceCamera.m_Lens.OrthographicSize = npc.GetComponent<CameraOrtoSize>().cameraOrtoSize;
            }

            if (iconIndex == _currentIconIndex)
                ShowInfo(iconIndex);

            UpdateNPCIcon(iconIndex, _currentService.GetCurrentBuildingStats());

            SyncButtonAnalytics();
        }

        private void CheckProp(int iconIndex)
        {
            var currentLvl = _currentService.GetPropsLevel(iconIndex - 1);
            var currentBuildingStats = _currentService.GetCurrentBuildingStats();
            //var isPropsResearched = _researchController.IsUnlocked(_currentService.Service + "Props",
            //    (currentLvl + 1).ToString());
            //  Debug.Log("logProps");
            if (currentLvl >= 0 /*&& !isPropsResearched*/ || currentBuildingStats.MaxPropsCount > _currentService.GetActivePropsCountForMenu() && currentBuildingStats.UpgradePrice < currentMoney || currentBuildingStats.UpgradePrice <= 0 )
            {
                ShowNotif();
                //Debug.Log("UPDATE");

            }
            else
            {
                //Debug.Log(currentLvl.ToString());
                PanelNotif.SetActive(false);
                //Debug.Log("NO UPDATE LOH");

              //  Debug.Log($"Upgrade :  {_currentService.Buildings.Name} ");
              // 
              // string s = BuildBlocker.Replace("{0}", _currentService.Buildings.Name);
              // string r = s.Replace("{1}", (_currentService.File.BuildingsLevel + 1).ToString());
              // notifTest.text = r;
            }

            // var currentProps = _currentService.Props[iconIndex - 1];
            //
            // if (!_currency.TryToGet(currentProps.Stats[0].UpgradePriceCoins))
            // {
            //     Debug.Log("GET NULL");
            //     return;
            // }
            //
            // var currentLvl = _currentService.GetPropsLevel(iconIndex - 1);
            // if (!_researchController.IsUnlocked(_currentService.Service + "Props",
            //         (currentLvl + 1).ToString()))
            // {
            //     
            // }
            // else
            // {
            //     Debug.Log($"Upgrade :  {_currentService.services.} ");
            //
            // }
        }

        private void ActivateProps(int iconIndex)
        {
            Deselect();

            var currentProps = _currentService.Props[iconIndex - 1];

            if (_currency.TryToGet(currentProps.Stats[0].UpgradePriceCoins))
            {

            }
               // return;

            if (_blueGems.Value >= currentProps.Stats[0].UpgradePriceBlueGems ) //TryToGet(currentProps.Stats[0].UpgradePriceBlueGems))
                {
                //Debug.Log("asdasd");
                _blueGems.Value -= currentProps.Stats[0].UpgradePriceBlueGems;
                }
               // return;

            if (_greenGems.TryToGet(currentProps.Stats[0].UpgradePriceGreenGems))
                    {
                _greenGems.Value -= currentProps.Stats[0].UpgradePriceGreenGems;
                    }
              //  return;

            if (_redGems.TryToGet(currentProps.Stats[0].UpgradePriceRedGems))
                        {
                _redGems.Value -= currentProps.Stats[0].UpgradePriceRedGems;
                        }
               // return;


            var currentLvl = _currentService.GetPropsLevel(iconIndex - 1);
            if (!_researchController.IsUnlocked(_currentService.Service + "Props",
                    (currentLvl + 1).ToString()))
                return;

            _currentService.AddPropsToService(iconIndex - 1);
            progress.UpdatePerMin();

            if (iconIndex == _currentIconIndex)
                ShowInfo(iconIndex);

            UpdatePropsIcon(iconIndex, _currentService.GetCurrentBuildingStats());
            ShowRoomPopulationInfo();

            SyncButtonAnalytics();
        }

        private void CheckBuild(int iconIndex)
        {
            if (!_researchController.IsUnlocked(_currentService.Service.ToString(),
                    _currentService.File.BuildingsLevel.ToString()))
            {
                PanelNotif.SetActive(true);
                serviceMenuRedirect.SetBlockerType(BlockerType.ResearchBlocker);
                string s = ResearchBlocker.Replace("{0}", _researchController.GetLabel(_currentService.SaveID));
                string r = s.Replace("{1}", _researchController.GetData(_currentService.SaveID).ToString());
                notifTest.text = r;

                notifTest.text = r; //$"Upgrade : {_researchController.GetLabel(_currentService.SaveID)} to level {_researchController.GetData(_currentService.SaveID)}";
                //Debug.Log("NO OPEn");
                //Debug.Log(_currentService.ToString());

                return;
            }
            //Debug.Log("OPEn");

            PanelNotif.SetActive(false);
        }

        private void IncreaseBuildingsLevel(int iconIndex)
        {
            if (!_researchController.IsUnlocked(_currentService.Service.ToString(),
                    _currentService.File.BuildingsLevel.ToString()))
            {
                PanelNotif.SetActive(true);
                serviceMenuRedirect.SetBlockerType(BlockerType.ResearchBlocker);
                string s = ResearchBlocker.Replace("{0}", _researchController.GetLabel(_currentService.SaveID));
                string r = s.Replace("{1}", _researchController.GetData(_currentService.SaveID).ToString());
                notifTest.text = r;

                notifTest.text = r; //$"Upgrade : {_researchController.GetLabel(_currentService.SaveID)} to level {_researchController.GetData(_currentService.SaveID)}";

                //Debug.Log(_currentService.ToString());

                return;
            }

            if (!_currency.TryToGet(_currentService.Buildings.Stats[_currentService.File.BuildingsLevel].UpgradePrice))
                return;

            // if (!_currency.TryToGet(_currentService.Buildings.Stats[_currentService.File.BuildingsLevel].))
            //    return;

            Deselect();

            _currentService.BuildingsLevelUp();

            ShowMenu(_currentService);

            SyncButtonAnalytics();
        }

        private void IncreasePropsLevel(int iconIndex)
        {
            Deselect();

            var currentLvl = _currentService.GetPropsLevel(iconIndex - 1);
            var isPropsResearched = _researchController.IsUnlocked(_currentService.Service + "Props",
                (currentLvl + 1).ToString());

            if (currentLvl >= 0 && !isPropsResearched)
                return;

            _currentService.IncreasePropsLevel(iconIndex - 1, _currency, _redGems, _blueGems, _greenGems);
            progress.UpdatePerMin();

            if (iconIndex == _currentIconIndex)
                ShowInfo(iconIndex);

            UpdatePropsIcon(iconIndex, _currentService.GetCurrentBuildingStats());
            ShowRoomPopulationInfo();

            SyncButtonAnalytics();
        }

        private void CheckUnitLvl(int iconIndex)
        {
            var currentLvl = _currentService.GetUnitLevel(iconIndex - 1);
            var isUnitResearched = _researchController.IsUnlocked(_currentService.Service + "Unit",
                (currentLvl + 1).ToString());

            if (currentLvl >= 0 && !isUnitResearched )
            {
                //Debug.Log("pidor");

                return;
            }
            else
            {
                //Debug.Log("is not availabe;");
                PanelNotif.SetActive(true);
                serviceMenuRedirect.SetBlockerType(BlockerType.BuildBlocker);
                // actionButtonDataContainer.buttonImage.sprite = _buttonGreyImage;
                //Debug.Log("неактивна покупка НПС показываем");
                string s = BuildBlocker.Replace("{0}", _currentService.Buildings.Name);
                string r = s.Replace("{1}", (_currentService.File.BuildingsLevel + 1).ToString());
                notifTest.text = r;//$"Upgrade : {_currentService.Buildings.Name} to level {_currentService.File.BuildingsLevel + 1}";

               // _icons[].ChangeButton(ActionButtonType.Hire, null);
            }

        }

        private void IncreaseUnitLevel(int iconIndex)
        {
            var currentLvl = _currentService.GetUnitLevel(iconIndex - 1);
            var isUnitResearched= _researchController.IsUnlocked(_currentService.Service + "Unit",
                (currentLvl + 1).ToString());

            if (currentLvl >= 0 && !isUnitResearched)
                return;

            _currentService.IncreaseUnitLevel(iconIndex - 1, _currency);
            progress.UpdatePerMin();

            if (iconIndex == _currentIconIndex)
                ShowInfo(iconIndex);

            UpdateNPCIcon(iconIndex, _currentService.GetCurrentBuildingStats());

            SyncButtonAnalytics();
        }

        private void OnDisable()
        {
            if (_currentService != null && _serviceCamera != null)
            {
                _serviceCamera.gameObject.SetActive(false);
                _serviceCamera.Follow = null;
            }

            currentBorder?.SetActive(false);
            currentBorder = null;
            _questPanel.SetActive(true);
            //_currency.onValueChanged -= ShowIcons;
        }
        
        private void SyncButtonAnalytics()
        {
            // 2)Нажатие кнопки "купить" или "нанять". Любой из них, подсчитываем общее количество покупок
            OnButtonClickedSuccessfully?.Invoke();
        }

        public void ShowTip() => _bigIconAnimator.SetTrigger("Scale");

        public void Close() 
        {
            try
            {
                exitEvent.Invoke();
            }
            catch
            {

            }
        }

        public void ScaleActionButton() => ActionButton.gameObject.GetComponent<Animator>().SetTrigger("Scale");
    
        public void ShowUnpurchasedNPC()
        {
            ShowNPCPanel();
            int unitIndex = 0;

            for (unitIndex = 0; unitIndex < _currentService.Units.Count; ++unitIndex)
                if (_currentService.GetUnitLevel(unitIndex) == -1)
                    break;

            ShowInfo(unitIndex + 1);
        }

        public void ShowUnupgradedNPC(int targetLevel)
        {
            ShowNPCPanel();
            int unitIndex = 0;
            int minDifference = 10000000;
            int minDifferenceIndex = 0;

            for (unitIndex = 0; unitIndex < _currentService.Units.Count; ++unitIndex)
            {
                int unitLevel = _currentService.GetUnitLevel(unitIndex);

                if (unitLevel < 0)
                    unitLevel = 0;

                if (targetLevel - unitLevel > 0 && targetLevel - unitLevel < minDifference)
                {
                    minDifference = targetLevel - unitLevel;
                    minDifferenceIndex = unitIndex;
                }
            }

            ShowInfo(minDifferenceIndex + 1);
        }

        public bool IsAvailableObject(IObjectID serviceObject, ServiceManager service)
        {
            if (serviceObject is GameData.Buildings)
                return _researchController.IsUnlocked(service.Service.ToString(),
                            service.File.BuildingsLevel.ToString());

            if(service.File.BuildingsLevel > 0)
            {
                var currentBuildingStats = service.GetCurrentBuildingStats();

                if (serviceObject is GameData.Props)
                {
                    if (service.ActiveProps.TryGetValue(serviceObject.ID, out var props) && props.Stats.Level > 0)
                        return props.Stats.Level != (serviceObject as GameData.Props).Stats.Count;
                   
                    return currentBuildingStats.MaxPropsCount > service.GetActivePropsCountForMenu();
                }

                if (serviceObject is GameData.Unit)
                {
                    if (service.ActiveNpc.TryGetValue(serviceObject.ID, out NPC npc) && npc.Stats.Level > 0)
                        return npc.Stats.Level != (serviceObject as GameData.Unit).Stats.Count;

                    return currentBuildingStats.MaxNPCCount > service.ActiveNpc.Count;
                }
            }

            return false;
        }
    }
}