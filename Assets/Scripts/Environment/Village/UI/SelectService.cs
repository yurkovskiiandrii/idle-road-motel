using NPCs;
using UI.Village;
using UnityEngine;

public class SelectService : SelectableArea
{
    [SerializeField] private ServiceMenu _serviceMenu;
    [SerializeField] private Transform _lookPosition;
    [SerializeField] private ServiceManager _serviceManager;
    protected override void Show()
    {
       // _serviceMenu.ShowMenu(_serviceManager, _lookPosition);
       // Debug.Log(gameObject.name);
    }

    public void ShowPos()
    {
        _serviceMenu.ShowMenu(_serviceManager, _lookPosition);
    }

    public void ShowTip() => _serviceMenu.ShowTip();

    public ServiceManager GetServiceManager() => _serviceManager;
}