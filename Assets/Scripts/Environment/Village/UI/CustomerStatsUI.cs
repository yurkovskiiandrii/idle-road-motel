using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomerStatsUI : CustomerControllerUI
{
    [SerializeField] private TMP_Text statsLabel;
    [SerializeField] private GameObject statsSprite;
    
    protected override void Awake()
    {
        base.Awake();
        coroutine = StartTimer();

        statsLabel.gameObject.SetActive(false);
        statsSprite.SetActive(false);
    }

    public void ShowStats(int stats)
    {
        statsLabel.SetText("+" + stats.ToString());
        statsLabel.gameObject.SetActive(true);
        statsSprite.SetActive(true);
        Debug.Log("SHOWED PAYMENT");
        StopCoroutine(coroutine);
        coroutine = StartTimer();
        
        _uiAdjuster.AdjustUI();
        holder.gameObject.SetActive(true);
        
        StartCoroutine(StartTimer());
    }

    IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(hideTime);
        statsLabel.gameObject.SetActive(false);
        statsSprite.SetActive(false);
        
        _uiAdjuster.AdjustUI();
        holder.gameObject.SetActive(false);
    }
}