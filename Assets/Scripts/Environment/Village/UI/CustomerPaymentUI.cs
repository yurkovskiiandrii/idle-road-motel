using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomerPaymentUI : CustomerControllerUI
{
    [SerializeField] private TMP_Text moneyLabel;
    [SerializeField] private GameObject coinSprite;

    protected override void Awake()
    {
        base.Awake();
        coroutine = StartTimer();

        moneyLabel.gameObject.SetActive(false);
        coinSprite.SetActive(false);
    }

    protected override void Start()
    {
        base.Start();
    }

    public void ShowPayment(int money)
    {
        moneyLabel.SetText("+" + money.ToString());
        moneyLabel.gameObject.SetActive(true);
        coinSprite.SetActive(true);
        Debug.Log("SHOWED PAYMENT");
        StopCoroutine(coroutine);
        coroutine = StartTimer();
        
        _uiAdjuster.AdjustUI();
        holder.gameObject.SetActive(true);
        
        StartCoroutine(StartTimer());
    }

    IEnumerator StartTimer()
    {
        _uiAdjuster.AdjustUI();
        yield return new WaitForSeconds(hideTime);
        moneyLabel.gameObject.SetActive(false);
        coinSprite.SetActive(false);
        
        _uiAdjuster.AdjustUI();
        holder.gameObject.SetActive(false);
    }
}