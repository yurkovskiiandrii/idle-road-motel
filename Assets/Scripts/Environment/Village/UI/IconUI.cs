using System;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum ActionButtonType
{
    MaxLevel,
    Hire,
    Build,
    UpgradeUnit,
    UpgradeBuildings,
    ActivateProps,
    UpgradeProps
}

public class IconUI : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] string freeText = "Free";
    public int index;
    public UIEvents ActionButton;
    public TextMeshProUGUI ActionButtonText;
    public GameObject Price;
    public TextMeshProUGUI PriceText;
    public Image IconImage;
    public Image ButtonImage;
    public event Action<int> OnSelected;

    [Header("Backgrounds"), Space(5)]
    [SerializeField] IconUIContainer unpurchased;
    [SerializeField] IconUIContainer blocked;
    [SerializeField] IconUIContainer readyToBuy;

    public GameObject border;

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        OnSelected?.Invoke(index);
    }

    private void OnDisable()
    {
        gameObject.SetActive(false);
    }

    public void ShowUnpurchasedBackground()
    {
        unpurchased.SetActive(true);
        readyToBuy.SetActive(false);
        blocked.SetActive(false);
    }

    public void ShowReadyToBuyBackground()
    {
        unpurchased.SetActive(false);
        readyToBuy.SetActive(true);
        blocked.SetActive(false);
    }

    public void ShowBlockedBackground()
    {
        unpurchased.SetActive(false);
        readyToBuy.SetActive(false);
        blocked.SetActive(true);
    }

    public void ChangeButton(ActionButtonType buttonType, Action<int> action)
    {
        ActionButton.onPointerDown = null;
        if (action != null)
        {
            ActionButton.onPointerDown += x =>
            {
                action.Invoke(index);
            };
        }

        var actionButtonDataContainer = ActionButton.GetComponent<ActionButtonDataContainer>();

        switch (buttonType)
        {
            case ActionButtonType.Build:
                actionButtonDataContainer.actionLabel.text = "Build";
                break;
            case ActionButtonType.Hire:
                actionButtonDataContainer.actionLabel.text = "Hire";
                break;
            case ActionButtonType.UpgradeBuildings:
            case ActionButtonType.UpgradeUnit:
            case ActionButtonType.UpgradeProps:
                actionButtonDataContainer.actionLabel.text = "Upgrade";
                break;
            case ActionButtonType.ActivateProps:
                actionButtonDataContainer.actionLabel.text = "Activate";
                break;
            case ActionButtonType.MaxLevel:
                actionButtonDataContainer.actionLabel.text = "MaxLevel";
                break;        
        }
    }
}