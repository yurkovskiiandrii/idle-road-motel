using UnityEngine;

public class UIAdjuster : MonoBehaviour
{
    public GameObject emotionsHolderCanvas;
    public GameObject payment;
    public GameObject stats;

    public float moveSpeed = 5f;

    private Vector3 targetPositionEmotions;
    private Vector3 targetPositionPayment;
    private Vector3 targetPositionStats;

    void Start()
    {
        targetPositionEmotions = emotionsHolderCanvas.transform.localPosition;
        targetPositionPayment = payment.transform.localPosition;
        targetPositionStats = stats.transform.localPosition;
    }

    void Update()
    {
        AdjustUI();
        MoveToPositions();
    }

    public void AdjustUI()
    {
        bool[] isActive = new bool[3];
        isActive[0] = emotionsHolderCanvas.activeSelf;
        isActive[1] = payment.activeSelf;
        isActive[2] = stats.activeSelf;

        int activeCount = 0;
        foreach (bool active in isActive)
            if (active) activeCount++;

        float baseX = -160;
        float stepX = 320;
        float yPosition = 200;
        float upperYPosition = 450;

        if (activeCount == 3)
        {
            targetPositionEmotions = new Vector3(baseX, yPosition, 0);
            targetPositionPayment = new Vector3(baseX + stepX, yPosition, 0);
            targetPositionStats = new Vector3(baseX + stepX / 2, upperYPosition, 0);
        }
        else
        {
            int index = 0;
            targetPositionEmotions = !isActive[0] ? targetPositionEmotions : new Vector3(baseX + stepX * index++, yPosition, 0);
            targetPositionPayment = !isActive[1] ? targetPositionPayment : new Vector3(baseX + stepX * index++, yPosition, 0);
            targetPositionStats = !isActive[2] ? targetPositionStats : new Vector3(baseX + stepX * index, yPosition, 0);
        }
    }

    void MoveToPositions()
    {
        emotionsHolderCanvas.transform.localPosition = Vector3.Lerp(emotionsHolderCanvas.transform.localPosition, targetPositionEmotions, Time.deltaTime * moveSpeed);
        payment.transform.localPosition = Vector3.Lerp(payment.transform.localPosition, targetPositionPayment, Time.deltaTime * moveSpeed);
        stats.transform.localPosition = Vector3.Lerp(stats.transform.localPosition, targetPositionStats, Time.deltaTime * moveSpeed);
    }
}
