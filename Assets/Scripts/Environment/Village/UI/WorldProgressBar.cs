﻿using System;
using System.Collections;
using CameraControlling;
using DG.Tweening;
using nickeltin.Extensions;
using nickeltin.Tweening;
using nickeltin.UI;
using Source;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GameEnvironment.Village.UI
{
    public class WorldProgressBar : MonoBehaviour
    {
        private Transform _targetedCamera;
        public float appearTime;
        public float hideTime;
        public TMP_Text label;
        public Image barImage;
        public float fillSpeed;
        public CanvasGroup m_canavasGroup;

        [SerializeField] Image progressBarIcon;

        public bool isTimeBar = true;

        private Vector3 m_lastCameraPosition;
        //private RectTransform m_rect;

        Coroutine timerCoroutine;

        public bool Shown { get; private set; }

        private void Start()
        {
            _targetedCamera = GameCamera.MainCamera.transform;
        }

        private void Update()
        {
            transform.LookAt(
                    transform.position + _targetedCamera.rotation * Vector3.forward,
                    _targetedCamera.rotation * Vector3.up);
        }

        private void OnBecameVisible()
        {
            enabled = true;
        }

        private void OnBecameInvisible()
        {
            enabled = false;
        }

        public void Fill(float amount)
        {
            barImage.DOFillAmount(amount, fillSpeed);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            m_canavasGroup.DOFade(1, appearTime);
        }

        public void Hide()
        {
            m_canavasGroup.DOFade(0, hideTime).onComplete += () =>
            {
                Shown = false;
                gameObject.SetActive(false);
            };
        }

        public void HideImmediate()
        {
            m_canavasGroup.alpha = 0;
            Shown = false;
            gameObject.SetActive(false);
        }

        public void StartTimer(float from, float to, string ending = "s", float timeStep = 1)
        {
            timerCoroutine = StartCoroutine(TimerProgressBar(from, to, ending, timeStep));
        }

        IEnumerator TimerProgressBar(float from, float to, string ending, float timeStep)
        {
            float valueToAdd = from > to ? -timeStep : timeStep;
            var maxValue = Math.Max(from, to);

            label.text = from.ToString() + ending;
            var waiter = new WaitForSeconds(timeStep);

            Fill(from / maxValue);

            while (valueToAdd < 0 && from > to || valueToAdd > 0 && from < to)
            {
                yield return waiter;
                from += valueToAdd;
                Fill(from / maxValue);
                label.text = from.ToString() + ending;
            }
        }

        private void OnDisable()
        {
            if(timerCoroutine != null)
                StopCoroutine(timerCoroutine);
        }

        public void SetPBIcon(Sprite sprite) => progressBarIcon.sprite = sprite;
    }
}