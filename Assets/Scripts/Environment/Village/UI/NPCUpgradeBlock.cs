﻿using System;
using nickeltin.GameData.DataObjects;
using NPCs;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NPCUpgradeBlock : MonoBehaviour
{
    [SerializeField] private TMP_Text m_strengthText;
    [SerializeField] private TMP_Text m_speedText;
    [SerializeField] private TMP_Text m_upgradePriceText;
    [SerializeField] private Button m_upgradeButton;
    [SerializeField] private Image m_npcIcon;
    [SerializeField] private NumberObject m_currency;
    [SerializeField] private Button m_hireButton;
    [SerializeField] private TMP_Text m_hirePriceText;
    [SerializeField] private RectTransform m_npcUiParent;
    [SerializeField] private TMP_Text m_npcNameText;

    private NPC m_npc;
    private float m_hirePrice;
    private Action m_hireAction;

    public void SetNPC(NPC npc)
    {
        m_npcUiParent.gameObject.SetActive(true);
        m_hireButton.gameObject.SetActive(false);
        
        m_npc = npc;
       // m_npcNameText.text = npc.Name;
        //m_strengthText.text = "x" + npc.DamageMultiplier.ToString("F2");
       // m_speedText.text = "x" + ((npc.MovementSpeedMultiplier + npc.WorkSpeedMultiplier) / 2).ToString("F2");
        //m_npcIcon.sprite = npc.icon;
        
        //UpdateUpgradeButton(m_currency.Value);
    }
    
    public void SetHireAction(float hirePrice, Action onButtonClick)
    {
        m_npcUiParent.gameObject.SetActive(false);
        m_hireButton.gameObject.SetActive(true);
        m_hirePrice = hirePrice;
        m_hireAction = onButtonClick;
        
        UpdateHireButton(m_currency.Value);
    }

    public void DisableContent()
    {
        m_npcUiParent.gameObject.SetActive(false);
        m_hireButton.gameObject.SetActive(false);
    }
    
    /*public void UpgradeNPC()
    {
        if (m_npc != null && m_currency >= m_npc.UpgradePrice)
        {
            m_currency.Value -= m_npc.UpgradePrice;
            m_npc.Level++;
            SetNPC(m_npc);
        }
    }*/

    public void HireNPC()
    {
        if (m_currency >= m_hirePrice)
        {
            m_hireAction?.Invoke();
            m_currency.Value -= m_hirePrice;
        }
    }

    /*private void UpdateUpgradeButton(float currencyValue)
    {
        if (m_npc != null)
        {
            if (m_currency >= m_npc.UpgradePrice) m_upgradeButton.interactable = true;
            else m_upgradeButton.interactable = false;
            m_upgradePriceText.text = m_npc.UpgradePrice.ToString("F0");
        }
    }*/

    private void UpdateHireButton(float currencyValue)
    {
        if (m_currency >= m_hirePrice) m_hireButton.interactable = true;
        else m_hireButton.interactable = false;
        m_hirePriceText.text = m_hirePrice.ToString("F0");
    }
    
    /*private void OnEnable()
    {
        m_currency.onValueChanged += UpdateUpgradeButton;
        m_currency.onValueChanged += UpdateHireButton;
    }

    private void OnDisable()
    {
        m_currency.onValueChanged -= UpdateUpgradeButton;
        m_currency.onValueChanged -= UpdateHireButton;
    }*/
}
