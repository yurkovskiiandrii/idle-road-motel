using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomerEmotionUI : CustomerControllerUI
{
    private Transform _targetedCamera;

    public List<Sprite> displeasureimagePool;
    public List<Sprite> pleasureimagePool;
    public Image isShowing;

    [SerializeField] private int maxRating;
    [SerializeField] private GameObject ratingBar;
    [SerializeField] private Image progressBar;
    [SerializeField] private TMP_Text label;

    public Color colorOne;
    public Color colorTwo;
    public Color colorThree;
    private IEnumerator coroutine;

    protected override void Awake()
    {
        base.Awake();
        coroutine = StartTimerEmotions();
    }

    public void ShowDispleasure()
    {
        isShowing.sprite = displeasureimagePool[Random.Range(0, displeasureimagePool.Count)];
        isShowing.gameObject.SetActive(true);
        Debug.Log("Showed displeasure");
        StopCoroutine(coroutine);
        coroutine = StartTimerEmotions();
        StartCoroutine(StartTimerEmotions());
    }

    public void ShowRamdomEmotion()
    {
        isShowing.sprite = pleasureimagePool[Random.Range(0, pleasureimagePool.Count)];
        isShowing.gameObject.SetActive(true);
        StopCoroutine(coroutine);
        coroutine = StartTimerEmotions();
        StartCoroutine(StartTimerEmotions());
    }

    public void ShowAddedRating(float ratingAdded, float currentRating)
    {
        ShowRamdomEmotion();

        ShowCurrentRating(ratingAdded, currentRating, false);
    }

    public void ShowLostRating(float ratingLost, float currentRating)
    {
        ShowDispleasure();

        ShowCurrentRating(ratingLost, currentRating, true);
    }

    public void ShowCurrentRating(float rating, float currentRating, bool isRatingLost)
    {
        int temp = (int)rating;
        if (isRatingLost)
            label.SetText("-" + temp.ToString());
        else
            label.SetText("+" + temp.ToString());
        ratingBar.SetActive(true);
        ChangeBarFill(rating, currentRating);
        StopCoroutine(coroutine);
        coroutine = StartTimerEmotions();
        
        _uiAdjuster.AdjustUI();
        holder.gameObject.SetActive(true);

        StartCoroutine(StartTimerEmotions());
    }

    IEnumerator StartTimerEmotions()
    {
        yield return new WaitForSeconds(hideTime);
        
        _uiAdjuster.AdjustUI();
        holder.gameObject.SetActive(false);
        
        isShowing.gameObject.SetActive(false);
        ratingBar.SetActive(false);
    }

    void ChangeBarFill(float rating, float currentRating)
    {
        Debug.Log("rating changed " + currentRating + " rating diff is:" + rating);

        float fillAmount = (float)currentRating / (float)maxRating;
        progressBar.fillAmount = fillAmount;

        // Змінюємо колір в залежності від заповненості
        if (fillAmount <= 0.33f)
        {
            progressBar.color = colorOne;
        }
        else if (fillAmount <= 0.66f)
        {
            progressBar.color = colorTwo;
        }
        else
        {
            progressBar.color = colorThree;
        }
    }
}