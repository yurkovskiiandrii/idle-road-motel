using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomerControllerUI : MonoBehaviour
{
    protected Transform _targetedCamera;
    public float hideTime;
    protected IEnumerator coroutine;
    public static Canvas сanvas;
    public UIAdjuster _uiAdjuster;
    [SerializeField] protected GameObject holder;

    protected virtual void Awake()
    {
        сanvas = GetComponent<Canvas>();
    }

    protected virtual void Update()
    {
        transform.LookAt(
            transform.position + _targetedCamera.rotation * Vector3.forward,
            _targetedCamera.rotation * Vector3.up);
    }

    protected virtual void Start()
    {
        _targetedCamera = GameCamera.MainCamera.transform;
        _uiAdjuster = this.GetComponent<UIAdjuster>();
    }
}