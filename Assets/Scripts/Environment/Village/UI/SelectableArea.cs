﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class SelectableArea : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        var dif = eventData.pointerCurrentRaycast.screenPosition.normalized -
                  eventData.pointerPressRaycast.screenPosition.normalized;
        if (dif.x == 0 && dif.y == 0)
            Show();
        Debug.Log("CLick");



    }

    protected abstract void Show();
}