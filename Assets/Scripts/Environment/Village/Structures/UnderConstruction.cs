﻿using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using nickeltin.Extensions;
using UnityEngine;

public class UnderConstruction : MonoBehaviour
{
    [MinMaxSlider(0f, 10f)] public Vector2 emitParticlesEvery;
    public Animator animator;
    public List<ParticleSystem> particles;

    private float timeSinceLastEmission = 0;
    private float timeBetweenEmissions;
    
    private void OnEnable()
    {
        timeSinceLastEmission = 0;
        timeBetweenEmissions = emitParticlesEvery.GetRandomValueBetweenAxis();
    }

    private void Update()
    {
        timeSinceLastEmission += Time.deltaTime;

        if (timeSinceLastEmission > timeBetweenEmissions)
        {
            timeBetweenEmissions = emitParticlesEvery.GetRandomValueBetweenAxis();
            timeSinceLastEmission = 0;
            particles.GetRandom().Play();
        }
    }
}
