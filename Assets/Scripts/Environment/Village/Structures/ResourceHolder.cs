﻿using GameEnvironment.Village.Grid;
using UnityEngine;

namespace GameEnvironment.Village.Structures
{
    [RequireComponent(typeof(Storage))]
    public abstract class ResourceHolder : ServiceZone
    {
        protected Storage _storage;
        protected ResourceHolder _transferZone;
        public float Capacity => _storage.Capacity;
        public float CurrentAmount => _storage.CurrentAmount;
        public override bool Unavailable { get; }
        public abstract void InvokeWorkAvailable();
        public virtual float Get(float amount) => _storage.Get(amount);
        public virtual float Add(float amount) => _storage.Add(amount);
        public virtual void Set(float amount, bool updateProgressBar = false) => _storage.Set(amount, updateProgressBar);

        protected override void Awake()
        {
            base.Awake();
            _storage = GetComponent<Storage>();
        }

        public void SetNewStats(float capacity)
        {
            _storage.Capacity = capacity;

            if (CurrentAmount > 0)
                Set(Capacity, true);
        }

        public virtual void DestroyEventInvoke()
        {

        }
    }
}