﻿using System;
using System.Collections;
using System.Threading.Tasks;
using nickeltin.Extensions;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace GameEnvironment.Village.Structures
{
    [RequireComponent(typeof(Storage))]
    public class ResourceTransfer: MonoBehaviour
    {
        [SerializeField,ReadOnly] private SpendingZone _transferFrom;
        [SerializeField] private GettingZone _transferTo;
        [SerializeField] private float _frequency;
        private float _amount;
        [SerializeField] private float _multiplier = 1;

        [SerializeField] UnityEvent onTransferComplete;

        private bool _alreadyTransfer;
        private WaitForSeconds _waitForSeconds = null;

        [SerializeField] GettingZone[] gettingZones;

        Storage storage;

        private void Awake()
        {
            _waitForSeconds = new WaitForSeconds(_frequency);

            _transferFrom = GetComponent<SpendingZone>();
        }

        void RegisterZoneToTransfer(GettingZone zone)
        {
            //zone.AddListenerOnDestroy(Transfer);

            //var zoneCopy = zone;
            //storage.SubscribeOnValueChanged(x => zoneCopy.DestroyEventInvoke());
        }

        private IEnumerator Start()
        {
            var serviceManager = _transferTo.GetService();

            if(gettingZones.Length == 0)
                gettingZones = serviceManager.GetZones<GettingZone>();

            storage = _transferFrom.GetStorage();

            //serviceManager.SubscribeOnPropsActivated(RegisterZoneToTransfer);

            while (true)
            {
                foreach(var zone in gettingZones)
                    Transfer(zone);

                yield return _waitForSeconds;
            }
        }
        ////private void Start()
        ////{

        ////    _transferFrom.OnResourceAdded += AsyncTransfer;
        ////    if(_transferFrom.CurrentAmount>0)
        ////        AsyncTransfer(false);
        ////}

        private void Transfer(GettingZone transferTo)
        {
            int needCount = transferTo.GetCountToFull();

            if (needCount > 0 && _transferFrom.CurrentAmount > 0 && transferTo.IsActivated()
                 && !transferTo.IsBooked())
            {
                _amount = needCount;

                var realAmount = Mathf.Clamp(_amount, 0, _transferFrom.Get(_amount));
                var lack = transferTo.Add(realAmount * _multiplier);
                transferTo.InvokeWorkAvailable();

                if (lack < realAmount * _multiplier)
                    _transferFrom.Add(realAmount - lack / _multiplier);

                onTransferComplete.Invoke();
            }
        }

        //private async void AsyncTransfer(bool inner)
        //{
            //if ((!_alreadyTransfer || inner) && _transferFrom.CurrentAmount > 0 &&
            //    _transferTo.CurrentAmount < _transferTo.Capacity)
            //{
            //    _alreadyTransfer = true;

            //    await Task.Delay(TimeSpan.FromSeconds(_frequency));

            //    var realAmount = Mathf.Clamp(_amount, 0, _transferFrom.Get(_amount));
            //    var lack = _transferTo.Add(realAmount * _multiplier);
            //    _transferTo.InvokeWorkAvailable();
            //    if (lack < realAmount * _multiplier)
            //        _transferFrom.Add(lack / _multiplier);
            //    if (_transferFrom.CurrentAmount > 0 &&
            //        _transferTo.CurrentAmount < _transferTo.Capacity)
            //    {
            //        AsyncTransfer(true);
            //    }
            //    else
            //        _alreadyTransfer = false;
            //}
        //}
    }
}