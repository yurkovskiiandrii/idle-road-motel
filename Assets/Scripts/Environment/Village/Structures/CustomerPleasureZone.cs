﻿using System;
using System.Linq;
using DG.Tweening;
using GameData;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.Saving;
using NPCs;
using UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace GameEnvironment.Village.Structures
{
    public class CustomerPleasureZone : SpendingZone
    {
        [SerializeField] RequirementsList requirements;

        [Dropdown("requirements.requirementsNames")]
        public string requirementType;

        public int zoneLevel;

        UnityEvent<SpendingZone> onStorageUpdated = new UnityEvent<SpendingZone>();

        protected override void Awake()
        {
            base.Awake();

            _storage.SubscribeOnValueChanged((int i) => onStorageUpdated.Invoke(this));

            transform.parent.gameObject.GetComponent<PropsBehaviour>().SubscribeOnPropsLeveling(SetZoneLevel);
        }

        public int GetZoneLevel() => zoneLevel;

        void SetZoneLevel(int level) => zoneLevel = level;

        public void SubscribeOnSrorageUpdate(UnityAction<SpendingZone> action) => onStorageUpdated.AddListener(action);

        public void RemoveAllOnSrorageUpdate() => onStorageUpdated.RemoveAllListeners();

        public void UnSubscribeOnSrorageUpdate(UnityAction<SpendingZone> action) => onStorageUpdated.RemoveListener(action);
    }
}