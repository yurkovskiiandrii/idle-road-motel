using GameEnvironment.Village.Structures;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.DataObjects;
using UnityEngine;

[RequireComponent(typeof(SpendingZone))]
public class TransferToCurrency : MonoBehaviour
{
    [SerializeField] private Currency _currency;

    public void AddToPoints(float value)
    {
        _currency.Value += value;           
    }
}
