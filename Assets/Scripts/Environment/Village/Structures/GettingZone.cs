﻿using System;
using System.Linq;
using DG.Tweening;
using GameData;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.Saving;
using NPCs;
using UI;
using UnityEngine;
using UnityEngine.Events;

namespace GameEnvironment.Village.Structures
{
    public class  GettingZone : ResourceHolder, IPopUpHolder
    {
        [SerializeField] private Animator animator;

        [SerializeField] bool isMaxAmountOnStart = true;

        private static readonly int Hit = Animator.StringToHash("Hit");
        private static readonly int Amount = Animator.StringToHash("AmountPercent");
        public override bool Unavailable => CurrentAmount <= 0 || !isActivated || isBooked;
        public override void InvokeWorkAvailable() => _service.InvokeGettingAvailable(this);
        
        [SerializeField] private int _resourceLevel;
        [SerializeField, Required] private PopUpWorldIconsScheme _popUpWorldIconsScheme;
        
        private GameData.Resource _resource;
        public ServiceManager.ServiceSave.Save _save;
        private float _upgradeTime;
        public PopUpWorldIconsScheme PopUpWorldIconsScheme  => _popUpWorldIconsScheme;

        [SerializeField] float doWorkEventDelay = 1;
        [SerializeField] UnityEvent doWorkEvent;

        UnityEvent<GettingZone, int> destroyEvent = new UnityEvent<GettingZone, int>();

        public void DoWork()
        {
            doWorkEvent.Invoke();

            if (animator != null)
                animator.SetTrigger(Hit);
            else
                Debug.LogError("animator is null");
        }

        protected override void Awake()
        {
            base.Awake();

            if (animator != null)
                animator.SetInteger(Amount, (int)(CurrentAmount/Capacity*100));

            if (TryGetComponent(out SpendingZone zone))
            {
                _transferZone = zone;
            }
        }

        public override void Initialization()
        {
            base.Initialization();
            SaveSystem.GetSavedItem(out ResourcesData resourcesData);
            if (!resourcesData.GetResourceData(_service.Service, out var resource)) return;

            _resource = new GameData.Resource();
            _resource = resource;
            _save = _service.File.Resource.FirstOrDefault(x => x.ID == _id);
            if (_save == null)
            {
                _resourceLevel = 1;
            }
            else
            {
                _resourceLevel = _save.Level;
            }

            //_storage.Capacity = _resource.Stats[_resourceLevel - 1].ResourcesCapacity;

            if (_save == null)
            {
                _service.File.Resource.Add(new ServiceManager.ServiceSave.Save(_id, 1, 0));
                _save = _service.File.Resource.FirstOrDefault(x => x.ID == _id);
                _save.CurrentAmount = isMaxAmountOnStart ? (int)_storage.Capacity : 0;

                return;
            }

            if (isActivated)
                _storage.Set(_service.File.Resource.FirstOrDefault(x => x.ID == _id).CurrentAmount);
        }

        public int Get() => (int)CurrentAmount;

        public override float Get(float amount)
        {
            var remains = base.Get(amount);

            if (animator != null)
            {
                animator.SetTrigger(Hit);
                animator.SetInteger(Amount, (int) (CurrentAmount / Capacity * 100));
            }

            if (CurrentAmount <= 0 && _resource != null)
                Destroy();

            return remains;
        }

        public float GetMaxCount()
        {
            var remains = base.Get(CurrentAmount);

            if (animator != null)
            {
                animator.SetTrigger(Hit);
                animator.SetInteger(Amount, (int)(CurrentAmount / Capacity * 100));
            }

            Invoke(nameof(Destroy), 0.01f);

            //DOVirtual.DelayedCall(0.01f, Destroy);

            return remains;
        }

        public override void Set(float amount, bool updateProgressBar = false)
        {
            base.Set(amount, updateProgressBar);

            //CurrentAmount = amount;
            _save.CurrentAmount = (int)amount;

            if (animator)
                animator.SetInteger(Amount, (int)(CurrentAmount / Capacity * 100));
        }

        public override float Add(float amount)
        {
            var count = base.Add(amount);

            //CurrentAmount = amount;
            _save.CurrentAmount = (int)count;

            if (animator)
                animator.SetInteger(Amount, (int)(CurrentAmount / Capacity * 100));

            return count;
        }

        private void Destroy()
        {
            //_upgradeTime = _resource.Stats[_resourceLevel].UpgradeTime;
            DestroyEventInvoke();

            var spendingZone = gameObject.GetComponent<SpendingZone>();

            BookService(false);

            if (spendingZone)
                spendingZone.InvokeSpendingAvailable();

            _save.CurrentAmount = (int)_storage.CurrentAmount;

            _resourceLevel++;
            _save.Level = _resourceLevel;

            //ShowProgressBar(_upgradeTime, () =>
            //{
            //    _resourceLevel++;
            //    _save.Level = _resourceLevel;
            //    animator.SetInteger(Amount, (int) (CurrentAmount / Capacity * 100));
            //    InvokeWorkAvailable();
            //});
        }

        public bool IsFull() => CurrentAmount == Capacity;

        private void ShowProgressBar(float time, Action callback)
        {
            _storage.TryToShowProgressBar(time/_upgradeTime, time);
            if (time < 0) 
                callback.Invoke();
            else
            {
                DOVirtual.DelayedCall(1, () => ShowProgressBar(time - 1, callback));
            }
        }

        public override void Activate(bool value = true)
        {
            base.Activate(value);

            var spendingZone = GetComponent<SpendingZone>();

            if (isMaxAmountOnStart)
                Set(Capacity);

            if (spendingZone)
                spendingZone.Activate(value);
        }

        public void UpdateSave()
        {
            _save.CurrentAmount = (int)_storage.CurrentAmount;

            if (animator)
                animator.SetInteger(Amount, (int)(CurrentAmount / Capacity * 100));
        }

        public void AddListenerOnDestroy(UnityAction<GettingZone, int> action) => destroyEvent.AddListener(action);

        public int GetCountToFull() => (int)(_storage.Capacity - _storage.CurrentAmount);

        public override void DestroyEventInvoke() => destroyEvent?.Invoke(this, (int)_storage.Capacity);

        public bool IsEndless() => _storage.IsEndless();
    }
}