﻿using NPCs;
using UnityEngine;

namespace GameEnvironment.Village.Structures
{
    public sealed class Couch : MonoBehaviour
    {
        public bool Free = true;
        public NPC NPC;
        public Transform LyingSpot;
    }
}