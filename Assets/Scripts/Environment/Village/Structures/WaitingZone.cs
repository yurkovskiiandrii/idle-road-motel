﻿using UnityEngine;

namespace GameEnvironment.Village.Structures
{
    public class WaitingZone : ServiceZone
    {
        public override bool Unavailable => isBooked;
    }
}