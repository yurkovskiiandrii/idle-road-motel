﻿using System;
using System.Collections;
using GameEnvironment.Village.UI;
using nickeltin.Editor.Attributes;
using UnityEngine;
using UI;
using UnityEngine.Events;
//using static UnityEditor.Experimental.GraphView.Port;

namespace GameEnvironment.Village.Structures
{
    [DisallowMultipleComponent]
    public class Storage : MonoBehaviour
    {
        [Header("Resource Values")] 
        [SerializeField] private float _capacity;
        [SerializeField] private float _currentAmount;
        [SerializeField] bool isEndlessResources;
        [Space] public UnityEvent[] OnAmountChanged;
        [Header("Components")]
        [SerializeField] private WorldProgressBar _progressBar;
        [SerializeField] private int _digitsAfterPointInNumbers=0;
        [SerializeField] private bool _alwaysShow = true;
        [SerializeField, HideIf("_alwaysShow")] private float _progressBarShowTime;
        public WorldProgressBar ProgressBar => _progressBar;
        private bool _haveProgressBar = true;
        private Coroutine _progressBarTween;

        [SerializeField] float percentToShowCollectButton;
        [SerializeField] UnityEvent onCollectingAvailable;
        [SerializeField] UnityEvent onResourceCollected;
        [SerializeField] Transform collectSpawnPoint;

        UnityEvent collectingResourcesEvent;

        UnityEvent<int> AmountChangedEvent = new UnityEvent<int>();

        public float Capacity
        {
            get =>  _capacity;
            set
            {
                if (_currentAmount > value)
                    _currentAmount = value;
                _capacity = value;

                TryToShowProgressBar(_currentAmount / _capacity);
            }
        }
        public float CurrentAmount => _currentAmount;

        private void Awake()
        {
            if (_progressBar == null)
            {
                _haveProgressBar = false;
                return;
            }

            InvokeAmountChangedEvent();

            collectingResourcesEvent = onCollectingAvailable;

            if (_alwaysShow) return;
            _progressBar.label.text = "";
            _progressBar.HideImmediate();
        }

        private void InvokeAmountChangedEvent()
        {
            InvokeAmountChanged((int)CurrentAmount);

            if (OnAmountChanged.Length < 1) return;
            if (OnAmountChanged.Length == 1)
            {
                OnAmountChanged[0].Invoke();
                return;
            }
            if (OnAmountChanged.Length == 2)
            {
                OnAmountChanged[_currentAmount == 0 ? 0:1].Invoke();
                return;
            }

            try
            {
                OnAmountChanged[(int)Mathf.Floor(_currentAmount * (OnAmountChanged.Length - 1) / _capacity)]?.Invoke();
            }
            catch
            {

            }
        }

        public float Add(float amount)
        {
            var lack = _capacity - _currentAmount;
            _currentAmount +=  Mathf.Clamp(amount, 0, lack);

            if (_currentAmount > _capacity) 
                _currentAmount = _capacity;

            AmountChanged();

            return lack;
        }

        public float Get(float amount, bool updateProgressBar = true)
        {
            var remains = _currentAmount;
            _currentAmount -= Mathf.Clamp(amount, 0, _currentAmount);
            if (_currentAmount < 1) _currentAmount = 0;
            AmountChanged(updateProgressBar);
            return remains;
        }

        public void Set(float amount, bool canResize = false, bool updateProgressBar = true)
        {
            if (canResize)
                _capacity = amount;
            //print(name + "   " + amount);
            _currentAmount = Mathf.Clamp(amount, 0, _capacity);

            if (isEndlessResources)
                _currentAmount = _capacity;

            AmountChanged(updateProgressBar);
        }

        private void AmountChanged(bool updateProgressBar = true)
        {
            if (_currentAmount >= Capacity * percentToShowCollectButton / 100)
            {
                collectingResourcesEvent?.Invoke();
                collectingResourcesEvent = null;
            }

            InvokeAmountChangedEvent();
            if(_haveProgressBar && updateProgressBar)
            {
                TryToShowProgressBar(_currentAmount / _capacity);
            }
        }

        public void TryToShowProgressBar(float normalizedValue, float time = -1)
        {
            IEnumerator ShowHealthBar()
            {
                yield return new WaitForSeconds(_progressBarShowTime);
                _progressBar.Hide();
                _progressBarTween = null;
            }

            if(_progressBar == null)
            {
                //Debug.LogError("_progressBar is null");
                return;
            }

            _progressBar.Fill(normalizedValue);

            if(_progressBar.isTimeBar)
            {
                if (time >= 0)
                    _progressBar.label.text = time.ToString("F" + _digitsAfterPointInNumbers) + "s";
                else
                    _progressBar.label.text = "";
            }
            else
                _progressBar.label.text = CoreGame.UI.LargeNumber.FormatEveryThirdPower((int)_currentAmount);


            if (_alwaysShow) return;
            
            if (!_progressBar.Shown) _progressBar.Show();
            if (_progressBarTween != null)
            {
                StopCoroutine(_progressBarTween);
                _progressBarTween = null;
            }
            _progressBarTween = StartCoroutine(ShowHealthBar());
        }

        public void CollectResources()
        {
            onResourceCollected.Invoke();
            collectingResourcesEvent = onCollectingAvailable;

            PopupText.PopupWorld(GetComponent<SpendingZone>(), collectSpawnPoint.position, _currentAmount);

            GetComponent<TransferToCurrency>()?.AddToPoints(_currentAmount);

            Set(0);
            gameObject.GetComponent<SpendingZone>().InvokeSpendingAvailable();
        }

        public void SubscribeOnValueChanged(UnityAction<int> action) => AmountChangedEvent.AddListener(action);

        public void InvokeAmountChanged(int value) => AmountChangedEvent.Invoke(value);

        public bool IsEndless() => isEndlessResources;
    }
}