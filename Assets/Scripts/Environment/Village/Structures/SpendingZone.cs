﻿using System;
using System.Threading.Tasks;
using UI;
using UnityEngine;

namespace GameEnvironment.Village.Structures
{
    public class SpendingZone : ResourceHolder, IPopUpHolder
    {
        [SerializeField] private float _spendingMultiplier = 1;
        [SerializeField] private PopUpWorldIconsScheme _popUpWorldIconsScheme;
        public PopUpWorldIconsScheme PopUpWorldIconsScheme  => _popUpWorldIconsScheme;

        public override bool Unavailable
        {
            get
            {
                return CurrentAmount >= Capacity && !_storage.IsEndless() || isBooked || !isActivated; 
            }
        }

        public event Action<bool> OnResourceAdded;
        public override void InvokeWorkAvailable() => _service.InvokeSpendingAvailable(this);
        protected override void Awake()
        {
            base.Awake();
            if (TryGetComponent(out GettingZone zone))
            {
                _transferZone = zone;
            }
        }

        public Storage GetStorage() => _storage;

        public override float Get(float amount)
        {
            var remains = base.Get(amount);
            InvokeWorkAvailable();
            return remains;
        }

        public float AddMax() => Add(Capacity);

        public override float Add(float amount)
        {
            var lack = base.Add(amount * _spendingMultiplier)/_spendingMultiplier;
            if(_transferZone != null)
                _transferZone.InvokeWorkAvailable();
            OnResourceAdded?.Invoke(false);

            return lack;
        }

        public void InvokeSpendingAvailable()
        {
            _service.InvokeSpendingAvailable(this);
        }
    }
}