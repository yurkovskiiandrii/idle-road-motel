﻿using System;
using System.Collections.Generic;
using System.Linq;
using NPCs;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Serialization;
using WaypointsSystem;

namespace GameEnvironment.Village.Structures
{
    public abstract class ServiceZone : MonoBehaviour, IObjectID
    {
        [SerializeField] protected bool addToServiceOnStart = true;
        [SerializeField] protected string _id;
        [SerializeField] protected InteractableObject[] _interactableObjects = { };

        [FormerlySerializedAs("_serviceEvent")] [SerializeField]
        protected ServiceManager _service;

        public readonly Dictionary<Transform, InteractableObject> Interactions =
            new Dictionary<Transform, InteractableObject>();

        [SerializeField] protected bool isBooked = false;
        [SerializeField] protected bool isActivated;

        public bool IsBooked() => isBooked;

        public virtual void BookService(bool isBooked)
        {
            this.isBooked = isBooked;
            foreach (var obj in _interactableObjects)
                obj.Occupy(false);
        }

        public virtual void Activate(bool value = true)
        {
            isActivated = value;
        }

        public bool IsActivated() => isActivated;

        public ServiceManager GetService() => _service;

        public string ID
        {
            get
            {
                if (_id == "")
                    _id = gameObject.name;
                return _id;
            }
        }

        public abstract bool Unavailable { get; }

        public InteractableObject[] InteractableObjects => _interactableObjects;

        protected virtual void Awake()
        {
            if (!addToServiceOnStart)
                return;

            try
            {
                _service.AddZoneToService(ID, this);
            }
            catch
            {
            }
        }

        public virtual void Initialization()
        {
        }

        public void RegisterToZone(Transform npc, string id)
        {
            var i = _interactableObjects.FirstOrDefault(x => x.NPCId == id);
            if (i != null)
                Interactions.Add(npc, i);
        }

        public void RegisterToZone(Transform npc)
        {
            //var i = _interactableObjects.FirstOrDefault(x => x.NPCId == id);
            //if (i != null)
            Interactions.Add(npc, null);
        }

        public Transform GetInteractable(int index) => _interactableObjects[index].transform;

        public float GetInteractionTime(int index) => _interactableObjects[index].interactionTime;

        public Transform GetInteractable(out int placeIndex, Vector3 targetPos)
        {
            float minDistance = float.MaxValue;
            placeIndex = -1;

            for (int i = 0; i < _interactableObjects.Length; ++i)
            {
                if (_interactableObjects[i].isOccupied) // Перевіряємо, чи зайнятий об'єкт
                    continue;

                float distance = Vector3.Distance(targetPos, _interactableObjects[i].transform.position);

                if (distance < minDistance)
                {
                    minDistance = distance;
                    placeIndex = i;
                }
            }

            if (placeIndex == -1)
            {
                Debug.LogError("No available IO!!!");
                return null; // Повернення null, якщо вільних точок немає
            }

            _interactableObjects[placeIndex].Occupy(true); // Встановлення статусу зайнятості
            return _interactableObjects[placeIndex].transform;
        }

        public Transform GetInteractable(out int index, bool isRandom = false, bool useQueue = false)
        {
            index = -1;
            if (isRandom)
            {
                List<int> availableIndices = new List<int>();
                for (int i = 0; i < _interactableObjects.Length; i++)
                {
                    if (!_interactableObjects[i].isOccupied)
                        availableIndices.Add(i);
                }

                if (availableIndices.Count == 0)
                {
                    Debug.Break();
                    Debug.LogError("No available IO!!!");
                    return null;
                }

                int randomIndex = UnityEngine.Random.Range(0, availableIndices.Count);
                index = availableIndices[randomIndex];
                _interactableObjects[index].Occupy(true); // Встановлення статусу як зайнятий
                Debug.Log("Occupied Position is random: "  + index);
                return _interactableObjects[index].transform;
            }
            else if (useQueue)
            {
                for (int i = 0; i < _interactableObjects.Length; i++)
                {
                    if (!_interactableObjects[i].isOccupied)
                    {
                        index = i;
                        _interactableObjects[i].Occupy(true);
                        Debug.Log("Occupied Position: " + i + " " + index + " " + _interactableObjects[i].gameObject.name);
                        // Встановлення статусу як зайнятий
                        return _interactableObjects[i].transform;
                    }
                }

                Debug.LogError("No available IO!!!");
                return null;
            }

            Debug.LogError("Method called without a valid mode (random or queue)");
            return null;
        }


        public bool IsLastInteractableObject(int index) => index == _interactableObjects.Length - 1;

        public bool StartInteraction(Transform npc, int index, bool updateNPCPosition = true)
        {
            //if (!Interactions.TryGetValue(npc, out var interactableObject))
            //{
            //    Debug.LogError("Can't find exist interactions between " + npc.name + " and "+ _id);
            //    return false;
            //}

            var interactableObject = _interactableObjects[index];

            //if (_interactableObjects.Length == 1)
            //    BookService(true);

            interactableObject.Occupy(true);

            if (_interactableObjects.Length == 1)
                isBooked = true;

            if (updateNPCPosition)
            {
                if (interactableObject.TeleportToRootOnInteract)
                    npc.position = interactableObject.transform.position;
                if (interactableObject.UseRootRotationOnInteract)
                    npc.rotation = interactableObject.transform.rotation;
            }

            return true;
        }

        public virtual void EndInteraction(Transform npc, int index)
        {
            //if (!Interactions.TryGetValue(npc, out var interactableObject))
            //{
            //    Debug.LogError("Can't find exist interactions between " + npc.name + " and "+ _id);
            //    return;
            //}

            if (_interactableObjects.Length == 1 || index == -1)
            {
                BookService(false);
                return;
            }

            if (index >= 0 && index < _interactableObjects.Length)
            {
                _interactableObjects[index].Occupy(false); // Звільнення об'єкта
            }
            else
            {
                Debug.LogError("Invalid index provided for interaction ending");
            }
        }
    }
}