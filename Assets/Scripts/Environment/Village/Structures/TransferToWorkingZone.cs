using GameEnvironment.Village.Structures;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.DataObjects;
using UnityEngine;

[RequireComponent(typeof(SpendingZone))]
public class TransferToWorkingZone : MonoBehaviour
{
    [SerializeField] private SpendingZone _transferFrom;
    [SerializeField] private GettingZone _transferTo;

    [SerializeField] bool fillFullStorage = true;

    private void Start()
    {
        if(!_transferFrom)
            _transferFrom = GetComponent<SpendingZone>();

        if (!_transferTo)
            _transferTo = GetComponent<GettingZone>();

        _transferFrom.OnResourceAdded += AddToPoints;
    }

    private void AddToPoints(bool b)
    {
        var amount = _transferFrom.Get(_transferFrom.CurrentAmount);

        _transferTo.Set(fillFullStorage ? _transferTo.Capacity : amount);
    }
}
