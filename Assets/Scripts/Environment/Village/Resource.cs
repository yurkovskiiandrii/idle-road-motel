﻿using System;
using UnityEngine;

namespace GameEnvironment.Village
{
    [CreateAssetMenu(menuName = "Enviorment/Resource")]
    public class Resource : ScriptableObject, IEquatable<Resource>
    {
        public float price;
        public new string name;
        public Sprite icon;
        
        public bool Equals(Resource other) => other != null && name.Equals(other.name);
    }
}