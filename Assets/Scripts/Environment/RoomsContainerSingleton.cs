using NPCs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsContainerSingleton : SingletonComponent<RoomsContainerSingleton>
{
    [SerializeField] private RoomsContainer _rooms;

    public int GetRoomsCount() => _rooms.GetRoomsCount();

    //public int GetFreeRoomsCount() => _rooms.GetFreeRoomsCount();

    public ServiceManager GetRoom(int index) => _rooms.GetRoom(index);
}
