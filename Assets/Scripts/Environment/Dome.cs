﻿using GameEnvironment;
using GameEnvironment.Mine;
using nickeltin.Editor.Attributes;
using nickeltin.Extensions;
using nickeltin.Singletons;
using NPCs;
using UnityEngine;
using System.Collections.Generic;

public class Dome : MonoSingleton<Dome>
{
    [SerializeField] private ServiceManager _miningService;
    [SerializeField] private Transform _minersParent;
    [SerializeField] private Transform _minersSpawnPoint;
    [SerializeField] private float _minersSpawnZoneRadius;
    [ReadOnly]public bool IsWorking;
    [SerializeField] private MeshFilter m_transparentCube;

    private Vector3 m_transparentCubeBaseScale;
    //private int m_minersCount = 0;
    protected override void Awake()
    {
        base.Awake();
        m_transparentCubeBaseScale = m_transparentCube.transform.localScale;
    }

    private void Start()
    {
        if(TimeCycle.timeOfDay == TimeCycle.TimesOfDay.Night)
            return;
        FillMine();
    }

    private void DayTimeChanged(TimeCycle.TimesOfDay timesOfDay)
    {
        if(!MineManager.IsOnScene) return;
        if (timesOfDay == TimeCycle.TimesOfDay.Day)
        {
            FillMine();
        }
        else
        {
            ClearMine();
        }
    }

    private void FillMine()
    {
        foreach (var miner in _miningService.ActiveNpc.Values)
        {
            SpawnMiner(miner);
        }

        IsWorking = true;
    }

    public static NPC SpawnMiner(NPC npc)
    {
        if (!IsOnScene)
        {
            MoveMinerToVillage(npc);
            return npc;
        }
        npc.enabled = false;
        npc.ActivateNPC(false);
        var minerWorld = npc.GetComponent<MinerWorld>();
        var minerTransform = minerWorld.transform;
        minerTransform.parent = instance._minersParent;
        minerTransform.position = instance.GetSpawnPosition();
        minerTransform.gameObject.SetActive(true);
        minerWorld.enabled = true;
        
        return npc;
    }

    private static void MoveMinerToVillage(NPC npc)
    {
        var minerWorld = npc.GetComponent<MinerWorld>();
        minerWorld.enabled = false;
        var trans = minerWorld.transform;
        trans.parent = MinerTransfer.Transform;
        trans.localPosition = Vector3.zero;
        trans.parent = null;
        trans.rotation = new Quaternion(0,0,0,0);
        npc.enabled = true;

        if (TimeCycle.timeOfDay == TimeCycle.TimesOfDay.Day)
        {
            npc.gameObject.SetActive(false);
        }else
            npc.ActivateNPC(true);
    }

    private void ClearMine()
    {
        if (!IsWorking) return;

        foreach (var npc in _miningService.ActiveNpc.Values)
        {
            MoveMinerToVillage(npc);
        }
        IsWorking = false;
    }

    private Vector3 GetSpawnPosition()
    {
        Vector3 randomOffset = new Vector3(
            Random.Range(-_minersSpawnZoneRadius, _minersSpawnZoneRadius),
            Random.Range(-_minersSpawnZoneRadius, _minersSpawnZoneRadius),
            Random.Range(-_minersSpawnZoneRadius, _minersSpawnZoneRadius));

        return _minersSpawnPoint.position + randomOffset;
    }

    public void SetSize(Vector3 scaleXZ)
    {
        m_transparentCube.transform.localScale = m_transparentCubeBaseScale.Mult(new Vector3(scaleXZ.x, 1, scaleXZ.z));
    }

    private void OnEnable()
    {
        TimeCycle.onTimeChanged += DayTimeChanged;
    }

    private void OnDisable()
    {
        TimeCycle.onTimeChanged -= DayTimeChanged;
        ClearMine();
    }
}
