using System;
using System.Collections;
using GameEnvironment;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class SkyLookController : MonoBehaviour
{
    [SerializeField] private List<GameObject> LightInMotel;
    //[SerializeField] private GameObject Moon;

    [Tooltip("Довжина 1 доби в секундах")]
    [SerializeField] private float DayLength = 30; 
    
    private void Start()
    {
        
    }
    
    private void OnEnableDay()
    {
        Debug.Log("Day started");

        foreach (var light in LightInMotel)
        {
            light.SetActive(false);
        }

        //Sun.intensity = 0.65f;
    }

    private void OnEnableNight()
    {
        Debug.Log("Night started");

        foreach (var light in LightInMotel)
        {
            light.SetActive(true);
        }

        //Sun.intensity = 0f;
    }

    public void OnDayTimeChange(TimeCycle.TimesOfDay time)
    {
        
    }
    

    void Update()
    {
        
    }
}