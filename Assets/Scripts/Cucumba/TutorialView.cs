using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cucumba
{
    public class TutorialView : MonoBehaviour
    {
        public event System.Action OnInvisibleClicked = null;

        [Header("Stuff realted to Tutorial")]

        [SerializeField] private Vector2 m_ButtonSize = new Vector2(250, 250);

        [SerializeField] private RectTransform m_ClickableZone = null;
        [SerializeField] private UnityEngine.UI.Button m_ClickableZoneButton = null;

        [SerializeField] private UnityEngine.UI.Image m_TransparentImage = null;

        [SerializeField] private TutorialArrowPointer m_ArrowPointer = null;

        [SerializeField] private Canvas m_TutorialCanvas = null;

        [Header("Core Buttons")]

        [SerializeField] private UI.UIEvents m_ButtonTeleportToMine = null;

        [SerializeField] private UnityEngine.UI.Button m_ButtonTeleportToVillage = null;

        [SerializeField] private UnityEngine.UI.Button m_ButtonOpenUnitService = null;

        [SerializeField] private UI.UIEvents m_ButtonCloseNotifyCanvas = null;

        [SerializeField] private UnityEngine.UI.Button m_ButtonCloseUnitService = null;

        [SerializeField] private IconUI[] m_UnitServiceButtons = null;

        private void Start()
        {
            m_ClickableZoneButton.onClick.AddListener(ClickHandler);

            // temp
            CloseNotificationCanvas();
        }

        private void ClickHandler()
        {
            OnInvisibleClicked?.Invoke();
        }

        //public void ToggleInput(bool enabled)
        //{
        //    m_TransparentImage.enabled = enabled;
        //}

        private void ShrinkInvisible()
        {
            Rect rect = m_TutorialCanvas.pixelRect;
            Vector2 size = new Vector2(rect.width, rect.height) / m_TutorialCanvas.scaleFactor;
            m_ClickableZone.sizeDelta = size;
        }

        private void PlaceInvisibleAbove(RectTransform other)
        {
            m_ClickableZone.position = other.position;
            m_ArrowPointer.PlaceAt(other.position);
            m_ClickableZone.sizeDelta = m_ButtonSize;
        }

        private void PlaceInvisibleAboveUnitActionButton(int index)
        {
            RectTransform rt = m_UnitServiceButtons[index].GetComponentInChildren<UI.UIEvents>().GetComponent<RectTransform>();
            PlaceInvisibleAbove(rt);
        }

        public void TriggerUnitActionButton(int index)
        {
            m_UnitServiceButtons[index].GetComponentInChildren<UI.UIEvents>().TriggerPointerDown();
        }

        public void Destroy()
        {
            Destroy(m_TutorialCanvas.gameObject);
            Destroy(gameObject);
        }

        [ContextMenu("ToggleVisibility")]
        private void ToggleVisibility()
        {
            var buttonImage = m_ClickableZoneButton.GetComponent<UnityEngine.UI.Image>();
            var color = buttonImage.color;
            color.a = color.a == 0.5f ? 0 : 0.5f;
            buttonImage.color = color;

            color = m_TransparentImage.color;
            color.a = color.a == 0.25f ? 0 : 0.25f;
            m_TransparentImage.color = color;
        }

        [ContextMenu("TeleportToMine")]
        public void TeleportToMine()
        {
            m_ButtonTeleportToMine.TriggerPointerDown();
        }

        [ContextMenu("TeleportToVillage")]
        public void TeleportToVillage()
        {
            m_ButtonTeleportToVillage.onClick.Invoke();
        }

        [ContextMenu("OpenUnitService")]
        public void OpenUnitService()
        {
            m_ButtonOpenUnitService.onClick.Invoke();
        }

        [ContextMenu("CloseNotificationCanvas")]
        public void CloseNotificationCanvas()
        {
            m_ButtonCloseNotifyCanvas.TriggerPointerDown();
        }

        [ContextMenu("CloseUnitServiceMenu")]
        public void CloseUnitServiceMenu()
        {
            m_ButtonCloseUnitService.onClick.Invoke();
        }

        public void Step_0()
        {
            m_ArrowPointer.ToggleActive(false);

            ShrinkInvisible();
        }

        public void Step_1()
        {
            ShrinkInvisible();
        }

        public void Step_2()
        {
            m_ArrowPointer.ToggleActive(true);

            PlaceInvisibleAbove(m_ButtonTeleportToMine.GetComponent<RectTransform>());
        }

        public void Step_3()
        {
            TeleportToMine();

            PlaceInvisibleAbove(m_ButtonOpenUnitService.GetComponent<RectTransform>());
        }

        public void Step_4()
        {
            OpenUnitService();

            var co = InvokeDelayed(0.1f, () =>
            {
                Debug.Log("Hand above build (price 0) - first button");
                PlaceInvisibleAboveUnitActionButton(0);
            });
            StartCoroutine(co);
        }

        public void Step_5()
        {
            TriggerUnitActionButton(0);
            Debug.Log("Hand above upgrade (0) - second button");
            PlaceInvisibleAboveUnitActionButton(1);
        }

        public void Step_6()
        {
            TriggerUnitActionButton(1);
            Debug.Log("Hand above hire (0) - second button");
            PlaceInvisibleAboveUnitActionButton(1);
        }

        public void Step_7()
        {
            TriggerUnitActionButton(1);
            Debug.Log("Hand above unit close button");

            var rt = m_ButtonCloseUnitService.GetComponent<RectTransform>();
            PlaceInvisibleAbove(rt);
        }

        public void Step_8()
        {
            CloseUnitServiceMenu();

            Debug.Log("Hand above swipe to dig");
            m_ClickableZone.anchoredPosition = Vector2.zero;
            m_ArrowPointer.PlaceAtAnchored(m_ClickableZone);
        }

        public void Step_9()
        {
            Debug.Log("done!");

            m_ArrowPointer.ToggleActive(false);
        }

        private IEnumerator InvokeDelayed(float delayInSeconds, System.Action action)
        {
            yield return new WaitForSeconds(delayInSeconds);

            action?.Invoke();
        }
    }
}