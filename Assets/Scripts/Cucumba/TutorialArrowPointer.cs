using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialArrowPointer : MonoBehaviour
{
    [SerializeField] private RectTransform m_ArrowTransform = null;

    [SerializeField] private float m_Frequency = 1;
    [SerializeField] private float m_Amplitude = 100;

    private float m_ElapsedTime = 0;

    private Vector3 m_CurrentPosition = Vector3.zero;

    private Vector3 m_Direction = Vector3.up;

    private void Update()
    {
        m_ElapsedTime += m_Frequency * Time.deltaTime;

        float value01 = Mathf.Sin(m_ElapsedTime) * 0.5f + 0.5f;

        m_ArrowTransform.position = m_CurrentPosition + m_Direction * value01 * m_Amplitude;
    }

    public void ToggleActive(bool active)
    {
        m_ArrowTransform.gameObject.SetActive(active);
    }

    public void PlaceAt(Vector3 position)
    {
        m_CurrentPosition = position;
    }

    public void PlaceAtAnchored(RectTransform other)
    {
        PlaceAt(other.position);
    }
}
