using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CucumbaAnalyticsMine : MonoBehaviour
{
    [SerializeField] private Mineshaft m_Mineshaft = null;

    private void OnEnable()
    {
        m_Mineshaft.OnLayerSpawned += M_Mineshaft_OnLayerSpawned;
    }

    private void OnDisable()
    {
        m_Mineshaft.OnLayerSpawned -= M_Mineshaft_OnLayerSpawned;
    }

    private void M_Mineshaft_OnLayerSpawned(Destructables.GroundLayer layer)
    {
        layer.onDestruction += Layer_onDestruction;
    }

    private void Layer_onDestruction(Destructables.GroundLayer layer)
    {
        layer.onDestruction -= Layer_onDestruction;

        CucumbaAnalytics.Instance.NotifyDestroyedLayer();
    }
}
