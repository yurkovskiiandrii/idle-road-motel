using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class CucumbaAnalytics : MonoBehaviour
{
    [System.Serializable]
    private class PlayerData
    {
        public int DestroyedLayersAmount = 0;
        public int DestroyedLayersAmountNotified = 0;

        public int CurrencySpentAmount = 0;
        public int CurrencySpentAmountNotified = 0;

        public int CurrencyCollectedAmount = 0;
        public int CurrencyCollectedAmountNotified = 0;

        public int ServiceButtonClickedAmount = 0;
        public int ServiceButtonClickedAmountNotified = 0;
    }

    private readonly List<int> m_DestroyedLayersSequence = new List<int>()
    {
        1, 2, 5, 10, 15, 20
    };

    private readonly List<int> m_ServiceButtonClickedSequence = new List<int>()
    {
        1, 5, 10, 20, 25, 30, 50, 100
    };

    private readonly List<int> m_CurrencyGainedSequence = new List<int>()
    {
        100, 500, 1 * 1000, 2 * 1000, 5 * 1000, 10 * 1000, 20 * 1000, 50 * 1000, 75 * 1000, 100 * 1000
    };

    private readonly List<int> m_CurrencySpentSequence = new List<int>()
    {
        50, 100, 200, 300, 500, 1 * 1000, 2 * 1000, 5 * 1000, 10 * 1000, 20 * 1000
    };

    private const string SAVE_KEY = "ANALYTICS_SAVE";

    // singleton4ik
    public static CucumbaAnalytics Instance = null;

    [SerializeField] private bool m_LogEvents = false;

    private PlayerData m_PlayerData = null;

    // used in demo scene
    private int m_TimesClicked = 0;


    private void Start()
    {
        Instance = this;

        // in case of deserialization fuck-up - do not fuck-up analytics initializaiton
        try
        {
            LoadSave();
        }
        catch
        {
            m_PlayerData = new PlayerData();
        }

        GameAnalytics.Initialize();
    }

    private void LoadSave()
    {
        if (PlayerPrefs.HasKey(SAVE_KEY))
        {
            string json = PlayerPrefs.GetString(SAVE_KEY);
            m_PlayerData = JsonUtility.FromJson<PlayerData>(json);
        }
        else
        {
            m_PlayerData = new PlayerData();
        }
    }

    private void Save()
    {
        string json = JsonUtility.ToJson(m_PlayerData);
        PlayerPrefs.SetString(SAVE_KEY, json);
    }

    #region Demo Scene
    public void ClickHandler()
    {
        m_TimesClicked += 1;
        SendClickEvent();
    }

    private void SendClickEvent()
    {
        var dict = new Dictionary<string, object>()
        {
            ["timesClicked"] = m_TimesClicked,
            ["magic"] = 42,
        };

        GameAnalytics.NewDesignEvent("clickEvent", dict);
    }
    #endregion

    public void NotifyDestroyedLayer()
    {
        m_PlayerData.DestroyedLayersAmount += 1;

        if (ShouldNotify(m_DestroyedLayersSequence, m_PlayerData.DestroyedLayersAmount, ref m_PlayerData.DestroyedLayersAmountNotified))
        {
            if (m_LogEvents)
            {
                Debug.Log($"[event] layersDestroyed - {m_PlayerData.DestroyedLayersAmountNotified}");
            }

            GameAnalytics.NewDesignEvent("layersDestroyed", m_PlayerData.DestroyedLayersAmountNotified);
        }

        Save();
    }

    public void NotifyCurrencyDecreased(int amount)
    {
        m_PlayerData.CurrencySpentAmount += amount;

        if (ShouldNotify(m_CurrencySpentSequence, m_PlayerData.CurrencySpentAmount, ref m_PlayerData.CurrencySpentAmountNotified))
        {
            if (m_LogEvents)
            {
                Debug.Log($"[event] currencySpent - {m_PlayerData.CurrencySpentAmountNotified}");
            }

            GameAnalytics.NewDesignEvent("currencySpent", m_PlayerData.CurrencySpentAmountNotified);
        }

        Save();
    }

    public void NotifyCurrencyIncreased(int amount)
    {
        m_PlayerData.CurrencyCollectedAmount += amount;

        if (ShouldNotify(m_CurrencyGainedSequence, m_PlayerData.CurrencyCollectedAmount, ref m_PlayerData.CurrencyCollectedAmountNotified))
        {
            if (m_LogEvents)
            {
                Debug.Log($"[event] currencyCollected - {m_PlayerData.CurrencyCollectedAmountNotified}");
            }

            GameAnalytics.NewDesignEvent("currencyCollected", m_PlayerData.CurrencyCollectedAmountNotified);
        }

        Save();
    }

    public void NotifyServiceButton()
    {
        m_PlayerData.ServiceButtonClickedAmount += 1;

        if (ShouldNotify(m_ServiceButtonClickedSequence, m_PlayerData.ServiceButtonClickedAmount, ref m_PlayerData.ServiceButtonClickedAmountNotified))
        {
            if (m_LogEvents)
            {
                Debug.Log($"[event] serviceMenuButtonClicked - {m_PlayerData.ServiceButtonClickedAmount}");
            }

            GameAnalytics.NewDesignEvent("serviceMenuButtonClicked", m_PlayerData.ServiceButtonClickedAmountNotified);
        }

        Save();
    }

    private static bool ShouldNotify(List<int> sequence, int current, ref int notified)
    {
        int index = sequence.FindLastIndex(i => i <= current);

        bool notify = false;

        if (index >= 0 && sequence[index] > notified)
        {
            notified = sequence[index];
            notify = true;
        }

        return notify;
    }
}
