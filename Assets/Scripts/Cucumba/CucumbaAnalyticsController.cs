using UnityEngine;

public class CucumbaAnalyticsController : MonoBehaviour
{
    [SerializeField] private UI.Village.ServiceMenu m_ServiceMenu = null;

    [SerializeField] private nickeltin.GameData.DataObjects.Currency m_Currency = null;

    private void OnEnable()
    {
        m_ServiceMenu.OnButtonClickedSuccessfully += M_ServiceMenu_OnButtonClickedSuccessfully;
        m_Currency.OnValueDecreased += Currency_OnValueDecreased;
        m_Currency.OnValueIncreased += Currency_OnValueIncreased;
    }

    private void OnDisable()
    {
        m_ServiceMenu.OnButtonClickedSuccessfully -= M_ServiceMenu_OnButtonClickedSuccessfully;
        m_Currency.OnValueDecreased -= Currency_OnValueDecreased;
        m_Currency.OnValueIncreased -= Currency_OnValueIncreased;
    }

    private void Currency_OnValueDecreased(int amount)
    {
        CucumbaAnalytics.Instance.NotifyCurrencyDecreased(amount);
    }

    private void Currency_OnValueIncreased(int amount)
    {
        CucumbaAnalytics.Instance.NotifyCurrencyIncreased(amount);
    }

    private void M_ServiceMenu_OnButtonClickedSuccessfully()
    {
        CucumbaAnalytics.Instance.NotifyServiceButton();
    }
}
