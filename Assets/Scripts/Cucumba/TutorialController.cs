using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cucumba
{
    [System.Serializable]
    public class TutorialSaveData
    {
        public bool IsTutorialCompleted = false;
    }

    public class TutorialController : MonoBehaviour
    {
        private const string SAVE_KEY = "TUTORIAL_DATA";

        [SerializeField] private TutorialView m_TutorialView = null;

        [SerializeField] private int m_CurrentStepIndex = 0;

        private List<System.Action> m_Actions = new List<System.Action>();

        private void Start()
        {
            m_Actions.Add(m_TutorialView.Step_1);
            m_Actions.Add(m_TutorialView.Step_2);
            m_Actions.Add(m_TutorialView.Step_3);
            m_Actions.Add(m_TutorialView.Step_4);
            m_Actions.Add(m_TutorialView.Step_5);
            m_Actions.Add(m_TutorialView.Step_6);
            m_Actions.Add(m_TutorialView.Step_7);
            m_Actions.Add(m_TutorialView.Step_8);
            m_Actions.Add(m_TutorialView.Step_9);

            if (IsTutorialCompleted())
            {
                DestroySelf();
            }
            else
            {
                m_TutorialView.Step_0();
            }
        }

        private void OnEnable()
        {
            m_TutorialView.OnInvisibleClicked += M_TutorialView_OnInvisibleClicked;
        }

        private void OnDisable()
        {
            m_TutorialView.OnInvisibleClicked -= M_TutorialView_OnInvisibleClicked;
        }

        private void M_TutorialView_OnInvisibleClicked()
        {
            Debug.Log("Invisible clicked");

            m_Actions[m_CurrentStepIndex++].Invoke();

            if (m_CurrentStepIndex == m_Actions.Count)
            {
                Debug.Log("DONE!");
                SaveProgressAndDestroy();
            }
        }

        public void SaveProgressAndDestroy()
        {
            CompleteTutorial();
            DestroySelf();
        }

        private static bool IsTutorialCompleted()
        {
            // if deserialization fails that means json is garbage or tutorial is not completed
            // in this case simply do nothing
            // in case if data loaded successfully and player completed tutorial then simply destroy all stuff related to tutorial

            if (PlayerPrefs.HasKey(SAVE_KEY))
            {
                string json = PlayerPrefs.GetString(SAVE_KEY);
                try
                {
                    TutorialSaveData data = JsonUtility.FromJson<TutorialSaveData>(json);
                    if (data.IsTutorialCompleted)
                    {
                        return true;
                    }
                }
                catch { }
            }

            return false;
        }

        private void DestroySelf()
        {
            m_TutorialView.Destroy();
            Destroy(gameObject);
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Cucumba/Tutorial/RestoreTutorial")]
        private static void RestoreTutorial_Item()
        {
            RestoreTutorial();
        }

        [UnityEditor.MenuItem("Cucumba/Tutorial/CompleteTutorial")]
        private static void CompleteTutorial_Item()
        {
            CompleteTutorial();
        }

        [UnityEditor.MenuItem("Cucumba/Tutorial/CompleteTutorial", true)]
        private static bool CompleteTutorial_ItemValidator()
        {
            // https://docs.unity3d.com/ScriptReference/MenuItem.html
            // Validate the menu item defined by the function above.
            // The menu item will be disabled if this function returns false.

            return !IsTutorialCompleted();
        }
#endif

        [ContextMenu("RestoreTutorial")]
        private static void RestoreTutorial()
        {
            PlayerPrefs.DeleteKey(SAVE_KEY);
        }

        [ContextMenu("CompleteTutorial")]
        private static void CompleteTutorial()
        {
            string json = JsonUtility.ToJson(new TutorialSaveData() { IsTutorialCompleted = true });
            PlayerPrefs.SetString(SAVE_KEY, json);
        }
    }
}