using GameEnvironment.Village.Structures;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/GettingProgressData")]
public class GettingZoneProgression : ScriptableObject
{
    public AnimationCurve AmountProgress;
    public AnimationCurve RespawnTimeProgress;
}
