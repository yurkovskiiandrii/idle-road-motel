﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameData.SaveTypes
{
    [Serializable]
    public class ProgressSaveFile
    {
        public int[,,] LayerState = null;
        public int LayerIndex = 0;
        public float LayerHP = 0;
        public int VoxelsInLayerLeft = 0;
        public string LastGameDateTime;
        public float SuperTapValue = 0;
        public int LayersDestoryed = 0;
        public Dictionary<int,int> QuestProgress = new Dictionary<int,int>();
        public List<int> CompletedQuests = new List<int>();
    }
}