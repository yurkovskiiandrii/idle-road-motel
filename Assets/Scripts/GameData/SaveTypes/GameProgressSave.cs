﻿using System;
using System.Collections.Generic;
using GameData.SaveTypes;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using UnityEngine;
using Voxelization;

namespace Destructables
{
    [CreateAssetMenu(menuName = "GameData/GameProgressSave")]
    public class GameProgressSave : Saveable<ProgressSaveFile>
    {
        public NumberObject superTap;
        public NumberObject layersDestroyed;
        public List<int> CompletedQuests;
        private Dictionary<int, int> _questsProgress;

        public void SetLayerState(Voxel[,,] model)
        {
            if(model == null || File == null) return;

            int xSize = model.GetLength(0);
            int ySize = model.GetLength(1);
            int zSize = model.GetLength(2);
            
            File.LayerState = new int[xSize, ySize, zSize];
            for (int z = 0; z < zSize; z++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    for (int x = 0; x < xSize; x++)
                    {
                        File.LayerState[x, y, z] = model[x, y, z].type;
                    }
                }
            }
        }
        
        
        public void GetLayerState(Voxel[,,] model)
        {
            if(File.LayerState == null) return;
            
            int saveStateXMax = File.LayerState.GetLength(0);
            int saveStateYMax = File.LayerState.GetLength(1);
            int saveStateZMax = File.LayerState.GetLength(2);
            
            int xSize = model.GetLength(0);
            int ySize = model.GetLength(1);
            int zSize = model.GetLength(2);
            
            for (int z = 0; z < zSize; z++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    for (int x = 0; x < xSize; x++)
                    {
                        if (x < saveStateXMax && y < saveStateYMax && z < saveStateZMax)
                        {
                            model[x, y, z].type = File.LayerState[x, y, z];
                        }
                    }
                }
            }
        }

        protected override ProgressSaveFile Serialize()
        {
            File.LayersDestoryed = (int) layersDestroyed;
            File.SuperTapValue = (int) superTap;
            File.LastGameDateTime =  DateTime.UtcNow.ToString();
            File.CompletedQuests = CompletedQuests;
            File.QuestProgress = _questsProgress;
            return base.Serialize();
        }

        protected override void Deserialize(ProgressSaveFile obj)
        {
            base.Deserialize(obj);
            layersDestroyed.Value = File.LayersDestoryed;
            superTap.Value = File.SuperTapValue;
            CompletedQuests = File.CompletedQuests;
            _questsProgress = File.QuestProgress;
        }
        
        protected override void LoadDefault() => Deserialize(new ProgressSaveFile());

        public void UpdateQuestProgress(int questNumber, int progress)
        {
            if(_questsProgress.ContainsKey(questNumber))
            {
                _questsProgress[questNumber] = progress;
            }
            else
            {
                _questsProgress.Add(questNumber, progress);
            }
        }

        public int GetQuestProgress(int questNumber)
        {
            if(_questsProgress.ContainsKey(questNumber))
            {
                return _questsProgress[questNumber];
            }

            return 0;
        }
    }
}