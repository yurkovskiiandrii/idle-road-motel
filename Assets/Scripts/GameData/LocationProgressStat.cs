[System.Serializable]
public class LocationProgressStat
{
    public float locationProgress;
    public LocationStats locationStats = new LocationStats();
}

[System.Serializable]
public class LocationStats
{
    public int clientBasePaymentMin;
    public int clientBasePaymentMax;
    public int checkOutPaymentMult;
    public int clientFrequencyMin;
    public int clientFrequencyMax;
    public int clientStatsValueMin;
    public int clientStatsValueMax;
    public int maxStatTier;
    public int maxStats;
    public float statBarMultiplierMin;
    public float statBarMultiplierMax;
    public int propLevelMin;
    public int propLevelMax;
    public int leaveRankBorder;
    public int leaveRankChance;
    public float oneClient;
    public float doubleClient;
    public float tripleClient;
    public float quadroClient;
}