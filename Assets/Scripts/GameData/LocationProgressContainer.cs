using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LocationProgressContainer")]
public class LocationProgressContainer : DataFile
{
    public List<LocationProgressStat> progressions;
}
