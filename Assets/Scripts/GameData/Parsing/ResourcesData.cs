using System;
using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(menuName = "Data/Resources")]
    public class ResourcesData : DataFile
    {
        public Resource PlumbersServiceResource;
        public Resource CleanersServiceResource;
        public Resource Room1Resource;
        public Resource Room2Resource;
        public Resource Room3Resource;
        public Resource Room4Resource;
        public Resource PoolServiceResource;
        public Resource BarServiceResource;
        public Resource ReceptionResource;
        public Resource ParkingResource;

        public bool GetResourceData(ServiceType serviceType, out Resource resource)
        {
            switch (serviceType)
            {
                case ServiceType.PlumbersService:
                    resource = PlumbersServiceResource;
                    return true;
                case ServiceType.CleanersService:
                    resource = CleanersServiceResource;
                    return true;
                case ServiceType.Room1:
                    resource = Room1Resource;
                    return true;
                case ServiceType.Room2:
                    resource = Room2Resource;
                    return true;
                case ServiceType.Room3:
                    resource = Room3Resource;
                    return true;
                case ServiceType.Room4:
                    resource = Room4Resource;
                    return true;
                case ServiceType.PoolService:
                    resource = PoolServiceResource;
                    return true;
                case ServiceType.BarService:
                    resource = BarServiceResource;
                    return true;
                case ServiceType.ReceptionService:
                    resource = ReceptionResource;
                    return true;
                case ServiceType.ParkingService:
                    resource = ParkingResource;
                    return true;
                default:
                    resource = null;
                    return false;
            }
        }
       
    }
    [Serializable]
    public class Resource : IObjectID
    {
        public string ID { get => id; set =>id = value; }
        [SerializeField, ReadOnly] private string id;
        public int CurrentAmount;
        public List<ResourceStats> Stats = new List<ResourceStats>();
        //public Texture Icon;
    }

    [Serializable]
    public struct ResourceStats
    {
        public int LevelUpgrade;
        public float ResourcesCapacity;
        public float UpgradeTime;
    }
}