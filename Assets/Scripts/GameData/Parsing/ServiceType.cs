
namespace GameData
{
    public enum ServiceType
    {
        PlumbersService,
        CleanersService,
        Room1,
        Room2,
        Room3,
        Room4,
        PoolService,
        BarService,
        ClientsService,
        ParkingService,
        ReceptionService
    }

    public enum ServiceSpendingType
    {
        None,
        Regain,
        Getting,
        Spending
    }
}