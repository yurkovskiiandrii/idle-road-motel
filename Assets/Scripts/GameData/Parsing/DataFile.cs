using GameData;
using Newtonsoft.Json;
using nickeltin.GameData.Saving;
using UnityEngine;

public abstract class DataFile : RegistryItem
{
    [JsonIgnore] public DataFilesContainer WebAssets;
}