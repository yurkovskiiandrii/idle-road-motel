using System;
using System.Collections.Generic;
using Destructables;
using GameAnalyticsSDK.Events;
using nickeltin.Editor.Attributes;
using nickeltin.Extensions;
using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(menuName = "Data/Quests")]
    public class QuestData : DataFile
    {
        public List<Quest> Quests;
    }

    [Serializable]
    public class Quest
    {
        public string ID { get => id; set =>id = value; }
        [SerializeField, ReadOnly] private string id;
        
        public int Number;
        public string Name;
        public Condition Condition;
        public List<Reward> Rewards;
        [NonSerialized] private int _progress;
        [NonSerialized] private bool _completed;
        public event Action<int> OnProgressUpdated;
        public event Action OnQuestComplete;
        private GameProgressSave _gameProgressSave;


        public void Init(GameProgressSave gameProgressSave)
        {
            _gameProgressSave = gameProgressSave;
        }
        public void UpdateProgress(int progress)
        {
            if (_completed)
            {
                return;
            }
            _progress += progress;
            
           SetProgress(_progress);
        }
        public void SetProgress(int progress)
        {
            if (_completed)
            {
                return;
            }
            _progress = progress;
            _gameProgressSave.UpdateQuestProgress(Number, _progress);
            
            if (_progress >= Condition.Amount)
            {
                OnProgressUpdated?.Invoke((int)Condition.Amount);
                OnProgressUpdated = null;
                OnQuestComplete?.Invoke();
                OnQuestComplete = null;
                
                _completed = true;
            }
            else
            {
                OnProgressUpdated?.Invoke(_progress);
            }
        }

        public void TryAddReward(string type, float amount)
        {
            if (type.IsNullOrEmpty())
            {
                return;
            }
            
            var reward = new Reward()
            {
                Type = type,
                Amount = amount,
            };
            Rewards.Add(reward);
        }

        public int GetProgress() =>
            _progress;


    }

    [Serializable]
    public class Reward
    {
        public float Amount;
        public string Type;
    }

    [Serializable]
    public class Condition
    {
        public float Amount;
        public string Type;
        public string Target;
    }
}