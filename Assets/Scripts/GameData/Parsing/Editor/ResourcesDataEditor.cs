using System;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using GameData.Editor;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(ResourcesData))]
    public class ResourcesDataEditor : BaseDataEditor
    {
        private ResourcesData _target;

        public void OnEnable()
        {
            _target = (ResourcesData) target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PlumbersServiceResource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("CleanersServiceResource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room1Resource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room2Resource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room3Resource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room4Resource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PoolServiceResource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("BarServiceResource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ReceptionResource"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ParkingResource"), true);

            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.PlumbersServiceResource = new Resource();
            _target.CleanersServiceResource = new Resource();
            _target.Room1Resource = new Resource();
            _target.Room2Resource = new Resource();
            _target.Room3Resource = new Resource();
            _target.Room4Resource = new Resource();
            _target.PoolServiceResource = new Resource();
            _target.BarServiceResource = new Resource();
            _target.ReceptionResource = new Resource();
            _target.ParkingResource = new Resource();

            foreach (string file in files)
            {
                if (FillResourcesData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillResourcesData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration {CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru")};
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new ResourcesDataMapping();
                        Resource resource = new Resource();
                        foreach (var line in csv.EnumerateRecords(record))
                        {
                            if (line.ProviderID != "")
                            {
                                switch (line.ProviderID)
                                {
                                    case "PlumbersService_workzone":
                                        resource = _target.PlumbersServiceResource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "CleanersService_workzone":
                                        resource = _target.CleanersServiceResource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "Room1_workzone":
                                        resource = _target.Room1Resource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "Room2_workzone":
                                        resource = _target.Room2Resource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "Room3_workzone":
                                        resource = _target.Room3Resource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "Room4_workzone":
                                        resource = _target.Room4Resource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "PoolService_workzone":
                                        resource = _target.PoolServiceResource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "BarService_workzone":
                                        resource = _target.BarServiceResource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "ReceptionService_workzone":
                                        resource = _target.ReceptionResource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    case "ParkingService_workzone":
                                        resource = _target.ParkingResource;
                                        resource.ID = line.ProviderID;
                                        continue;
                                    default:
                                        return false;
                                }
                            }

                            var stats = new ResourceStats
                            {
                                LevelUpgrade = line.LevelUpgrade,
                                ResourcesCapacity = line.ResourcesCapacity,
                                UpgradeTime = line.UpgradeTime
                            };
                            resource.Stats.Add(stats);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
            return false;
        }
    }
}