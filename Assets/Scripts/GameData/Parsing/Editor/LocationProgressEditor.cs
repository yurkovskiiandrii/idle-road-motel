using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(LocationProgressContainer))]
    public class LocationProgressEditor : BaseDataEditor
    {
        LocationProgressContainer _target;

        public void OnEnable()
        {
            _target = (LocationProgressContainer)target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("progressions"), true);
            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.progressions.Clear();

            foreach (string file in files)
            {
                if (FillData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration { CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru") };
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new LocationProgressMapping();
                        _target.progressions = new List<LocationProgressStat>();

                        var res = csv.EnumerateRecords(record);

                        foreach (var line in res)
                        {
                            LocationProgressStat stats = new LocationProgressStat();

                            stats.locationProgress = line.LocationProgress;

                            stats.locationStats.clientBasePaymentMin = line.ClientBasePaymentMin;
                            stats.locationStats.clientBasePaymentMax = line.ClientBasePaymentMax;
                            stats.locationStats.checkOutPaymentMult = line.CheckOutPaymentMult;
                            stats.locationStats.clientFrequencyMin = line.ClientFrequencMin;
                            stats.locationStats.clientFrequencyMax = line.ClientFrequencyMax;
                            stats.locationStats.clientStatsValueMin = line.ClientStatsValueMin;
                            stats.locationStats.clientStatsValueMax = line.ClientStatsValueMax;
                            stats.locationStats.maxStatTier = line.MaxStatTier;
                            stats.locationStats.maxStats = line.MaxStats;
                            stats.locationStats.statBarMultiplierMin = line.StatBarMultiplierMin;
                            stats.locationStats.statBarMultiplierMax = line.StatBarMultiplierMax;
                            stats.locationStats.propLevelMin = line.propLevelMin;
                            stats.locationStats.propLevelMax = line.propLevelMax;
                            stats.locationStats.leaveRankBorder = line.LeaveRankBorder;
                            stats.locationStats.leaveRankChance = line.LeaveRankChance;
                            stats.locationStats.oneClient = line.OneClient;
                            stats.locationStats.doubleClient = line.DoubleClient;
                            stats.locationStats.tripleClient = line.TripleClient;
                            stats.locationStats.quadroClient = line.QuadroClient;

                            _target.progressions.Add(stats);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
            return false;
        }
    }
}