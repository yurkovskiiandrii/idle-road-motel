using System;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using GameData.Editor;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(BuildingsData))]
    public class BuildingsDataEditor : BaseDataEditor
    {
        private BuildingsData _target;

        public void OnEnable()
        {
            _target = (BuildingsData) target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PlumbersService"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("CleanersService"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room1"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room2"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room3"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room4"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PoolService"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("BarService"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ParkingService"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ReceptionService"), true);

            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.PlumbersService = new Buildings();
            _target.CleanersService = new Buildings();
            _target.Room1 = new Buildings();
            _target.Room2 = new Buildings();
            _target.Room3 = new Buildings();
            _target.Room4 = new Buildings();
            _target.PoolService = new Buildings();
            _target.BarService = new Buildings();
            _target.ParkingService = new Buildings();

            foreach (string file in files)
            {
                if (FillBuildingsData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillBuildingsData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration {CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru")};
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new BuildingsDataMapping();
                        Buildings buildings = new Buildings();
                        foreach (var line in csv.EnumerateRecords(record))
                        {
                            if (line.ServiceID != "")
                            {
                                var s = line.ServiceID.Trim();
                                if(!Enum.TryParse(s , out ServiceType serviceType))
                                    continue;
                                switch (serviceType)
                                {
                                    case ServiceType.PlumbersService:
                                        buildings = _target.PlumbersService;
                                        break;
                                    case ServiceType.CleanersService:
                                        buildings = _target.CleanersService;
                                        break;
                                    case ServiceType.Room1:
                                        buildings = _target.Room1;
                                        break;
                                    case ServiceType.Room2:
                                        buildings = _target.Room2;
                                        break;
                                    case ServiceType.Room3: 
                                        buildings = _target.Room3;
                                        break;
                                    case ServiceType.Room4:
                                        buildings = _target.Room4;
                                        break;
                                    case ServiceType.PoolService:
                                        buildings = _target.PoolService;
                                        break;
                                    case ServiceType.BarService:
                                        buildings = _target.BarService;
                                        break;
                                    case ServiceType.ParkingService:
                                        buildings = _target.ParkingService;
                                        break;
                                    case ServiceType.ReceptionService:
                                        buildings = _target.ReceptionService;
                                        break;
                                    default:
                                        return false;
                                }
                                buildings.ID = line.ServiceID;
                                buildings.Name = line.Name;
                                buildings.Description = line.Description;
                                continue;
                            }

                            var stats = new BuildingsStats
                            {
                                LevelUpgrade = line.LevelUpgrade,
                                //ResourcesCapacity = line.ResourcesCapacity,
                                UpgradePrice = line.UpgradePrice,
                                MaxNPCCount = line.MaxNPCCount,
                                MaxPropsCount = line.MaxPropsCount
                            };
                            buildings.Stats.Add(stats);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
            return false;
        }
    }
}