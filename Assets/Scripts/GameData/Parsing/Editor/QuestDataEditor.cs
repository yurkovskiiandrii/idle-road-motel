using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using GameData.Editor;
using nickeltin.Extensions;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(QuestData))]
    public class QuestDataEditor : BaseDataEditor
    {
        private QuestData _target;

        public void OnEnable()
        {
            _target = (QuestData)target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Quests"), true);

            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.Quests = new List<Quest>();

            foreach (string file in files)
            {
                if (FillData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration { CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru") };
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new QuestDataMapping();
                        
                        foreach (var line in csv.EnumerateRecords(record))
                        {

                            var quest = new Quest
                            {
                                Rewards = new List<Reward>()
                            };
                            
                            quest.Number = line.Number;
                            quest.Name = line.QuestId;
                            quest.TryAddReward(line.RewardType1, line.RewardAmount1);
                            quest.TryAddReward(line.RewardType2, line.RewardAmount2);
                            quest.TryAddReward(line.RewardType3, line.RewardAmount3);
                            quest.TryAddReward(line.RewardType4, line.RewardAmount4);
                            quest.TryAddReward(line.RewardType5, line.RewardAmount5);
                            
                            quest.Condition = new Condition()
                            {
                                Target = line.TargetId,
                                Amount = line.ConditionAmount,
                                Type = line.ConditionType,
                            };
                            _target.Quests.Add(quest);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }

            return false;
        }
    }
}