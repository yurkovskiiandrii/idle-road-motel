using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(UnitsData))]
    public class UnitsDataEditor : BaseDataEditor
    {
        private UnitsData _target;

        public void OnEnable()
        {
            _target = (UnitsData) target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PlumbersServiceUnits"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("CleanersServiceUnits"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room1Units"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room2Units"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room3Units"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room4Units"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PoolServiceUnits"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("BarServiceUnits"), true);
            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.PlumbersServiceUnits.Clear();
            _target.CleanersServiceUnits.Clear();
            _target.Room1Units.Clear();
            _target.Room2Units.Clear();
            _target.Room3Units.Clear();
            _target.Room4Units.Clear();
            _target.PoolServiceUnits.Clear();
            _target.BarServiceUnits.Clear();

            foreach (string file in files)
            {
                if (FillUnitsData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillUnitsData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration {CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru")};
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new UnitsDataMapping();
                        Unit unit = null;
                        List<Unit> service = null;
                        foreach (var line in csv.EnumerateRecords(record))
                        {
                            if (line.Service != "")
                            {
                                if (service != null)
                                {
                                    service.Add(unit);
                                    unit = null;
                                }
                                switch (line.Service)
                                {
                                    case "PlumbersService":
                                        service = _target.PlumbersServiceUnits;
                                        continue;
                                    case "CleanersService":
                                        service = _target.CleanersServiceUnits;
                                        continue;
                                    case "Room1":
                                        service = _target.Room1Units;
                                        continue;
                                    case "Room2":
                                        service = _target.Room2Units;
                                        continue;
                                    case "Room3":
                                        service = _target.Room3Units;
                                        continue;
                                    case "Room4":
                                        service = _target.Room4Units;
                                        continue;
                                    case "PoolService":
                                        service = _target.PoolServiceUnits;
                                        continue;
                                    case "BarService":
                                        service = _target.BarServiceUnits;
                                        continue;
                                    default:
                                        return false;
                                }
                            }
                            if (line.UnitID != "")
                            {
                                if (unit != null)
                                    service.Add(unit);
                                unit = new Unit {ID = line.UnitID, Name = line.Names, Description = line.Description};
                                continue;
                            }

                            var stats = new NpcStats()
                            {
                                Level = line.LevelUpgrade,
                                UpgradePrice = line.UpgradePrice,
                                WorkEfficiency = line.Efficiency,
                                MoveSpeed = line.MoveSpeed,
                                CritChance = line.CritChance,
                                CritMultiplier = line.CritMultiplier,
                                WorkSpeed = line.WorkSpeed,
                                CarriedMoveSpeed = line.CarriedMoveSpeed,
                                MaxCarriedResources = line.MaxCarriedResources,
                                TimeCycle = line.TimeCycle
                            };

                            if(stats.TimeCycle > 0)
                                unit.Stats.Add(stats);
                        }
                        service.Add(unit);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                 Debug.Log(ex.ToString());
            }
            return false;
        }
    }
}

