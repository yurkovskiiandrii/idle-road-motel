using CsvHelper.Configuration.Attributes;

public class ClientsDataMapping : DataMapping
{
    [Name("StatName"), Default("")] public string StatName { get; set; }
    [Name("Value"), Default(0)] public float Value { get; set; }
    [Name("Obligatory"), Default(0)] public float Obligatory { get; set; }
    [Name("Tier"), Default(0)] public int Tier { get; set; }
    [Name("TimeCycleMin"), Default(0)] public float TimeCycleMin { get; set; }
    [Name("TimeCycleMax"), Default(0)] public float TimeCycleMax { get; set; }
    [Name("StatBarCapacity"), Default(0)] public float StatBarCapacity { get; set; }
    [Name("RankObligatory"), Default(0)] public float RankMinus { get; set; }
    [Name("RankSecondary"), Default(0)] public float RankPlus { get; set; }
    [Name("ForKids"), Default(0)] public int ForKids { get; set; }
}
