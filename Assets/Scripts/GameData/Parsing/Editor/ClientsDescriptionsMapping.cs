using CsvHelper.Configuration.Attributes;

public class ClientsDescriptionsMapping : DataMapping
{
    [Name("Name"), Default("")] public string Name { get; set; }
    [Name("Gender"), Default("")] public string Gender { get; set; }
    [Name("Icon name"), Default("")] public string IconName { get; set; }
    [Name("For kids"), Default(0)] public int ForKids { get; set; }
    [Name("Location"), Default("")] public string Location { get; set; }
    [Name("Profession"), Default("")] public string Profession { get; set; }
    [Name("Description"), Default("")] public string Description { get; set; }
}
