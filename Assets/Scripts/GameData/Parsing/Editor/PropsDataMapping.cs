using CsvHelper.Configuration.Attributes;

namespace GameData.Editor
{
    public class PropsDataMapping : DataMapping
    {
        [Name("ServiceID"), Default("")] public string ServiceID{ get; set; }
        [Name("PropsID"), Default("")] public string PropsID { get; set; }
        [Name("Names"), Default("")] public string Name{ get; set; }
        [Name("Description"), Default("")] public string Description{ get; set; }
        [Name("LevelUpgrade"), Default(1)] public int LevelUpgrade{ get; set; }
        [Name("ResourcesCapacity"), Default(1)] public float ResourcesCapacity{ get; set; }
        [Name("StatPleasure"), Default(0)] public float StatPleasure { get; set; }
        [Name("RegenerationTime"), Default(1)] public float RegenerationTime { get; set; }
        [Name("UpgradePriceCoins"), Default(1)] public float UpgradePriceCoins{ get; set; }
        [Name("UpgradePriceRedGems"), Default(1)] public float UpgradePriceRedGems { get; set; }
        [Name("UpgradePriceBlueGems"), Default(1)] public float UpgradePriceBlueGems { get; set; }
        [Name("UpgradePriceGreenGems"), Default(1)] public float UpgradePriceGreenGems { get; set; }
    }
}