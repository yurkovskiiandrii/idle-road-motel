using CsvHelper.Configuration.Attributes;

namespace GameData.Editor
{
    public sealed class QuestDataMapping : DataMapping
    {
        [Name("№"), Default(0)] public int Number{ get; set; }
        [Name("Quest ID/Name"), Default("")] public string QuestId { get; set; }
        [Name("Quest condition type"), Default("")] public string ConditionType{ get; set; }
        [Name("Quest target id"), Default("")] public string TargetId{ get; set; }
        [Name("Quest condition amount"), Default(0)] public float ConditionAmount{ get; set; }
        [Name("Reward type 1"), Default("")] public string RewardType1{ get; set; }
        [Name("Reward amount 1"), Default(0)] public float RewardAmount1{ get; set; }
        [Name("Reward type 2"), Default("")] public string RewardType2{ get; set; }
        [Name("Reward amount 2"), Default(0)] public float RewardAmount2{ get; set; }
        [Name("Reward type 3"), Default("")] public string RewardType3{ get; set; }
        [Name("Reward amount 3"), Default(0)] public float RewardAmount3{ get; set; }
        [Name("Reward type 4"), Default("")] public string RewardType4{ get; set; }
        [Name("Reward amount 4"), Default(0)] public float RewardAmount4{ get; set; }
        [Name("Reward type 5"), Default("")] public string RewardType5{ get; set; }
        [Name("Reward amount 5"), Default(0)] public float RewardAmount5{ get; set; }
        
    }
}