using CsvHelper.Configuration.Attributes;

public class UnitsDataMapping : DataMapping
{
    [Name("Service"), Default("")] public string Service{ get; set; }
    [Name("UnitID"), Default("")] public string UnitID{ get; set; }
    [Name("Names"), Default("")] public string Names{ get; set; }
    [Name("Description"), Default("")] public string Description { get; set; }
    [Name("LevelUpgrade"), Default(1)] public int LevelUpgrade{ get; set; }
    //public Texture Icon;
    [Name("WorkEfficiency"), Default(1)] public float Efficiency{ get; set; }
    [Name("MoveSpeed"), Default(1)] public float MoveSpeed{ get; set; }
    [Name("CarriedSpeed"), Default(1)] public float CarriedMoveSpeed{ get; set; }
    [Name("CriticalMultiplier"), Default(0)] public float CritMultiplier{ get; set; }
    [Name("CriticalChance"), Default(0)] public float CritChance{ get; set; }
    [Name("MaxCarriedResources"), Default(1)] public float MaxCarriedResources{ get; set; }
    [Name("WorkingSpeedMultiplier"), Default(1)] public float WorkSpeed{ get; set; }
    [Name("UpgradePriceSoftCurrency"), Default(0)] public float UpgradePrice{ get; set; }
    [Name("TimeCycle"), Default(-1)] public float TimeCycle { get; set; }

}