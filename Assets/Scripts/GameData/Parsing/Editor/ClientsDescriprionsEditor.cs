using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(DescriptionContainer))]
    public class ClientsDescriprionsEditor : BaseDataEditor
    {
        DescriptionContainer _target;

        public void OnEnable()
        {
            _target = (DescriptionContainer)target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("descriptions"), true);
            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.descriptions.Clear();

            foreach (string file in files)
            {
                if (FillData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration { CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru") };
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new ClientsDescriptionsMapping();
                        _target.descriptions = new List<CustomerDescription>();

                        var res = csv.EnumerateRecords(record);

                        foreach (var line in res)
                        {
                            CustomerDescription description = new CustomerDescription();

                            description.Name = line.Name;
                            description.Gender = line.Gender;
                            description.ForKids = line.ForKids;
                            description.IconName = line.IconName;
                            description.Location = line.Location;
                            description.Profession = line.Profession;
                            description.Description = line.Description;

                            _target.descriptions.Add(description);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
            return false;
        }
    }
}