using CsvHelper.Configuration.Attributes;

namespace GameData.Editor
{
    public class BuildingsDataMapping : DataMapping
    {
        [Name("ServiceID"), Default("")] public string ServiceID{ get; set; }
        [Name("Names"), Default("")] public string Name{ get; set; }
        [Name("CustomDescription"), Default("")] public string Description{ get; set; }
        [Name("LevelUpgrade"), Default(1)] public int LevelUpgrade{ get; set; }
        //public Texture Icon;
        [Name("ResourcesCapacity"), Default(1)] public float ResourcesCapacity{ get; set; }
        [Name("UpgradePrice"), Default(1)] public float UpgradePrice{ get; set; }
        [Name("MaxNPCCount"), Default(1)] public int MaxNPCCount { get; set; }
        [Name("MaxPropsCount"), Default(1)] public int MaxPropsCount { get; set; }
    }
}