using CsvHelper.Configuration.Attributes;
using GameData;
using static Cinemachine.DocumentationSortingAttribute;

public class LocationProgressMapping : DataMapping
{
    [Name("Number"), Default(0)] public int Number { get; set; }
    [Name("LocationProgress"), Default(0)] public float LocationProgress { get; set; }
    [Name("ClientBasePaymentMin"), Default(0)] public int ClientBasePaymentMin { get; set; }
    [Name("ClientBasePaymentMax"), Default(0)] public int ClientBasePaymentMax { get; set; }
    [Name("CheckOutPaymentMult"), Default(0)] public int CheckOutPaymentMult { get; set; }
    [Name("ClientFrequencMin"), Default(0)] public int ClientFrequencMin { get; set; }
    [Name("ClientFrequencyMax"), Default(0)] public int ClientFrequencyMax { get; set; }
    [Name("ClientStatsValueMin"), Default(0)] public int ClientStatsValueMin { get; set; }
    [Name("ClientStatsValueMax"), Default(0)] public int ClientStatsValueMax { get; set; }
    [Name("MaxStatTier"), Default(0)] public int MaxStatTier { get; set; }
    [Name("MaxStats"), Default(0)] public int MaxStats { get; set; }
    [Name("StatBarMultiplierMin"), Default(0)] public float StatBarMultiplierMin { get; set; }
    [Name("StatBarMultiplierMax"), Default(0)] public float StatBarMultiplierMax { get; set; }
    [Name("propLevelMin"), Default(0)] public int propLevelMin { get; set; }
    [Name("propLevelMax"), Default(0)] public int propLevelMax { get; set; }
    [Name("LeaveRankBorder"), Default(0)] public int LeaveRankBorder { get; set; }
    [Name("LeaveRankChance"), Default(0)] public int LeaveRankChance { get; set; }
    [Name("OneClient"), Default(0)] public float OneClient { get; set; }
    [Name("DoubleClient"), Default(0)] public float DoubleClient { get; set; }
    [Name("TripleClient"), Default(0)] public float TripleClient { get; set; }
    [Name("QuadroClient"), Default(0)] public float QuadroClient { get; set; }
}
