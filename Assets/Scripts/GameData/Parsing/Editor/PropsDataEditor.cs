using System;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using GameData.Editor;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(PropsData))]
    public class PropsDataEditor : BaseDataEditor
    {
        private PropsData _target;

        public void OnEnable()
        {
            _target = (PropsData) target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PlumbersServiceProps"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("CleanersServiceProps"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room1Props"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room2Props"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room3Props"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Room4Props"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("PoolServiceProps"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("BarServiceProps"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ParkingServiceProps"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ReceptionServiceProps"), true);
            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.PlumbersServiceProps.Clear();
            _target.CleanersServiceProps.Clear();
            _target.Room1Props.Clear();
            _target.Room2Props.Clear();
            _target.Room3Props.Clear();
            _target.Room4Props.Clear();
            _target.PoolServiceProps.Clear();
            _target.BarServiceProps.Clear();
            _target.ParkingServiceProps.Clear();
            _target.ReceptionServiceProps.Clear();

            foreach (string file in files)
            {
                if (FillPropsData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillPropsData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration { CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru") };
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new PropsDataMapping();
                        Props props = null;
                        List<Props> service = null;

                        foreach (var line in csv.EnumerateRecords(record))
                        {
                            if (line.ServiceID != "")
                            {
                                if (service != null && props != null)
                                {
                                    service.Add(props);
                                    props = null;
                                }
                                switch (line.ServiceID)
                                {
                                    case "PlumbersService":
                                        service = _target.PlumbersServiceProps;
                                        continue;
                                    case "CleanersService":
                                        service = _target.CleanersServiceProps;
                                        continue;
                                    case "Room1":
                                        service = _target.Room1Props;
                                        continue;
                                    case "Room2":
                                        service = _target.Room2Props;
                                        continue;
                                    case "Room3":
                                        service = _target.Room3Props;
                                        continue;
                                    case "Room4":
                                        service = _target.Room4Props;
                                        continue;
                                    case "PoolService":
                                        service = _target.PoolServiceProps;
                                        continue;
                                    case "BarService":
                                        service = _target.BarServiceProps;
                                        continue;
                                    case "ParkingService":
                                        service = _target.ParkingServiceProps;
                                        continue;
                                    case "ReceptionService":
                                        service = _target.ReceptionServiceProps;
                                        continue;
                                    default:
                                        return false;
                                }
                            }

                            if (line.PropsID != "")
                            {
                                if (props != null)
                                    service.Add(props);
                                props = new Props { ID = line.PropsID, Name = line.Name, Description = line.Description };
                                continue;
                            }

                            var stats = new PropsStats()
                            {
                                Level = line.LevelUpgrade,
                                UpgradeTime = line.RegenerationTime,
                                ResourcesCapacity = line.ResourcesCapacity,
                                StatPleasure = line.StatPleasure,
                                UpgradePriceCoins = line.UpgradePriceCoins,
                                UpgradePriceRedGems = line.UpgradePriceRedGems,
                                UpgradePriceGreenGems = line.UpgradePriceGreenGems,
                                UpgradePriceBlueGems = line.UpgradePriceBlueGems,
                            };
                            props.Stats.Add(stats);
                        }

                        service.Add(props);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
            return false;
        }
    }
}