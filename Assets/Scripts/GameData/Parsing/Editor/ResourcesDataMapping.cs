using CsvHelper.Configuration.Attributes;

namespace GameData.Editor
{
    public class ResourcesDataMapping : DataMapping
    {
        [Name("ProviderID"), Default("")] public string ProviderID{ get; set; }
        [Name("LevelUpgrade"), Default(1)] public int LevelUpgrade{ get; set; }
        //public Texture Icon;
        [Name("ResourcesCapacity"), Default(1)] public float ResourcesCapacity{ get; set; }
        [Name("UpgradeTime"), Default(1)] public float UpgradeTime{ get; set; }
    }
}