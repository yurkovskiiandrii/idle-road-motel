using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using UnityEditor;
using UnityEngine;

namespace GameData.Parsing.Editor
{
    [CustomEditor(typeof(RequirementsList))]
    public class RequirementsListEditor : BaseDataEditor
    {
        RequirementsList _target;

        public void OnEnable()
        {
            _target = (RequirementsList)target;
            _baseUpdateGui = false;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("requirements"), true);
            serializedObject.ApplyModifiedProperties();
        }

        protected override void ParseFolder(params string[] files)
        {
            _target.requirements.Clear();

            foreach (string file in files)
            {
                if (FillData(file))
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.UpdateIfRequiredOrScript();
                    continue;
                }

                serializedObject.UpdateIfRequiredOrScript();
                EditorUtility.DisplayDialog("parsing error", file, "OK");
                Debug.LogException(new Exception($"parsing error {file}"));
            }

            EditorUtility.SetDirty(_target);
        }

        private bool FillData(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    Configuration info = new Configuration { CultureInfo = CultureInfo.CreateSpecificCulture("ru-ru") };
                    using (CsvReader csv = new CsvReader(reader, info))
                    {
                        csv.Configuration.HasHeaderRecord = true;
                        csv.Configuration.Delimiter = ",";

                        var record = new ClientsDataMapping();
                        _target.requirements = new List<CustomerRequirement>();

                        var res = csv.EnumerateRecords(record);

                        foreach (var line in res)
                        {
                            CustomerRequirement requirement = new CustomerRequirement();

                            requirement.StatName = line.StatName;
                            requirement.Value = line.Value;
                            requirement.Obligatory = line.Obligatory;
                            requirement.Tier = line.Tier;
                            requirement.TimeCycleMin = line.TimeCycleMin;
                            requirement.TimeCycleMax = line.TimeCycleMax;
                            requirement.StatBarCapacity = (int)line.StatBarCapacity;
                            requirement.RankMinus = line.RankMinus;
                            requirement.RankPlus = line.RankPlus;
                            requirement.ForKids = line.ForKids;

                            _target.requirements.Add(requirement);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }

            return false;
        }
    }
}