using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using nickeltin.GameData.Saving.Editor;
using Outloud.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace GameData.Parsing.Editor
{
    public abstract class BaseDataEditor : SaveableBaseEditor
    {
        private string _infoText;
        private float _progress;

        private const string _webAssetsName = "WebAssets";

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.UpdateIfRequiredOrScript();

            EditorGUILayout.PropertyField(serializedObject.FindProperty(_webAssetsName), true);
            
            EditorGUILayout.Space();

            var rects = EditorGUILayout.GetControlRect(false, EditorGUIUtility.singleLineHeight * 2.5f)
                .SplitHorizontal(3);

            if (GUI.Button(rects[0], "Go to web page"))
            {
                var container = ((DataFile) target).WebAssets;
                Application.OpenURL(
                    $"https://docs.google.com/spreadsheets/d/{container.DocumentId}/edit#gid={container.FileAssets.FirstOrDefault()?.SheetId}");
            }

            if (GUI.Button(rects[1], "Update locally"))
            {
                UpdateData();
            }

            if (GUI.Button(rects[2], "Update from web"))
            {
                UpdateDataFromWeb();
            }

            EditorGUILayout.Space();

            serializedObject.ApplyModifiedProperties();
            
        }

        public void UpdateDataFromWeb()
        {
            var focus = EditorWindow.focusedWindow;
            bool isSuccess = true;
            try
            {
                var container = ((DataFile) target).WebAssets;

                EditorUtility.DisplayCancelableProgressBar("Download", $"Downloading 0/{container.FileAssets.Count}",
                    0);

                if (container == null || string.IsNullOrEmpty(container.DocumentId) || container.FileAssets.Count == 0)
                {
                    EditorUtility.DisplayDialog("Please fill in Web Assets data", "", "OK");
                }
                else
                {
                    for (int i = 0; i < container.FileAssets.Count; i++)
                    {
                        _infoText = $"Downloading {i}/{container.FileAssets.Count}";
                        _progress = (float) i / (container.FileAssets.Count - 1) * 0.9f;
                        EditorUtility.DisplayCancelableProgressBar("Download", _infoText, _progress);
                        DownloadGoogleSheet(container.DocumentId, container.FileAssets[i]);
                    }

                    EditorUtility.DisplayCancelableProgressBar("Download", $"Parse Files", 0.95f);
                    UpdateData();
                }
            }
            catch
            {
                isSuccess = false;
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }

            try
            {
                focus.ShowNotification(new GUIContent(isSuccess ? "Data was imported!" : "parsing error"));
            }
            catch
            {
            }
        }

        private void UpdateData()
        {
            List<string> files = new List<string>();
            var setup = serializedObject.FindProperty(_webAssetsName + ".FileAssets");
            for (int i = 0; i < setup.arraySize; i++)
            {
                files.Add(AssetDatabase.GetAssetPath(setup.GetArrayElementAtIndex(i).FindPropertyRelative("TextAsset")
                    .objectReferenceValue));
            }

            ParseFolder(files.ToArray());
        }

        protected abstract void ParseFolder(params string[] files);

        private void DownloadGoogleSheet(string docId, ParsingFileAsset fileAsset)
        {
            var iterator = DownloadSheet(docId, fileAsset.SheetId, t => DownloadComplete(t, fileAsset),
                DisplayDownloadProgressbar);
            while (iterator.MoveNext())
            {
            }
        }

        private IEnumerator DownloadSheet(string docsId, string sheetId, Action<string> done,
            Func<float, bool> progressbar = null)
        {
            if (progressbar != null && progressbar(0))
            {
                done(null);
                yield break;
            }

            var url = $"https://docs.google.com/spreadsheets/d/{docsId}/export?format=csv&gid={sheetId}";
            var www = UnityWebRequest.Get(url);
            www.SendWebRequest();
            while (!www.isDone)
            {
                var progress = www.downloadProgress;
                if (progressbar != null && progressbar(progress))
                {
                    done(null);
                    yield break;
                }

                yield return null;
            }

            if (progressbar != null && progressbar(1))
            {
                done(null);
                yield break;
            }

            var text = www.downloadHandler.text;

            if (text.StartsWith("<!"))
            {
                Debug.LogError("Google sheet could not be downloaded.\nURL:" + url + "\n" + text);
                done(null);
                yield break;
            }

            
            done(text);
        }

        private static void DownloadComplete(string text, ParsingFileAsset fileAsset)
        {
            if (string.IsNullOrEmpty(text))
            {
                Debug.LogError("Could not download google sheet");
                return;
            }

            var path = fileAsset.TextAsset != null ? AssetDatabase.GetAssetPath(fileAsset.TextAsset) : null;

            if (string.IsNullOrEmpty(path))
            {
                path = EditorUtility.SaveFilePanelInProject("Save parsing file", "", "txt",
                    "Please enter a file name to save the csv to", path);
            }

            if (string.IsNullOrEmpty(path))
            {
                return;
            }

            File.WriteAllText(path, text);

            AssetDatabase.ImportAsset(path);

            fileAsset.TextAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            EditorUtility.SetDirty(fileAsset.TextAsset);
            AssetDatabase.SaveAssets();
        }

        private bool DisplayDownloadProgressbar(float progress)
        {
            if (progress < 1)
            {
                return EditorUtility.DisplayCancelableProgressBar("Download parsing files",
                    _infoText + " " + progress.ToString("P"), _progress);
            }

            return false;
        }
    
    }
}
