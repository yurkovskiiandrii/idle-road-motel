using System;
using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameData
{
    [CreateAssetMenu(menuName = "Data/Props")]
   public class PropsData : DataFile
   {
        public List<Props> PlumbersServiceProps;
        public List<Props> CleanersServiceProps;
        public List<Props> Room1Props;
        public List<Props> Room2Props;
        public List<Props> Room3Props;
        public List<Props> Room4Props;
        public List<Props> PoolServiceProps;
        public List<Props> BarServiceProps;
        public List<Props> ParkingServiceProps;
        public List<Props> ReceptionServiceProps;

        public List<Props> GetServiceProps(ServiceType serviceType)
        {
            switch (serviceType)
            {
                case ServiceType.PlumbersService:
                    return PlumbersServiceProps;
                case ServiceType.CleanersService:
                    return CleanersServiceProps;
                case ServiceType.Room1:
                    return Room1Props;
                case ServiceType.Room2:
                    return Room2Props;
                case ServiceType.Room3:
                    return Room3Props;
                case ServiceType.Room4:
                    return Room4Props;
                case ServiceType.PoolService:
                    return PoolServiceProps;
                case ServiceType.BarService:
                    return BarServiceProps;
                case ServiceType.ParkingService:
                    return ParkingServiceProps;
                case ServiceType.ReceptionService:
                    return ReceptionServiceProps;
            }
            Debug.LogError("Can't find service " + Enum.GetName(typeof(ServiceType), serviceType));
            return null;
        }
    }

    [Serializable]
    public class Props : IObjectID
    {
        public string ID { get => id; set =>id = value; }
        [SerializeField, ReadOnly] private string id;
        [ReadOnly] public string Name;
        [ReadOnly] public string Description;
        public List<PropsStats> Stats = new List<PropsStats>();
    }

    [Serializable]
    public struct PropsStats
    {
        public int Level;
        public float ResourcesCapacity;
        public float UpgradeTime;
        public float UpgradePriceCoins;
        public float StatPleasure;
        public float UpgradePriceRedGems;
        public float UpgradePriceGreenGems;
        public float UpgradePriceBlueGems;

        public override string ToString()
        {
            base.ToString();
            return $"ResourcesCapacity : {ResourcesCapacity} " +
                   $"\n UpgradeTime : {UpgradeTime}" +
                   $"\n  UpgradePriceCoins : {UpgradePriceCoins}" +
                   $"\n UpgradePriceRedGems : {UpgradePriceRedGems}" +
                   $"\n UpgradePriceGreenGems : {UpgradePriceGreenGems}" +
                   $"\n UpgradePriceBlueGems : {UpgradePriceBlueGems}";
        }
    }
}