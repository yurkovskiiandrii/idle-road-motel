using System;
using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameData
{
    [CreateAssetMenu(menuName = "Data/Buildings")]
   public class BuildingsData : DataFile
    {
        public Buildings PlumbersService;
        public Buildings CleanersService;
        public Buildings Room1;
        public Buildings Room2;
        public Buildings Room3;
        public Buildings Room4;
        public Buildings PoolService;
        public Buildings BarService;
        public Buildings ParkingService;
        public Buildings ReceptionService;

        public Buildings GetBuildingsData(ServiceType serviceType)
        {
            switch (serviceType)
            {
                case ServiceType.PlumbersService:
                    return PlumbersService;
                case ServiceType.CleanersService:
                    return CleanersService;
                case ServiceType.Room1:
                    return Room1;
                case ServiceType.Room2:
                    return Room2;
                case ServiceType.Room3:
                    return Room3;
                case ServiceType.Room4:
                    return Room4;
                case ServiceType.PoolService:
                    return PoolService;
                case ServiceType.BarService:
                    return BarService;
                case ServiceType.ParkingService:
                    return ParkingService;
                case ServiceType.ReceptionService:
                    return ReceptionService;
            }
            Debug.LogError("Can't find service " + Enum.GetName(typeof(ServiceType), serviceType));
            return null;
        }
    }
    [Serializable]
    public class Buildings : IObjectID
    {
        public string ID { get => id; set =>id = value; }
        [SerializeField, ReadOnly] private string id;
        [ReadOnly] public string Name;
        [ReadOnly] public string Description;
        public List<BuildingsStats> Stats = new List<BuildingsStats>();
        //public Texture Icon;
    }

    [Serializable]
    public struct BuildingsStats
    {
        public int LevelUpgrade;
        //public float ResourcesCapacity;
        public float UpgradePrice;
        public int MaxNPCCount;
        public int MaxPropsCount;
    }
}