using nickeltin.GameData.Saving;
using UnityEngine;

namespace GameEnvironment.Village.Grid
{
    [CreateAssetMenu(menuName = "Data/DataRegistry")]
    public class DataRegistry : SaveRegistry<DataFile>
    {
        
    }
}