using System;
using System.Collections.Generic;
using nickeltin.Editor.Attributes;
using NPCs;
using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(menuName = "Data/Units")]
    [Serializable]
    public class UnitsData : DataFile
    {
        public List<Unit> PlumbersServiceUnits;
        public List<Unit> CleanersServiceUnits;
        public List<Unit> Room1Units;
        public List<Unit> Room2Units;
        public List<Unit> Room3Units;
        public List<Unit> Room4Units;
        public List<Unit> PoolServiceUnits;
        public List<Unit> BarServiceUnits;
        public List<Unit> ReceptionServiceUnits;
        public List<Unit> ParkingServiceUnits;
        public List<Unit> ClientsServiceUnits;

        public List<Unit> GetServiceUnits(ServiceType serviceType)
        {
            switch (serviceType)
            {
                case ServiceType.PlumbersService:
                    return PlumbersServiceUnits;
                case ServiceType.CleanersService:
                    return CleanersServiceUnits;
                case ServiceType.Room1:
                    return Room1Units;
                case ServiceType.Room2:
                    return Room2Units;
                case ServiceType.Room3:
                    return Room3Units;
                case ServiceType.Room4:
                    return Room4Units;
                case ServiceType.PoolService:
                    return PoolServiceUnits;
                case ServiceType.BarService:
                    return BarServiceUnits;
                case ServiceType.ReceptionService:
                    return ReceptionServiceUnits;
                case ServiceType.ParkingService:
                    return ParkingServiceUnits;
                case ServiceType.ClientsService:
                    return ClientsServiceUnits;
            }
            Debug.LogError("Can't find service " + Enum.GetName(typeof(ServiceType), serviceType));
            return null;
        }
    }

    [Serializable]
    public class Unit : IObjectID
    {
        public string ID { get => id; set => id = value; }
        [SerializeField, ReadOnly] private string id;
        [ReadOnly] public string Name;
        [ReadOnly] public string Description;
        [ReadOnly] public List<NpcStats> Stats = new List<NpcStats>();

        public override string ToString()
        {
            return Name;
        }
    }

    [Serializable]
    public struct NpcStats
    {
        public int Level;
        public float UpgradePrice;
        public float WorkEfficiency;
        public float CritChance;
        public float CritMultiplier;
        public float WorkSpeed;
        public float MoveSpeed;
        public float CarriedMoveSpeed;
        public float MaxCarriedResources;
        public float TimeCycle;

        public override string ToString()
        {
            base.ToString();
            return $"UpgradePriceProgression : {UpgradePrice} " +
                   $"\n WorkEfficiency : {WorkEfficiency} " +
                   $"\n CritChance : {CritChance}" +
                   $"\n CritMultiplier : {CritMultiplier}" +
                   $"\n  WorkSpeed : {WorkSpeed}" +
                   $"\n MoveSpeed : {MoveSpeed}" +
                   $"\n CarriedMoveSpeed : {CarriedMoveSpeed}"+
                   $"\n MaxCarriedResources : {MaxCarriedResources}";
        }
    }
}