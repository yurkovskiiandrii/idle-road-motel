using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    /// <summary>
    /// Class container of Google Sheets document ID, Sheet IDs and corresponding file assets for parsing
    /// </summary>
    [Serializable]
    public class DataFilesContainer
    {
        [Tooltip("Google Sheets document ID")]
        public string DocumentId;
        
        [Tooltip("Sheet IDs and corresponding file assets")]
        public List<ParsingFileAsset> FileAssets;
    }
    
    [Serializable]
    public class ParsingFileAsset
    {
        public string SheetId;
        public TextAsset TextAsset;
    }
}