using DG.Tweening;
using GameData;
using GameEnvironment.Village.Structures;
using GameEnvironment;
using nickeltin.StateMachine;
using NPCs;
using System;
using System.Linq;
using UI;
using UnityEngine;
using UnityEngine.Events;
using WaypointsSystem;
using nickeltin.Editor.Attributes;
using System.Collections.Generic;
using UnityEngine.InputSystem.HID;
using System.Security.Cryptography;
using GameEnvironment.Village.UI;
using XInputDotNetPure;
using System.Collections;

namespace Customers
{
    [SelectionBase, DisallowMultipleComponent, RequireComponent(typeof(Animator))]
    public class CustomerAI : MonoBehaviour
    {
        [Serializable]
        private class CustomerStateEvent : StateEvent
        {
            public CustomerState State;
        }

        public enum CustomerState
        {
            Idle,
            Walk,
            Sleep,
            Exit,
            Reception,
            LivingCycle,
            Spending,
            Waiting,
            Ready
        }

        [SerializeField] private string _id;
        private Animator _animator;
        [SerializeField] private RuntimeAnimatorController _animatorController;

        [Space] [SerializeField] private float _delayBeforeWalk;
        private float _currentWalkDelay;
        [SerializeField] private float _rotationSpeed = 5;
        [SerializeField] private float _baseSpeed = 1;
        [SerializeField, ReadOnly] private ServiceZone _currentZone;

        [SerializeField, ReadOnly] private float customerRating;

        private SpendingZone[] _receptionsSpendingZones;
        private WaitingZone[] _waitingZones;

        ServiceManager receptionService;

        Transform endWayPoint;

        [Header("Events"), Space, SerializeField]
        private CustomerStateEvent[] _stateEvents;

        [SerializeField] string waitingOnServiceAnimationBool = "empty";

        private CustomerStateEvent[] _existsStateEvents;

        private Vector3 _prevPosition;
        private Quaternion _targetRotation;
        private WaypointMovement _movement;
        private StateMachine _states;

        int workIndex = -1;

        int receptionIndex = -1;

        [SerializeField] WorldProgressBar _waitingOnReceptionProgressBar;
        [SerializeField] WorldProgressBar _receptionProgressBar;
        [SerializeField] WorldProgressBar _statProgressBar;

        [SerializeField] CustomerEmotionUI _customerEmotionUI;
        [SerializeField] CustomerPaymentUI _customerPaymentUI;
        [SerializeField] CustomerStatsUI _customerStatsUI;

        [SerializeField] CustomerLiveData customerLiveData;
        [SerializeField] CustomerDescription customerDescription;

        [SerializeField] CustomersPseudofamily customerFamily;

        [SerializeField] float minPeriodicPaymentTime = 5;
        [SerializeField] float maxPeriodicPaymentTime = 15;
        [SerializeField] Transform graphicPack;

        public bool isChild;
        bool isInRoom;

        float maxReceptionTime;
        float remainReceptionTime;

        Action<float> receprionProgressBarAction;

        readonly string queueTag = "QueueObject";

        [SerializeField] List<CustomerPleasureZone> roomPleasureZones;
        WaitingZone roomWaitingZone;

        WaitingZone locationWaitingZone;

        [SerializeField] GameObject collectButton;

        UnityEvent onRecepted = new UnityEvent();

        [SerializeField] float currentSatRating;
        float baseSatRating;

        public bool isExitState;

        Coroutine periodicPaymentCoroutine = null;

        float basePayment;

        LocationProgressController locationProgressController;

        bool isSpending = false;

        [SerializeField] float SatRating
        {
            get { return currentSatRating; }
            set
            {
                // if ((int)value > (int)SatRating)
                //     _customerEmotionUI.ShowAddedRating(value - currentSatRating, SatRating);
                // else if ((int)value < (int)SatRating)
                //     _customerEmotionUI.ShowLostRating(currentSatRating - value, SatRating);

                currentSatRating = Mathf.Clamp(value, 0, 100);

                if (baseSatRating == 0)
                    baseSatRating = currentSatRating;

                Debug.Log("currentSatRating = " + currentSatRating);

                var locationStats = locationProgressController.GetLocationStats();

                if (currentSatRating != baseSatRating && currentSatRating < locationStats.leaveRankBorder
                                                      && UnityEngine.Random.Range(0, 100) <
                                                      locationStats.leaveRankChance)
                    customerFamily.Refuse();
            }
        }

        private void Awake()
        {
            _movement = new WaypointMovement(transform.position, _baseSpeed);
            _animator = GetComponent<Animator>();
        }

        bool IsCarOnParking() => customerFamily.IsCarUsing;

        public void SetFamily(CustomersPseudofamily family) => customerFamily = family;

        public void ActivateCustomer(bool enable)
        {
            if (enable)
            {
                TimeCycle.onTimeChanged += TimeCycleChange;
                _animator.runtimeAnimatorController = _animatorController;
                _movement.Teleport(transform.position);
                _states.enabled = true;
                _states.SwitchState(CustomerState.Walk);
            }
            else
            {
                TimeCycle.onTimeChanged -= TimeCycleChange;
                //_states.ExitState();
                _states.enabled = false;
                _movement.Stop();
            }
        }

        public void Initialize(ServiceManager parking, ServiceManager reception, RoomsContainer notRoomServices,
            Transform endWayPoint, bool isChild, WaitingZone locationWaitingZone)
        {
            _states = new StateMachine(transform, true, false, IdleState(),
                WalkState(), ReceptionState(), ExitState(), LivingCycleState(), SpendingState(), WaitingState(),
                ReadyState());

            locationProgressController = LocationProgressController.GetInstance();

            this.isChild = isChild;

            customerFamily.AddPersonToFamily(this);

            customerLiveData = locationProgressController.GetUserRequirements(isChild);
            customerDescription = CustomerDescriptionGenerator.GetInstance().GenerateDescription(isChild);

            //parkingService = parking.GetZones<SpendingZone>()[0];
            //parkingService = parking.GetZones<SpendingZone>()[0];
            receptionService = reception /*.GetZones<SpendingZone>()[0]*/;
            this.endWayPoint = endWayPoint;

            this.locationWaitingZone = locationWaitingZone;
            _receptionsSpendingZones = reception.GetZones<SpendingZone>();
            _waitingZones = reception.GetZones<WaitingZone>();

            roomPleasureZones = FilterPleasureZones(notRoomServices.GetPleasureZones());

            if (_stateEvents.Length > 0)
            {
                var values = Enum.GetValues(typeof(CustomerState));
                _existsStateEvents = new CustomerStateEvent[values.Length];

                for (int i = 0; i < values.Length; i++)
                    _existsStateEvents[i] = _stateEvents.FirstOrDefault(x => (int)x.State == i);
            }

            basePayment = locationProgressController.GetBasePayment();

            ActivateCustomer(true);
            GenerateAppearence();
        }

        void GenerateAppearence()
        {
            DestroyImmediate(transform.GetChild(isChild ? 2 : 1).gameObject);

            DestroyImmediate(graphicPack.GetChild(customerDescription.Gender == "Male" ? 1 : 0).gameObject);
            graphicPack = graphicPack.transform.GetChild(0);

            while (graphicPack.transform.childCount != 1)
                DestroyImmediate(graphicPack.GetChild(UnityEngine.Random.Range(0, graphicPack.childCount)).gameObject);

            graphicPack = graphicPack.transform.GetChild(0);

            List<int> torsoIDs = new List<int>();

            for (int i = 0; i < graphicPack.GetChild(0).childCount; i++)
                torsoIDs.Add(i + 1);

            var rng = new System.Random();
            var shuffledIDs = torsoIDs.OrderBy(a => rng.Next()).ToList();
            torsoIDs = shuffledIDs;

            int torsoID = 0;

            foreach (var id in torsoIDs)
            {
                bool containsTorsoID = false;

                for (int i = 1; i < graphicPack.childCount; ++i)
                {
                    containsTorsoID = false;

                    for (int j = 0; j < graphicPack.GetChild(i).childCount; ++j)
                    {
                        if (graphicPack.GetChild(i).GetChild(j).name.Contains(id.ToString()))
                        {
                            containsTorsoID = true;
                            break;
                        }
                    }

                    if (!containsTorsoID)
                        break;
                }

                torsoID = id;

                if (containsTorsoID)
                    break;
            }

            shuffledIDs.Remove(torsoID);

            for (int i = graphicPack.childCount - 1; i >= 0; --i)
            {
                for (int j = graphicPack.GetChild(i).childCount - 1; j >= 0; --j)
                {
                    if (!graphicPack.GetChild(i).GetChild(j).name.Contains(torsoID.ToString()))
                        DestroyImmediate(graphicPack.GetChild(i).GetChild(j).gameObject);
                }
            }
        }

        List<CustomerPleasureZone> FilterPleasureZones(List<CustomerPleasureZone> zones)
        {
            for (int i = 0; i < zones.Count; i++)
            {
                if (!customerLiveData.ContainsStat(zones[i].requirementType))
                {
                    zones.RemoveAt(i);
                    --i;
                }
            }

            return zones;
        }

        private bool GoToNextZone<T>(T[] zones) where T : ServiceZone
        {
            foreach (var zone in zones)
            {
                if (!zone.Unavailable)
                {
                    _currentZone = zone;
                    _currentZone.BookService(!_currentZone.CompareTag(queueTag));
                    return true;
                }
            }

            return false;
        }

        private bool TryGetNextZone()
        {
            if ((IsCarOnParking())
                && GoToNextZone(_receptionsSpendingZones))
            {
                return true;
            }

            return false;
        }

        #region IdleState

        private State IdleState()
        {
            return new State(CustomerState.Idle, onStateStart: () =>
            {
                _existsStateEvents?[(int)CustomerState.Idle]?.Invoke(true);
                return true;
            }, onStateEnd: () =>
            {
                //UnsubscribeOnWorkAvailable();
                _existsStateEvents?[(int)CustomerState.Idle]?.Invoke(false);
                return true;
            });
        }

        #endregion

        #region Walk

        private bool Move()
        {
            if (_currentWalkDelay < Time.time)
            {
                UpdatePosition(_movement.CurrentPosition);
                UpdateRotation();
                _movement.Update(Time.deltaTime);
            }

            return _movement.IsMoving;
        }

        private void UpdateRotation()
        {
            transform.rotation =
                Quaternion.Lerp(
                    transform.rotation,
                    _targetRotation,
                    Time.deltaTime * _rotationSpeed);
        }

        private void UpdatePosition(Vector3 originPos)
        {
            transform.position = originPos;
            var delta = transform.position - _prevPosition;
            delta.y = 0;
            if (delta != Vector3.zero)
            {
                _targetRotation = Quaternion.LookRotation(delta);
            }

            _prevPosition = originPos;
        }

        bool TryGoToReceptionBool()
        {
            var freeReceptionPlaces = receptionService.GetFreeSpendingZonesCount(_receptionsSpendingZones, queueTag);

            if (!customerFamily.CanContainsAtReception(freeReceptionPlaces))
                return false;

            receptionService.UnSubscribeOnPropsAvailable(TryGoToReception);

            _states.SwitchState(CustomerState.Walk);
            return true;
        }

        void TryGoToReception() => TryGoToReceptionBool();

        private State WalkState()
        {
            return new State(CustomerState.Walk, onStateStart: () =>
                {
                    _currentWalkDelay = Time.time + _delayBeforeWalk;

                    var freeReceptionPlaces =
                        receptionService.GetFreeSpendingZonesCount(_receptionsSpendingZones, queueTag);

                    Vector3 movementTarget = transform.position;

                    if (customerFamily.CanContainsAtReception(freeReceptionPlaces) /*GetCanStopAtReception()*/)
                    {
                        GoToNextZone(_receptionsSpendingZones);
                        movementTarget = _currentZone.GetInteractable(out receptionIndex, false, true).position;
                        _waitingOnReceptionProgressBar.Hide();
                        customerFamily.ReceptClient();
                    }
                    else
                    {
                        GoToNextZone(_waitingZones);
                        movementTarget = _currentZone.GetInteractable(out int placeIndex, false, true).position;
                        workIndex = placeIndex;
                    }

                    _movement.GoTo(movementTarget, b =>
                    {
                        if (!b)
                            return;

                        if (_currentZone.CompareTag(queueTag))
                        {
                            _states.SwitchState(CustomerState.Idle);

                            if (TryGoToReceptionBool())
                                return;

                            receptionService.SubscribeOnPropsAvailable(TryGoToReception);

                            _waitingOnReceptionProgressBar.Show();
                            _waitingOnReceptionProgressBar.StartTimer(receptionService.timers.entrancePoitWait, 0);

                            _states.SwitchState(CustomerState.Idle);
                            DOVirtual.DelayedCall(receptionService.timers.entrancePoitWait, () =>
                            {
                                Exit();
                                _customerEmotionUI.ShowDispleasure();
                            });

                            return;
                        }

                        _states.SwitchState(CustomerState.Reception);
                    });

                    return true;
                }, Move,
                onStateEnd: () =>
                {
                    _movement.SetSpeedCustom(_baseSpeed);
                    return true;
                });
        }

        #endregion

        IEnumerator PeriodicPayment()
        {
            var waiter = new WaitForSeconds(UnityEngine.Random.Range(minPeriodicPaymentTime, maxPeriodicPaymentTime));

            while (true)
            {
                yield return waiter;

                var payment = GetPeriodicProfit();
                Managers.GameController.Instance.AddMoney(payment);
                ShowCollectEffect(payment);
                
                _customerPaymentUI.ShowPayment(payment);
            }
        }

        void StopPeriodicPayment()
        {
            if (periodicPaymentCoroutine != null)
            {
                StopCoroutine(periodicPaymentCoroutine);
                periodicPaymentCoroutine = null;
            }
        }

        private State ReceptionState()
        {
            return new State(CustomerState.Reception, onStateStart: () =>
                {
                    GetComponent<BoxCollider>().enabled = true;

                    maxReceptionTime = receptionService.timers.serviceWorkTime;

                    _receptionProgressBar?.Show();

                    _currentZone.StartInteraction(transform, 0);

                    Sequence exitWaiter = DOTween.Sequence();

                    exitWaiter.Append(DOVirtual.DelayedCall(maxReceptionTime, () =>
                    {
                        Exit();
                        _customerEmotionUI.ShowRamdomEmotion();
                        CancelInvoke();
                    }));

                    onRecepted.AddListener(() => exitWaiter.Kill());
                    onRecepted.AddListener(CancelInvoke);

                    remainReceptionTime = maxReceptionTime;

                    _receptionProgressBar.StartTimer(remainReceptionTime, 0);

                    InvokeRepeating(nameof(UpdateRemainReceptionTime), 0, 1f);

                    return true;
                }, null,
                onStateEnd: () =>
                {
                    GetComponent<BoxCollider>().enabled = false;

                    _receptionProgressBar?.Hide();

                    _currentZone.BookService(false);
                    _currentZone.EndInteraction(transform, 0);
                    //receptionService.GetComponent<Storage>().Get(1);
                    receptionService.PropsBecameAvailable();

                    return true;
                });
        }

        private State LivingCycleState()
        {
            return new State(CustomerState.LivingCycle, onStateStart: () =>
                {
                    if (customerLiveData.IsFullStats())
                    {
                        _states.SwitchState(CustomerState.Ready);
                        return true;
                    };

                    Vector3 movementTarget;
                    var pleasureZone = _currentZone as CustomerPleasureZone;

                    if (pleasureZone == null || pleasureZone.Unavailable)
                    {
                        _currentWalkDelay = Time.time + _delayBeforeWalk;

                        float minDistance = float.MaxValue;

                        ServiceZone nearestZone = null;

                        string firstAvailableZone = "";

                        var rng = new System.Random();
                        customerLiveData.Shuffle();

                        roomPleasureZones = roomPleasureZones.OrderBy(a => rng.Next()).ToList();

                        foreach (var zone in roomPleasureZones)
                        {
                            if (customerLiveData.ContainsStat(zone.requirementType) && zone.IsActivated())
                            {
                                _currentZone = zone;
                                firstAvailableZone = zone.requirementType;
                                break;
                            }
                        }

                        if (firstAvailableZone != "")
                        {
                            foreach (var zone in roomPleasureZones)
                            {
                                if (!zone.Unavailable && zone.requirementType == firstAvailableZone
                                                       && minDistance > Vector3.Distance(transform.position,
                                                           zone.GetInteractable(0).position))
                                {
                                    nearestZone = zone;
                                    minDistance = Vector3.Distance(transform.position,
                                        zone.GetInteractable(0).position);
                                }
                            }

                            if (nearestZone == null)
                            {
                                minDistance = float.MaxValue;

                                foreach (var zone in roomPleasureZones)
                                {
                                    if (zone.IsActivated() && zone.requirementType == firstAvailableZone
                                                           && minDistance > Vector3.Distance(transform.position,
                                                               zone.GetInteractable(0).position))
                                    {
                                        nearestZone = zone;
                                        minDistance = Vector3.Distance(transform.position,
                                            zone.GetInteractable(0).position);
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var zone in roomPleasureZones)
                            {
                                if (minDistance >
                                    Vector3.Distance(transform.position, zone.GetInteractable(0).position))
                                {
                                    nearestZone = zone;
                                    minDistance = Vector3.Distance(transform.position,
                                        zone.GetInteractable(0).position);
                                }
                            }
                        }

                        _currentZone = nearestZone;
                        pleasureZone = _currentZone as CustomerPleasureZone;

                        if (pleasureZone == null || _currentZone.Unavailable || 
                         pleasureZone.GetZoneLevel() < customerLiveData.requirements.First(x => x.StatName == pleasureZone.requirementType)
                                                       .requirementPropsLevel)
                        {
                            //pleasureZone.SubscribeOnSrorageUpdate(OnZoneUpdated);
                            _states.SwitchState(CustomerState.Waiting);
                            return true;
                        }
                    }

                    foreach (var zone in roomPleasureZones)
                    {
                        if (zone.requirementType == pleasureZone.requirementType)
                            zone.UnSubscribeOnSrorageUpdate(OnZoneUpdated);
                    }

                    movementTarget = _currentZone.GetInteractable(0).transform.position;

                    _movement.GoTo(movementTarget, b =>
                    {
                        if (!b)
                            return;

                        _states.SwitchState(CustomerState.Spending);
                    });

                    return true;
                }, Move,
                onStateEnd: () => { return true; });
        }

        void OnZoneUpdated(SpendingZone pleasureZone)
        {
            if (pleasureZone.Unavailable || isSpending)
                return;

            _currentZone = pleasureZone;

            _states.SwitchState(CustomerState.LivingCycle);
        }

        private State WaitingState()
        {
            Tween callBack = null;

            return new State(CustomerState.Waiting, onStateStart: () =>
                {
                    var pleasureZone = _currentZone as CustomerPleasureZone;
                    var currentRequirement =
                        customerLiveData.requirements.First(x => x.StatName == pleasureZone.requirementType);

                    foreach (var zone in roomPleasureZones)
                    {
                        if(zone.requirementType == currentRequirement.StatName)
                            zone.SubscribeOnSrorageUpdate(OnZoneUpdated);
                    }

                    Debug.LogError(pleasureZone);

                    _currentZone = roomWaitingZone;

                    var pleasureZonePos = transform.position;

                    var movementTarget = _currentZone.GetInteractable(out int placeIndex, pleasureZonePos).position;
                    workIndex = placeIndex;

                    _movement.GoTo(movementTarget, b =>
                    {
                        if (!b)
                            return;

                        _currentZone.StartInteraction(transform, placeIndex);
                        _animator.SetBool(waitingOnServiceAnimationBool, true);

                        callBack = DOVirtual.DelayedCall(_currentZone.GetInteractionTime(workIndex), () =>
                        {
                            _animator.SetBool(waitingOnServiceAnimationBool, false);
                            SatRating -= currentRequirement.RankMinus;
                            _customerEmotionUI.ShowLostRating(currentRequirement.RankMinus, SatRating);
                            if (SatRating <= 0)
                            {
                                Exit();
                                isExitState = true;
                            }

                            _states.SwitchState(isExitState ? CustomerState.Exit : CustomerState.LivingCycle);
                        });

                        callBack.Play();
                    });

                    return true;
                }, Move,
                onStateEnd: () =>
                {
                    callBack.Kill();
                    roomWaitingZone.EndInteraction(transform, workIndex);
                    return true;
                });
        }

        private State SpendingState()
        {
            return new State(CustomerState.Spending, onStateStart: () =>
                {
                    var pleasureZone = _currentZone as CustomerPleasureZone;
                    var currentRequirement =
                        customerLiveData.requirements.First(x => x.StatName == pleasureZone.requirementType);

                    Debug.Log("un subscribed");
                    foreach (var zone in roomPleasureZones)
                    {
                        if (zone.requirementType == pleasureZone.requirementType)
                            zone.UnSubscribeOnSrorageUpdate(OnZoneUpdated);
                    }

                    if (pleasureZone != null && !_currentZone.Unavailable
                                             && customerLiveData.ContainsStat(pleasureZone.requirementType)
                                             && pleasureZone.GetZoneLevel() >= customerLiveData.requirements
                                                 .First(x => x.StatName == pleasureZone.requirementType)
                                                 .requirementPropsLevel)
                    {
                        isSpending = true;

                        _currentZone.BookService(true);
                        _currentZone.StartInteraction(transform, 0);

                        var propBeh = _currentZone.GetComponentInParent<PropsBehaviour>();

                        float spendTime = currentRequirement.TimeCycleMin;

                        _statProgressBar.Show();

                        _statProgressBar.SetPBIcon((_currentZone as IPopUpHolder).PopUpWorldIconsScheme.Icon);
                        _statProgressBar.StartTimer(spendTime, 0, timeStep: 0.3f);

                        DOVirtual.DelayedCall(spendTime, () =>
                        {
                            isSpending = false;

                            SatRating += currentRequirement.RankPlus;
                            _customerEmotionUI.ShowAddedRating(currentRequirement.RankPlus, SatRating);
                            if (SatRating <= 0)
                            {
                                Exit();
                                isExitState = true;
                            }
                            float statAmountToAdd = propBeh.Stats.StatPleasure;
                            //float statAmountToAdd = 500;

                            customerLiveData.AddValueToStat(statAmountToAdd, pleasureZone.requirementType);
                            _statProgressBar.Hide();
                            //PopupText.PopupWorld(_currentZone as IPopUpHolder, transform.position, statAmountToAdd);
                            _customerStatsUI.ShowStats((int)statAmountToAdd);
                            _states.SwitchState(isExitState ? CustomerState.Exit : CustomerState.LivingCycle);
                        });
                    }
                    else
                    {
                        //SatRating -= currentRequirement.RankMinus;

                        //get value from room params
                        //if (!isExitState)
                        //    DOVirtual.DelayedCall(2, () => _states.SwitchState(CustomerState.Waiting));  
                    }

                    return true;
                }, null,
                onStateEnd: () =>
                {
                    _currentZone.BookService(false);
                    _currentZone.EndInteraction(transform, 0);

                    try
                    {
                        _currentZone.gameObject.GetComponent<SpendingZone>().AddMax();
                    }
                    catch
                    {
                    }

                    return true;
                });
        }

        void UpdateRemainReceptionTime()
        {
            remainReceptionTime -= 1;
            receprionProgressBarAction?.Invoke(remainReceptionTime);
        }

        public void SubscribeOnReceptionTimeUpdate(Action<float> action) => receprionProgressBarAction += action;

        public void RemoveAllFromReceptionTimeUpdate() => receprionProgressBarAction = null;

        public float GetMaxReceptionTime() => maxReceptionTime;

        public float GetRemainReceptionTime() => remainReceptionTime;

        private State ReadyState()
        {
            return new State(CustomerState.Ready, onStateStart: () =>
            {
                StopPeriodicPayment();

                customerFamily.AddReadyPerson();

                _currentZone = locationWaitingZone;

                //_currentZone.EndInteraction(transform, workIndex);

                try
                {
                    _currentZone.EndInteraction(transform, workIndex);
                }
                catch
                {
                }

                var interactable = _currentZone.GetInteractable(out int placeIndex, true, true);
                var movementTarget = interactable.position;
                workIndex = placeIndex;

                _movement.GoTo(movementTarget, b =>
                {
                    if (!b)
                        return;

                    //_currentZone.StartInteraction(transform, placeIndex);

                    _states.SwitchState(CustomerState.Idle);
                    DOVirtual.DelayedCall(_currentZone.GetInteractionTime(workIndex),
                        () => _states.SwitchState(CustomerState.Ready));
                });

                return true;
            }, Move, onStateEnd: () => { return true; });
        }

        private State ExitState()
        {
            return new State(CustomerState.Exit, onStateStart: () =>
            {
                _waitingOnReceptionProgressBar.Hide();

                collectButton.SetActive(false);

                customerFamily.CheckoutPerson();
                ReceptionMenuController.Instance.TryHideMenu(customerFamily);

                _currentWalkDelay = Time.time + _delayBeforeWalk;

                Vector3 movementTarget = endWayPoint.position;

                try
                {
                    _waitingZones[0].EndInteraction(transform, workIndex);
                }
                catch
                {
                    Debug.LogError("try to check");
                }

                if (IsCarOnParking())
                {
                    //_currentZone = parkingService;
                    movementTarget = customerFamily.GetCar().transform.position;
                }

                _movement.GoTo(movementTarget, b =>
                {
                    if (!b)
                        return;

                    customerFamily.RemovePerson(this);

                    TimeCycle.onTimeChanged -= TimeCycleChange;
                    Destroy(gameObject);
                });

                return true;
            }, Move, onStateEnd: () => { return true; });
        }

        private void ShowProgressBar(float time)
        {
            //_storage.TryToShowProgressBar(time / Stats.TimeCycle, time);
            //if (time < 0)
            //    return;

            //DOVirtual.DelayedCall(1, () => ShowProgressBar(time - 1));
        }

        public void Exit()
        {
            StopPeriodicPayment();

            try
            {
                _currentZone?.EndInteraction(transform, workIndex);
            }
            catch
            {
                Debug.LogError("try to check");
            }

            receptionService.UnSubscribeOnPropsAvailable(TryGoToReception);

            isExitState = true;

            if (_states.CurrentState != SpendingState())
            {
                _customerEmotionUI.ShowDispleasure();
                _states.SwitchState(CustomerState.Exit);
            }
        }

        private void OnMouseUpAsButton()
        {
            if (UIPointerExtensions.IsPointerOverUIObject(Input.mousePosition))
                return;

            ReceptionMenuController.Instance.ShowMenu(customerFamily);
        }

        #region SleepState

        private void TimeCycleChange(TimeCycle.TimesOfDay timesOfDay)
        {
            //TryGetNextZone();
            //_states.ExitState();
            //_states.SwitchState(CustomerState.Walk);
        }

        #endregion

        public CustomerDescription GetDescription() => customerDescription;

        public bool IsChild() => isChild;

        public void SetSatRating(float satRating) => SatRating = satRating;

        public void Checkin(NPCs.ServiceManager pickedRoom)
        {
            periodicPaymentCoroutine = StartCoroutine(PeriodicPayment());

            onRecepted.Invoke();

            var roomPleasureZones = FilterPleasureZones(pickedRoom.GetZones<CustomerPleasureZone>().ToList());

            this.roomPleasureZones.AddRange(roomPleasureZones);

            roomWaitingZone = pickedRoom.GetZones<WaitingZone>()[0];

            isInRoom = true;

            GetComponent<BoxCollider>().enabled = false;

            _currentZone.BookService(false);
            //receptionService.EndInteraction(transform, receptionIndex);
            //receptionService.GetStorage().Get(1);

            //SatRating = SatRating;

            _states.SwitchState(CustomerState.LivingCycle);
        }

        public List<CustomerInGameRequirement> GetRequirements() => customerLiveData.requirements;

        public float GetMinPayment() => customerLiveData.minPayment;

        public float GetMaxPayment() => customerLiveData.maxPayment;

        public void ShowCollectButton() => collectButton.SetActive(true);

        public void CollectMoney() => customerFamily.CollectMoney();

        public int GetProfit() => (int)Math.Ceiling(basePayment * (SatRating / 100) *
                                                    locationProgressController.GetLocationStats().checkOutPaymentMult);

        public int GetPeriodicProfit() => (int)Math.Ceiling(basePayment * (SatRating / 100));

        public void ShowCollectEffect(float coins) => Debug.Log("added coins " + coins);

        public void ShowCollectEffect() => ShowCollectEffect(GetProfit());

        public List<(string, int)> GetAllRequirements()
        {
            var result = new List<(string, int)>();

            foreach (var requirement in customerLiveData.requirements)
                result.Add((requirement.StatName, requirement.requirementPropsLevel));

            return result;
        }
    }
}