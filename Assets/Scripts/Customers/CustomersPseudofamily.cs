using NPCs;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using static UnityEditor.Progress;
using static UnityEditor.Recorder.OutputPath;

namespace Customers
{
    [System.Serializable]
    public class CustomersPseudofamily
    {
        bool isCarUsing = false;

        CustomerCar familyCar;

        List<CustomerAI> familyPersons = new List<CustomerAI>();

        CustomerAI mainPerson;

        ServiceManager bookedRoom;

        int receptedClientsCount = 0;
        int readyClientsCount = 0;

        public CustomersPseudofamily(bool isCarUsing)
        {
            this.isCarUsing = isCarUsing;
        }

        public bool IsCarUsing => isCarUsing;

        public int familySize;

        public List<bool> GeneratePersonsAge(int customersCount)
        {
            List<bool> personsIsChild = new List<bool>();
            personsIsChild.Add(false);

            for (int i = 0; i < customersCount; i++)
                personsIsChild.Add(Random.Range(0, 2) == 0);

            return personsIsChild;
        }

        public void SetCar(CustomerCar car) => familyCar = car;

        public CustomerCar GetCar() => familyCar;

        public void AddPersonToFamily(CustomerAI person) => familyPersons.Add(person);
    
        public void RemovePerson(CustomerAI person)
        {
            familyPersons.Remove(person);

            if (familyPersons.Count == 0)
                familyCar?.Exit();
        }

        public List<CustomerAI> GetClients() => familyPersons;

        public int FamilySize
        {
            get
            {
                return Mathf.Max(1, familySize + 1);
            }

            set
            {
                familySize = value;
            }
        }

        public void Refuse() => familyPersons.ForEach(x => x.Exit());

        public CustomerAI MainPerson()
        {
            if(mainPerson != null)
                return mainPerson;

            if(FamilySize == 1)
            {
                mainPerson = familyPersons[0];
                return mainPerson;
            }

            mainPerson = familyPersons.First(x => !x.IsChild());

            return mainPerson;
        }

        public CustomerDescription MemberDescription(int memberIndex) => familyPersons[memberIndex].GetDescription();
    
        public void Checkin(ServiceManager room)
        {
            bookedRoom = room;
            familyPersons.ForEach(x => x.Checkin(room));
            bookedRoom.GetStorage().Add(FamilySize);
        }

        //only for test
        public void Checkout()
        {
            if (bookedRoom == null)
                return;

            bookedRoom.GetStorage().Get(FamilySize);
            Refuse();
        }

        public void CheckoutPerson() => bookedRoom?.GetStorage().Get(1);

        public bool CanContainsAtReception(int receptionFreeSpace)
        {
            return receptionFreeSpace >= FamilySize - receptedClientsCount;
        }

        public Dictionary<string, (int, bool)> GetRequirements()
        {
            List<CustomerInGameRequirement> nonFilteredRequirements = new List<CustomerInGameRequirement>();

            foreach (var client in familyPersons)
                nonFilteredRequirements.AddRange(client.GetRequirements());

            var dictionary = nonFilteredRequirements.GroupBy(x => x.StatName);

            var result = new Dictionary<string, (int, bool)>(); 

            foreach (var pair in dictionary)
            {
                pair.OrderBy(x => x.requirementPropsLevel);
                var reversedList = pair.Reverse();
                var requirement = pair.FirstOrDefault(x => x.isObligatory);

                requirement = requirement ?? pair.First();

                result.Add(requirement.StatName, (requirement.requirementPropsLevel, requirement.isObligatory));
            }

            return result;
        }

        public List<(string, int)> GetAllRequirements()
        {
            List<CustomerInGameRequirement> nonFilteredRequirements = new List<CustomerInGameRequirement>();

            var result = new List<(string, int)>();

            foreach (var client in familyPersons)
            {
                var requirements = client.GetRequirements();
                requirements.ForEach(x => result.Add((x.StatName, x.requirementPropsLevel)));
            }

            return result;
        }

        public List<float> GetMinPayments()
        {
            List<float> values = new List<float>();

            familyPersons.ForEach(x => values.Add(x.GetMinPayment()));

            return values;
        }

        public List<float> GetMaxPayments()
        {
            List<float> values = new List<float>();

            familyPersons.ForEach(x => values.Add(x.GetMaxPayment()));

            return values;
        }

        public void AddReadyPerson()
        {
            ++readyClientsCount;

            if(readyClientsCount == FamilySize)
                familyPersons.ForEach(x => x.ShowCollectButton());
        }

        public void CollectMoney()
        {
            Managers.GameController.Instance.AddMoney(familyPersons.Sum(x => x.GetProfit()));

            familyPersons.ForEach(x => x.ShowCollectEffect());
            Refuse();
        }

        public void ReceptClient() => receptedClientsCount++;
    }
}