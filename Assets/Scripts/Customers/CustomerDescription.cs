[System.Serializable]
public class CustomerDescription
{
    public string Name;
    public string Gender;
    public int ForKids;
    public string IconName;
    public string Location;
    public string Profession;
    public string Description;
}
