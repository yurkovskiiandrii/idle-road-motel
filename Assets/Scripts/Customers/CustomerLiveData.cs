using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class CustomerLiveData
{
    public List<CustomerInGameRequirement> requirements;
    public float minPayment;
    public float maxPayment;
    public int leaveRankBorder;
    public int leaveRankChance;

    public List<string> GetRequirements()
    {
        var res = new List<string>();

        requirements.ForEach(x => res.Add(x.StatName));

        return res;
    }

    public bool IsFullStats()
    {
        foreach (var item in requirements)
            if (item.statBarCurrentValue < item.StatBarCapacity)
                return false;

        return true;
    }

    public void AddValueToStat(float value, string statType)
    {
        requirements.First(x => x.StatName == statType).statBarCurrentValue += value;
    }

    public bool ContainsStat(string statType)
    {
        return requirements.Where(x => x.StatName == statType).Count() > 0;
    }

    public void Shuffle()
    {
        var rng = new System.Random();
        var shuffledRequirements = requirements.OrderBy(a => rng.Next()).ToList();
        requirements = shuffledRequirements;
    }
}