using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DescriptionContainer")]
public class DescriptionContainer : DataFile
{
    public List<CustomerDescription> descriptions;
}
