using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RequirementsList/RequirementsList")]
public class RequirementsList : DataFile
{
    public List<CustomerRequirement> requirements;

    [HideInInspector]
    public List<string> requirementsNames;

    private void OnValidate()
    {
        requirementsNames = new List<string>();

        foreach (var requirement in requirements)
            requirementsNames.Add(requirement.StatName);
    }

    public List<CustomerRequirement> GetRequirements() => new List<CustomerRequirement>(requirements);
}
