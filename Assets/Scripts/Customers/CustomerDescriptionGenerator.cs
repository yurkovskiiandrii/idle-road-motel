using nickeltin.Singletons;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using Source;

[CreateAssetMenu(menuName = "Managers/CustomerDescriptionGenerator", fileName = "CustomerDescriptionGenerator")]
public class CustomerDescriptionGenerator : SOSingleton<CustomerDescriptionGenerator>
{
    [SerializeField] DescriptionContainer descriptionContainer;

    List<string> genders = new List<string>();

    public override bool Initialize()
    {
        if (base.Initialize())
        {
            var gendersGrouped = descriptionContainer.descriptions.GroupBy(x => x.Gender).ToList();
            
            foreach (var group in gendersGrouped)
                genders.Add(group.Key);
            
            return true;
        }

        return false;
    }

    public CustomerDescription GenerateDescription(bool isChild)
    {
        var description = new CustomerDescription();

        var availableDescriptionData = descriptionContainer.descriptions.Where(x => isChild ? x.ForKids == 1 : true).ToList();

        description.Gender = genders.GetRandom();

        var sortedByGender = availableDescriptionData.Where(x => x.Gender == description.Gender).ToList();

        description.Name = sortedByGender.GetRandom().Name;
        description.IconName = sortedByGender.GetRandom().IconName;
        description.Description = sortedByGender.GetRandom().Description;
        description.Location = descriptionContainer.descriptions.GetRandom().Location;
        description.Profession = descriptionContainer.descriptions.GetRandom().Profession;

        return description;
    }
}
