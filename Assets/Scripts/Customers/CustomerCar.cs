using DG.Tweening;
using GameData;
using GameEnvironment.Village.Structures;
using GameEnvironment;
using nickeltin.StateMachine;
using NPCs;
using System;
using System.Linq;
using UI;
using UnityEngine;
using UnityEngine.Events;
using WaypointsSystem;
using nickeltin.Editor.Attributes;
using System.Collections.Generic;
using static Customers.CustomerAI;
using System.Collections;

namespace Customers
{
    [SelectionBase, DisallowMultipleComponent]
    public class CustomerCar : MonoBehaviour
    {
        [Serializable]
        private class CustomerCarStateEvent: StateEvent
        {
            public CustomerCarState State;
        }

        public enum CustomerCarState
        {
            Start,
            Idle,
            Moving,
            Parking,
            Wait,
            Exit
        }

        [SerializeField] private string _id;
        private Animator _animator;
        [SerializeField] private RuntimeAnimatorController _animatorController;
        [SerializeField, ReadOnly] private ServiceManager _serviceManager;

        [Space][SerializeField] private float _delayBeforeWalk;
        private float _currentWalkDelay;
        [SerializeField] private float _rotationSpeed = 5;
        [SerializeField] private float _baseSpeed = 1;
        [SerializeField, ReadOnly] private ServiceZone _currentZone;

        Transform endWayPoint;

        [Header("Events"), Space, SerializeField]
        private CustomerCarStateEvent[] _stateEvents;

        private CustomerCarStateEvent[] _existsStateEvents;

        private Vector3 _prevPosition;
        private Quaternion _targetRotation;
        private WaypointMovement _movement;
        private StateMachine _states;

        int parkingCarPlace = -1;

        CustomersPseudofamily family;

        readonly string queueTag = "QueueObject";

        private GettingZone[] _gettingZones;
        private WaitingZone[] _waitingZones;

        public string ID
        {
            get
            {
                if (_id == "")
                    _id = gameObject.name;
                return _id;
            }
            set => _id = value;
        }

        private void Awake()
        {
            _movement = new WaypointMovement(transform.position, _baseSpeed);
            _animator = GetComponent<Animator>();
        }

        bool IsCarOnParking() => parkingCarPlace != -1;

        public void SetFamily(CustomersPseudofamily family) => this.family = family;

        public void ActivateCar(bool enable)
        {
            if (enable)
            {
                _animator.runtimeAnimatorController = _animatorController;
                _movement.Teleport(transform.position);
                _states.enabled = true;
                TryGetNextZone();
                _states.SwitchState(CustomerCarState.Moving);
            }
            else
            {
                _states.ExitState();
                _states.enabled = false;
                _movement.Stop();
            }
        }

        public void Initialize(ServiceManager parking, Transform endWayPoint)
        {
            _serviceManager = parking;

            _states = new StateMachine(transform, true, false, StartState(), IdleState(), DriveState(),
                ParkingState(), WaitingState(), ExitState());

            this.endWayPoint = endWayPoint;

            if (_stateEvents.Length > 0)
            {
                var values = Enum.GetValues(typeof(CustomerCarState));
                _existsStateEvents = new CustomerCarStateEvent[values.Length];

                for (int i = 0; i < values.Length; i++)
                    _existsStateEvents[i] = _stateEvents.FirstOrDefault(x => (int)x.State == i);
            }

            _gettingZones = _serviceManager.GetZones<GettingZone>();
            _waitingZones = _serviceManager.GetZones<WaitingZone>();

            ActivateCar(true);
        }

        private State StartState()
        {
            return new State(CustomerCarState.Start, () =>
            {
                _existsStateEvents?[(int)CustomerCarState.Start]?.Invoke(true);
                return true;
            });
        }

        private void SubscribeOnWorkAvailable()
        {
            //to test
            _serviceManager.OnGettingAvailable += ResourcesReceived;
            _serviceManager.subscribed.Add(name);
        }

        private void UnsubscribeOnWorkAvailable()
        {
            _serviceManager.subscribed.Remove(name);

            _serviceManager.OnGettingAvailable -= ResourcesReceived;
            //_serviceManager.OnSpendingAvailable -= NextZoneAvailable;
        }

        private void ResourcesReceived(ServiceZone zone)
        {
            DOVirtual.DelayedCall(Time.deltaTime,
                () => NextZoneAvailable(zone.transform.parent.GetChild(0).GetComponent<GettingZone>()));
        }

        private void NextZoneAvailable(ServiceZone zone)
        {
            if (!zone.Unavailable)
            {
                UnsubscribeOnWorkAvailable();

                _currentZone.EndInteraction(transform, 0);

                _currentZone = zone;

                _states.SwitchState(CustomerCarState.Moving);
                return;
            }

            TryGetNextZone();

            //_currentZone = null;
        }

        private bool GoToNextZone<T>(T[] zones) where T : ServiceZone
        {
            foreach (var zone in zones)
            {
                if (!zone.Unavailable)
                {
                    _currentZone = zone;
                    return true;
                }
            }
            return false;
        }

        private bool GoToNextZone<T>(T zone) where T : ServiceZone
        {
            if (!zone.Unavailable)
            {
                _currentZone = zone;
                return true;
            }

            return false;
        }

        private bool TryGetNextZone()
        {
            if (!IsCarOnParking()
                && GoToNextZone(_gettingZones))
                    return true;

            return false;
        }

        #region IdleState

        private State IdleState()
        {
            return new State(CustomerCarState.Idle, onStateStart: () =>
            {
                _existsStateEvents?[(int)CustomerCarState.Idle]?.Invoke(true);
                return true;
            }, onStateEnd: () =>
            {
                //UnsubscribeOnWorkAvailable();
                _existsStateEvents?[(int)CustomerCarState.Idle]?.Invoke(false);
                return true;
            });
        }

        #endregion

        #region Movement

        private bool Move()
        {
            if (_currentWalkDelay < Time.time)
            {
                UpdatePosition(_movement.CurrentPosition);
                UpdateRotation();
                _movement.Update(Time.deltaTime);
            }

            return _movement.IsMoving;
        }

        private void UpdateRotation()
        {
            transform.rotation =
                Quaternion.Lerp(
                    transform.rotation,
                    _targetRotation,
                    Time.deltaTime * _rotationSpeed);
        }

        private void UpdatePosition(Vector3 originPos)
        {
            transform.position = originPos;
            var delta = transform.position - _prevPosition;
            delta.y = 0;
            if (delta != Vector3.zero)
            {
                _targetRotation = Quaternion.LookRotation(delta);
            }

            _prevPosition = originPos;
        }

        #endregion

        private State DriveState()
        {
            return new State(CustomerCarState.Moving, onStateStart: () =>
            {
                StopAllCoroutines();
                _currentWalkDelay = Time.time + _delayBeforeWalk;

                Vector3 movementTarget = endWayPoint.position;

                try
                {
                    if (!IsCarOnParking())
                        GoToNextZone(_gettingZones);

                    movementTarget = _currentZone.GetInteractable(out parkingCarPlace, false, true).position;
                    parkingCarPlace = _serviceManager.GetZoneIndex(_currentZone);
                    _currentZone.BookService(true);
                }
                catch
                {
                    GoToNextZone(_waitingZones);

                    movementTarget = _currentZone.GetInteractable(out int parkingPlace, false, true).position;
                    parkingCarPlace = parkingPlace;
                    _currentZone.StartInteraction(transform, parkingCarPlace);
                }
                //_currentZone.BookService(true);

                _movement.GoTo(movementTarget, b =>
                {
                    if (!b)
                        return;

                    if (_currentZone.CompareTag(queueTag)/* _serviceManager.IsLastZone(parkingCarPlace)*/)
                    {
                        if (GoToNextZone(_gettingZones)
                             /*&& _currentZone.InteractableObjects.Count(x => !x.isOccupied) > 1*/)
                        {
                            parkingCarPlace = -1;
                            _states.SwitchState(CustomerCarState.Idle);
                            _states.SwitchState(CustomerCarState.Moving);

                            return;
                        }

                        _states.SwitchState(CustomerCarState.Wait);
                        return;
                    }

                    _states.SwitchState(CustomerCarState.Parking);
                });

                //if (isCarUsing)
                //{
                //    _existsStateEvents?[(int)CustomerState.Driving]?.Invoke(true);
                //    //_animator.SetBool(WalkAnimator, true);
                //}

                return true;
            }, Move,
                onStateEnd: () =>
                {
                    _movement.SetSpeedCustom(_baseSpeed);

                    return true;
                });
        }

        private State ParkingState()
        {
            return new State(CustomerCarState.Parking, onStateStart: () =>
            {
                var spawner = _currentZone.GetInteractable(0).GetComponentInChildren<CustomerSpawner>();
                family.SetCar(this);
                spawner.SpawnFamily(family);

                return true;
            }, null,
            onStateEnd: () => 
            {
                _currentZone.transform.parent.GetComponent<SpendingZone>().Add(1);
                _currentZone.GetComponent<Storage>().Set(0);
                return true; 
            } );
        }

        private State WaitingState()
        {
            return new State(CustomerCarState.Wait, onStateStart: () =>
            {
                if (!GoToNextZone(_gettingZones))
                {
                    SubscribeOnWorkAvailable();
                    StartCoroutine(ExitStateWaiter());
                }
                else
                {
                    parkingCarPlace = -1;
                    _states.SwitchState(CustomerCarState.Idle);
                    _states.SwitchState(CustomerCarState.Moving);
                }

                return true;
            }, null,
            onStateEnd: () => true );
        }

        IEnumerator ExitStateWaiter()
        {
            yield return new WaitForSeconds(_serviceManager.timers.entrancePoitWait);

            if (GoToNextZone(_gettingZones)
                             /*&& _currentZone.InteractableObjects.Count(x => !x.isOccupied) > 1*/)
            {
                parkingCarPlace = -1;
                _states.SwitchState(CustomerCarState.Idle);
                _states.SwitchState(CustomerCarState.Moving);

                yield break;
            }

            _states.SwitchState(CustomerCarState.Exit);
        }

        private State ExitState()
        {
            return new State(CustomerCarState.Exit, onStateStart: () =>
            {
                UnsubscribeOnWorkAvailable();

                _currentWalkDelay = Time.time + _delayBeforeWalk;

                Vector3 movementTarget = endWayPoint.position;

                _currentZone.EndInteraction(transform, _currentZone is WaitingZone ? parkingCarPlace : 0);

                parkingCarPlace = -1;

                _movement.GoTo(movementTarget, b =>
                {
                    if (!b)
                        return;

                    _serviceManager.subscribed.Remove(name);

                    Destroy(gameObject);
                });

                return true;
            }, Move, onStateEnd: () => true);
        }

        public void Exit() => _states.SwitchState(CustomerCarState.Exit);
    }
}