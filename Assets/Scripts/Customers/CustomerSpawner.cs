using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using NPCs;
using RandomGeneratorWithWeight;
using GameEnvironment.Village.Structures;

namespace Customers
{
    public class CustomerSpawner : MonoBehaviour
    {
        [SerializeField] bool startSpawnOnStart = false;

        [SerializeField] Transform startWaypoint;
        [SerializeField] Transform endWaypoint;
        [SerializeField] GameObject customerPrefab;
        [SerializeField] List<GameObject> customerCarPrefabs;

        [SerializeField] ServiceManager parkingService;
        [SerializeField] ServiceManager receptionService;
        [SerializeField] RoomsContainer notRoomServices;
        [SerializeField] WaitingZone locationWaitingZone;

        [SerializeField] float carCustomerChance = 0.5f;
        [SerializeField] float walkCustomerChance = 0.5f;

        [SerializeField] float familySpawnDelay = 0.1f;

        LocationStats locationStats;
        List<ItemForRandom<int>> clientsGenerationData;

        WaitForSeconds familyWaiter;

        public bool canProcessInput = false;

        private void Start()
        {
            familyWaiter = new WaitForSeconds(familySpawnDelay);

            var locationController = LocationProgressController.GetInstance();
            locationController.SubscribeOnProgressChanged(UpdateLocationStats);

            UpdateLocationStats(locationController.GetLocationStats());

            if(startSpawnOnStart)
                StartCoroutine(DelayGenerator());
        }

        void UpdateLocationStats(LocationStats newStats)
        {
            locationStats = newStats;

            clientsGenerationData = new List<ItemForRandom<int>>();

            clientsGenerationData.Add(new ItemForRandom<int>(locationStats.quadroClient, 3));
            clientsGenerationData.Add(new ItemForRandom<int>(locationStats.tripleClient, 2));
            clientsGenerationData.Add(new ItemForRandom<int>(locationStats.doubleClient, 1));
            clientsGenerationData.Add(new ItemForRandom<int>(locationStats.oneClient, 0));
        }

        IEnumerator DelayGenerator()
        {
            yield return new WaitForSeconds(3f);

            while (true)
            {
                bool isCarUsing = Random.Range(0, carCustomerChance + walkCustomerChance) < carCustomerChance;

                var family = new CustomersPseudofamily(isCarUsing);

                if (isCarUsing)
                    SpawnCar(family);
                else
                    StartCoroutine(FamilySpawner(family));

                yield return new WaitForSeconds(Random.Range(locationStats.clientFrequencyMin, locationStats.clientFrequencyMax));
            }
        }

        void SpawnCar(CustomersPseudofamily family)
        {
            var car = Instantiate(customerCarPrefabs[Random.Range(0, customerCarPrefabs.Count)], startWaypoint.transform.position, Quaternion.identity);
            var carScript = car.GetComponent<CustomerCar>();
            carScript.SetFamily(family);
            carScript.Initialize(parkingService, endWaypoint);
        }

        public void SpawnFamily(CustomersPseudofamily family) => StartCoroutine(FamilySpawner(family));

        IEnumerator FamilySpawner(CustomersPseudofamily family)
        {
            int clientsCount = GetItemWithWeight.GetItem(clientsGenerationData);

            var familyAges = family.GeneratePersonsAge(clientsCount);

            family.FamilySize = clientsCount;

            int clientNumber = 1;

            foreach (var familyAge in familyAges)
            {
                SpawnCustomer(family, familyAge, clientNumber);
                yield return familyWaiter;

                ++clientNumber;
            }
        }

        public void SpawnCustomer(CustomersPseudofamily family, bool isChild, int clientNumber)
        {
            var customer = Instantiate(customerPrefab, startWaypoint.transform.position, Quaternion.identity);
            var customerAI = customer.GetComponent<CustomerAI>();
            customerAI.SetFamily(family);
            customerAI.Initialize(parkingService, receptionService, notRoomServices, endWaypoint, isChild, locationWaitingZone);
        }

        private void Update()
        {
            if (canProcessInput && Input.GetMouseButtonUp(1))
                Generate();
        }

        public void Generate()
        {
            bool isCarUsing = Random.Range(0, carCustomerChance + walkCustomerChance) < carCustomerChance;

            var family = new CustomersPseudofamily(isCarUsing);

            if (isCarUsing)
                SpawnCar(family);
            else
                StartCoroutine(FamilySpawner(family));
        }
    }


    [CustomEditor(typeof(CustomerSpawner))]
    public class CustomerSpawnerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var spawner = (CustomerSpawner)target;

            if (GUILayout.Button("Generate customer"))
                spawner.Generate();
        }
    }
}