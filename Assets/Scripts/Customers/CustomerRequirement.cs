using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomerRequirement
{
    public string StatName;
    public float Value;
    public float Obligatory;
    public int Tier;
    public float TimeCycleMin;
    public float TimeCycleMax;
    public int StatBarCapacity;
    public float RankMinus;
    public float RankPlus;
    public int ForKids;
}

[System.Serializable]
public class CustomerInGameRequirement : CustomerRequirement
{
    public bool isObligatory;
    public int requirementPropsLevel;
    public float statBarCurrentValue;

    public CustomerInGameRequirement()
    {

    }

    public CustomerInGameRequirement(CustomerRequirement requirement)
    {
        StatName = requirement.StatName;
        Value = requirement.Value;
        Obligatory = requirement.Obligatory;
        Tier = requirement.Tier;
        TimeCycleMin = requirement.TimeCycleMin;
        TimeCycleMax = requirement.TimeCycleMax;
        RankMinus = requirement.RankMinus;
        RankPlus = requirement.RankPlus;

        StatBarCapacity = requirement.StatBarCapacity;
    }
}