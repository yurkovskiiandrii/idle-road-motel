using System;
using Cinemachine;
using DG.Tweening;
using Managers;
using Source;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using TouchPhase = UnityEngine.TouchPhase;

public enum GameCameraState
{
    Default,
    BuildingView,
    Following,
    Focus
}
public class CameraMove : MonoBehaviour
{
    public Bounds Bounds;
    public float DefaultY = 23;
    public float ZoomSensitivity = 1f;
    public float ZoomMinSize = 3f;
    public float ZoomMaxSize = 50f;
    public float ZoomElasticity = 0.2f;
    public float ZoomElasticityLerpSpeed = 5f;
    public float ZoomLerpSpeed = 2f;
    public float DefaultDampSmoothTime = 0.2f;
    public float DampMaxSpeed = 300f;
    public float SuccessiveVelocityMult = 0.2f;
    public float SuccessiveVelocityThreshold = 1f;
    public float SmoothTimeVelocityDependenceModif = 15;
    public float SmoothTimeVelocityDependenceClamp = 0.5f;
    public float CamDistanceFromTarget = 50;
    public Vector3 CamOffsetFromTarget;
    public float CamZoomToTarget;
    public static event Action onClick;

    private static bool MoveBlocked => InputManager.CameraMoveBlocked;

    private Camera _camera;
    private Plane _plane;
    private float _currentDampSmoothTime;
    private int _prevTouchCount;
    private Vector3 _prevCursorPos;
    private Vector3 _currentDampVelocity;
    private bool _prevIsFocused;
    private GameCameraState _state;
    private Vector3 _previousCameraPosition;
    private float _previousOrthoSize;
    private Transform _currentViewTarget;
    private Vector3 _currentTargetPoint;
    private float _currentTargetZoom;
    private Transform _followTarget;
    private CinemachineVirtualCamera _virtualCamera;
    private bool IsStandalone;

    public static CameraMove Instance;

    private void Start()
    {
        Instance = this;
        _camera = GameCamera.MainCamera;
        _plane = new Plane(Vector3.up, Vector3.zero);
        _currentTargetPoint = transform.position;
        _virtualCamera = GetComponent<CinemachineVirtualCamera>();
        _currentTargetZoom = _virtualCamera.m_Lens.OrthographicSize;
        _currentDampSmoothTime = DefaultDampSmoothTime;
#if UNITY_EDITOR
        IsStandalone = true;
#endif
        //Debug.Log(Application.platform);

    }
    private void Update()
    {
        if (MoveBlocked) 
            return;

        //if (_inputService == null || _inputService.IsInputLocked || _switchStateSeq != null) return;

        if (!IsStandalone)
            UpdateMobileInput();
        else
            UpdateStandaloneInput();
    }

    void LateUpdate()
    {
        switch (_state)
        {
            case GameCameraState.Default:
                UpdateMovement();
                UpdateZoom();
                break;
            case GameCameraState.Following:
                if (_followTarget == null)
                    break;

                _currentTargetPoint = _followTarget.position -
                                      _camera.transform.rotation * Vector3.forward * CamDistanceFromTarget +
                                      CamOffsetFromTarget;
                UpdateMovement();
                break;
            case GameCameraState.BuildingView:
                break;
        }

        var a = SmoothTimeVelocityDependenceModif / _currentDampVelocity.magnitude;
        var b = SmoothTimeVelocityDependenceClamp;
        var clamp = a > b ? b : a;
        _currentDampSmoothTime = DefaultDampSmoothTime *
                                 (1 - clamp);
    }

    private void UpdateMovement()
    {
        _previousCameraPosition = transform.position;
        transform.position = Vector3.SmoothDamp(transform.position, _currentTargetPoint,
            ref _currentDampVelocity, _currentDampSmoothTime,
            DampMaxSpeed, Time.deltaTime);
    }

    private void UpdateZoom()
    {
        if (_currentTargetZoom > ZoomMaxSize)
            _currentTargetZoom =
                Mathf.Lerp(_currentTargetZoom, ZoomMaxSize, ZoomElasticityLerpSpeed * Time.deltaTime);
        if (_currentTargetZoom < ZoomMinSize)
            _currentTargetZoom =
                Mathf.Lerp(_currentTargetZoom, ZoomMinSize, ZoomElasticityLerpSpeed * Time.deltaTime);

        _virtualCamera.m_Lens.OrthographicSize =
            Mathf.Lerp(_virtualCamera.m_Lens.OrthographicSize, _currentTargetZoom, Time.deltaTime * ZoomLerpSpeed);
    }

    private void UpdateMobileInput()
    {
        if (UnityEngine.Input.touchCount == 0 
            || IsPointerOverUI.IsPointerOverUIObject(Input.GetTouch(0).position))
            return;

        //Scroll
        if (UnityEngine.Input.touchCount == 1)
        {
            Vector3 touchPos = UnityEngine.Input.GetTouch(0).position;
            if (UnityEngine.Input.GetTouch(0).phase == TouchPhase.Moved && _prevCursorPos != touchPos)
            {
                var delta = PlanePositionDelta(touchPos);
                AddDelta(delta);
            }

            _prevCursorPos = touchPos;
            if (!EventSystem.current.IsPointerOverGameObject()) onClick?.Invoke();
        }

        //Pinch
        if (UnityEngine.Input.touchCount >= 2)
        {
            var pos1 = PlanePosition(UnityEngine.Input.GetTouch(0).position);
            var pos2 = PlanePosition(UnityEngine.Input.GetTouch(1).position);
            var pos1b = PlanePosition(UnityEngine.Input.GetTouch(0).position -
                                      UnityEngine.Input.GetTouch(0).deltaPosition);
            var pos2b = PlanePosition(UnityEngine.Input.GetTouch(1).position -
                                      UnityEngine.Input.GetTouch(1).deltaPosition);

            //calc zoom
            var zoom = Vector3.Distance(pos1, pos2) /
                       Vector3.Distance(pos1b, pos2b);

            var isEdgeCase = float.IsNaN(zoom) || zoom == 0 || zoom > 10;

            if (!isEdgeCase)
            {
                var orthoSize = _currentTargetZoom / zoom;
                var elasticityMult = 1 + Mathf.Abs(ZoomElasticity);
                _currentTargetZoom = Mathf.Clamp(orthoSize, ZoomMinSize / elasticityMult,
                    ZoomMaxSize * elasticityMult);
            }
        }

        if (UnityEngine.Input.touchCount == 0 && _prevTouchCount > 0)
            TrySuccessiveVelocity();

        _prevTouchCount = UnityEngine.Input.touchCount;
    }

    private void UpdateStandaloneInput()
    {
        if (IsPointerOverUI.IsPointerOverUIObject(Input.mousePosition))
            return;

        var zoomAxisValue = UnityEngine.Input.GetAxis("Mouse ScrollWheel");
        if (zoomAxisValue != 0)
        {
            _currentTargetZoom -= zoomAxisValue * ZoomSensitivity;
            var elasticityMult = 1 + Mathf.Abs(ZoomElasticity);
            _currentTargetZoom = Mathf.Clamp(_currentTargetZoom, ZoomMinSize / elasticityMult,
                ZoomMaxSize * elasticityMult);
        }

        if (UnityEngine.Input.GetMouseButton(0))
        {
            if (_prevCursorPos == null || _prevIsFocused != Application.isFocused)
                _prevCursorPos = UnityEngine.Input.mousePosition;

            var delta = PlanePositionDelta(UnityEngine.Input.mousePosition);
            if (UnityEngine.Input.mousePosition != _prevCursorPos)
                AddDelta(delta);
        }

        if (UnityEngine.Input.GetMouseButtonUp(0))
            TrySuccessiveVelocity();

        _prevCursorPos = UnityEngine.Input.mousePosition;
        _prevIsFocused = Application.isFocused;
    }

    private void TrySuccessiveVelocity()
    {
        if (_currentDampVelocity.magnitude > SuccessiveVelocityThreshold)
        {
            _currentTargetPoint += _currentDampVelocity * SuccessiveVelocityMult;
            _currentTargetPoint.x = Mathf.Clamp(_currentTargetPoint.x, Bounds.min.x, Bounds.max.x);
            _currentTargetPoint.z = Mathf.Clamp(_currentTargetPoint.z, Bounds.min.z, Bounds.max.z);
        }
    }

    private void AddDelta(Vector3 delta)
    {
        var pos = _currentTargetPoint + delta;
        pos.x = Mathf.Clamp(pos.x, Bounds.min.x, Bounds.max.x);
        pos.z = Mathf.Clamp(pos.z, Bounds.min.z, Bounds.max.z);
        _currentTargetPoint = pos;
    }

    public void SwitchToFollow(Transform followTarget)
    {
        _state = GameCameraState.Following;
        _followTarget = followTarget;
        _previousCameraPosition = _currentTargetPoint;
        _previousOrthoSize = _camera.orthographicSize;
        _camera.orthographicSize = CamZoomToTarget;
    }

    public void SwitchToDefault()
    {
        InputManager.CameraMoveBlocked = false;
        _state = GameCameraState.Default;
        _currentTargetPoint = _previousCameraPosition;
        _currentTargetZoom = _previousOrthoSize;
    }

    public void SetMovementTarget(Transform followTarget)
    {
        _currentTargetPoint = new Vector3(followTarget.transform.position.x, _currentTargetPoint.y, followTarget.transform.position.z);
    }

    public void SetMovementTarget(CinemachineVirtualCamera followTarget)
    {
        _currentTargetPoint = followTarget.transform.position;
        transform.position = _currentTargetPoint;
       // _previousCameraPosition = _currentTargetPoint;
        Bounds.center = _currentTargetPoint;
        _virtualCamera.m_Lens.OrthographicSize = followTarget.m_Lens.OrthographicSize;
    }

    public void SwitchToBuilding(Vector3 buildingPosition)
    {
        InputManager.CameraMoveBlocked = true;
        _state = GameCameraState.BuildingView;
        _previousCameraPosition = transform.position;
        _previousOrthoSize = _camera.orthographicSize;
        MoveToPosition(buildingPosition);
    }

    private Vector3 PlanePositionDelta(Vector3 mousePos)
    {
        //not moved
        if (_prevCursorPos == mousePos)
            return Vector3.zero;

        //delta
        var rayBefore = _camera.ScreenPointToRay(mousePos - (mousePos - _prevCursorPos));
        var rayNow = _camera.ScreenPointToRay(mousePos);
        float enterBefore;
        float enterNow;
        if (_plane.Raycast(rayBefore, out enterBefore) && _plane.Raycast(rayNow, out enterNow))
            return rayBefore.GetPoint(enterBefore) - rayNow.GetPoint(enterNow);

        //not on plane
        return Vector3.zero;
    }

    private Vector3 PlanePosition(Vector2 screenPos)
    {
        //position
        var rayNow = _camera.ScreenPointToRay(screenPos);
        if (_plane.Raycast(rayNow, out var enterNow))
            return rayNow.GetPoint(enterNow);

        return Vector3.zero;
    }

    public void MoveToPosition(Vector3 position, bool instantly = false, bool ignoreOffset = false)
    {
        var target = ignoreOffset ? position : GetTargetPosWithOffset(position);
        _currentTargetPoint = target;

        DOVirtual.Float(0, 1, instantly ? 0 : 1, (value) =>
        {
            _camera.transform.position = Vector3.Lerp(_camera.transform.position, target, value);
            _currentTargetPoint = _camera.transform.position;
        });
    }

    public void ZoomTo(float value)
    {
        _currentTargetZoom = Mathf.Clamp(value, ZoomMinSize, ZoomMaxSize);
    }

    private Vector3 GetTargetPosWithOffset(Vector3 position)
    {
        var targetPos = position - transform.rotation * Vector3.forward * CamDistanceFromTarget;
        targetPos.y = DefaultY;
        return targetPos;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(Bounds.center, Bounds.extents*2);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(
            transform.position.x,
            0,
            transform.position.z));
    }
    public void Shake(float time, float amplitude)
    {
        //GUIController.LogDebug("Camera Shake t: " + time + " amp: " + amplitude);
        transform.DOShakePosition(time, amplitude);
    }
}
