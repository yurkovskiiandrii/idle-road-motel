using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtCameraLooker : MonoBehaviour
{
    private Transform _targetedCamera;

    private void Start()
    {
        _targetedCamera = GameCamera.MainCamera.transform;
    }

    private void Update()
    {
        transform.LookAt(
                transform.position + _targetedCamera.rotation * Vector3.forward,
                _targetedCamera.rotation * Vector3.up);
    }
}
