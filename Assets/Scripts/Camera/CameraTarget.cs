﻿using System;
using System.Collections.Generic;
using nickeltin.Extensions;
using UnityEditor;
using UnityEngine;

namespace CameraControlling
{
    public class CameraTarget : MonoBehaviour
    {
        private static readonly List<CameraTarget> allTargets = new List<CameraTarget>();
        private static readonly CameraTargetOrderComarer comarer = new CameraTargetOrderComarer();
        private static int currentTargetIndex = 0;
        
        [SerializeField] private int order;
        [SerializeField] private CameraTarget m_left;
        [SerializeField] private CameraTarget m_right;
        [SerializeField] private bool m_leftDeadEnd;
        [SerializeField] private bool m_rightDeadEnd;
        public bool overrideCameraSettings;
        public OverrideProps cameraOverrideProps;
        
        public static event Action<CameraTarget> onTargetChange;

        /// <returns> Returns true if there is more than 0 targets</returns>
        public static bool TryGetCurrentTarget(out CameraTarget cameraTarget)
        {
            if (allTargets.Count > 0)
            {
                cameraTarget = allTargets[Mathf.Clamp(currentTargetIndex, 0, allTargets.Count - 1)];
                return true;
            }

            cameraTarget = null;
            return false;
        }
        
        public static void ChangeTarget(CameraTarget target)
        {
            currentTargetIndex = allTargets.IndexOf(target);
            onTargetChange?.Invoke(target);
        }
        /// <param name="direction">-1 or 1</param>
        public static void ChangeTargetByDirection(int direction)
        {
            if (allTargets.Count <= 0) return;
            var currentTarget = allTargets[currentTargetIndex];
            
            if (direction.Negative() && !currentTarget.m_leftDeadEnd)
            {
                if (currentTarget.m_left != null) ChangeTarget(currentTarget.m_left);
                else ChangeTargetByIndex(currentTargetIndex - 1);
            }
            else if (direction.Positive() && !currentTarget.m_rightDeadEnd) 
            {
                if (currentTarget.m_right != null) ChangeTarget(currentTarget.m_right);
                else ChangeTargetByIndex(currentTargetIndex + 1);
            }
        }
        public static void ChangeTargetByIndex(int index) => ChangeTarget(allTargets[index]);
        
        public static void ChangeTargetByOrder(int targetOrder)
        {
            var target = allTargets.Find(t => t.order == targetOrder);
            if (target != null) ChangeTarget(target);
        }
        
        [Serializable]
        public class OverrideProps
        {
            public float cameraX;
            public float cameraY;
            public float cameraYAngle;
            public float ortoSize;
            public Collider viewBox;
        }
        
        private class CameraTargetOrderComarer : IComparer<CameraTarget>
        {
            public int Compare(CameraTarget a, CameraTarget b)
            {
                if (a.order > b.order) return 1;
                if (a.order < b.order) return -1;
                return 0;
            }
        }

        private void OnDisable() => allTargets.Remove(this);

        private void OnEnable()
        {
            allTargets.Add(this);
            allTargets.Sort(comarer);
            if (order == 0) currentTargetIndex = allTargets.IndexOf(this);
        }
        
#if UNITY_EDITOR
        [ContextMenu("Copy props")]
        private void CopyOverrideProps_Context()
        {
            Undo.RecordObject(this, "Camera props copied");
            CameraController.copiedProps = cameraOverrideProps;
        }

        [ContextMenu("Paste props")]
        private void PasteOverrideProps_Context()
        {
            Undo.RecordObject(this, "Camera props pasted");
            cameraOverrideProps = CameraController.copiedProps;
        }
#endif
    }
}