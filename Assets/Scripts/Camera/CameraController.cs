﻿using System;
using System.Collections;
using nickeltin.Editor.Attributes;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace CameraControlling
{
    [ExecuteInEditMode, SelectionBase]
    public class CameraController : MonoBehaviour
    {
        private static CameraController instance;
        public static CameraTarget.OverrideProps copiedProps = new CameraTarget.OverrideProps();
        public new static Camera camera;
        public static Camera uiCamera;
        public static bool movementEnabled { get => instance.m_movementEnabled; set => instance.m_movementEnabled = value; }
        public static event Action onDragBegin;
        public static event Action onDrag;
        public static event Action onDragEnd;
        public static event Action onClick;

        [SerializeField] private Skybox m_skybox;
        [SerializeField] private Camera m_camera;
        [SerializeField] private Camera m_uiCamera;

        [Space]
        [SerializeField] private bool m_movementEnabled = true;
        [SerializeField, ReadOnly] private CameraTarget.OverrideProps m_defaultProps;
        
        [Header("Input & Movement")] 
        [SerializeField] private float m_interpolationSpeed;
        [SerializeField] private float m_axisInputSensetivity;
        [SerializeField] private float m_touchInputSensetivity;
        [SerializeField, Range(0f, 0.1f)] private float m_touchInputDeadzone;

        private Vector3 m_newPosition;
        private Vector3 m_lastTouchPosition;
        private float m_lastSize;
        private float m_newSize;
        private Bounds? m_bounds;

        private bool m_pointerDown;
        private bool m_pointerDragged;

        private void Awake()
        {
            instance = this;
            camera = m_camera;
            uiCamera = m_uiCamera;
            m_newPosition = transform.position;
            m_uiCamera.orthographicSize = m_camera.orthographicSize;
            m_defaultProps = GetProps();
            m_newSize = m_lastSize = m_camera.orthographicSize;
        }

        private void Update()
        {
            if (m_camera == null) return;

            m_camera.transform.LookAt(transform);

            if (!Application.isPlaying || !m_movementEnabled) return;

            AxisInput();
            PointerInput();
        }

        private void Start()
        {

            m_bounds = null;
            //Refreshing ui camera, for some reason only after that ui starts to render
            m_uiCamera.gameObject.SetActive(false);
            m_uiCamera.gameObject.SetActive(true);
            
            if (CameraTarget.TryGetCurrentTarget(out var t)) SelectTarget(t);
        }

        private void LateUpdate()
        {
            if (!Application.isPlaying) return;
            
            Vector3 pos = Vector3.Lerp(transform.position, m_newPosition, Time.deltaTime * m_interpolationSpeed);

            if (m_bounds.HasValue)
            {
                pos = new Vector3(Mathf.Clamp(pos.x, m_bounds.Value.min.x, m_bounds.Value.max.x),
                    Mathf.Clamp(pos.y, m_bounds.Value.min.y, m_bounds.Value.max.y),
                    Mathf.Clamp(pos.z, m_bounds.Value.min.z, m_bounds.Value.max.z));
            }
            
            transform.position = pos;

            m_camera.orthographicSize = m_uiCamera.orthographicSize = Mathf.Lerp(m_camera.orthographicSize, m_newSize,
                Time.deltaTime * m_interpolationSpeed);
        }

        private void PointerInput()
        {
            Vector3 mousePosition = UnityEngine.Input.mousePosition;
            
            if (UnityEngine.Input.touchCount > 1)
            {
                m_lastTouchPosition = UnityEngine.Input.GetTouch(0).position;
                m_pointerDown = false;
                m_pointerDragged = false;
                return;
            }
            
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                m_lastTouchPosition = mousePosition;
                m_pointerDown = true;
                m_pointerDragged = false;
            }
            else if (UnityEngine.Input.GetMouseButton(0))
            {
                if(EventSystem.current.IsPointerOverGameObject()) return;
                
                Vector3 delta = m_lastTouchPosition - mousePosition;
                Vector3 viewportDelta = m_camera.ScreenToViewportPoint(m_lastTouchPosition) -
                                        m_camera.ScreenToViewportPoint(mousePosition);
                
                if (viewportDelta.magnitude > m_touchInputDeadzone)
                {
                    if (m_pointerDown)
                    {
                        onDragBegin?.Invoke();
                        m_pointerDown = false;
                        return;
                    }
                    
                    m_pointerDragged = true;
                    onDrag?.Invoke();
                    
                    ChangePosition(delta.normalized.x * m_touchInputSensetivity * delta.magnitude,
                        delta.normalized.y * m_touchInputSensetivity * delta.magnitude, m_touchInputSensetivity);
                }
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                if (m_pointerDragged) onDragEnd?.Invoke();
                else if(!EventSystem.current.IsPointerOverGameObject()) onClick?.Invoke();

                m_pointerDown = false;
                m_pointerDragged = false;
            }

            m_lastTouchPosition = mousePosition;
        }

        private void AxisInput()
        {
            ChangePosition(UnityEngine.Input.GetAxisRaw("Horizontal"), UnityEngine.Input.GetAxisRaw("Vertical"),
                m_axisInputSensetivity);
        }

        private void ChangePosition(float x, float z, float amount)
        {
            m_newPosition += transform.right * (amount * -z);
            m_newPosition += transform.forward * (amount * x);
        }

        public static void Move(Vector3 position) => instance.m_newPosition = position;
        public void MoveImmediate(Vector3 position) => transform.position = position;

        public static void SetSize(float size) => instance.m_newSize = size;

        public static void RestoreSize() => instance.m_newSize = instance.m_lastSize;

        private void SelectTarget(CameraTarget cameraTarget)
        {
            Move(cameraTarget.transform.position);
            if (cameraTarget.overrideCameraSettings) OverrideProps(cameraTarget.cameraOverrideProps);
            else OverrideProps(m_defaultProps);
        }

        public void OverrideProps(CameraTarget.OverrideProps props)
        {
            m_newSize = props.ortoSize;
            m_camera.transform.localPosition = new Vector3(props.cameraX, props.cameraY, 0);
            transform.eulerAngles = new Vector3(0, props.cameraYAngle, 0);

            if (props.viewBox != null) m_bounds = props.viewBox.bounds;
            else m_bounds = null;
            
            m_lastSize = props.ortoSize;
        }

        public CameraTarget.OverrideProps GetProps()
        {
            return new CameraTarget.OverrideProps
            {
                cameraX = m_camera.transform.localPosition.x,
                cameraY = m_camera.transform.localPosition.y,
                cameraYAngle = transform.eulerAngles.y,
                ortoSize = m_camera.orthographicSize
            };
        }
        
        private static Coroutine shake;
        public static void Shake(float time, float amplitude)
        {
            //GUIController.LogDebug("Camera Shake t: " + time + " amp: " + amplitude);
            Vector3 originalPos = instance.transform.position;
            
            IEnumerator Shake()
            {
                for (float t = 0; t < time; t += Time.deltaTime)
                {
                    instance.transform.position = originalPos + Random.insideUnitSphere * amplitude;
                    yield return null;
                }
                
                instance.transform.position = originalPos;
                shake = null;
            }

            if(shake != null) instance.StopCoroutine(shake);
            shake = instance.StartCoroutine(Shake());
        }
        
        private void OnEnable() => CameraTarget.onTargetChange += SelectTarget;

        private void OnDisable() => CameraTarget.onTargetChange -= SelectTarget;
        
        private void OnDrawGizmos()
        {
            if(m_camera == null) return;
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, m_camera.transform.position);
        }
        
#if UNITY_EDITOR
        [ContextMenu("Copy props")]
        private void CopyOverrideProps_Context()
        {
            Undo.RecordObject(this, "Camera props copied");
            copiedProps = GetProps();
        }

        [ContextMenu("Paste props")]
        private void PasteOverrideProps_Context()
        {
            Undo.RecordObject(this, "Camera props pasted");
            OverrideProps(copiedProps);
        }
#endif
    }
}