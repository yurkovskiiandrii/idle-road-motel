﻿using System;
using Cinemachine;
using DG.Tweening;
using nickeltin.Singletons;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameCamera : MonoSingleton<GameCamera>
{
    private Camera _camera;
    public static Camera MainCamera => instance._camera;

    protected override void Awake()
    {
        base.Awake();
        _camera = GetComponent<Camera>();
    }
}