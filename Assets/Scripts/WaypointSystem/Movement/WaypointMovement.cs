﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace WaypointsSystem
{
    public class WaypointMovement : IMovement
    {
        public float WalkSpeed => _defaultSpeed;
        
        public Vector3 CurrentPosition { get; private set; }
        public MapWaypoint CurrentStartMapWaypoint { get; private set;}
        public MapWaypoint CurrentEndMapWaypoint { get; private set;}

        public bool IsMoving => _currentPath != null && _currentPath.Count > 0;

        private Stack<IWaypoint> _currentPath;
        private IWaypoint _currentStartWaypoint;
        private IWaypoint _currentEndWaypoint;
        protected Vector3 _currentTargetPos;
        private float _moveTimeTotal;
        private float _moveTimeCurrent;
        private Action<bool> _movementEndCallback;

        private float _defaultSpeed;

        private static WaypointSystem _waypointSystem;
        protected WaypointSystem WaypointSystem
        {
            get
            {
                if (_waypointSystem == null)
                {
                    _waypointSystem = new WaypointSystem();
                }

                return _waypointSystem;
            }
        }

        public WaypointMovement(Vector3 startPos, float speed)
        {
            _currentTargetPos = startPos;
            CurrentPosition = startPos;
            _defaultSpeed = speed;
        }

        public void Update(float deltaTime)
        {
            UpdateMovement(deltaTime);
            UpdateCurrentPosition(deltaTime);
        }

        public void GoTo(Vector3 destination, Action<bool> callback = null)
        {
            _movementEndCallback?.Invoke(false);
            _movementEndCallback = callback;

            var path = GetPath(destination);
            if (path == null)
            {
                _currentPath = null;
                InvokeMovementEndCallback();
                return;
            }
            Stop(false);

            if (path.Count > 1)
            {
                if ((MapWaypoint) path[path.Count - 1] == CurrentStartMapWaypoint &&
                    path[path.Count - 2] == _currentEndWaypoint ||
                    path[path.Count - 1] == _currentEndWaypoint &&
                    (MapWaypoint) path[path.Count - 2] == CurrentStartMapWaypoint ||
                    path[path.Count - 1] == _currentEndWaypoint &&
                    CurrentStartMapWaypoint == (MapWaypoint) _currentEndWaypoint)
                {
                    path.RemoveAt(path.Count - 1);
                }
            }

            _currentPath = new Stack<IWaypoint>();
            foreach (var waypoint in path)
                _currentPath.Push(waypoint);

            _currentPath.Push(new VirtualWaypoint(_currentTargetPos));
        }

        protected virtual List<IWaypoint> GetPath(Vector3 destination)
        {
            var path = WaypointSystem.GetPath(_currentTargetPos, destination);
            return path;
        }

        public void GoToDirect(Vector3 destination, Action<bool> callback = null)
        {
            _movementEndCallback?.Invoke(false);
            _movementEndCallback = callback;
            Stop(false);
            _currentPath = new Stack<IWaypoint>();
            _currentPath.Push(new VirtualWaypoint(destination));
            _currentPath.Push(new VirtualWaypoint(_currentTargetPos));
        }

        public void Teleport(Vector3 destination, bool callbackSuccess = false)
        {
            InvokeMovementEndCallback(callbackSuccess);
            Stop(false);
            CurrentPosition = destination;
            _currentTargetPos = destination;
        }

        public void TeleportWithoutInvoke(Vector3 destination)
        {
            CurrentPosition = destination;
            _currentTargetPos = destination;
        }

        private void UpdateMovement(float deltaTime)
        {
            if (_currentPath == null || _currentPath.Count == 0)
                return;

            if (_moveTimeCurrent < _moveTimeTotal)
            {
                _moveTimeCurrent += deltaTime;
                if (_moveTimeCurrent > _moveTimeTotal)
                    _moveTimeCurrent = _moveTimeTotal;

                _currentTargetPos = Vector3.Lerp(
                    _currentStartWaypoint.Position,
                    _currentPath.Peek().Position,
                    _moveTimeCurrent / _moveTimeTotal);
            }
            else
            {
                if (_currentPath.Count > 0)
                    _currentTargetPos = _currentPath.Peek().Position;

                _currentStartWaypoint = _currentPath.Pop();
                var mapWaypoint = _currentStartWaypoint as MapWaypoint;
                if (mapWaypoint != null)
                    CurrentStartMapWaypoint = mapWaypoint;

                if (_currentPath.Count == 0)
                {
                    Stop(false);
                    InvokeMovementEndCallback();
                }
                else
                {
                    _currentEndWaypoint = _currentPath.Peek();
                    mapWaypoint = _currentEndWaypoint as MapWaypoint;
                    if (mapWaypoint != null)
                        CurrentEndMapWaypoint = mapWaypoint;

                    _moveTimeCurrent = 0;
                    _moveTimeTotal = Vector3.Distance(_currentStartWaypoint.Position, _currentPath.Peek().Position) / _defaultSpeed;
                }
            }
        }

        private void UpdateCurrentPosition(float deltaTime)
        {
            if (Vector3.Distance(_currentTargetPos, CurrentPosition) > 1)
                CurrentPosition = _currentTargetPos;
            else
                CurrentPosition = Vector3.MoveTowards(CurrentPosition, _currentTargetPos, deltaTime * _defaultSpeed);
        }

        public void Stop(bool invokeCallback = true)
        {
            _currentPath = null;
            _moveTimeTotal = 0;
            _moveTimeCurrent = 0;
            if (invokeCallback)
                InvokeMovementEndCallback(false);
        }
        public void SetSpeedCustom(float speed)
        {
            _defaultSpeed = speed;
        }

        private void InvokeMovementEndCallback(bool success = true)
        {
            var tempCallback = _movementEndCallback;
            _movementEndCallback = null;
            tempCallback?.Invoke(success);
//            if (tempCallback == _movementEndCallback)
//                _movementEndCallback = null;
        }
    }
}
