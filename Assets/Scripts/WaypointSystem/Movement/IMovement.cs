﻿using System;
using UnityEngine;

namespace WaypointsSystem
{
    public interface IMovement
    {
        Vector3 CurrentPosition { get; }
        bool IsMoving { get; }
        void Update(float deltaTime);
        void GoTo(Vector3 destination, Action<bool> callback = null);
        void GoToDirect(Vector3 destination, Action<bool> callback = null);
        void Teleport(Vector3 destination, bool callbackSuccess = false);
        void TeleportWithoutInvoke(Vector3 destination);
        void Stop(bool invokeCallback = true);
        void SetSpeedCustom(float speed);
    }
}
