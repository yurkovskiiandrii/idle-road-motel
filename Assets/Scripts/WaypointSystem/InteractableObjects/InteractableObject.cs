﻿using System;
using nickeltin.Editor.Attributes;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace WaypointsSystem
{
    public class InteractableObject : MonoBehaviour
    {
        public string NPCId;
        public float GizmoSizeMultiplier = 1;
        public bool TeleportToRootOnInteract = true;
        public bool UseRootRotationOnInteract;
        public bool PlayTeleportFx = false;
        public Transform ExitPosition;
        public UnityEvent OnInteractionStart;
        public UnityEvent OnInteractionEnd;

        public string InteractionStartAnimationTrigger;
        public string InteractionMovingAnimationTrigger;

        [Tooltip("May be left blank. Required if interaction involves animation.")]
        public string InteractAnimationBoolParam;

        public bool isOccupied;

        public float interactionTime = 10;

        public void Occupy(bool isOccupied)
        {
            this.isOccupied = isOccupied;

            if (isOccupied)
            {
                OnInteractionStart?.Invoke();
            }
            else
            {
                OnInteractionEnd?.Invoke();
            }
        }

#if UNITY_EDITOR
        [Header("Preview")]
        public bool UsePreview;
        public GameObject PreviewObject;
        public AnimationClip PreviewAnimation;
        [Range(0,100)]public float animationPosition;
        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1,0.92f, 0.016f, 0.5f);
            var upPos = transform.position + Vector3.up * 1.5f* GizmoSizeMultiplier;
            Gizmos.DrawLine(transform.position, upPos);
            Gizmos.DrawSphere(upPos, 0.2f * GizmoSizeMultiplier);
            
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(upPos, upPos + transform.forward * 0.5f* GizmoSizeMultiplier);
            Gizmos.DrawCube(upPos + transform.forward * 0.5f* GizmoSizeMultiplier, Vector3.one * 0.15f* GizmoSizeMultiplier);
            
            if (ExitPosition != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position + Vector3.up * 0.3f, ExitPosition.position + Vector3.up * 0.3f* GizmoSizeMultiplier);
                Gizmos.DrawSphere(ExitPosition.position + Vector3.up * 0.3f, 0.12f* GizmoSizeMultiplier);
                Gizmos.color = Color.blue;
                var exitPosUp = ExitPosition.position + Vector3.up * 0.33f* GizmoSizeMultiplier;
                var exitPosForwardUp = ExitPosition.position + Vector3.up * 0.33f + ExitPosition.forward * 0.25f* GizmoSizeMultiplier;
                Gizmos.DrawLine(exitPosUp, exitPosForwardUp);
                Gizmos.DrawCube(exitPosForwardUp, Vector3.one * 0.07f* GizmoSizeMultiplier);
            }

            if (UsePreview)
            {
                if(Application.isPlaying)
                {
                    UsePreview = false;
                    return;
                }
                PreviewAnimation.SampleAnimation(PreviewObject, PreviewAnimation.length/100* animationPosition);
                PreviewObject.transform.position = transform.position;
                PreviewObject.transform.rotation = transform.rotation;
            }
        }

        private void OnValidate()
        {
            if (NPCId == "")
                name = "Empty";
            else
                name = NPCId +"_IO";
        }

#endif
    }
}
