﻿using UnityEngine;

namespace WaypointsSystem
{
    public interface IWaypoint
    {
        Vector3 Position { get; }
    }
}
