﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WaypointsSystem
{
    public enum MapWaypointType
    {
        Undefined = 0,
        Pets = 1,
        NeedFillEnum
    }

    public enum MapWaypointInteractionType
    {
        None = 0,
        NeedFillEnum
    }

    [ExecuteAlways]
    public class MapWaypoint : MonoBehaviour, IWaypoint
    {
        public MapWaypointType WaypointType = MapWaypointType.Undefined;
        public List<MapWaypoint> Neighbors = new List<MapWaypoint>();

        private float _waypointsSizeMultiplier=1;


        [HideInInspector] public Transform Transform;

        public Vector3 Position => transform.position;

        [HideInInspector] public MapWaypoint previous;
        [HideInInspector] public float heuristicDist;
        [HideInInspector] public List<string> buildingIds;
        

        private void OnDrawGizmos()
        {
            Gizmos.color = GetGizmosColor();
            Gizmos.DrawWireSphere(transform.position, 1.3f * _waypointsSizeMultiplier);
            Gizmos.DrawSphere(transform.position, 1* _waypointsSizeMultiplier);

            Gizmos.color = Color.red;// new Color(1,1,1,0.2f);
            foreach (var neighbor in Neighbors)
            {
                if (neighbor == null)
                    continue;
                Gizmos.DrawLine(
                    transform.position + Vector3.up * 0.05f, 
                    neighbor.transform.position + Vector3.up * 0.05f);
            }
        }

        public void SetSpheresSize(float mult)
        {
            _waypointsSizeMultiplier = mult;
        }
        private Color GetGizmosColor()
        {
            /*switch (WaypointType)
            {
                case MapWaypointType.GuestSpawn: return Color.blue;
                
            }*/
            //return new Color(1,1,1,0.5f);
            return Color.red;
        }

        private void OnValidate()
        {
            Validate();

            Transform = GetComponent<Transform>();
        }

        public void Validate()
        {
            Neighbors = Neighbors.Distinct().ToList();
            Neighbors.RemoveAll(waypoint => waypoint == null);
            foreach (var neighbor in Neighbors)
            {
                if (!neighbor.Neighbors.Contains(this))
                {
                    neighbor.Neighbors.Add(this);
                }
            }

            for (int i = 0; i < Neighbors.Count; i++)
            {
                if (Neighbors[i] == null)
                {
                    Neighbors.RemoveAt(i);
                }
            }
        }
        
#if UNITY_EDITOR
        private void OnDestroy()
        {
            var container = transform.GetComponentInParent<WaypointsContainer>();
            if (container != null)
                container.CacheWaypoints();
        }

        public void TypeChanged()
        {
            gameObject.name = $"Waypoint_{WaypointType}";
            
            var container = transform.GetComponentInParent<WaypointsContainer>();
            if (container != null)
                container.MakeReferences();
        }
        public void SetAsEntranceTo(string buildingId)
        {
            /*WaypointType = MapWaypointType.RoomEntrance;
            TypeChanged();
            
            var container = transform.GetComponentInParent<WaypointsContainer>();
            if (container != null)
                container.SetWaypointAsEntranceTo(this, buildingId);*/
        }
        
#endif

    }
}
