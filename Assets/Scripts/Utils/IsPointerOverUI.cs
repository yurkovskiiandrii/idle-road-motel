using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class IsPointerOverUI
{
    public static bool IsPointerOverUIObject(Vector3 mousePos)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = mousePos;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        return results.Count > 0;
    }

    public static List<GameObject> GetUIElements(Vector3 mousePos)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = mousePos;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        var resultsGO = new List<GameObject>();

        for (int i = 0; i < results.Count; ++i)
            resultsGO.Add(results[i].gameObject);

        //UnityEngine.Debug.Log(string.Join("   ", resultsGO));

        return resultsGO;
    }
}
