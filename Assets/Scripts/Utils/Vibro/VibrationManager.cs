using UnityEngine;

namespace Managers
{
    public static class VibrationManager
    {
        private const string VibrationKey = "Vibration";
        public static bool IsVibrationEnable { get; private set; }

        static VibrationManager()
        {
            IsVibrationEnable = PlayerPrefs.GetInt(VibrationKey, 1) == 1;
        }

        public static void SetVibrationState(bool state)
        {
            PlayerPrefs.SetInt(VibrationKey, state ? 1 : 0);
            IsVibrationEnable = state;
        }

        public static void Vibrate()
        {
            if (!IsVibrationEnable)
                return;

            Handheld.Vibrate();
        }

        public static void HapticVibrate()
        {
            if (!IsVibrationEnable)
                return;
#if UNITY_IOS && !UNITY_EDITOR
            IOSNative.StartHapticFeedback(HapticFeedbackTypes.LIGHT);
#elif UNITY_ANDROID && !UNITY_EDITOR
            AndroidManager.HapticFeedbackCall();
#endif
        }
    }
}