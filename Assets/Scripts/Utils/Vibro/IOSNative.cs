﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Managers
{
    #if UNITY_IOS
    public class IOSNative 
    {
        [DllImport("__Internal")]
        private static extern void HapticFeedback (int type);

        [DllImport("__Internal")]
        private static extern float NativeTime (int type);
        
        public static void StartHapticFeedback(HapticFeedbackTypes type)
        {
            HapticFeedback ((int)type);
        }
        
            
        public static float GetNativeTime(int type)
        {
            return NativeTime (type);
        }
    }
#endif
    public enum HapticFeedbackTypes
    {
        LIGHT,
        MEDIUM,
        HEAVY,
        SOFT //IOS 13
    }
}