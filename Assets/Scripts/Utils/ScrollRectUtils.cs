﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class ScrollRectUtils
{
    public enum ScrollRectCentering
    {
        Top = 0,
        Center = 1,
        Bottom = 2,
    }

    private static RectTransform maskTransform;
    private static RectTransform mContent;

    public static Vector2 GetRectNormalizedPosition(RectTransform target, ScrollRect scroll, RectTransform maskRect, ScrollRectCentering centering, Vector3 shift = new Vector3())
    {
        var scrollTransfrom = scroll.GetComponent<RectTransform>();
        mContent = scroll.content;
        maskTransform = maskRect;

        var itemCenterPositionInScroll = GetWorldPointInWidget(scrollTransfrom, GetWidgetWorldPoint(target, centering, shift));
        var targetPositionInScroll = GetWorldPointInWidget(scrollTransfrom, GetWidgetWorldPoint(maskTransform, centering));
        var difference = targetPositionInScroll - itemCenterPositionInScroll;

        difference.z = 0f;

        if (!scroll.horizontal)
        {
            difference.x = 0f;
        }
        if (!scroll.vertical)
        {
            difference.y = 0f;
        }

        var normalizedDifference = new Vector2(
            difference.x / (mContent.rect.size.x - scrollTransfrom.rect.size.x),
            difference.y / (mContent.rect.size.y - scrollTransfrom.rect.size.y));

        var newNormalizedPosition = scroll.normalizedPosition - normalizedDifference;

        if (scroll.movementType != ScrollRect.MovementType.Unrestricted)
        {
            newNormalizedPosition.x = Mathf.Clamp01(newNormalizedPosition.x);
            newNormalizedPosition.y = Mathf.Clamp01(newNormalizedPosition.y);
        }

        return newNormalizedPosition;
    }

    private static Vector3 GetWidgetWorldPoint(RectTransform target, ScrollRectCentering centering, Vector3 offset = new Vector3())
    {
        var pivotOffset = new Vector3(
            (GetCenteringValue(centering) - target.pivot.x) * target.rect.size.x,
            (GetCenteringValue(centering) - target.pivot.y) * target.rect.size.y,
            0f);
        var localPosition = target.localPosition + pivotOffset + offset;

        return target.parent.TransformPoint(localPosition);
    }

    private static Vector3 GetWorldPointInWidget(RectTransform target, Vector3 worldPoint)
    {
        return target.InverseTransformPoint(worldPoint);
    }

    private static float GetCenteringValue(ScrollRectCentering centering)
    {
        switch (centering)
        {
            case ScrollRectCentering.Top: return 1;
            case ScrollRectCentering.Center: return 0.5f;
            case ScrollRectCentering.Bottom: return 0f;
            default: return 0f;
        }
    }
}