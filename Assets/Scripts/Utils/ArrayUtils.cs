﻿using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Outloud.Utils
{
    public static class ArrayUtils
    {
        public static T GetRandom<T>(this IList<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }
        public static void InsertToIndex<T>(this List<T> list,  int index, T item)
        {
            try
            {
                list[index] = item;
            }
            catch
            {
                list.Add(default);
                InsertToIndex(list,index,item);
            }
        }
    }
}
