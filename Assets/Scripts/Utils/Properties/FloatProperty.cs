﻿namespace Outloud.Utils.Properties
{
    /// <summary>
    /// Property class that store <c>float</c> value and raise an event if it has changed
    /// </summary>
    public class FloatProperty : BaseProperty<float>
    {
        public static implicit operator float(FloatProperty d) => d.Value;
        public static explicit operator FloatProperty(float d) => new FloatProperty(){Value = d};
        
        public static FloatProperty operator +(FloatProperty i, FloatProperty j)
        {
            i.Value += j.Value;
            return i;
        }
       
        public static FloatProperty operator -(FloatProperty i, FloatProperty j)
        {
            i.Value += j.Value;
            return i;
        }
        
        public static FloatProperty operator +(FloatProperty i, float j)
        {
            i.Value += j;
            return i;
        }
        
        public static FloatProperty operator -(FloatProperty i, float j)
        { 
            i.Value = i.Value - j;
            return i;
        }
    }
}