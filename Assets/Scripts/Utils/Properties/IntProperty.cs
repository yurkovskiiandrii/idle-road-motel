﻿using UnityEngine;

namespace Outloud.Utils.Properties
{
    /// <summary>
    /// Property class that store <c>int</c> value and raise an event if it has changed
    /// </summary>
    public class IntProperty : BaseProperty<int>
    {
        public static implicit operator int(IntProperty d) => d.Value;
        public static explicit operator IntProperty(int d) => new IntProperty(){Value = d};

        public static IntProperty operator +(IntProperty i, IntProperty j)
        {
            i.Value += j.Value;
            return i;
        }

        public static IntProperty operator -(IntProperty i, IntProperty j)
        {
            i.Value -= j.Value;
            return i;
        }

        public static IntProperty operator +(IntProperty i, int j)
        {
            i.Value += j;
            return i;
        }
        
        public static IntProperty operator -(IntProperty i, int j)
        { 
            i.Value = i.Value - j;
            return i;
        }
    }
}