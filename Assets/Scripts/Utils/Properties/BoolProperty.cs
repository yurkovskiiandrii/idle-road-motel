namespace Outloud.Utils.Properties
{
    /// <summary>
    /// Property class that store <c>bool</c> value and raise an event if it has changed
    /// </summary>
    public class BoolProperty : BaseProperty<bool>
    {
        public static implicit operator bool(BoolProperty d) => d.Value;
        public static explicit operator BoolProperty(bool d) => new BoolProperty(){Value = d};
        
    }
}