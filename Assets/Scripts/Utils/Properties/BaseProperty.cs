using System;

namespace Outloud.Utils.Properties
{
    /// <summary>
    /// Base property class that store value and raise an event if value has changed
    /// </summary>
    /// <typeparam name="T">Generic value type</typeparam>
    public class BaseProperty<T>
    {
        /// <summary>
        /// Event that is raised when value has changed
        /// </summary>
        public event Action<T> OnValueChanged
        {
            add { _updated += value; }
            remove { _updated -= value; }
        }

        /// <summary>
        /// Event that is raised when value has changed AND during first subscription OnValueChange event
        /// </summary>
        public event Action<T> OnValueChangedWithRaise
        {
            add
            {
                _updated += value;
                value?.Invoke(_value);
            }
            remove { _updated -= value; }
        }

        private event Action<T> _updated;

        private T _value = default;

        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _updated?.Invoke(_value);
            }
        }

        public static implicit operator T(BaseProperty<T> d) => d.Value;
        public static explicit operator BaseProperty<T>(T d) => new BaseProperty<T>(){Value = d};
    }
}