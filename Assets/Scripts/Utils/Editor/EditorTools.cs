using System.IO;
using UnityEditor;
using UnityEngine;

public class EditorTools : Editor
{
    [MenuItem("Tools/Player Prefs Delete All", priority = 1)]
    public static void DeleteAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        var path = Path.Combine(Application.persistentDataPath, "saves");

        if (!Directory.Exists(path))
            return;

        DirectoryInfo directoryInfo = new DirectoryInfo(path);
        foreach (FileInfo file in directoryInfo.EnumerateFiles())
        {
            file.Delete();
        }
    }
}