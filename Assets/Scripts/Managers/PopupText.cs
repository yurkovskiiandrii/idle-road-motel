﻿using FXs;
using nickeltin.ObjectPooling;
using Other;
using UnityEngine;


namespace UI
{
    [CreateAssetMenu(menuName = "Managers/PopupText")]
    public class PopupText : EffectsManagerBase<PopupText>
    {

        [SerializeField] private PopupTextObject _popUpPrefab;
        [SerializeField] private PopupWorldTextObject _worldpopUpPrefab;

        private static Pool<PopupTextObject> PopupTextPool;
        private static Pool<PopupWorldTextObject> PopupWorldTextPool;
        

        public override bool Initialize()
        {
            if (base.Initialize())
            {
                ResetUseTime();
                GUIController.onLoad += () =>
                {
                    PopupTextPool = new Pool<PopupTextObject>(_popUpPrefab, PopUpCanvas.Canvas.transform, 20);
                    PopupWorldTextPool = new Pool<PopupWorldTextObject>(_worldpopUpPrefab, WorldPopUpCanvas.Canvas.transform, 20);
                };
                
                return true;
            }

            return false;
        }
        private static void Popup(Vector3 worldPosition, float number, bool isCritical = false)
        {
            if(Instance.Disallowed) return;
            Instance.SetUseTime();
            var screenPoint = RectTransformUtility.WorldToScreenPoint(GameCamera.MainCamera, worldPosition);
            var obj = PopupTextPool.Get();
            obj.Appear(screenPoint, number,() => { PopupTextPool?.Add(obj); }, isCritical);
            
        }
        public static void Popup(Damage dmg)
        {
           Popup(dmg.Position, dmg.Value, dmg.IsCritical);
        }

        public static void PopupWorld(IPopUpHolder popUpHolder, Vector3 worldPosition, float number, bool isCritical = false)
        {
            var obj = PopupWorldTextPool.Get();
            obj.Appear(popUpHolder.PopUpWorldIconsScheme, worldPosition, number,() => { PopupWorldTextPool.Add(obj); }, isCritical);
        }
        public static void PopupWorld(IPopUpHolder marker,Damage dmg)
        {
            PopupWorld(marker,dmg.Position, dmg.Value, dmg.IsCritical);
        }
    }
}