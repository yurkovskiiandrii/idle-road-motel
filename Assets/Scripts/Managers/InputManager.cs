using nickeltin.Singletons;
using UnityEngine;
using UnityEngine.Serialization;

namespace Managers
{
    [CreateAssetMenu(menuName = "Managers/Input")]
    public class InputManager : SOSingleton<InputManager>
    {
        public Inputs Inputs;
        [SerializeField] private bool _cameraMoveBlocked;

        public static bool CameraMoveBlocked
        {
            get => Instance._cameraMoveBlocked;
            set => Instance._cameraMoveBlocked = value;
        } 
       

        public override bool Initialize()
        {
            if (base.Initialize())
            {
                Inputs = new Inputs();
                Inputs.Enable();
                return true;
            }

            return false;
        }
        
        
    }
}
