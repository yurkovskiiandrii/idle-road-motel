namespace UI
{
    public interface IPopUpHolder
    {
        public PopUpWorldIconsScheme PopUpWorldIconsScheme { get;}
    }
}