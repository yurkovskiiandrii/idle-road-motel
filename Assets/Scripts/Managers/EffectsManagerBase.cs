﻿using nickeltin.Singletons;
using UnityEngine;

namespace FXs
{
    public abstract class EffectsManagerBase<T> : SOSingleton<T> where T : SOSingleton<T>
    {
        [SerializeField, Range(0,2)] private float m_minUseInterval = 0.02f;
        private float m_lastUseTime = 0;
        protected bool Disallowed => Time.time - m_lastUseTime < m_minUseInterval;
        protected void SetUseTime() => m_lastUseTime = Time.time;
        protected void ResetUseTime() => m_lastUseTime = 0;
    }
}