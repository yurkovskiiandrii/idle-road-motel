﻿using nickeltin.Editor.Attributes;
using nickeltin.GameData.DataObjects;
using nickeltin.Singletons;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MineManager : MonoSingleton<MineManager>
{
   

    [Header("Layer destruction settings")]
    [SerializeField] [Range(20, 1000)] private int m_layerDestructionAsyncRate = 10;
    [SerializeField] [Range(1, 10000)] private int m_layerDestructionMaxIterations = 100;
    [SerializeField] [Range(0, 1)] private float m_layerMinIntervalBetweenCollisions = 0.02f;

    [Scene] public string MineScene;
   
    public static int LayerDestructionAsyncRate => instance.m_layerDestructionAsyncRate;
    public static int LayerDestructionMaxIterations => instance.m_layerDestructionMaxIterations;
    public static float LayerMinIntervalBetweenCollisions => instance.m_layerMinIntervalBetweenCollisions;
    private void Start()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(MineScene));
    }

    

    // private void OnGUI()
    // {
    //     if (GUI.Button(new Rect(100, 100, 200, 150), "Load Village"))
    //     {
    //         m_loadVillage = true;
    //         LoadVillage();
    //     }
    // }
}
