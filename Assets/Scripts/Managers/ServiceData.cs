﻿using System;
using System.Collections.Generic;
using nickeltin.Singletons;
using NPCs;
using UnityEngine;

[CreateAssetMenu(menuName = "Managers/ServiceLocator")]
public class ServiceData : SOSingleton<ServiceData>
{
    public List<DataFile> Data;
    public override bool Initialize()
    {
        if (base.Initialize())
        {
            return true;
        }

        return false;
    }
}
