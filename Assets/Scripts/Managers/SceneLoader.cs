﻿using System;
using System.Collections;
using System.Collections.Generic;
using nickeltin.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public static class SceneLoader
{
    private static AsyncOperation loadingOperation;
    private static readonly List<string> unloadingScenes = new List<string>();
    public static event Action<string> onSceneLoad;
    public static event Action<string> onSceneUnload;
    public static event Action<string> beforeSceneUnload;
    public static event Action beforeSceneReload;

    public static readonly List<string> loadedScenes = new List<string>();

    public static string currentScene;
    
    private class SceneLoaderInstance : MonoBehaviour { }

    private static readonly SceneLoaderInstance instance;

    static SceneLoader()
    {
        instance = new GameObject("scene_loader").AddComponent<SceneLoaderInstance>();
        Object.DontDestroyOnLoad(instance);
    }

    public static void FakeSceneLoad(string sceneName) => onSceneLoad?.Invoke(sceneName);

    public static void FakeSceneUnload(string sceneName, float delay = 0)
    {
        beforeSceneUnload.DelayedInvoke(sceneName, delay, instance);
        onSceneUnload.DelayedInvoke(sceneName, delay, instance);
    }

    public static void LoadScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single, Action onSceneLoaded = null, bool invokeUpdate = true)
    {
        if (loadingOperation == null && !unloadingScenes.Contains(sceneName))
        {
            loadingOperation = SceneManager.LoadSceneAsync(sceneName, mode);
            loadingOperation.completed += operation =>
            {
                loadingOperation = null;
                onSceneLoaded?.Invoke();
                if (invokeUpdate)
                {
                    loadedScenes.Add(sceneName);
                    onSceneLoad?.Invoke(sceneName);
                }
            };
        }
    }

    public static void UnloadScene(string sceneName, Action onSceneUnloaded = null, float delay = 0, bool invokeUpdate = true)
    {
        if(unloadingScenes.Contains(sceneName)) return;

        unloadingScenes.Add(sceneName);
        if(invokeUpdate) beforeSceneUnload?.Invoke(sceneName);
        IEnumerator DelayedCall()
        {
            yield return new WaitForSeconds(delay);
            AsyncOperation unloadingOperation = SceneManager.UnloadSceneAsync(sceneName);
            unloadingOperation.completed += operation =>
            {
                unloadingScenes.Remove(sceneName);
                onSceneUnloaded?.Invoke();
                if (invokeUpdate)
                {
                    loadedScenes.Remove(sceneName);
                    onSceneUnload?.Invoke(sceneName);
                }
            };
        }

        instance.StartCoroutine(DelayedCall());
    }

    public static void Reload(bool invokeCallback = true)
    {
        if (invokeCallback) beforeSceneReload?.Invoke();
        LoadScene(SceneManager.GetActiveScene().name);
    }
}
