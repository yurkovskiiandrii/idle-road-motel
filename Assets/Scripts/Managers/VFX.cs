﻿using GameData.DataObjects;
using UnityEngine;

namespace FXs
{
    [CreateAssetMenu(menuName = "Managers/VFX")]
    public class VFX : EffectsManagerBase<VFX>
    {
        [SerializeField] private ParticleSystem m_layerCollision;
        [SerializeField] private ParticleSystem m_criticalLayerCollision;
        public ParticleSystem m_tapVfx;
        public ParticleSystem m_superTapVfx;
        public GameObject m_RedGem, m_BlueGem, m_GreenGem;

        public static ParticleSystem LayerCollision => Instance.m_layerCollision;

        public static GameObject RedGem => Instance.m_RedGem;

        public static GameObject BlueGem => Instance.m_BlueGem;

        public static GameObject GreenGem => Instance.m_GreenGem;
        public static ParticleSystem CriticalLayerCollision => Instance.m_criticalLayerCollision;

        public override bool Initialize()
        {
            if (base.Initialize())
            {
                ResetUseTime();
                return true;
            }

            return false;
        }

        public static void PlayDefault(ParticleSystem particles) => Play(particles);

        public static void Play(ParticleSystem particles, Vector3? position = null, Color? color = null, bool forced = false)
        {
            if (!forced)
            {
                if(Instance.Disallowed) return;
                Instance.SetUseTime();
            }
            
            Play_Internal(particles, position, color);
        }

        private static void Play_Internal(ParticleSystem particles, Vector3? position, Color? color)
        {
            if (!particles.gameObject.activeInHierarchy)
            {
                particles = Instantiate(particles);
                Destroy(particles.gameObject, 5);
            }

            if (color.HasValue)
            {
                var mainModule = particles.main;
                mainModule.startColor = (Color)color;
            }

            if (position.HasValue)
            {
                particles.transform.position = (Vector3)position;
            }
            particles.Play();
        }
    }
}