﻿using CameraControlling;
using Destructables;
using Other;
using UnityEngine;
using UnityEngine.Events;
using FXs;
using Managers;
using nickeltin.Editor.Attributes;
using nickeltin.Extensions;
using nickeltin.GameData.DataObjects;
using Source;
using UI;
using UI.Village;
using UnityEngine.EventSystems;
using Voxelization;

public class Clicker : MonoBehaviour
{
    [Header("Raycast")] 
    [SerializeField] private LayerMask m_mask;
    [SerializeField] private float m_range = 200;
    
    [Header("Resources")]
    [SerializeField] private NumberObject m_layersDestroyed;
    [SerializeField] private NumberObject m_superTapCurrentValue;
    [SerializeField] private BoolObject m_isSupertapEverUsed;

    [Header("Super Tap")] 
    [SerializeField] private float m_cameraShakeDuration = 0.1f;
    [SerializeField] private float m_cameraShakeAmplitude = 0.2f;
    [SerializeField] private float m_profitMultiplier = 1;
    [SerializeField] private Damage.Stats m_baseDamage;
    [SerializeField] private float m_baseMaxValue;
    [SerializeField] private float m_damageIncreasePerLayer = 5;
    [SerializeField] private float m_maxValueIncreasePerLayer = 5;
    [SerializeField] private UnityEvent m_onSuperTap;
    
    [Header("Supertap current values")]
    [SerializeField, ReadOnly] private Damage.Stats m_currentDamage;
    [SerializeField, ReadOnly] private float m_currentMaxValue;

    [Header("Tap properties")] 
    [SerializeField] private float m_doubleTapInterval = 0.2f;
    [SerializeField] private float m_continiousClickInterval = 0.2f;

    private bool m_superTapFull => m_superTapCurrentValue.Value >= m_currentMaxValue;
    private float m_lastClickCounter = 0;
    private float m_lastClickTime = 0;
    
    private void Start()
    {
        if (Bootstrap.DoNewGame)
            m_superTapCurrentValue.Value = 0;
        m_superTapCurrentValue.SetMinMax(0, m_baseMaxValue);
        m_currentDamage = new Damage.Stats()
        {
            critChance = m_baseDamage.critChance, 
            critMultiplier = m_baseDamage.critMultiplier
        };
        UpdateSuperTapValues(m_layersDestroyed.Value);
        
        InputManager.GetInstance().Inputs.Player.Click.performed += x => SingleClick();
    }

    private void UpdateSuperTapValues(float layersDestroyed)
    {
        m_currentDamage.value = m_baseDamage.value * (1 + m_damageIncreasePerLayer * layersDestroyed);
        m_currentMaxValue = m_baseMaxValue * (1 + m_maxValueIncreasePerLayer * layersDestroyed);
        m_superTapCurrentValue.SetMinMax(0, m_currentMaxValue);
    }

    public void Disable() => gameObject.SetActive(false);

    public void Enable() => gameObject.SetActive(true);

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;
        
        if (UnityEngine.Input.GetMouseButtonDown(0)) SingleClick();
        else if (UnityEngine.Input.GetMouseButton(0)) ContiniousClick();
        else if (UnityEngine.Input.GetMouseButtonUp(0)) m_lastClickCounter = 0;
    }
    
    private void ContiniousClick()
    {
        if (m_lastClickCounter > m_continiousClickInterval)
        {
            if (Raycast(UnityEngine.Input.mousePosition, out var hit))
            {
                if (hit.collider.TryGetComponent<IDamageable>(out var _))
                {
                    SetBallsDestination(hit.point);
                }
            }
            m_lastClickCounter = 0;
        }

        m_lastClickCounter += Time.deltaTime;
    }

    private void SingleClick()
    {
        if (Raycast(UnityEngine.Input.mousePosition, out var hit))
        {
            if (hit.collider.TryGetComponent<Chunk>(out var chunk))
            {
                if (SuperTap(chunk, hit.point)) return;
                SetBallsDestination(hit.point);
            }
            else if (hit.transform.TryGetComponent<Interactable>(out var interactable)) interactable.Interact(hit.point);
        }
        m_lastClickCounter = 0;
        m_lastClickTime = Time.time;
    }

    
    private bool Raycast(Vector2 pos, out RaycastHit hit)
    {
        Ray ray = GameCamera.MainCamera.ScreenPointToRay(pos);
        Debug.DrawRay(ray.origin, ray.direction * m_range, Color.red);
        return CameraExt.RaycastFromScreenpoint(ray, out hit, m_range, m_mask, true);
    }

    private void SetBallsDestination(Vector3 point)
    {
        VFX.Play(VFX.GetInstance().m_tapVfx, point);
        MinerWorld.SetDestination(point);
        //TODO: Vibration
        Vibration.Vibrate(Vibration.VibrationTarget.Tap);
    }

    private bool SuperTap(IDamageable damageable, Vector3 point)
    {
        if (m_superTapFull && Time.time - m_lastClickTime < m_doubleTapInterval)
        {
            if (damageable.TakeDamage(new Damage(m_currentDamage, point, Vector3.zero, m_profitMultiplier)))
            {
                if (!m_isSupertapEverUsed.Value) m_isSupertapEverUsed.Value = true;
                
                //GameCamera.Shake(m_cameraShakeDuration, m_cameraShakeAmplitude);
                
                m_superTapCurrentValue.Value = 0;
                
                m_onSuperTap.Invoke();
                
                if (damageable is Chunk chunk)
                {
                    var layer = chunk.transform.parent.GetComponentInParent<GroundLayer>();
                    if (layer != null)
                    {
                        VFX.Play(VFX.GetInstance().m_superTapVfx, point, layer.Color, true);
                    }
                }
                
                Vibration.VibrateForSure(Vibration.VibrationTarget.SuperTap);
                
                return true;
            }
        }
        return false;
    }
}
