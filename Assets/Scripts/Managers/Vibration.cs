﻿using System;
using GameData.DataObjects;
using MoreMountains.NiceVibrations;
using nickeltin.GameData.DataObjects;
using UI;
using UnityEngine;
using UnityEngine.Android;

namespace FXs
{
    [CreateAssetMenu(menuName = "Managers/Vibration")]
    public class Vibration : EffectsManagerBase<Vibration>
    {
        public enum VibrationTarget 
        {
            SuperTap, Tap, CriticalDamage, BallCollision
        }
        
        public class VibrationInstance : MonoBehaviour { }

        private static VibrationInstance worldInstance;
        public override bool Initialize()
        {
            if (base.Initialize())
            {
                worldInstance = new GameObject("vibration").AddComponent<VibrationInstance>();
                DontDestroyOnLoad(worldInstance.gameObject);
                ResetUseTime();
                Handheld.Vibrate();
                return true;
            }

            return false;
        }
        
        [SerializeField] private BoolObject m_enabled;
        [Space]
        [SerializeField] private HapticTypes m_superTap;
        [SerializeField] private HapticTypes m_tap;
        [SerializeField] private HapticTypes m_crit;
        [SerializeField] private HapticTypes m_ballCollision;

        private bool m_enabledLocal = true;

        //public static void sVibrate(VibrationTarget targetedType) => Instance.Vibrate(targetedType);

        public static void Vibrate(VibrationTarget targetedType)
        {
            if (!Instance.m_enabled.Value || !Instance.m_enabledLocal) return;
            
            if(Instance.Disallowed) return;
            Instance.SetUseTime();
        
            VibrateForSure(targetedType);
        }

        public static void VibrateForSure(VibrationTarget targetedType)
        {
            if (!Instance.m_enabled.Value || !Instance.m_enabledLocal) return;
            
            try
            {
                switch (targetedType)
                {
                    case VibrationTarget.SuperTap:
                        MMVibrationManager.Haptic(Instance.m_superTap, false, true, worldInstance);
                        break;
                    case VibrationTarget.Tap:
                        MMVibrationManager.Haptic(Instance.m_tap, false, true, worldInstance);
                        break;
                    case VibrationTarget.CriticalDamage:
                        MMVibrationManager.Haptic(Instance.m_crit, false, true, worldInstance);
                        break;
                    case VibrationTarget.BallCollision:
                        MMVibrationManager.Haptic(Instance.m_ballCollision, false, true, worldInstance);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(targetedType), targetedType, null);
                }
            }
            catch
            {
                // ignored
            }
        }

        public void SetActive(bool state) => m_enabledLocal = state;
    }
}