﻿using System;
using System.Collections;
using System.Collections.Generic;
using Managers.Audio;
using nickeltin.Editor.Attributes;
using nickeltin.GameData.DataObjects;
using nickeltin.ObjectPooling;
using nickeltin.Singletons;
using UnityEngine;
using UnityEngine.Serialization;

namespace FXs
{
    [CreateAssetMenu(menuName = "Managers/Audio")]
    public class Audio : EffectsManagerBase<Audio>
    {
        public enum Type
        {
            Sound, UISound, BackgroundMusic, Ambient
        }
        
        public class AudioInstance : MonoBehaviour
        {
            public AudioSource BackgroundMusic { get; private set; }
            public AudioSource UISounds { get; private set; }
            public AudioSource Sounds { get; private set; }
            public AudioSource Ambient { get; private set; }

            private NumberObject m_musicVolume;
            private NumberObject m_soundsVolume;

            private void OnApplicationQuit()
            {
                m_musicVolume.onValueChanged -= OnMusicVolumeChanged;
                m_soundsVolume.onValueChanged -= OnSoundsVoulmeChanged;
            }

            private void OnMusicVolumeChanged(float f) => BackgroundMusic.volume = f;

            private void OnSoundsVoulmeChanged(float f)
            {
                Sounds.volume = f;
                UISounds.volume = f;
                Ambient.volume = f;
            }

            public AudioInstance Initialize(NumberObject musicVolume, NumberObject soundVolume)
            {
                DontDestroyOnLoad(gameObject);

                m_musicVolume = musicVolume;
                m_soundsVolume = soundVolume;
                
                BackgroundMusic = gameObject.AddComponent<AudioSource>();
                UISounds = gameObject.AddComponent<AudioSource>();
                Sounds = gameObject.AddComponent<AudioSource>();
                Ambient = gameObject.AddComponent<AudioSource>();

                BackgroundMusic.volume = musicVolume.Value;
                UISounds.volume = Sounds.volume = Ambient.volume = soundVolume.Value;

                musicVolume.onValueChanged += OnMusicVolumeChanged;
                soundVolume.onValueChanged += OnSoundsVoulmeChanged;

                return this;
            } 
        }
        
        [Serializable]
        public struct AudioSequence
        {
            [SerializeField] private List<AudioEvent> _audioEvents;
            public IReadOnlyList<AudioEvent> AudioEvents => _audioEvents;
            
            public AudioEvent this[int i] => AudioEvents[i];
        }
        
        [Serializable]
        public struct AudioSequences
        {
            [SerializeField] private List<AudioSequence> m_sequences;
            public IReadOnlyList<AudioSequence> Sequences => m_sequences;

            public AudioSequence this[int i] => Sequences[i];
        }

        [SerializeField, Scene] private string m_mineScene;
        [SerializeField, Scene] private string m_villageScene;
        
        [Header("Settings")]
        [SerializeField] private NumberObject m_sounds;

        public NumberObject SoundsVolume => m_sounds;
        [SerializeField] private NumberObject m_music;
        public float MusicVolume => m_music;

        [Header("Music")]
        [SerializeField] private AudioSequences m_sequences;
        
        [Header("Entries")]
        [SerializeField] private AudioClip m_anyButton;
        

        private AudioInstance worldInstance;

        private Coroutine m_bgMusicLoop;
        private Coroutine m_bgAmbientLoop;
        
        public override bool Initialize()
        {
            if (base.Initialize())
            {
                worldInstance = new GameObject("audio").AddComponent<AudioInstance>()
                    .Initialize(m_music, m_sounds);
                ResetUseTime();
                return true;
            }

            return false;
        }
        
        public static void PlayAnyButton() => Instance.PlaySound(Instance.m_anyButton);

        public void PlaySoundOnGameScene(AudioEvent audioEvent)
        {
            if (SceneLoader.currentScene == m_mineScene)
            {
                PlaySound(audioEvent);
            }
        }
        
        public void PlaySoundOnVillageScene(AudioEvent audioEvent)
        {
            if (SceneLoader.currentScene == m_villageScene)
            {
                PlaySound(audioEvent);
            }
        }

        public void PlaySound(AudioEvent audioEvent)
        {
            Play(audioEvent.clip, Type.Sound, audioEvent.pitch, audioEvent.volume);
        }
        public void PlaySoundForSure(AudioEvent audioEvent)
        {
            PlayForSure(audioEvent.clip, Type.Sound, audioEvent.pitch, audioEvent.volume);
        }

        public void PlaySound(AudioClip clip) => Play(clip, Type.Sound);
        public void PlaySoundForSure(AudioClip clip) => PlayForSure(clip, Type.Sound);

        public void PlayUISoundForSure(AudioClip clip) => PlayForSure(clip, Type.UISound);
        public void PlayUISound(AudioClip clip) => Play(clip, Type.UISound);

        public void PlayBackgroundMusicOneShot(AudioClip clip)
        {
            StopBackgroundMusic();
            Play(clip, Type.BackgroundMusic);
        }

        public void StartAmbient(int sequenceId)
        {
            if (sequenceId < 0 || sequenceId >= m_sequences.Sequences.Count)
            {
                Debug.LogError($"Sequence ID {sequenceId} is out of range, insert ID thata matches your Sequeneces list");
                return;
            }
            
            StopAmbient();
            
            IEnumerator Loop()
            {
                for (int i = 0; i < m_sequences[sequenceId].AudioEvents.Count; i++)
                {
                    var clip = m_sequences[sequenceId][i].clip;
                    PlayForSure(clip, Type.Ambient);
                    yield return new WaitForSeconds(clip.length);
                }
            
                m_bgAmbientLoop = worldInstance.StartCoroutine(Loop());
            }
            
            m_bgAmbientLoop = worldInstance.StartCoroutine(Loop());
        }
        
        
        public void StartBackgroundMusic(int sequenceId)
        {
            if (sequenceId < 0 || sequenceId >= m_sequences.Sequences.Count)
            {
                Debug.LogError($"Sequence ID {sequenceId} is out of range, insert ID thata matches your Sequeneces list");
                return;
            }
           if (worldInstance.BackgroundMusic.isPlaying == false)
            {
                StopBackgroundMusic();

                IEnumerator Loop()
                {
                    for (int i = 0; i < m_sequences[sequenceId].AudioEvents.Count; i++)
                    {
                   // int m  = UnityEngine.Random.Range(0, m_sequences[sequenceId][i].);
                    var clip = m_sequences[sequenceId][i].clip;
                        PlayForSure(clip, Type.BackgroundMusic);
                    //Debug.Log(clip.name);
                        yield return new WaitForSeconds(clip.length);
                    }

                    m_bgMusicLoop = worldInstance.StartCoroutine(Loop());
                }

                m_bgMusicLoop = worldInstance.StartCoroutine(Loop());
            }
        }

        public void StopBackgroundMusic()
        {
            if(m_bgMusicLoop != null) worldInstance.StopCoroutine(m_bgMusicLoop);
            worldInstance.BackgroundMusic.Stop();
        }

        public void MuteAmbient()
        {
            worldInstance.Ambient.volume = 0;
        }

        public void UnmuteAmbient()
        {
            worldInstance.Ambient.volume = m_music.Value;
        }
        
        public void StopAmbient()
        {
            if(m_bgAmbientLoop != null) worldInstance.StopCoroutine(m_bgAmbientLoop);
            worldInstance.Ambient.Stop();
        }
        
        private void Play(AudioClip audio, Type type, float pitch = 1, float volume = 1)
        {
            if (Disallowed) return;
            SetUseTime();
            
            PlayForSure(audio, type, pitch, volume);
        }

        private void PlayForSure(AudioClip audio, Type type, float pitch = 1, float volume = 1)
        {
            switch (type)
            {
                case Type.Sound:
                    worldInstance.Sounds.pitch = pitch;
                    worldInstance.Sounds.PlayOneShot(audio, volume);
                    break;
                case Type.UISound:
                    worldInstance.UISounds.pitch = pitch;
                    worldInstance.UISounds.PlayOneShot(audio, volume);
                    break;
                case Type.BackgroundMusic:
                    worldInstance.BackgroundMusic.pitch = pitch;
                    worldInstance.BackgroundMusic.PlayOneShot(audio, volume);
                    break;
                case Type.Ambient:
                    worldInstance.Ambient.pitch = pitch;
                    worldInstance.Ambient.PlayOneShot(audio, volume);
                    break;
            }
        }
    }
}