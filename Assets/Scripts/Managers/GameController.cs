using System;
using nickeltin.GameData.DataObjects;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.Events;

namespace Managers
{
    [System.Serializable]
    public class DayChangesToNightEvent : UnityEvent<bool>
    {
    }
    
    public class GameController : SingletonComponent<GameController>
    {
        [SerializeField] Currency coinsCurrency;
        public Clock clockUI;
        public void AddMoney(float value)
        {
            Debug.Log("Added " + value + " coins");
            coinsCurrency.Value += value;
        }

        public DayChangesToNightEvent m_DayChangesToNightEvent;
        private void Start()
        {
            if (m_DayChangesToNightEvent == null)
                m_DayChangesToNightEvent = new DayChangesToNightEvent();

        }
    }
}