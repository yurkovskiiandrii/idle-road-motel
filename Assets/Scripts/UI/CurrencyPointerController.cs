using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[System.Serializable]
public class Pointer
{
    public nickeltin.GameData.DataObjects.Currency currency;
    public GameObject pointerGO;
    public float lifeTime;
}

public class CurrencyPointerController : MonoBehaviour
{
    [SerializeField] int maxPointerCount = 1;
    [SerializeField] List<Pointer> pointers;

    int pointersCount = 0;

    void Start()
    {
        foreach(var pointer in pointers)
            pointer.currency.SubscribeOnNotEnough(delegate { ShowPointer(pointer); });
    }

    void ShowPointer(Pointer pointer)
    {
        if (pointer.pointerGO.activeInHierarchy)
            return;

        if (pointersCount == maxPointerCount)
            return;

        pointersCount++;

        pointer.pointerGO.SetActive(true);
        DOVirtual.DelayedCall(pointer.lifeTime, delegate { pointer.pointerGO.SetActive(false); pointersCount = Mathf.Max(0, pointersCount - 1); });
    }

    private void OnDisable()
    {
        foreach (var pointer in pointers)
            pointer.currency.RemoveOnNotEnough(delegate { ShowPointer(pointer); });
    }
}
