using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Researches;
using Other;
using GameData;
using UnityEngine.Events;
using DG.Tweening;

static class QuestConditionType
{
    public const string CollectAmountKey = "CollectAmount";
    public const string UpgradeToLevelKey = "UpgradeToLevel";
    public const string HireAmountKey = "HireAmount";
    public const string SpendAmountKey = "SpendAmount";
}

static class MoneyType
{
    public const string Gold = "Gold";
    public const string BlueGems = "BlueGem";
    public const string RedGems = "RedGem";
    public const string GreenGems = "GreenGem";
}

public class QuestMenuRedirect : MonoBehaviour, IRedirectWindow
{
    [SerializeField] private UI.GUIController guiController;
    [SerializeField] private QuestIcons questIcons;

    [SerializeField] private UI.Village.ServiceMenu serviceMenu;
    [SerializeField] private SelectLocation researchSelect;
    [SerializeField] private Researches.ResearchCenterView researchview;
    [SerializeField] private RedirectionsData redirectionsData;
    [SerializeField] private NPCs.ServiceManager miningService;
    [SerializeField] private GameObject swipeToDig;
    [SerializeField] private UnityEvent closePanelEvent;

    [SerializeField] List<NPCs.ServiceManager> services;

    Quest currentQuest;

    public void Redirect()
    {
        switch (currentQuest.Condition.Type)
        {
            case QuestConditionType.CollectAmountKey:
                CollecMoneyRedirect();
                break;
            case QuestConditionType.UpgradeToLevelKey:
                UpgradeToLevelRedirect();
                break;
            case QuestConditionType.HireAmountKey:
                HireAmountRedirect();
                break;
            case QuestConditionType.SpendAmountKey:
                SpendMoneyRedirect();
                break;
        }

        closePanelEvent.Invoke();
    }

    void CollecMoneyRedirect()
    {
        guiController.ToMineScene();
        float delayTime = guiController.IsOpenMineScene() ? 0f : 0.5f;
        DOVirtual.DelayedCall(delayTime, () => swipeToDig.SetActive(true));
        DOVirtual.DelayedCall(delayTime + 2, () => swipeToDig.SetActive(false));
    }

    void UpgradeToLevelRedirect()
    {
        int npcIndex = redirectionsData.servicesData.FindIndex(x => x.questTargetID == currentQuest.Condition.Target);

        if (npcIndex == -1)
        {
            ShowService(currentQuest.Condition.Target);

            serviceMenu.ScaleActionButton();

            return;
        }

        var serviceName = redirectionsData.servicesData.First(x => x.questTargetID == currentQuest.Condition.Target).key;
        ShowService(serviceName);
        serviceMenu.ShowUnupgradedNPC((int)currentQuest.Condition.Amount);
        serviceMenu.ScaleActionButton();
    }

    void HireAmountRedirect()
    {
        var serviceName = redirectionsData.servicesData.First(x => x.questTargetID == currentQuest.Condition.Target).key;
        ShowService(serviceName);
        serviceMenu.ShowUnpurchasedNPC();
        serviceMenu.ScaleActionButton();
    }

    void SpendMoneyRedirect()
    {
        IObjectID cheapestValue = null;
        float lowerPrice = float.MaxValue;
        NPCs.ServiceManager serviceToShow = null;
        int buttonIndexToShow = 0;
        var checkCurrency = redirectionsData.currenciesData.First(x => x.Id == currentQuest.Condition.Target);

        ShuffleServicesList();

        foreach (var service in services)
        {
            var serviceObject = service.GetCheapestObject(checkCurrency.Id, out float price, out int index, serviceMenu);
            
            if(serviceObject != null && price > 0 && price < lowerPrice)
            {
                cheapestValue = serviceObject;
                serviceToShow = service;
                buttonIndexToShow = index;
                lowerPrice = price;
            }
        }

        if (cheapestValue == null)
            foreach (var service in services)
            {
                var serviceObject = service.GetCheapestObject(checkCurrency.Id, out float price, out int index, null);

                if (serviceObject != null && price > 0 && price < lowerPrice)
                {
                    cheapestValue = serviceObject;
                    serviceToShow = service;
                    buttonIndexToShow = index;
                    lowerPrice = price;
                }
            }

        ShowService(serviceToShow);

        if (cheapestValue is Buildings)
            serviceMenu.ShowPropsPanel();
        if (cheapestValue is Props)
            serviceMenu.ShowPropsInfo(buttonIndexToShow);
        if (cheapestValue is Unit)
            serviceMenu.ShowNPCInfo(buttonIndexToShow);

        serviceMenu.ScaleActionButton();
    }

    void ShowService(NPCs.ServiceManager service)
    {
        gameObject.SetActive(false);

        if (service == null)
            return;

        if (service == miningService)
        {
            guiController.ToMineScene();
            serviceMenu.ShowMenu(miningService);

            return;
        }

        var pickedService = redirectionsData.servicesData.First(x => x.serviceButton && x.serviceButton.GetServiceManager() == service).serviceButton;

        guiController.ToVillageScene();
        pickedService.ShowPos();
    }

    void ShowService(string serviceKey)
    {
        var pickedService = redirectionsData.servicesData.First(x => x.key == serviceKey).serviceButton;
        gameObject.SetActive(false);

        if (pickedService == null)
        {
            guiController.ToMineScene();
            serviceMenu.ShowMenu(miningService);
        }
        else
        {
            guiController.ToVillageScene();
            pickedService.ShowPos();
        }
    }

    public void SetQuest(Quest currentQuest) => this.currentQuest = currentQuest;

    void ShuffleServicesList()
    {
        var newShuffledList = new List<NPCs.ServiceManager>();
        var listcCount = services.Count;
        for (int i = 0; i < listcCount; i++)
        {
            var randomElementInList = Random.Range(0, services.Count);
            newShuffledList.Add(services[randomElementInList]);
            services.Remove(services[randomElementInList]);
        }
        services = newShuffledList;
    }
}
