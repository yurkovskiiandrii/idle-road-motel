using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Researches;

public enum BlockerType
{
    BuildBlocker,
    ResearchBlocker
}

public class ServiceMenuRedirect : MonoBehaviour, IRedirectWindow
{
    [SerializeField] private UI.GUIController guiController;
    [SerializeField] private UI.Village.ServiceMenu serviceMenu;
    [SerializeField] private SelectLocation researchSelect;
    [SerializeField] private Researches.ResearchCenterView researchview;
    [SerializeField] private RedirectionsData serviceSelections;

    string currentService;
    BlockerType blockerType;

    public void Redirect()
    {
        if(blockerType == BlockerType.BuildBlocker)
        {
            serviceMenu.ShowPropsPanel();
            serviceMenu.ScaleActionButton();
            return;
        }

        var currentServiceInfo =  serviceSelections.servicesData.First(x => x.key == currentService);

        if (currentServiceInfo.serviceButton == null)
            guiController.ToVillageScene();

        serviceMenu.Close();

        researchSelect.ShowPos();
        researchview.ClickOnResearch(currentServiceInfo.researchID);
        researchview.ShowScaleTip();
    }

    public void SetBlockerType(BlockerType blockerType) => this.blockerType = blockerType;

    public void SetCurrentService(NPCs.ServiceManager service) => currentService = service.SaveID;
}
