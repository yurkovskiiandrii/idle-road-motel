using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRedirectWindow
{
    public void Redirect();
}
