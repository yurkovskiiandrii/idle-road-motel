using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using nickeltin.GameData.DataObjects;

[System.Serializable]
public class ServiceSelectionDictionary
{
    public string key;
    public int researchID;
    public string questTargetID;
    public SelectService serviceButton;
}

public class RedirectionsData : MonoBehaviour
{
    public List<ServiceSelectionDictionary> servicesData;
    public List<Currency> currenciesData;
}
