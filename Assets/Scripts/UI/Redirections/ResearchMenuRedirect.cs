using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Researches;

public class ResearchMenuRedirect : MonoBehaviour, IRedirectWindow
{
    [SerializeField] private UI.GUIController guiController;
    [SerializeField] private UI.Village.ServiceMenu serviceMenu;
    [SerializeField] private NPCs.ServiceManager miningService;
    [SerializeField] private RedirectionsData serviceSelections;

    private ResearchCenter.ResearchData _data;
    SelectService pickedService;

    public void SetCurrentResearch(ResearchCenter.ResearchData data) => _data = data;

    public void Redirect()
    {
        pickedService = serviceSelections.servicesData.First(x => x.key == _data.Unlocks[0].Id).serviceButton;
        gameObject.SetActive(false);
        
        Invoke(nameof(ShowMenu), pickedService == null ? 0.5f : 0);

        if(pickedService == null)
            guiController.ToMineScene();
    }

    void ShowMenu()
    {
        if (pickedService == null)
            serviceMenu.ShowMenu(miningService);
        else
            pickedService.ShowPos();

        serviceMenu.ShowTip();
    }
}
