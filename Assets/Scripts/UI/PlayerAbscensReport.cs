﻿using nickeltin.Tweening;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class PlayerAbscensReport : MonoBehaviour
    {
        [SerializeField] private TMP_Text m_pointsText;
        [SerializeField] [Range(0, 4)] private int m_digitsAfterPoint = 0;
        [SerializeField] private float m_tweenTime;
        [SerializeField] private LeanTweenType m_tweenType;

        public void Appear(float points)
        {
            gameObject.SetActive(true);
            LeanTween.scale(GetComponent<RectTransform>(), Vector3.one, m_tweenTime).setEase(m_tweenType);
            m_pointsText.text = points.ToString("F" + m_digitsAfterPoint);
        }

        public void Hide()
        {
            LeanTween.scale(GetComponent<RectTransform>(), Vector3.zero, m_tweenTime).setEase(m_tweenType)
                .setOnComplete(() => gameObject.SetActive(false));
        }
    }
}