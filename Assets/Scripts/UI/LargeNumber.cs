﻿using System;
using System.Numerics;
using UnityEngine;

namespace CoreGame.UI
{
    public class LargeNumber
    {
        // private string[] notations = new string[12] {"", "k", "M", "B", "T", "Qa", "Qi", "Sx", "Sp", "Oc", "No", "Dc"};
        private static string[] notations = new string[4] {"", "k", "M", "B"};// "T", "Qa", "Qi", "Sx", "Sp", "Oc", "No", "Dc"};
 
        public static string FormatEveryThirdPower(float target)
        {
            float value = target;
            int baseValue = 0;
            string notationValue = "";
            string toStringValue = "";
 
            if (value >= 10000) // I start using the first notation at 10k
            {
                value /= 1000;
                baseValue++;
                while (Mathf.Round((float)value) >= 1000)
                {
                    value /= 1000;
                    baseValue++;
                }
                if (baseValue < 2)
                {
                    toStringValue = "N1"; // display 1 decimal while under 1 million
                }
                else
                {
                    toStringValue = "N2"; // display 2 decimals for 1 million and higher   
                }
                if (baseValue > notations.Length)
                {
                    return null;
                }
                else
                {
                    notationValue = notations[baseValue];
                }
            }
            return value.ToString(toStringValue) + notationValue;
        }
    }
}