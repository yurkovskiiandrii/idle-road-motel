using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class IconUIContainer
{
    [SerializeField] GameObject backgroundGO;
    [SerializeField] GameObject icons;

    public void SetActive(bool value)
    {
        backgroundGO.SetActive(value);
        icons.SetActive(value);
    }

    public Button GetIconButton() => backgroundGO.GetComponent<Button>();
}