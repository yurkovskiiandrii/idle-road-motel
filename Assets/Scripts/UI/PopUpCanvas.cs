using nickeltin.Singletons;
using UnityEngine;

namespace UI
{
    public class PopUpCanvas : MonoSingleton<PopUpCanvas>
    {
        public static Canvas Canvas;

        protected override void Awake()
        {
            base.Awake();
            Canvas = GetComponent<Canvas>();
            Canvas.renderMode = RenderMode.ScreenSpaceCamera;
            Canvas.worldCamera = GameCamera.MainCamera;
            Canvas.planeDistance = 1;
        }
    }
}