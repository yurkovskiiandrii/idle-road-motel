using Customers;
using DG.Tweening;
using GameEnvironment.Village.Structures;
using NPCs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ReceptionMenuController : SingletonComponent<ReceptionMenuController>
{
    [SerializeField] ServiceManager clientsService;
    [SerializeField] CustomRequirements notRoomStatsIDs;
    [SerializeField] CustomRequirements roomStatsIDs;

    [SerializeField] GameObject menuObjectsContainer;

    [SerializeField] Button checkinFamilyButton;
    [SerializeField] Button refuseFamilyButton;

    [SerializeField] TextMeshProUGUI customerNameText;
    [SerializeField] TextMeshProUGUI customerDescriptionText;
    [SerializeField] TextMeshProUGUI paymentValueText;
    [SerializeField] GameObject familyPlag;

    [SerializeField] Image mainPersonIcon;

    [SerializeField] GameObject leftArrow;
    [SerializeField] GameObject rightArrow;

    [SerializeField] TextMeshProUGUI roomNameText;
    [SerializeField] TextMeshProUGUI roomLevelText;
    [SerializeField] Transform roomsIconsContainer;
    [SerializeField] Transform otherFacilitiesIconsContainer;

    [SerializeField] GameObject canCheckinButton;
    [SerializeField] GameObject grayCheckinButton;

    [SerializeField] TextMeshProUGUI customerTimer;
    [SerializeField] Image customerTimerProgressBar;

    [SerializeField] CustomerRequirementsUIScheme pleasureIconScheme;

    [SerializeField] Transform obligatoryContainer;
    [SerializeField] Transform notObligatoryContainer;

    [SerializeField] Image needsSatisfactionPB;
    [SerializeField] TextMeshProUGUI needsSatisfactionText;

    [Header("ProgressBarColors")]
    [Space(10)]
    [SerializeField] ProgressBarColor more66Color;
    [SerializeField] ProgressBarColor between33_66Color;
    [SerializeField] ProgressBarColor less33Color;

    [SerializeField] Image customerEmoji;

    [SerializeField] float FictMult = 1;

    [System.Serializable]
    class ProgressBarColor
    {
        public Color color;
        public Sprite pbImage;
    }

    CustomersPseudofamily currentFamily;

    int currentRoom = 0;
    NPCs.ServiceManager pickedRoom;

    CustomerAI currentMainCustomer;

    float maxReceptionTime;

    Dictionary<string, int> currentPleasureZones;
    Dictionary<string, (int, bool)> currentRequirements;

    private void Start()
    {
        currentPleasureZones = new Dictionary<string, int>();
        currentRequirements = new Dictionary<string, (int, bool)>();

        refuseFamilyButton.onClick.AddListener(() => currentFamily.Refuse());
        refuseFamilyButton.onClick.AddListener(HideMenu);
    }

    public void ShowMenu(CustomersPseudofamily pseudofamily)
    {
        currentFamily = pseudofamily;
        menuObjectsContainer.SetActive(true);

        currentRoom = 0;

        UpdateCustomerInfo();
        UpdateRoomInfo(currentRoom);

        SpawnNotRoomFecilitiesIcons();

        SpawnCustomersReqIcons();
    }

    void SpawnNotRoomFecilitiesIcons()
    {
        var notRoomRequirements = new List<(string, int)>();

        foreach(var item in currentPleasureZones)
            if (notRoomStatsIDs.requirementsNames.Contains(item.Key))
                notRoomRequirements.Add((item.Key, 0));

        var notRoomServices = clientsService.GetCustomerPleasureTypes();

        for (int i = 0; i < notRoomRequirements.Count; ++i)
        {
            var itemType = notRoomRequirements[i].Item1;

            if (notRoomServices.Contains(itemType)
                && clientsService.GetActivePleasureZonesCount(itemType) > 0)
            {
                notRoomRequirements[i] = (itemType, clientsService.GetMaxPleasureZonesLevel(itemType));
                currentPleasureZones[itemType] = notRoomRequirements[i].Item2;
            }
        }

        var icons = SpawnRequirementsIcons(notRoomRequirements, otherFacilitiesIconsContainer);

        for (int i = 0; i < icons.Count; i++)
        {
            bool isAvailable = currentRequirements[notRoomRequirements[i].Item1].Item1 <= notRoomRequirements[i].Item2;
            icons[i].SetAvailibility(isAvailable);
        }
    }

    void UpdateRoomInfo(int roomIndex)
    {
        pickedRoom = RoomsContainerSingleton.Instance.GetRoom(roomIndex);

        leftArrow.SetActive(roomIndex != 0);
        rightArrow.SetActive(roomIndex != RoomsContainerSingleton.Instance.GetRoomsCount() - 1);
        
        roomNameText.text = pickedRoom.Buildings.Name;

        roomLevelText.text = pickedRoom.File.BuildingsLevel == 0 ? "Unbuilt" : "Level " + pickedRoom.File.BuildingsLevel;

        var roomProps = pickedRoom.ActiveProps.Values.Where(x => x.GetStorage() == null).ToList();

        List<CustomerInGameRequirement> nonFilteredRequirements = new List<CustomerInGameRequirement>();

        foreach (var prop in roomProps)
        {
            CustomerInGameRequirement requirement = new CustomerInGameRequirement();
            requirement.requirementPropsLevel = prop.GetLevel();
            requirement.StatName = prop.GetComponentInChildren<CustomerPleasureZone>().requirementType;

            nonFilteredRequirements.Add(requirement);
        }

        var dictionary = nonFilteredRequirements.GroupBy(x => x.StatName);

        var keys = currentPleasureZones.Keys.ToList();
        currentPleasureZones.Clear();

        foreach (var key in keys)
            currentPleasureZones.Add(key, 0);

        foreach (var pair in dictionary)
        {
            pair.OrderBy(x => x.requirementPropsLevel);
            var reversedList = pair.Reverse();
            var requirement = pair.FirstOrDefault(x => x.isObligatory);

            requirement = requirement ?? pair.First();

            if(currentPleasureZones.ContainsKey(requirement.StatName))
                currentPleasureZones[requirement.StatName] = requirement.requirementPropsLevel;
        }

        var roomRequirements = new List<(string, int)>();

        foreach (var item in currentPleasureZones)
            if(roomStatsIDs.requirementsNames.Contains(item.Key))
                roomRequirements.Add((item.Key, item.Value));

        SpawnNotRoomFecilitiesIcons();

        var icons = SpawnRequirementsIcons(roomRequirements, roomsIconsContainer/*, currentRequirements*/);
        SpawnCustomersReqIcons();

        for (int i = 0; i < icons.Count; i++)
        {
            bool isAvailable = currentRequirements[roomRequirements[i].Item1].Item1 <= roomRequirements[i].Item2;
            icons[i].SetAvailibility(isAvailable);
        }

        ShowCheckinButton();

        float needsSatisfaction = GetNeedsSatisfaction(currentFamily.GetAllRequirements());
        needsSatisfactionPB.fillAmount = needsSatisfaction / 100;
        needsSatisfactionText.text = ((int)Math.Ceiling(needsSatisfaction)).ToString() + "%";

        var progressBarColor = GetProgressBarColor(needsSatisfaction / 100);
        needsSatisfactionPB.color = progressBarColor.color;
        customerEmoji.sprite = progressBarColor.pbImage;

        UpdateHourlyPaymentLabel(needsSatisfaction);

        var clients = currentFamily.GetClients();

        foreach (var client in clients)
            client.SetSatRating(GetNeedsSatisfaction(client.GetAllRequirements()));
    }

    void UpdateHourlyPaymentLabel(float satLevel)
    {
        satLevel /= 100;
        float CheckProfitMin = currentFamily.GetMinPayments().Sum() * FictMult * satLevel;
        float CheckProfitMax = currentFamily.GetMaxPayments().Sum() * FictMult * satLevel; 

        paymentValueText.text = ((int)CheckProfitMin).ToString() + "-" + ((int)CheckProfitMax).ToString();
    }

    float GetNeedsSatisfaction(List<(string, int)> requirements)
    {
        float sum = 0;

        foreach (var clientRequirement in requirements)
            sum += (float)currentPleasureZones[clientRequirement.Item1] / (float)clientRequirement.Item2;

        sum /= requirements.Count;

        return sum * 100;
    }

    void ShowCheckinButton()
    {
        bool canCheckinToThisRoom = pickedRoom.CanCheckinFamily(currentFamily);

        if(canCheckinToThisRoom)
        {
            foreach (var clientRequirement in currentRequirements)
                if (clientRequirement.Value.Item1 > currentPleasureZones[clientRequirement.Key])
                {
                    canCheckinToThisRoom = false;
                    break;
                }
        }

        canCheckinButton.SetActive(canCheckinToThisRoom);
        grayCheckinButton.SetActive(!canCheckinToThisRoom);
    }

    public void CheckinFamily()
    {
        HideMenu();
        currentFamily.Checkin(pickedRoom);
        //DOVirtual.DelayedCall(5, () => currentFamily.Checkout());
    }

    public void ShowNextRoomInfo()
    {
        ++currentRoom;
        UpdateRoomInfo(currentRoom);
    }

    public void ShowPrevRoomInfo()
    {
        --currentRoom;
        UpdateRoomInfo(currentRoom);
    }

    void UpdateCustomerInfo()
    {
        currentRequirements.Clear();
        currentPleasureZones.Clear();

        currentMainCustomer = currentFamily.MainPerson();
        var customerDescription = currentMainCustomer.GetDescription();

        customerNameText.text = customerDescription.Name;
        customerDescriptionText.text = customerDescription.Description;
        mainPersonIcon.sprite = Utils.ResourcesLoader.LoadSprite(customerDescription.IconName);

        familyPlag.SetActive(false);
        //familyIconsContainer.SetActive(false);

        maxReceptionTime = currentMainCustomer.GetMaxReceptionTime();

        UpdateCustomerProgressBar(currentMainCustomer.GetRemainReceptionTime());
        currentMainCustomer.SubscribeOnReceptionTimeUpdate(UpdateCustomerProgressBar);

        currentRequirements = currentFamily.GetRequirements();

        foreach (var item in currentRequirements)
            currentPleasureZones.Add(item.Key, 0);

        if (currentFamily.FamilySize > 1)
        {
            familyPlag.SetActive(true);
            //familyIconsContainer.SetActive(true);

            //int familyIconsCount = familyIconsContainer.transform.childCount;
            //for(int i = 0; i < familyIconsCount; i++)
            //    familyIconsContainer.transform.GetChild(i).gameObject.SetActive(false);

            //for (int i = 0; i < currentFamily.FamilySize; i++)
            //{
            //    var childObj = familyIconsContainer.transform.GetChild(i).gameObject;
            //    childObj.SetActive(true);
            //    childObj.GetComponent<Image>().sprite = Utils.ResourcesLoader.LoadSprite(
            //                                                currentFamily.MemberDescription(i).IconName);
            //}
        }
    }

    void SpawnCustomersReqIcons()
    {
        var obligatotyRequirements = new List<(string, int)>();
        var notObligatotyRequirements = new List<(string, int)>();

        foreach (var item in currentRequirements)
        {
            if(item.Value.Item2)
            {
                obligatotyRequirements.Add((item.Key, item.Value.Item1));
                continue;
            }

            notObligatotyRequirements.Add((item.Key, item.Value.Item1));
        }

        var icons = SpawnRequirementsIcons(obligatotyRequirements, obligatoryContainer);

        for(int i = 0; i < icons.Count; i++)
        {
            bool isAvailable = currentPleasureZones[obligatotyRequirements[i].Item1] >= obligatotyRequirements[i].Item2;
            icons[i].SetAvailibility(isAvailable);
        }

        icons = SpawnRequirementsIcons(notObligatotyRequirements, notObligatoryContainer);

        for (int i = 0; i < icons.Count; i++)
        {
            bool isAvailable = currentPleasureZones[notObligatotyRequirements[i].Item1] >= notObligatotyRequirements[i].Item2;
            icons[i].SetAvailibility(isAvailable);
        }
    }

    List<RequirementsIcon> SpawnRequirementsIcons(List<(string, int)> requirements, Transform container)
    {
        var spawnedIcons = new List<RequirementsIcon>();

        foreach (Transform child in container)
            child.gameObject.SetActive(false);

        for(int i = 0; i < requirements.Count; i++)
        {
            var iconGO = container.GetChild(i).gameObject;
            iconGO.SetActive(true);

            var iconScript = iconGO.GetComponent<RequirementsIcon>();
            iconScript.UpdateIcon(requirements[i].Item2,
                                                        pleasureIconScheme.GetSprite(requirements[i].Item1));
            spawnedIcons.Add(iconScript);
        }

        return spawnedIcons;
    }

    void UpdateCustomerProgressBar(float time)
    {
        customerTimer.text = ((int)time).ToString() + "s";

        var fillAmount = time / maxReceptionTime;
        customerTimerProgressBar.fillAmount = fillAmount;
        customerTimerProgressBar.color = GetProgressBarColor(fillAmount).color;
    }

    ProgressBarColor GetProgressBarColor(float fillAmount) => fillAmount >= 0.66f ? more66Color : fillAmount >= 0.33f ? between33_66Color : less33Color;

    public void HideMenu()
    {
        currentMainCustomer.RemoveAllFromReceptionTimeUpdate();
        menuObjectsContainer.SetActive(false);
    }

    public void TryHideMenu(CustomersPseudofamily pseudofamily)
    {
        if (pseudofamily == currentFamily)
            HideMenu();
    }
}
