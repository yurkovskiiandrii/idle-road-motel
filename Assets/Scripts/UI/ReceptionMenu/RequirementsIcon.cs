using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RequirementsIcon : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI levelLabel;
    [SerializeField] Image requirementIcon;

    [SerializeField] GameObject grayMaskGO;
    [SerializeField] GameObject warningSymbol;
    [SerializeField] GameObject okSymbol;

    [SerializeField] Color availableColor = Color.green;
    [SerializeField] Color noAvailableColor = Color.red;

    public void UpdateIcon(int level, Sprite sprite)
    {
        requirementIcon.sprite = sprite;
        levelLabel.text = level.ToString();
    }

    public void SetAvailibility(bool value)
    {
        levelLabel.color = value ? availableColor : noAvailableColor;
        grayMaskGO.SetActive(!value);
        warningSymbol.SetActive(!value);
        okSymbol.SetActive(value);
    }
}
