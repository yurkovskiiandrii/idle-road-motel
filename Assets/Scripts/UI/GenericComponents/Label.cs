﻿using System;
using CoreGame.UI;
using GameData.DataObjects;
using nickeltin.GameData.DataObjects;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Image = UnityEngine.UI.Image;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class Label : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] protected NumberObject m_source;
        [SerializeField] protected TMP_Text m_value;
        [SerializeField] protected Image m_backGround;

        [Header("Text Properties")]
        [SerializeField] [Range(0, 4)] protected int m_digitsAfterPoint = 2; 
        [SerializeField] protected string m_postfix = "x";
        [SerializeField] protected UnityEvent OnValueChange;
        protected virtual void UpdateValue(float newValue)
        {
            m_value.text = LargeNumber.FormatEveryThirdPower(newValue);//newValue.ToString("F" + m_digitsAfterPoint) + m_postfix;
            OnValueChange?.Invoke();
        }
    
        protected virtual void OnEnable()
        {
            UpdateValue(m_source.Value);
            m_source.onValueChanged += UpdateValue;
        }

        protected virtual void OnDisable() => m_source.onValueChanged -= UpdateValue;
    }
}