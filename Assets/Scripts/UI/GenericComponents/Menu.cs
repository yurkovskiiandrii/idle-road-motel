﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Canvas), typeof(CanvasScaler), typeof(GraphicRaycaster))]
    public abstract class Menu : MonoBehaviour
    {
        protected Canvas m_canvas;
        protected RectTransform m_canvasRect;

        protected virtual void Awake()
        {
            m_canvas = GetComponent<Canvas>();
            m_canvasRect = m_canvas.GetComponent<RectTransform>();
        }
    }
}