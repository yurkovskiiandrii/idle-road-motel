﻿using nickeltin.ObjectPooling;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Image))]
    public class ImageObject : PoolObject<ImageObject>
    {
        private Image m_image;

        public Image Image
        {
            get
            {
                if (m_image == null) m_image = GetComponent<Image>();
                return m_image;
            }
        }
    }
}