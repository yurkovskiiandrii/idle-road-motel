﻿using System;
using CoreGame.UI;
using GameData.DataObjects;
using Managers;
using nickeltin.GameData.DataObjects;
using TMPro;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Events;
using Image = UnityEngine.UI.Image;

namespace UI
{
    [System.Serializable]
    public class TimeUpdate : UnityEvent<string, bool>
    {
    }

    [RequireComponent(typeof(RectTransform))]
    public class Clock : MonoBehaviour
    {
        [Header("Components")] [SerializeField]
        protected TMP_Text m_value;

        [SerializeField] protected TMP_Text m_day_night;
        [SerializeField] protected Image m_backGround;

        [Header("Text Properties")] public TimeUpdate m_TimeUpdate;

        private void Start()
        {
            m_day_night.text = "Day";
            GameObject gameController = GameObject.FindWithTag("GameController");
            //Debug.Log(gameController);
            gameController.GetComponent<GameController>().clockUI = this.gameObject.GetComponent<Clock>();
            if (m_TimeUpdate == null)
                m_TimeUpdate = new TimeUpdate();
            m_TimeUpdate.AddListener(UpdateValue);
        }

        protected void UpdateValue(string newValue, bool dayNight)
        {
            if (dayNight)
                m_day_night.text = "Day";
            else if (!dayNight)
                m_day_night.text = "Night";

            m_value.text = newValue;
        }
    }
}