﻿using nickeltin.GameData.DataObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleUI : MonoBehaviour
    {
        [SerializeField] private BoolObject m_source;
        
        private Toggle m_toggle;

        private void Awake()
        {
            m_toggle = GetComponent<Toggle>();
            m_toggle.isOn = m_source.Value;
        }
    }
}