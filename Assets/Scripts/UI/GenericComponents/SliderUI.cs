﻿using nickeltin.GameData.DataObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Slider))]
    public class SliderUI : MonoBehaviour
    {
        [SerializeField] private NumberObject m_source;
        
        private Slider m_slider;

        private void Awake()
        {
            m_slider = GetComponent<Slider>();
            m_slider.value = m_source.Value;
        }
    }
}