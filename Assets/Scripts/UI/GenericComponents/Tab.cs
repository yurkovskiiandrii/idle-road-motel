﻿using System;
using nickeltin.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Image))]
    public class Tab : MonoBehaviour, IPointerClickHandler
    {
        private Image m_image;

        public Action<Tab> onSelectionInternal;
        public UnityEvent onSelection;
        
        private void Awake()
        {
            m_image = GetComponent<Image>();
        }
        
        public void OnPointerClick(PointerEventData eventData) => Select();

        public Tab Select()
        {
            m_image.color = Color.yellow;
            LeanTween.scale(m_image.rectTransform, Vector2.one * 1.1f, 0.1f);
            onSelectionInternal?.Invoke(this);
            onSelection?.Invoke();
            return this;
        }

        public void Deselect()
        {
            m_image.color = Color.white;
            LeanTween.scale(m_image.rectTransform, Vector2.one, 0.1f);
        }
    }
}