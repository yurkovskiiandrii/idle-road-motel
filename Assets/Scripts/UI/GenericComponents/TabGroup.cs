﻿using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class TabGroup : MonoBehaviour
    {
        private List<Tab> m_tabs;
        private Tab m_selectedTab;

        private void Awake()
        {
            m_tabs = new List<Tab>(GetComponentsInChildren<Tab>());

            if (m_tabs.Count > 0)
            {
                foreach (var tab in m_tabs)
                {
                    tab.Deselect();
                    tab.onSelectionInternal = OnTabSelection;
                }

                m_selectedTab = m_tabs[0].Select();
            }
        }

        private void OnTabSelection(Tab tab)
        {
            if (m_selectedTab != null && m_selectedTab != tab) m_selectedTab.Deselect();
            m_selectedTab = tab;
        }
    }
}