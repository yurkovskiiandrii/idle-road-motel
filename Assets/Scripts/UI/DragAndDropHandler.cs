﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace nickeltin.UI
{
    public class DragAndDropHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler,
        IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public ScrollRect scrollRect;
        [Range(0f, 1f)]public float draggingDelay = 0.5f;
        public bool draggable = true;
        private bool draggingSlot;

        public event Action<PointerEventData> onBeginDrag; 
        public event Action<PointerEventData> onDrag;
        public event Action<PointerEventData> onEndDrag; 
        public event Action<PointerEventData> onPointerUp; 
        
        public void OnPointerDown(PointerEventData eventData)
        {
            if (!draggable) return;
            
            IEnumerator StartTimer(PointerEventData data)
            {
                yield return new WaitForSeconds(draggingDelay);
                draggingSlot = true;
                onBeginDrag?.Invoke(data);
            }
            
            StartCoroutine(StartTimer(eventData));
        }

        public void OnPointerExit(PointerEventData eventData) => StopAllCoroutines();

        public void OnPointerUp(PointerEventData eventData)
        {
            StopAllCoroutines();
            onPointerUp?.Invoke(eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.beginDragHandler);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (draggingSlot) onDrag?.Invoke(eventData);
            else
            {
                ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.dragHandler);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.endDragHandler);
            if (draggingSlot)
            {
                draggingSlot = false;
                onEndDrag?.Invoke(eventData);
            }
        }
    }
}