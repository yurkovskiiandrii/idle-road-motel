using System;
using DG.Tweening;
using nickeltin.ObjectPooling;
using Source;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class PopupWorldTextObject : PoolObject<PopupWorldTextObject>
{
    private Transform _cameraTransform;
    [SerializeField] private Image Icon;
    [SerializeField] private TMP_Text Text;
    [SerializeField] private Color _reguralColor;
    [SerializeField] private Color _criticalColor;
    [SerializeField] private int _digitsAfterPointInNumbers;
    [SerializeField] private Vector3 _appearanceOffset;
    [SerializeField] private float _appeareTime;
    [SerializeField] private Vector3 _startScale;
    [SerializeField] private float _scaleTime;
    [SerializeField] private float _stayTime;

    private float _defaultTextSize;
    private Transform CameraTransform
    {
        get
        {
            if (_cameraTransform == null)
                _cameraTransform = GameCamera.MainCamera.transform;
            return _cameraTransform;
        }
    }

 

    public void Appear(PopUpWorldIconsScheme iconsScheme, Vector3 worldPoint, float text, Action callback,bool isCritical)
    {
        
        Icon.sprite = iconsScheme.Icon;
        
        Text.rectTransform.localScale = _startScale;
        Text.color =  isCritical ? _criticalColor : _reguralColor;
        Text.text = iconsScheme.Prefix + text.ToString("F" + _digitsAfterPointInNumbers);
        Text.rectTransform.localPosition = worldPoint;
            
        var lifetime = _appeareTime + _scaleTime +_stayTime;
        var rect = Text.rectTransform;

        DOVirtual.DelayedCall(lifetime, callback.Invoke);
        rect.DOMove(worldPoint + _appearanceOffset, _appeareTime).onComplete += () =>
        {
            transform.DOScale(1, _scaleTime);
        };
    }

    private void FixedUpdate()
    {
        transform.LookAt(
                transform.position + CameraTransform.rotation * Vector3.forward,
                CameraTransform.rotation * Vector3.up);
    }
}
