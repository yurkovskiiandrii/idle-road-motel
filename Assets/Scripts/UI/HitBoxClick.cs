using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HitBoxClick : MonoBehaviour
{
    [SerializeField] float inputTrethhold = 30;

    // Start is called before the first frame update
    public Vector2 startClick;
    public Vector2 EndCLick;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Managers.InputManager.CameraMoveBlocked
            || IsPointerOverUI.IsPointerOverUIObject(Input.mousePosition))
            return;

        if (Input.GetMouseButtonDown(0))
        {
            startClick = Input.mousePosition;

        }
        if (Input.GetMouseButtonUp(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            EndCLick = Input.mousePosition;
            if (Vector2.Distance(startClick, EndCLick) < inputTrethhold)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.GetComponent<SelectService>() != null)
                    {
                        hit.transform.GetComponent<SelectService>().ShowPos();

                    }
                    if (hit.transform.GetComponent<SelectLocation>() != null)
                    {
                        hit.transform.GetComponent<SelectLocation>().ShowPos();

                    }

                }
            }
        }
    }
    // protected abstract void Show();


}
