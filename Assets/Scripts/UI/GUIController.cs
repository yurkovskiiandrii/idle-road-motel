﻿using System;
using System.Collections.Generic;
using CameraControlling;
using UnityEngine;
using nickeltin.Singletons;
using nickeltin.Editor.Attributes;
using nickeltin.Extensions;
using nickeltin.Tweening;
using Source;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    [RequireComponent(typeof(Canvas))]
    public class GUIController : MonoSingleton<GUIController>
    {
        [FormerlySerializedAs("m_gameSceneCanvas")]
        [Header("Menus")]
        [SerializeField] private Canvas _mineSceneCanvas;
        [SerializeField] private Canvas _villageSceneCanvas;
        [SerializeField] private Canvas _serviceCanvas;
        [SerializeField] private Canvas m_settingsCanvas;
        [SerializeField] private Canvas m_statusPanelCanvas;
        [SerializeField] private Canvas _notificationsCanvas;
        [SerializeField] private Image m_overlay;
        [SerializeField] private Canvas _researchCanvas;

        [Space]
        [SerializeField] private RectTransform m_debugPanel;
        [SerializeField] private TMP_Text m_debugText;
        [SerializeField] private PlayerAbscensReport m_report;
        [SerializeField, Scene] private string _villageScene;
        [SerializeField, Scene] private string _mineScene;

        [SerializeField] UnityEvent onMineSceneLoadedTutorialEvent;

        public static Canvas Canvas;
        public AutomateProgress automate;
        public static event Action onLoad;

        public static MonoBehaviour ConsistentMonoBehaviour => instance;
            
        // private Pool<ImageObject> m_coinsPool;
        // private Coroutine m_coinsAnim;
        
        protected override void Awake()
        {
            base.Awake();
           
            Canvas = GetComponent<Canvas>();
            /*Canvas.renderMode = RenderMode.ScreenSpaceCamera;
            Canvas.planeDistance = 1;*/

            //m_coinsPool = new Pool<ImageObject>(m_coinsPrefab, transform, 100);
            
            // #if UNITY_EDITOR
            // m_debugPanel.gameObject.SetActive(true);
            // #endif
            
            foreach (var subMenu in GetSubMenus())
            {
                RectTransform rect = subMenu.GetComponent<RectTransform>();
                rect.anchorMax = Vector2.one;
                rect.anchorMin = Vector2.zero;
                rect.anchoredPosition = Vector2.zero;
            }
        }

        private List<Canvas> GetSubMenus()
        {
            return new List<Canvas>
            {
                _villageSceneCanvas, 
                _mineSceneCanvas, 
                _serviceCanvas,
                m_settingsCanvas,
                m_statusPanelCanvas,
                _notificationsCanvas,
                _researchCanvas
            };
        }
        
        private void Start()
        {
            onLoad?.Invoke();
            Canvas.worldCamera = GameCamera.MainCamera;
            SceneLoader.currentScene = _villageScene;
        }

        public void ToVillageScene()
        {
            if (SceneLoader.currentScene == _villageScene)
                return;

            GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");

            GetGemFrom getGem = mine[0].gameObject.GetComponent<GetGemFrom>();
            getGem.OutScene();
            FadeInOut();
            SceneLoader.currentScene = _villageScene;
            SceneLoader.FakeSceneLoad(_villageScene);
            SceneLoader.UnloadScene(_mineScene);
            _villageSceneCanvas.gameObject.SetActive(true);
            _mineSceneCanvas.gameObject.SetActive(false);
        }

        public void ToMineScene()
        {
            if (SceneLoader.currentScene == _mineScene)
                return;

            SceneLoader.LoadScene(_mineScene, LoadSceneMode.Additive, () =>
            {
                FadeInOut();
                _villageSceneCanvas.gameObject.SetActive(false);
                _mineSceneCanvas.gameObject.SetActive(true);
                SceneLoader.FakeSceneUnload(_villageScene);
                SceneLoader.currentScene = _mineScene;
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(_mineScene));
                automate.CalculateEfficiency(0);
            });
        }

        public void DelayedToMineScene()
        {
            if (SceneLoader.currentScene == _mineScene)
                return;

            SceneLoader.LoadScene(_mineScene, LoadSceneMode.Additive, () =>
            {
                FadeInOut();
                _villageSceneCanvas.gameObject.SetActive(false);
                _mineSceneCanvas.gameObject.SetActive(true);
                SceneLoader.FakeSceneUnload(_villageScene);
                SceneLoader.currentScene = _mineScene;
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(_mineScene));
                automate.CalculateEfficiency(0);
                onMineSceneLoadedTutorialEvent.Invoke();
            });
        }

        public void QuitGame() => Application.Quit();

        public static void DisplayReportMessage(ProgressCalculator idleProgress)
        {
            
        }
        private void FadeInOut()
        {
            LeanTween.cancel(instance.m_overlay.rectTransform);
            m_overlay.color = Color.black.With(a: 0);
            LeanTween.color(m_overlay.rectTransform, Color.black, 0.1f).setOnComplete(() =>
            {
                LeanTween.color(m_overlay.rectTransform, Color.black.With(a: 0), 0.1f).setDelay(0.2f);
            });
        }

        public static void LogDebug(string text)
        {
            if (instance.m_debugText.text.Length > 1000)
            {
                instance.m_debugText.text = "";
            }
            instance.m_debugText.text += text + "\n";
            Debug.Log(text);
        }

        public bool IsOpenMineScene() => SceneLoader.currentScene == _mineScene;
    }
}