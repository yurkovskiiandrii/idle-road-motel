using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using nickeltin.Extensions;
using nickeltin.GameData.DataObjects;
using nickeltin.GameData.Saving;
using Destructables;


[System.Serializable]
public class Gem
{
    public CurrencyType type;
    public GameObject gameObject;
    public Text text;
    public Image image;
}
[System.Serializable]
public class RequiredObj
{
    public Gem[] gems;

}
public class SetAutomateStat : MonoBehaviour
{
    // Start is called before the first frame update
    public AutomateProgress auto;
    public SetAutomateStat autoMineStat;
    [SerializeField] private GameProgressSave _save;
    public List<RequiredGem> GemList = new List<RequiredGem>();
    public RequiredObj requiredObj;
    public RequiredObj ProfMin;
    public Text DurationText;
    public GameObject AutomatePanel;
    int ww; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public void MinusStat()
    {
        // if(GemList.Count == 0)
        //  {
        //      return;
        //  }

        if (auto.Mining == false)
        {
            GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");
            Mineshaft mineScript = mine[0].gameObject.GetComponent<Mineshaft>();
            int x = 0;

            foreach (RequiredGem required in GemList)
            {
                foreach (Currency i in mineScript.currency)
                {
                    if (i.Id == required.currencyType.ToString() && i.EnoughtCurrency(required.GemRequired))
                        x++;
                }
            }

            if (x == GemList.Count)
            {
                foreach (RequiredGem required in GemList)
                {
                    foreach (Currency i in mineScript.currency)
                    {
                        if (i.Id == required.currencyType.ToString() && i.Value >= required.GemRequired)
                        {
                            i.Value -= required.GemRequired;
                            Debug.LogError("������");
                        }
                    }
                }
                
                x = 0; 
                auto.Mining = true;
                Debug.LogError("MinusStat");
                ProgressUI.GetButton = 1;
                UpdateStat();
                auto.mines.Gold = 0;
                auto.mines.BlueGem = 0;
                auto.mines.RedGem = 0;
                auto.mines.RedGem = 0;
                _save.Save();
                auto.SetMin(auto.TimeMinning);
                auto.CalculateOfflineProgress(1);

                MinusStat();
            }
        }
        else
        {
            Debug.LogError("MinusStat");
            ProgressUI.GetButton = 0;
        }
    }

    public void UpdateStat()
    {
        if (auto.Mining == false)
        {
            if (GemList.Count != 0)
            {
                GemList.Clear();
            }

            // Red = null;
            // Green = null;
            // Blue = null;
            // Gold = null;
            GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");
            Mineshaft mineScript = mine[0].gameObject.GetComponent<Mineshaft>();
            GemList = mineScript.requiredGem();
            foreach (Gem e in requiredObj.gems)
            {
                e.gameObject.SetActive(false);
            }
            foreach (RequiredGem required in GemList)
            {
                foreach (Gem a in requiredObj.gems)
                {
                    //a.gameObject.SetActive(false);
                    if (required.currencyType == a.type)
                    {

                        foreach (Currency s in mineScript.currency)
                        {
                            if (s.Id == a.type.ToString() && required.GemRequired != 0)
                            {
                                a.gameObject.SetActive(true);

                                a.text.text = required.GemRequired.ToString(); //s.Value.ToString() + "/"
                            }
                        }
                    }

                }
            }

            foreach (Gem e in ProfMin.gems)
            {
                e.gameObject.SetActive(false);
            }
            foreach (RequiredGem required in GemList)
            {
                foreach (Gem a in ProfMin.gems)
                {
                    //a.gameObject.SetActive(false);
                    if (required.currencyType == a.type)
                    {

                        foreach (Currency s in mineScript.currency)
                        {
                            if (s.Id == a.type.ToString()&&required.GetReward )
                            {
                                a.gameObject.SetActive(true);
                                auto.CalculateMiningEfficiency();

                                a.text.text = auto.ProfMin.ToString();
                            }
                        }
                    }

                }
            }

            DurationText.text = (auto.TimeMinning / 60).ToString() + " MINUTES";
          //  ProgressUI.GetButton = 1;
        }
        else
        {
            GemList.Clear();
            GameObject[] mine = GameObject.FindGameObjectsWithTag("Mineshaft");
            Mineshaft mineScript = mine[0].gameObject.GetComponent<Mineshaft>();
            GemList = mineScript.requiredGem();
            foreach (Gem e in requiredObj.gems)
            {
                e.gameObject.SetActive(false);
            }
            foreach (RequiredGem required in GemList)
            {
                foreach (Gem a in requiredObj.gems)
                {
                    //a.gameObject.SetActive(false);
                    if (required.currencyType == a.type)
                    {

                        foreach (Currency s in mineScript.currency)
                        {
                            if (s.Id == a.type.ToString() && required.GemRequired != 0)
                            {
                                a.gameObject.SetActive(true);

                                a.text.text =  required.GemRequired.ToString();//s.Value.ToString() + "/" +
                            }
                        }
                    }

                }
            }

            foreach (Gem e in ProfMin.gems)
            {
                e.gameObject.SetActive(false);
            }
            foreach (RequiredGem required in GemList)
            {
                foreach (Gem a in ProfMin.gems)
                {
                    //a.gameObject.SetActive(false);
                    if (required.currencyType == a.type)
                    {

                        foreach (Currency s in mineScript.currency)
                        {
                            if (s.Id == a.type.ToString() && required.GetReward)
                            {
                                a.gameObject.SetActive(true);
                                auto.CalculateMiningEfficiency();

                                a.text.text = auto.ProfMin.ToString();
                            }
                        }
                    }

                }
            }

            DurationText.text = (auto.TimeMinning / 60).ToString() + " MINUTES";
          //  ProgressUI.GetButton = 0;

        }
       
        //   Red = mineScript.requiredGem(CurrencyType.Red);
        //   Green = mineScript.requiredGem(CurrencyType.Green);
        //  Blue = mineScript.requiredGem(CurrencyType.Blue);
        // Gold = mineScript.requiredGem(CurrencyType.Gold);
        // if (Red != null)
        // {
        //     requiredObj.RedGemObject.text.text = mineScript.currency[2].Value.ToString() + " / " + Red.GemRequired;
        //     requiredObj.RedGemObject.image.sprite = mineScript.currency[2].Icon;
        // }
        // else
        // {
        //     requiredObj.RedGemObject.gameObject.SetActive(false);
        // }
        // if (Green != null)
        // {
        //     requiredObj.GreenGemObject.text.text = mineScript.currency[0].Value.ToString() + " / " + Green.GemRequired;
        //     requiredObj.GreenGemObject.image.sprite = mineScript.currency[0].Icon;
        // }
        // else
        // {
        //     requiredObj.GreenGemObject.gameObject.SetActive(false);
        // }
        // if (Blue != null)
        // {
        //     requiredObj.BlueGemObject.text.text = mineScript.currency[1].Value.ToString() + " / " + Blue.GemRequired;
        //     requiredObj.BlueGemObject.image.sprite = mineScript.currency[1].Icon;
        // }
        // else
        // {
        //     requiredObj.BlueGemObject.gameObject.SetActive(false);
        // }
        // if (Gold != null)
        // {
        //     requiredObj.GoldObject.text.text = mineScript.Currency.Value.ToString() + " / " + Gold.GemRequired;
        //     requiredObj.GoldObject.image.sprite = mineScript.Currency.Icon;
        // }
        // else
        // {
        //     requiredObj.GoldObject.gameObject.SetActive(false);
        // }

    }
    public void OnOff()
    {
        if (ww == 0)
        {
            AutomatePanel.SetActive(true);
            ww++;
        }
        else if(ww ==1)
        {
            AutomatePanel.SetActive(false);
            ww--;
        }

    }

    public void Village()
    {
        auto.goToVillage();
    }
}
