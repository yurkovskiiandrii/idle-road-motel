﻿using System;
using System.Collections;
using System.Collections.Generic;
using nickeltin.Extensions;
using nickeltin.GameData.DataObjects;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class TimedValueChanger : MonoBehaviour
    {
        [SerializeField] private float m_value = 1;
        [SerializeField] private float m_activeValue = 2;
        [SerializeField] private NumberObject m_currency;
        [SerializeField] private Sprite m_inactiveSprite;
        [SerializeField] private Sprite m_activeSprite;
        [SerializeField] private Color m_cooldowColor;
        [SerializeField] private float m_price;
        [SerializeField] private float m_duration = 1;
        [SerializeField] private float m_cooldownTime = 1;
        [SerializeField] private List<NumberObject> m_entries = new List<NumberObject>();
        [SerializeField] private UnityEvent m_onExectution;
        
        private Image m_image;
        private Coroutine m_routine;

        private void Awake()
        {
            var btn = GetComponent<Button>();
            m_image = btn.targetGraphic as Image;
            btn.onClick.AddListener(OnClick);
        }

        private void Start() => UpdateState(m_currency.Value);

        private void OnEnable() => m_currency.onValueChanged += UpdateState;

        private void OnDisable() => m_currency.onValueChanged -= UpdateState;

        private void UpdateState(float value)
        {
            if (m_routine == null)
            {
                if (value >= m_price)
                {
                    m_image.sprite = m_inactiveSprite;
                    m_image.color = Color.white;
                }
                else
                {
                    m_image.color = m_cooldowColor;
                }
            }
        }

        private void OnClick()
        {
            IEnumerator Cooldown()
            {
                m_image.color = m_cooldowColor;

                for (float t = 0; t < m_cooldownTime; t+=Time.deltaTime)
                {
                    m_image.fillAmount = t.To01Ratio(0, m_cooldownTime);
                    yield return null;
                }
                
                m_image.color = Color.white;
                m_routine = null;
            }

            IEnumerator Execution()
            {
                m_onExectution?.Invoke();
                m_image.color = Color.white;
                m_image.sprite = m_activeSprite;

                for (int i = 0; i < m_entries.Count; i++)
                {
                    m_entries[i].Value = m_activeValue;
                }

                for (float t = m_duration; t >= 0; t-=Time.deltaTime)
                {
                    m_image.fillAmount = t.To01Ratio(0, m_duration);
                    yield return null;
                }

                for (int i = 0; i < m_entries.Count; i++)
                {
                    m_entries[i].Value = m_value;
                }
                m_routine = GUIController.ConsistentMonoBehaviour.StartCoroutine(Cooldown());
            }

            if (m_routine == null && m_price <= m_currency.Value)
            {
                m_routine = GUIController.ConsistentMonoBehaviour.StartCoroutine(Execution());
                m_currency.Value -= m_price;
            }
        }
    }
}