using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CompareTextUI : MonoBehaviour
{
    public TextMeshProUGUI LeftText;
    public TextMeshProUGUI RightText;
    public Image LeftIcon;
    public Image RightIcon;
    public void ShowCompare(string left, string right, Sprite icon)
    {
        LeftText.text = left;
        LeftIcon.sprite = icon;
        RightText.text = right;
        RightIcon.sprite = icon;
    }
}
