﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI
{
    [RequireComponent(typeof(Graphics))]
    public class UIEvents : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
    {
        public Action<PointerEventData> onPointerDown;
        public Action<Vector2> onPointerHeldOver;
        public Action<PointerEventData> onPointerUp;
        public Action<PointerEventData> onPointerEnter;
        public Action<PointerEventData> onPointerExit;
        [SerializeField] private UnityEvent _onPointerDown;
        [SerializeField] private UnityEvent _onPointerHeldOver;
        [SerializeField] private UnityEvent _onPointerUp;
        [SerializeField] private UnityEvent _onPointerEnter;
        [SerializeField] private UnityEvent _onPointerExit;

        public PointerEventData LastClickData { get; private set; }

        private bool m_pressed;
        private bool m_pointerOver;

        public void OnPointerDown(PointerEventData eventData)
        {
            m_pressed = true;
            LastClickData = eventData;
            TriggerPointerDown();
        }
        
        public void OnPointerUp(PointerEventData eventData)
        {
            m_pressed = false;
            onPointerUp?.Invoke(eventData);
            _onPointerUp?.Invoke();
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            m_pointerOver = true;
            onPointerEnter?.Invoke(eventData);
            _onPointerEnter?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            m_pointerOver = false;
            onPointerExit?.Invoke(eventData);
            _onPointerExit?.Invoke();
        }

        private void Update()
        {
            if (m_pressed && m_pointerOver)
            {
                onPointerHeldOver?.Invoke(UnityEngine.Input.mousePosition);
                _onPointerHeldOver?.Invoke();
            }
        }

        /// <summary>
        /// Used to manually invoke event in tutorial stage
        /// </summary>
        public void TriggerPointerDown()
        {
            onPointerDown?.Invoke(LastClickData);
            _onPointerDown?.Invoke();
        }
    }
}