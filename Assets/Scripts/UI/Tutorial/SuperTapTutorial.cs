﻿using UI.Tutorial;
using UnityEngine;
using ProgressBar = nickeltin.UI.ProgressBar;

[RequireComponent(typeof(ProgressBar))]
public class SuperTapTutorial : TutorialBase
{
    private ProgressBar bar;
    
    private void Start()
    {
        m_finger.gameObject.SetActive(false);
        if(!ShouldShow) return;

        bar = GetComponent<ProgressBar>();

        bar.onFill += Show;
    }

    private void Show()
    {
        if(!ShouldShow) return;
        m_shown = true;
        bar.onFill -= Show;
        m_finger.gameObject.SetActive(true);
            
        RepeatTween(m_finger.transform, () =>  m_finger.gameObject.SetActive(false));
        RepeatTween(bar.transform);
    }
}
