﻿using System;
using System.Collections;
using nickeltin.Extensions;
using nickeltin.GameData.DataObjects;
using UnityEngine;
using UnityEngine.UI;
using UtilsLocal;

namespace UI.Tutorial
{
    public class TutorialBase : MonoBehaviour
    {
        [SerializeField] protected Image m_finger;
        [SerializeField] protected BoolObject m_used;
        [SerializeField] protected float m_scale;
        [SerializeField] protected float m_oneTweenTime;
        [SerializeField] protected int m_cycles = 3;
        
        protected bool m_shown;

        protected bool ShouldShow => !m_shown && !m_used.Value; 
        
        protected void Tween(Transform target, Action onComplete = null)
        {
            target.ScaleInOut(Vector3.one.Mult(m_scale), Vector3.one, m_oneTweenTime, onComplete);
        }

        protected void RepeatTween(Transform target, Action onComplete = null)
        {
            IEnumerator Repeat()
            {
                for (int i = 0; i < m_cycles; i++)
                {
                    Tween(target);
                    yield return new WaitForSeconds(m_oneTweenTime + 0.001f);
                }
                
                onComplete?.Invoke();
            }

            GUIController.ConsistentMonoBehaviour.StartCoroutine(Repeat());
        }
    }
}