using nickeltin.Singletons;
using UnityEngine;

namespace UI
{
    public class WorldPopUpCanvas : MonoSingleton<WorldPopUpCanvas>
    {
        public static Canvas Canvas;

        protected override void Awake()
        {
            base.Awake();
            Canvas = GetComponent<Canvas>();
        }
    }
}
