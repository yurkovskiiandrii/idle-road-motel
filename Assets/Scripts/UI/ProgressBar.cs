using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private Image _current;

        public void SetValue(float current, float max) => 
            _current.fillAmount = current / max;
    }

}
