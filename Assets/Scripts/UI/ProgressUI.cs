using nickeltin.Singletons;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class ProgressUI : MonoSingleton<ProgressUI>
{
    public GameObject Canvas;
    [SerializeField] private TextMeshProUGUI _timeWasLeftText;
    [SerializeField] private TextMeshProUGUI _generalEfficiencyText;
    [SerializeField] private TextMeshProUGUI _currencyAcquiredText;
    [SerializeField] private Button buttonRunMine;
    [SerializeField] private GameObject _NotificationMoreHoure;
    public GameObject VfxTakeGem;
    public GameObject OpenButtonVFX;

    public GameObject WorldCanvas;
    public UnityEvent NotMine;
    public UnityEvent Mine;


    public UnityEvent More2Hours;
    public UnityEvent Under2housr;

    public TextMeshProUGUI textTimerDev;
    public TextMeshProUGUI textCurrencyDev;
    public static GameObject CanvasObject => instance.Canvas;
    public static Button ButtonObject => instance.buttonRunMine;
    public static GameObject takeVFX => instance.VfxTakeGem;
    public static GameObject opnButton => instance.OpenButtonVFX;
    public static GameObject worldCan => instance.WorldCanvas;
    public static GameObject notificationMore => instance._NotificationMoreHoure;
    public static UnityEvent notMine => instance.NotMine;
    public static UnityEvent MineActive => instance.Mine;

    public static UnityEvent _more2Hours => instance.More2Hours;
    public static UnityEvent _under2Hours => instance.Under2housr;

    public static int GetButton
    {
        set
        {
            Debug.LogError("value = " + value);

            if (value == 1)
            {
                //ButtonObject.enabled = false;
                ButtonObject.gameObject.GetComponent<Image>().color = Color.gray;
                // opnButton.SetActive(false);
                MineActive.Invoke();
            }

            if (value == 0)
            {
                // ButtonObject.enabled = true;
                // opnButton.SetActive(true);
                notMine.Invoke();
                // MineActive.Invoke();
                // ButtonObject.gameObject.GetComponent<Image>().color = Color.green;

            }
        }
      
    }
    public static string TimeDev
    {
        set => instance.textTimerDev.text = value;
    }
    public static string CurrencyDev
    {
        set => instance.textCurrencyDev.text = value;
    }
    public static string TimeWasLeftText
    {
        set => instance._timeWasLeftText.text = value;
    }

    public static string GeneralEfficiencyText
    {
        set => instance._generalEfficiencyText.text = value;
    }
    public static string CurrencyAcquiredText
    {
        set => instance._currencyAcquiredText.text = value;
    }

}
