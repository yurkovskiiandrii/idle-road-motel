using System.Collections.Generic;

namespace UnityEngine.EventSystems
{
    public class CustomPointerInputModule : StandaloneInputModuleTemp
    {
        public static List<GameObject> targetButtons = new List<GameObject>();

        protected override bool CanProcessTouch(GameObject currentOverGo)
        {
            //if (targetButtons.Count != 0)
            //{
            //    //foreach (var obj in raycastObjects)
            //    //    print(obj.name);
            //    //print("---------------------------------");

            //    bool isTargetButton = false;

            //    if (targetButtons.Contains(currentOverGo))
            //        return true;
            //    for (int i = 0; i < targetButtons.Count; ++i)
            //        if (raycastObjects.Contains(targetButtons[i]))
            //        {
            //            isTargetButton = true;
            //            break;
            //        }

            //    if (!isTargetButton)
            //        return;
            //}

            //Debug.LogError("clicked on " + currentOverGo.name);

            return targetButtons.Count == 0 || targetButtons.Contains(currentOverGo);
        }

        protected override void ProcessMouseEvent(int id)
        {
            var mouseData = GetMousePointerEventData(id);
            var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;

            m_CurrentFocusedGameObject = leftButtonData.buttonData.pointerCurrentRaycast.gameObject;

            //print("mouse: " + Input.mousePosition);

            if (targetButtons.Count != 0)
            {
                var raycastObjects = IsPointerOverUI.GetUIElements(Input.mousePosition);

                //foreach (var obj in raycastObjects)
                //    print(obj.name);
                //print("---------------------------------");

                bool isTargetButton = false;

                for (int i = 0; i < targetButtons.Count; ++i)
                    if (raycastObjects.Contains(targetButtons[i]))
                    {
                        isTargetButton = true;
                        break;
                    }

                if (!isTargetButton)
                    return;
            }

            // Process the first mouse button fully
            ProcessMousePress(leftButtonData);
            ProcessMove(leftButtonData.buttonData);
            ProcessDrag(leftButtonData.buttonData);

            // Now process right / middle clicks
            ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData);
            ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Right).eventData.buttonData);
            ProcessMousePress(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData);
            ProcessDrag(mouseData.GetButtonState(PointerEventData.InputButton.Middle).eventData.buttonData);

            if (!Mathf.Approximately(leftButtonData.buttonData.scrollDelta.sqrMagnitude, 0.0f))
            {
                var scrollHandler = ExecuteEvents.GetEventHandler<IScrollHandler>(leftButtonData.buttonData.pointerCurrentRaycast.gameObject);
                ExecuteEvents.ExecuteHierarchy(scrollHandler, leftButtonData.buttonData, ExecuteEvents.scrollHandler);
            }
        }
    }
}
