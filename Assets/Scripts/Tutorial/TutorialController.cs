using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;
using GameAnalyticsSDK;

namespace Game.Tutorial
{
    public class TutorialController : MonoBehaviour
    {
        [SerializeField] CinemachineVirtualCamera _serviceCamera;
        [SerializeField] List<TutorialStep> tutorialSteps;

        Tween stepSkipTimer;

        int tutorialStep;

        public static bool isStartedTutorial = false;

        void OnValidate()
        {
            for (int i = 0; i < tutorialSteps.Count; ++i)
            {
                tutorialSteps[i].number = i + 1;
                tutorialSteps[i].name = tutorialSteps[i].number.ToString();

                if (tutorialSteps[i].customName.Length != 0)
                    tutorialSteps[i].name += ". " + tutorialSteps[i].customName;
            }
        }

        public void StartTutorial()
        {
            Managers.InputManager.CameraMoveBlocked = false;

            tutorialStep = PlayerPrefs.GetInt("TutorialStep", 0);

            //tutorialStep = 34;
            isStartedTutorial = true;

            if (tutorialStep == tutorialSteps.Count)
            {
                isStartedTutorial = false;
                Destroy(gameObject);
                return;
            }

            if (SceneManager.GetActiveScene().name == tutorialSteps[tutorialStep].ActiveScene
                && tutorialSteps[tutorialStep].additionTriggerStepButton == null)
            {
                ShowTutorialStep();
                return;
            }

            if(tutorialSteps[tutorialStep].additionTriggerStepButton)
            {
                tutorialSteps[tutorialStep].additionTriggerStepButton.gameObject.SetActive(true);
                tutorialSteps[tutorialStep].additionTriggerStepButton?.onClick.AddListener(ShowTutorialStepWithoutLog);
                return;
            }

            tutorialSteps[tutorialStep].triggerStepButton?.onClick.AddListener(ShowTutorialStepWithoutLog);
        }

        void ShowTutorialStepWithLog() => ShowTutorialStep(true);

        void ShowTutorialStepWithoutLog() => ShowTutorialStep(false);

        void ShowTutorialStep(bool logCustomEvent = true)
        {
            if (logCustomEvent)
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "TutorialStep", tutorialStep + 1);

            isStartedTutorial = true;

            float tutSkipTime = tutorialSteps[tutorialStep].canSkipByTime ? tutorialSteps[tutorialStep].secondsToSkip : float.MaxValue - 1;

            Debug.LogError("started " + (tutorialStep + 1).ToString());

            bool isCurrencyStep = tutorialSteps[tutorialStep].neenedCurrency != null;

            if (tutorialSteps[tutorialStep].cameraTarget)
            {
                _serviceCamera.Follow = tutorialSteps[tutorialStep].cameraTarget.transform;
                _serviceCamera.m_Lens.OrthographicSize = tutorialSteps[tutorialStep].cameraTarget.cameraOrtoSize;
                _serviceCamera.gameObject.SetActive(true);
                _serviceCamera.m_Lens.NearClipPlane = -100;
                //CameraMove.Instance.SetMovementTarget(tutorialSteps[tutorialStep].cameraTarget.transform);
            }

            tutorialSteps[tutorialStep].StartStep();

            tutorialSteps[tutorialStep].triggerStepButton?.onClick.RemoveListener(ShowTutorialStepWithLog);
            tutorialSteps[tutorialStep].triggerStepButton?.onClick.RemoveListener(ShowTutorialStepWithoutLog);
            tutorialSteps[tutorialStep].additionTriggerStepButton?.onClick.RemoveListener(ShowTutorialStepWithLog);
            tutorialSteps[tutorialStep].additionTriggerStepButton?.onClick.RemoveListener(ShowTutorialStepWithoutLog);
            //tutorialSteps[tutorialStep + 1].triggerStepButton?.onClick.Invoke();

            if (tutorialStep + 1 < tutorialSteps.Count)
            {
                if (isCurrencyStep)
                {
                    tutorialSteps[tutorialStep].neenedCurrency.SubscribeOnValueChanged(CheckOnCurrencyEnought);
                    stepSkipTimer = DOVirtual.DelayedCall(tutSkipTime, delegate {
                        tutorialSteps[tutorialStep].neenedCurrency.RemoveOnValueChanged(CheckOnCurrencyEnought);
                        CompleteTutorialStep();
                    });
                }
                else
                {
                    if (tutorialSteps[tutorialStep].completeByGemsCollecting)
                    {
                        GetGemFrom.SuscribeOnGemCollected(CompleteTutorialStep);
                        stepSkipTimer = DOVirtual.DelayedCall(tutSkipTime, delegate {
                            GetGemFrom.RemoveOnGemCollected(CompleteTutorialStep);
                            CompleteTutorialStep();
                        });
                    }
                    else
                    {
                        tutorialSteps[tutorialStep + 1].triggerStepButton?.onClick.AddListener(CompleteTutorialStep);
                        tutorialSteps[tutorialStep + 1].triggerStepButton?.onClick.AddListener(ShowTutorialStepWithLog);
                        stepSkipTimer = DOVirtual.DelayedCall(tutSkipTime, delegate { tutorialSteps[tutorialStep + 1].triggerStepButton?.onClick.Invoke(); });
                    }
                }
            }
            else
            {
                stepSkipTimer = DOVirtual.DelayedCall(tutSkipTime, CompleteTutorialStep);
                tutorialSteps[tutorialStep].GetTargetButton()?.onClick.AddListener(CompleteTutorialStep);
            }

            if (!tutorialSteps[tutorialStep].canSkipByTime)
                stepSkipTimer.Kill();
        }

        void CompleteTutorialStep()
        {
            if (tutorialSteps[tutorialStep].completeByGemsCollecting)
            {
                GetGemFrom.RemoveOnGemCollected(CompleteTutorialStep);
            }

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "TutorialStep", tutorialStep + 1);

            stepSkipTimer.Kill();

            Debug.LogError("completed " + (tutorialStep + 1).ToString());


            _serviceCamera.gameObject.SetActive(false);
            _serviceCamera.Follow = null;
            //CameraMove.Instance.SwitchToDefault();

            CameraMove.Instance.SetMovementTarget(_serviceCamera);

            if (tutorialStep < tutorialSteps.Count - 1)
            {
                tutorialSteps[tutorialStep + 1].triggerStepButton?.onClick.RemoveListener(CompleteTutorialStep);
                //tutorialSteps[tutorialStep + 1].additionTriggerStepButton?.onClick.RemoveListener(CompleteTutorialStep);
            }
                
            tutorialSteps[tutorialStep].EndStep();

            tutorialStep++;

            PlayerPrefs.SetInt("TutorialStep", tutorialStep);

            if (tutorialStep == tutorialSteps.Count)
            {
                isStartedTutorial = false;
                tutorialSteps[tutorialStep - 1].GetTargetButton()?.onClick.RemoveListener(CompleteTutorialStep);
                Debug.LogError("tutorial completed");
                Destroy(gameObject);
                return;
            }

            if (tutorialSteps[tutorialStep - 1].neenedCurrency != null || tutorialSteps[tutorialStep - 1].completeByGemsCollecting)
                ShowTutorialStep();
        }

        void CheckOnCurrencyEnought(float value)
        {
            if (value >= tutorialSteps[tutorialStep].neenedCurrencyAmount)
            {
                tutorialSteps[tutorialStep].neenedCurrency.RemoveOnValueChanged(CheckOnCurrencyEnought);
                CompleteTutorialStep();
            }
        }
    }
}

