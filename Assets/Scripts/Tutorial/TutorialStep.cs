using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Managers;
using UnityEngine.EventSystems;
using nickeltin.Editor.Attributes;

namespace Game.Tutorial
{
    [System.Serializable]
    public class CurreencyAdder
    {
        public nickeltin.GameData.DataObjects.Currency currency;
        public int amountToAdd;
    }

    [System.Serializable]
    public class StorageAdder
    {
        public NPCs.ServiceManager serviceManager;
        public List<ZoneAdder> zoneAdding;

        [System.Serializable]
        public class ZoneAdder
        {
            public string zoneID;
            public int amountToAdd;
        }
    }

    [System.Serializable]
    public class TutorialStep
    {
        [HideInInspector]
        public string name;
        public string customName;
        [HideInInspector]
        public int number;
        [SerializeField] bool blockScrollMap;
        [Scene] public string ActiveScene;
        public Button triggerStepButton;
        public Button additionTriggerStepButton;
        public bool completeByGemsCollecting;
        public bool canSkipByTime;
        public int secondsToSkip = 30;
        [SerializeField] UnityEvent onStepBegin;
        [SerializeField] UnityEvent onStepEnd;
        [SerializeField] List<GameObject> targetUIElements;

        public int neenedCurrencyAmount;
        public nickeltin.GameData.DataObjects.Currency neenedCurrency;

        [SerializeField] List<CurreencyAdder> currencyToAdd;
        [SerializeField] List<StorageAdder> storagesToAdd;

        public CameraOrtoSize cameraTarget;
        //public Button completeStepButton;

        public void StartStep()
        {
            currencyToAdd.ForEach(x => x.currency.Value += x.amountToAdd);

            foreach(var adder in storagesToAdd)
            {
                foreach (var zone in adder.zoneAdding)
                    adder.serviceManager.AddValueToServiceZone(zone.zoneID, zone.amountToAdd);              
            }

            InputManager.CameraMoveBlocked = blockScrollMap;
            CustomPointerInputModule.targetButtons = targetUIElements;
            onStepBegin.Invoke();
        }

        public void EndStep()
        {
            InputManager.CameraMoveBlocked = false;
            CustomPointerInputModule.targetButtons = new List<GameObject>();
            onStepEnd.Invoke();
        }

        public Button GetTargetButton()
        {
            return targetUIElements[0].GetComponent<Button>();
        }
    }
}

