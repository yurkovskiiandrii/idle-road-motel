﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RandomGeneratorWithWeight
{
    //extend this class for more functionality
    [Serializable]
    public class ItemForRandom <T> : IItem
    {
        [SerializeField]
        int _weight;

        [SerializeField]
        T _item;

        public ItemForRandom(int _weight, T item)
        {
            this._weight = _weight;
            this._item = item;
        }

        public ItemForRandom(float _weight, T item)
        {
            this._weight = (int)(_weight * 100);
            this._item = item;
        }

        public int GetWeight() => _weight;

        public T GetItem() => _item;
    }
}
